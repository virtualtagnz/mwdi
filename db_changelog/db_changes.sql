/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  myMac
 * Created: Nov 1, 2017
 */

CREATE TABLE IF NOT EXISTS `super_admin_tb` (
  `super_id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `add_street_no` varchar(20) NOT NULL,
  `add_street_name` varchar(50) NOT NULL,
  `add_city` varchar(50) NOT NULL,
  `add_region` varchar(50) NOT NULL,
  `add_post_code` int(11) NOT NULL,
  `image_link` varchar(300) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `role_create` varchar(45) NOT NULL,
  `code_create` tinyint(1) NOT NULL,
  `date_mod` datetime NOT NULL,
  `role_mod` varchar(45) NOT NULL,
  `code_mod` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `super_admin_tb`
--

INSERT INTO `super_admin_tb` (`super_id`, `u_id`, `first_name`, `last_name`, `dob`, `gender`, `phone`, `mobile`, `address`, `add_street_no`, `add_street_name`, `add_city`, `add_region`, `add_post_code`, `image_link`, `description`, `date_create`, `role_create`, `code_create`, `date_mod`, `role_mod`, `code_mod`) VALUES
(1, 1, 'Soraya', 'Simpson', '1978-10-10', 'Female', '094664676', '0228846646', '21 Rankin Av,New Lynn, Auckalnd', '21', 'Rankin Av', 'New Lynn', 'Auckalnd', 9938, NULL, NULL, '2017-10-26 16:05:22', 'super', 1, '2017-10-26 14:22:17', 'super', 1);

--
-- Indexes for table `super_admin_tb`
--
ALTER TABLE `super_admin_tb`
  ADD PRIMARY KEY (`super_id`);

ALTER TABLE `super_admin_tb`
  MODIFY `super_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;

/*
Alter all tables with auto increment
*/
ALTER TABLE `user_tb` CHANGE `user_id` `user_id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `admin_tb` CHANGE `admin_id` `admin_id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `accounts_tb` CHANGE `accounts_id` `accounts_id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `clients_tb` CHANGE `client_id` `client_id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `coach_tb` CHANGE `coach_id` `coach_id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `mwdi_resource` CHANGE `res_id` `res_id` INT(11) NOT NULL AUTO_INCREMENT;

/*
replace path of images
*/
update admin_tb
set `image_link` = replace(`image_link`, 'public/uploads/', '')

ALTER TABLE `mwdi_resource` ADD `date_mod` DATETIME NOT NULL AFTER `create_role`, ADD `code_mod` INT NOT NULL AFTER `date_mod`, ADD `role_mod` INT NOT NULL AFTER `code_mod`;

-- ALTER TABLE `assigned_resource` CHANGE `ares_res_id` `form_id` INT(11) NULL DEFAULT NULL;
-- ALTER TABLE `assigned_resource` CHANGE `ares_user` `user_id` INT(11) NULL DEFAULT NULL;
-- ALTER TABLE `assigned_resource` CHANGE `ares_user_role` `user_role` TINYINT(1) NULL DEFAULT NULL;


CREATE TABLE `email_bulk_creation` (
  `uid` INT NOT NULL AUTO_INCREMENT,
  `sending_type` VARCHAR(45) NULL,
  `email_list` LONGTEXT NULL,
  `subject` VARCHAR(255) NULL,
  `content` LONGTEXT NULL,
  `status` VARCHAR(45) NULL,
  `dispatch` TEXT(500) NULL,
  `create_date` DATETIME NULL,
  `create_by` INT NULL,
  `update_date` DATETIME NULL,
  `update_by` INT NULL,
  PRIMARY KEY (`uid`));

ALTER TABLE `email_bulk_creation` 
ADD COLUMN `pagination` INT NULL AFTER `status`;

ALTER TABLE `email_bulk_creation` 
CHANGE COLUMN `dispatch` `dispatch` INT NULL DEFAULT NULL ;

RENAME TABLE email_log TO email_log_old;

CREATE TABLE `email_log` (
  `uid` INT NOT NULL AUTO_INCREMENT,
  `bulk_email_ref` INT NOT NULL,
  `email_sent_stat` VARCHAR(45) NOT NULL,
  `error` MEDIUMTEXT NULL,
  `date_sent` DATETIME NOT NULL,
  PRIMARY KEY (`uid`));

ALTER TABLE `email_log` 
ADD COLUMN `user_email` VARCHAR(45) NOT NULL AFTER `bulk_email_ref`,
ADD COLUMN `user_id` INT NOT NULL AFTER `user_email`;

ALTER TABLE `email_log` ADD `user_name` VARCHAR(100) NOT NULL AFTER `bulk_email_ref`;

ALTER TABLE `coach_form_main` CHANGE `chf_id` `chf_id` INT(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `coach_form_main` ADD `form_type` INT NOT NULL AFTER `filled_by`;
UPDATE `coach_form_main` SET form_type = '5'

ALTER TABLE `coach_form_p3` ADD `form_type` INT NOT NULL AFTER `submit_date`;
UPDATE `coach_form_p3` SET form_type = '8'

ALTER TABLE `coach_form_p3` 
CHANGE COLUMN `chp3_f24` `chp3_f24` VARCHAR(500) NULL DEFAULT NULL ,
CHANGE COLUMN `chp3_f27` `chp3_f27` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp3_f29` `chp3_f29` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp3_f30` `chp3_f30` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp3_f32` `chp3_f32` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp3_f33` `chp3_f33` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp3_f34` `chp3_f34` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp3_f35` `chp3_f35` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp3_f36` `chp3_f36` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp3_f37` `chp3_f37` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp3_f38` `chp3_f38` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp3_f39` `chp3_f39` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp3_f40` `chp3_f40` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp3_f41` `chp3_f41` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp3_f42` `chp3_f42` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp3_f43` `chp3_f43` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp3_f44` `chp3_f44` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp3_f45` `chp3_f45` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp3_f46` `chp3_f46` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp3_f47` `chp3_f47` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp3_f49` `chp3_f49` VARCHAR(45) NULL DEFAULT NULL ;

ALTER TABLE `coach_form_p4` ADD `form_type` INT NOT NULL AFTER `submit_date`;

UPDATE `coach_form_p4` SET form_type = '9'

ALTER TABLE `coach_form_p4` 
CHANGE COLUMN `chp4_f55` `chp4_f55` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp4_f56` `chp4_f56` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp4_f58` `chp4_f58` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp4_f59` `chp4_f59` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp4_f60` `chp4_f60` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp4_f61` `chp4_f61` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp4_f62` `chp4_f62` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp4_f63` `chp4_f63` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp4_f64` `chp4_f64` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp4_f65` `chp4_f65` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp4_f66` `chp4_f66` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp4_f67` `chp4_f67` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `chp4_f68` `chp4_f68` LONGTEXT NULL DEFAULT NULL ;

ALTER TABLE `coach_form_p5` ADD `form_type` INT NOT NULL AFTER `submit_date`;

UPDATE `coach_form_p4` SET form_type = '10'

--Add coach form 2 columns to main table
ALTER TABLE `coach_form_main` 
ADD COLUMN `chp2_f1` VARCHAR(45) NOT NULL AFTER `chf_purpose`,
ADD COLUMN `chp2_f2` VARCHAR(45) NOT NULL AFTER `chp2_f1`,
ADD COLUMN `chp2_f3` VARCHAR(45) NOT NULL AFTER `chp2_f2`,
ADD COLUMN `chp2_f4` LONGTEXT NULL AFTER `chp2_f3`,
ADD COLUMN `chp2_f5` LONGTEXT NULL AFTER `chp2_f4`,
ADD COLUMN `chp2_f6` LONGTEXT NULL AFTER `chp2_f5`,
ADD COLUMN `chp2_f7` LONGTEXT NULL AFTER `chp2_f6`,
ADD COLUMN `chp2_f8` DATE NULL AFTER `chp2_f7`,
ADD COLUMN `chp2_f9` LONGTEXT NULL AFTER `chp2_f8`,
ADD COLUMN `chp2_f10` LONGTEXT NULL AFTER `chp2_f9`,
ADD COLUMN `chp2_f11` LONGTEXT NULL AFTER `chp2_f10`,
ADD COLUMN `is_submit` TINYTEXT NOT NULL AFTER `form_type`,
ADD COLUMN `submit_date` DATETIME NOT NULL AFTER `is_submit`;

ALTER TABLE `coach_form_main` 
CHANGE COLUMN `form_type` `form_type` INT(11) NOT NULL AFTER `submit_date`;

ALTER TABLE `coach_form_main` CHANGE `chp2_f8` `chp2_f8` VARCHAR(50) NULL DEFAULT NULL;

--Merge coach form main and coach form 2
UPDATE coach_form_main c1 INNER JOIN coach_form_p2 c2 ON c1.chf_id = c2.chf_p2_main_ref
SET c1.chp2_f1 = c2.chp2_f1, c1.chp2_f2 = c2.chp2_f2,c1.chp2_f3 = c2.chp2_f3,c1.chp2_f4 = c2.chp2_f4,c1.chp2_f5 = c2.chp2_f5,c1.chp2_f6 = c2.chp2_f6,c1.chp2_f7 = c2.chp2_f7,c1.chp2_f8 = c2.chp2_f8,c1.chp2_f9 = c2.chp2_f9,c1.chp2_f10 = c2.chp2_f10,c1.chp2_f11 = c2.chp2_f11,c1.end_date = c2.end_time,c1.is_submit = c2.is_submit,c1.submit_date = c2.submit_date;

ALTER TABLE `ppms_form_tb` CHANGE `ppms_id` `ppms_id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `ppms_form_tb` ADD `file_link` TEXT NOT NULL AFTER `sub_stat`;
ALTER TABLE `ppms_form_tb` ADD `form_type` INT NOT NULL AFTER `file_link`;
UPDATE `ppms_form_tb` SET `form_type` = '2'

-- --------------------------------------------------------

--
-- Table structure for table `client_temp_form`
--

CREATE TABLE `client_temp_form` (
  `uid` int(11) NOT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `city_region` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `business_description` mediumtext,
  `created_date` datetime DEFAULT NULL,
  `answered` int(1) DEFAULT '0',
  `answered_by` varchar(50) DEFAULT NULL,
  `answered_date` datetime DEFAULT NULL,
  `coach` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `client_temp_form`
--
ALTER TABLE `client_temp_form`
  ADD PRIMARY KEY (`uid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `client_temp_form`
--
ALTER TABLE `client_temp_form`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `client_temp_form` CHANGE `answered_by` `answered_by` INT(11) NULL DEFAULT NULL, CHANGE `coach` `coach` INT(11) NOT NULL;
  
-- --------------------------------------------------------  
ALTER TABLE `sec_main` CHANGE `sec_id` `sec_id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `sec_main` CHANGE `sec_email` `sec_email` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

ALTER TABLE `sec_main` ADD `form_type` INT NOT NULL AFTER `sub_date`;
UPDATE `sec_main` SET `form_type` = '4' 

ALTER TABLE `ba_main` ADD `form_type` INT NOT NULL AFTER `date_submit`;
UPDATE `ba_main` SET `form_type` = '1'

ALTER TABLE `ba_bank_details` CHANGE `bk_no` `bk_no` VARCHAR(20) NULL DEFAULT NULL;

ALTER TABLE `ba_bank_details` CHANGE `owe_yrs` `owe_yrs` DECIMAL(20,2) NULL DEFAULT NULL;
ALTER TABLE `ba_bank_details` CHANGE `owe_int_rate` `owe_int_rate` DECIMAL(20,2) NULL DEFAULT NULL;

-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

ALTER TABLE `notification_tb` 
ADD COLUMN `sent_by_role` TINYINT(1) NULL AFTER `client_mark`,
ADD COLUMN `sent_by_id` INT(11) NULL AFTER `sent_by_role`;

-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

ALTER TABLE `approved_loan_tb` CHANGE `loan_id` `loan_id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `approved_loan_tb` ADD `ln_description` TEXT NULL AFTER `date_last_pmt`;
ALTER TABLE `approved_loan_tb` CHANGE `ln_amount` `ln_amount` DECIMAL(20,2) NOT NULL;
ALTER TABLE `approved_loan_tb` CHANGE `date_last_pmt` `date_last_pmt` DATE NOT NULL;

ALTER TABLE `client_temp_form` ADD `is_temp` BOOLEAN NOT NULL AFTER `coach`;
UPDATE `client_temp_form` SET `is_temp`='1'

-- 22-05-2018 +BY Chris ++++++++++++++++++++++++++++++++++++++++++++++++++++++++

ALTER TABLE `client_coach_rel` ADD `strat_work` BOOLEAN NOT NULL DEFAULT FALSE AFTER `previous_coah`, ADD `start_date` DATETIME NULL DEFAULT NULL AFTER `strat_work`;


UPDATE `forms_tb` SET `frm_name` = 'ENGAGEMENT 1 (Whanaungatanga/Relationship Building)' WHERE `forms_tb`.`frm_id` = 5;
UPDATE `forms_tb` SET `frm_name` = 'ENGAGEMENT 2 (Building on the Relationship)' WHERE `forms_tb`.`frm_id` = 8;
UPDATE `forms_tb` SET `frm_name` = 'ENGAGEMENT 3 (Building on the Relationship)' WHERE `forms_tb`.`frm_id` = 9;
UPDATE `forms_tb` SET `frm_name` = 'ENGAGEMENT 4 (Final Sponsored Session – Follow Up)' WHERE `forms_tb`.`frm_id` = 10;

ALTER TABLE `coach_form_main` ADD `nd_link` VARCHAR(255) NOT NULL AFTER `chp2_f11`, ADD `nd_name` VARCHAR(255) NOT NULL AFTER `nd_link`;

ALTER TABLE `coach_form_p4` ADD `frequency` INT NOT NULL AFTER `form_type`;
UPDATE `coach_form_p4` SET `frequency`=2 WHERE `is_submit`='N'
UPDATE `coach_form_p4` SET `frequency`=3 WHERE `is_submit`='Y'

UPDATE `mwdi_redevelop_db`.`forms_tb` SET `frm_name` = 'ENGAGEMENT MULTIPLE (Building on the Relationship)' WHERE `forms_tb`.`frm_id` = 9;
UPDATE `mwdi_redevelop_db`.`forms_tb` SET `frm_name` = 'ENGAGEMENT FINAL (Final Sponsored Session – Follow Up)' WHERE `forms_tb`.`frm_id` = 10;
UPDATE `mwdi_redevelop_db`.`forms_tb` SET `frm_name` = 'FINAL ENGAGEMENT (Final Sponsored Session – Follow Up)' WHERE `forms_tb`.`frm_id` = 10;