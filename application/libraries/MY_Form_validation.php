<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation {

    public function __construct() {
        parent::__construct();
    }
    
    public function dob_check($param) {
        $current_date = date('Y-m-d');
        $select_date = date_format(date_create($param), 'Y-m-d');
        $datetime1 = new DateTime($current_date);
        $datetime2 = new DateTime($select_date);
        $interval = date_diff($datetime1, $datetime2);
        //$diff = strtotime($current_date) - strtotime($select_date);

        $years = $interval->y;

        $CI = & get_instance();
        if ($years >= 16) {
            return TRUE;
        } else {
            $CI->form_validation->set_message('dob_check', '{field} must be over age 16.');
            return FALSE;
        }
    }

    public function date_compare($param) {
        $CI = & get_instance();
        $start_date = "";
        if ($CI->input->post('exp_dt') != NULL) {
            $start_date = $CI->input->post('exp_dt');
        } else {
            $start_date = $CI->input->post('exp_dt');
        }
        $end_date = $param;
        if (strtotime($end_date) >= strtotime($start_date)) {
            return TRUE;
        } else {
            $CI->form_validation->set_message('date_gt', '{field} must be greter than start date');
            return FALSE;
        }
    }

    public function date_gt($param) {
        $CI = & get_instance();
        $current_date = date('Y-m-d');
        $select_date = $param;
        $datetime1 = new DateTime($current_date);
        $datetime2 = new DateTime($select_date);
        if ($datetime2 >= $datetime1) {
            return TRUE;
        } else {
            $CI->form_validation->set_message('date_gt', '{field} must be greter than or equal today');
            return FALSE;
        }
    }
    
    public function time_gt($param) {
        $CI = & get_instance();
        $current_time = date('H:i:s');
        $select_time = date_format(date_create($param), 'H:i:s');
        if ($select_time >= $current_time) {
            return TRUE;
        } else {
            $CI->form_validation->set_message('time_gt', '{field} must be greter than current time');
            return FALSE;
        }
    }

    public function accident_date_gt($param) {
        $CI = & get_instance();
        $start_date = $CI->input->post('dateOfAccident');
        $end_date = $param;
        if (strtotime($end_date) >= strtotime($start_date)) {
            return TRUE;
        } else {
            $CI->form_validation->set_message('accident_date_gt', '{field} must be greter than date of accident');
            return FALSE;
        }
    }

    public function standard_pwd($param) {
        $CI = & get_instance();
        if (strlen($param) < 6) {
            $CI->form_validation->set_message('standard_pwd', '{field} must contain at least 6 characters');
            return FALSE;
//        }elseif (!preg_match("#[0-9]+#",$param)) {
//            $CI->form_validation->set_message('standard_pwd', '{field} must contain at least 1 number');
//            return FALSE;
//        }elseif (!preg_match("#[A-Z]+#",$param)) {
//            $CI->form_validation->set_message('standard_pwd', '{field} must contain at least 1 capital letter');
//            return FALSE;
//        }elseif (!preg_match("#[a-z]+#",$param)) {
//            $CI->form_validation->set_message('standard_pwd', '{field} must contain at least 1 lowercase letter');
//            return FALSE;
        } else {
            return TRUE;
        }
    }
}
