<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once dirname(__FILE__) . '/tcpdf/tcpdf.php';
class Pdf extends TCPDF
{

    var $footer_align = 90;

    function __construct()
    {
        parent::__construct();
    }

     public function Header() {
        $image_file = K_PATH_IMAGES.'img/logo.png';
        $this->Image($image_file, 13, 7, 50, '', 'png', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $this->SetFont('helvetica', 'B', 10);
    }

    public function Footer($align=90) {
        $this->SetFont('helvetica', false, 8);
        $this->MultiCell(90, 5, '© '.date('Y').' Maori Women\'s Development Inc. All Rights Reserved.', 0, 'C', 1, 0, '', '', true);
		$this->MultiCell($this->footer_align, 5, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, 'R', 0, 1, '', '', true);
    }
}