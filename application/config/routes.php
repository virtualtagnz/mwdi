<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Auth';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//Auth routing
$route['login'] = 'Auth/do_login';
$route['logout'] = 'Auth/signout';
$route['password_recovery'] = 'Auth/password_recover';
$route['recover'] = 'Auth/do_password_recover';

//Users routing
$route['view_user_lists/(:any)'] = 'System_users/index';
$route['create_user/(:any)'] = 'System_users/create_user';
$route['create/(:any)'] = 'System_users/create';
$route['edit_user/(:any)/(:any)'] = 'System_users/edit_user/$1';
$route['edit/(:any)/(:any)'] = 'System_users/edit/$1';
$route['deactivate/(:any)'] = 'System_users/deactivate_users/$1';
$route['activate/(:any)'] = 'System_users/activate_users/$1';
$route['view_profile/(:any)/(:any)'] = 'System_users/static_user_view/$1';
$route['convert_client/(:any)/(:any)'] = 'System_users/create_user/$1';
$route['mark_as_start_work'] = 'System_users/mark_as_start_work_with_client';

//Resource routing
$route['resource_list'] = 'Resources/index';
$route['go_create_resource'] = 'Resources/create_resource_view';
$route['create_resource'] = 'Resources/create';
$route['go_edit_resource/(:any)'] = 'Resources/edit_resource_view/$1';
$route['edit_resource/(:any)'] = 'Resources/edit/$1';

//Assignment routing
$route['assign_resource/(:any)'] = 'Assignment/index';
$route['assign_forms/(:any)'] = 'Assignment/assign_forms/$1';
$route['view_assigned_forms/(:any)'] = 'Assignment/assigned_resources';
$route['assign_application_forms/(:any)'] = 'Assignment/assign_application_forms';
$route['assign_application_forms_to_users/(:any)'] = 'Assignment/assign_application_forms_to_users/$1';
$route['view_assigned_application_forms/(:any)'] = 'Assignment/view_assigned_forms/$1';
$route['my_forms/(:any)/(:any)'] = 'Assignment/get_application_forms_assigned_to_user/$1';
$route['archived_forms/(:any)/(:any)'] = 'Assignment/get_all_archived_forms/$1';
$route['assign_clients_to_coaches'] = 'Assignment/client_to_coach';
$route['my_users/(:any)'] = 'Assignment/view_my_users/$1';
$route['assign_users'] = 'Assignment/assign_clients_to_coach';
$route['view_assigned_clients'] = 'Assignment/get_assigned_client_list';
$route['my_resource_list'] = 'Assignment/view_my_resource';
$route['reassign_coaches'] = 'Assignment/link_reassign_coaches';
$route['reassign'] = 'Assignment/reassign_coaches';

//Bulk email routing
$route['baform/ajax/client']['post'] = 'BAForm/ajaxRequest/client';
$route['baform/ajax/education_details']['post'] = 'BAForm/ajaxRequest/education_details';
$route['baform/ajax/employment_details']['post'] = 'BAForm/ajaxRequest/employment_details';
$route['baform/save/form/step1']['post'] = 'BAForm/saveForm/step1';
$route['baform/save/form/step2']['post'] = 'BAForm/saveForm/step2';
$route['baform/save/form/step3']['post'] = 'BAForm/saveForm/step3';
$route['baform/save/form/step4']['post'] = 'BAForm/saveForm/step4';
$route['baform/save/form/step5']['post'] = 'BAForm/saveForm/step5';
$route['baform/save/form/step6']['post'] = 'BAForm/saveForm/step6';
$route['baform/list'] = 'BAForm/listBAForms';
$route['baform/check/edit/(.*)'] = 'BAForm/check/edit/$1';
$route['baform/check/pdf/(.*)'] = 'BAForm/check/pdf/$1';
$route['baform/step1/(.*)'] = 'BAForm/step1';
$route['baform/step2/(.*)'] = 'BAForm/step2';
$route['baform/step3/(.*)'] = 'BAForm/step3';
$route['baform/step4/(.*)'] = 'BAForm/step4';
$route['baform/step5/(.*)'] = 'BAForm/step5';
$route['baform/step6/(.*)'] = 'BAForm/step6';
$route['baform/submitted'] = 'BAForm/submittedForms';
$route['baform/pdf/(.*)'] = 'BAForm/pdf/$1';

//Bulk email routing
$route['new_email'] = 'Email/create_new_email';
$route['composer'] = 'Email/composer';
$route['dispatch/(:any)'] = 'Email/dispatch/$1';

//Email routing
$route['email_detail_view/(:any)'] = 'Email/bulk_email_detail_view/$1';

//Coach form routing
$route['submited_coach_form'] = 'Coach_form/index';
$route['view_engagement/(:any)'] = 'Coach_form/view_engagement/$1';
$route['view_engagement/(:any)/(:any)'] = 'Coach_form/view_engagement/$1/$2';
$route['create_coach_form_1'] = 'Coach_form/coach_form_1_create';
$route['edit_coach_form_1/(:any)'] = 'Coach_form/coach_form_1_edit/$1';
$route['view_edit_forms/(:any)/(:any)'] = 'Coach_form/view_edit_forms/$1/$2';
$route['create_coach_form_2/(:any)'] = 'Coach_form/coach_form_2_create/$1';
$route['create_coach_form_3'] = 'Coach_form/coach_form_3_create';
$route['edit_coach_form_3/(:any)'] = 'Coach_form/coach_form_3_edit/$1';
$route['submit_form/(:any)/(:any)'] = 'Coach_form/submit_form/$1/$2';
$route['create_coach_form_4'] = 'Coach_form/coach_form_4_create';
$route['edit_coach_form_4/(:any)'] = 'Coach_form/coach_form_4_edit/$1';
$route['create_coach_form_5'] = 'Coach_form/coach_form_5_create';
$route['edit_coach_form_5/(:any)'] = 'Coach_form/coach_form_5_edit/$1';
$route['view_single_form/(:any)/(:any)'] = 'Coach_form/view_single_form_pdf/$1/$2';

//PPMS form routing
$route['ppms_form'] = 'Ppms_form/index';
$route['create_ppms'] = 'Ppms_form/create_download';
$route['upload_ppms'] = 'Ppms_form/upload_ppms_form';
$route['is_submitted'] = 'Ppms_form/check_ppms_has_submitted';
$route['client_forms'] = 'Forms/index';
$route['submitted_ppms_forms'] = 'Ppms_form/view_submitted_ppms_forms';

//Reports
$route['reports/enquires'] = 'Reports/show_enquires';
$route['reports/temporary_enquires'] = 'Reports/show_temporary_enquires';
$route['reports/loan_enquires'] = 'Loan_enquire/loan_enquire_report';
$route['reports/loan_applications'] = 'Reports/show_loan_applications';
$route['reports/loan_approved_rejected'] = 'Reports/show_loan_approved_rejected';
$route['reports/jobs_created'] = 'Reports/show_jobs_created';
$route['reports/bank_declined_summary'] = 'Reports/show_bank_declined_summary';
$route['reports/client_regions'] = 'Reports/show_client_regions';
$route['reports/industry'] = 'Reports/show_industry';
$route['reports/print/(:any)'] = 'Reports/view_report_pdf/$1';

//Coaching Enquire Form
$route['coaching_form'] = 'Coaching_form/index';
$route['create_coaching'] = 'Coaching_form/create_coaching';
$route['register'] = 'Coaching_form/create_coaching_outside_crm';
$route['edit_coaching/(:any)'] = 'Coaching_form/edit_coaching/$1';
$route['print_coachingy/(:any)'] = 'Coaching_form/print_coaching/$1';
$route['become_coach/(:any)'] = 'Coaching_form/become_coach/$1';
$route['print_coaching_full_form'] = 'Coaching_form/print_coaching_full_form/$1';

//Security form routing
$route['security_form/(:any)/(:any)'] = 'Security_form/index/$1';
$route['save_security_form_step1/(:any)'] = 'Security_form/save_security_form_step1';
$route['edit_security_form_step1/(:any)'] = 'Security_form/update_security_form_step1';
$route['is_main_form_available/(:any)'] = 'Security_form/check_main_form_availablity';
$route['save_security_form_step2/(:any)'] = 'Security_form/save_security_form_step2';
$route['edit_security_form_step2/(:any)'] = 'Security_form/update_security_form_step2';
$route['save_security_form_step3/(:any)'] = 'Security_form/save_security_form_step3';
$route['edit_security_form_step3/(:any)'] = 'Security_form/update_security_form_step3';
$route['save_security_form_step4/(:any)'] = 'Security_form/save_security_form_step4';
$route['edit_security_form_step4/(:any)'] = 'Security_form/update_security_form_step4';
$route['sumbit_security_form/(:any)'] = 'Security_form/sumbit_security_form';
$route['view_security_form/(:any)'] = 'Security_form/view_security_form_pdf';
$route['submitted_security_forms'] = 'Security_form/view_submitted_security_forms';
$route['sec_submit_status/(:any)'] = 'Security_form/check_submit_status';
$route['delete_single_vehicle'] = 'Security_form/delete_single_vehicle';
$route['delete_single_property'] = 'Security_form/delete_single_property';
$route['delete_single_chattel'] = 'Security_form/delete_single_chattel';

//Notifications routing
$route['mark_selected_as_read'] = 'Notification/mark_selected_as_read';
$route['mark_all_as_read'] = 'Notification/mark_all_as_read';
$route['notification_check'] = 'Notification/notification_check';
$route['increase_current_notification_count'] = 'Notification/increase_current_notification_count';

//Loans Management
$route['loan_approval_link/(:any)'] = 'Loans_management/loan_approval_view/$1';
$route['approve/(:any)'] = 'Loans_management/loan_approve/$1';

//File Attachements routing
$route['file_attachments'] = 'File_attachment/index';
$route['new_attachment'] = 'File_attachment/new_file_attachment';
$route['load_my_files/(:any)'] = 'File_attachment/show_files_belongs_to_client/$1';
$route['delete_file'] = 'File_attachment/delete_attached_files';
$route['attach_file'] = 'File_attachment/upload_attach_files';

//Comment module routing
$route['add_comment/(:any)/(:any)'] = 'Comment/add_comment/$1/$2';

//Checklist routing
$route['toggle_checklist'] = 'Check_list/toggle_checklist';

//Dashboard Routing
$route['get_client_stat'] = 'Dashboard/get_client_stat';
$route['global_search'] = 'Dashboard/search';

//Loan_enquiry
$route['loan/enquire/form'] = 'Loan_enquire/loan_enquire_form';
$route['loan/enquire/ajax']['post'] = 'Loan_enquire/loan_enquire_ajax';
$route['loan/enquire/print/full'] = 'Loan_enquire/print_full_form';
$route['mark_as_answered/(:any)'] = 'Loan_enquire/mark_as_answered/$1';
