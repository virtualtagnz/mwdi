<?php $user_type = $this->encrypt->decode(end($this->uri->segments)); ?>
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <?php //var_dump($check_list) ?>
            <h3 class="page-title">Users <i class="lnr lnr-chevron-right"></i> <?php echo (($user_type == '2') ? 'Admin' : (($user_type == '3') ? 'Accountant' : (($user_type == '4') ? 'Coach' : 'Client'))) ?> <i class="lnr lnr-chevron-right"></i> View Profile</h3>
            <div class="panel panel-profile">
                <div class="clearfix">
                    <div class="profile-left">
                        <div class="profile-header">
                            <div class="overlay"></div>
                            <div class="profile-main">
                                <?php if ($single_user->image_link) { ?>
                                    <img src="<?php echo base_url('uploads/profile_image/' . $single_user->image_link) ?>" class="img-circle" alt="">
                                <?php } ?>
                                <h3 class="name"><?php echo $single_user->first_name . ' ' . $single_user->last_name; ?></h3>
                                Status: <?php echo $single_user->status == '1' ? '<span class="text-success">Active</span>' : '<span class="text-danger">Inactive</span>' ?>
                            </div>
                        </div>
                        <div class="profile-detail">
                            <div class="profile-info">
                                <ul class="list-unstyled list-justify">
                                    <li><strong>Birthday:</strong> <span><?php echo date('d/m/Y', strtotime($single_user->dob)); ?></span></li>
                                    <li><strong>Gender:</strong> <span><?php echo $single_user->gender; ?></span></li>
                                    <li><strong>Email:</strong> <span><?php echo $single_user->user_email; ?></span></li>
                                    <li><strong>Landline or Home Phone:</strong> <span><?php echo $single_user->phone; ?></span></li>
                                    <li><strong>Mobile:</strong> <span><?php echo $single_user->mobile; ?></span></li>
                                    <li><strong>Address:</strong> <span><?php echo $single_user->address; ?></span></li>
                                    <?php if ($user_type == '5') { ?>
                                        <li><strong>Industry:</strong> <span><?php echo $single_user->industry; ?></span></li>
                                        <li><strong>Category:</strong> <span><?php echo readable_client_category($single_user->client_category); ?></span></li>
                                    <?php } ?>
                                    <li><strong>Description:</strong> <span><?php echo $single_user->description; ?></span></li>
                                </ul>
                            </div>
                            <?php if ($this->session->userdata('role') == '1' || $this->session->userdata('role') == '2' || $this->session->userdata('role') == '4') { ?>
                                <div class="text-center"><a href="<?php echo base_url('edit_user/' . $this->encrypt->encode($single_user->user_id) . '/' . $this->encrypt->encode($single_user->role_id)); ?>" class="btn btn-primary">Edit Profile</a></div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php if ($user_type == '5') { ?>
                        <div class="profile-right">
                            <div class="custom-tabs-line tabs-line-bottom left-aligned">
                                <ul class="nav" role="tablist">
                                    <li class="active"><a href="#businessProgress" role="tab" data-toggle="tab">Business Application Progress</a></li>
                                    <li><a href="#securityProgress" role="tab" data-toggle="tab">Security Application Progress</a></li>
                                </ul>
                            </div>
                            <div class="tab-content" style="padding: 20px 0;">
                                <div class="tab-pane fade in active" id="businessProgress">
                                    <div class="row">
                                        <div class="col-md-2 col-xs-6">
                                            <div class="c100 p<?php echo $main_ba_progress;?> small green center">
                                                <span><?php echo $main_ba_progress.'%';?></span>
                                                <div class="slice">
                                                    <div class="bar"></div>
                                                    <div class="fill"></div>
                                                </div>
                                            </div>
                                            <p class="text-center c100-text">Personal Details</p>
                                        </div>
                                        <div class="col-md-2 col-xs-6">
                                            <div class="c100 p<?php echo $edu_progress;?> small green center">
                                                <span><?php echo $edu_progress.'%';?></span>
                                                <div class="slice">
                                                    <div class="bar"></div>
                                                    <div class="fill"></div>
                                                </div>
                                            </div>
                                            <p class="text-center c100-text">Educational Details</p>
                                        </div>
                                        <div class="col-md-2 col-xs-6">
                                            <div class="c100 p<?php echo $nxt_kin_progress;?> small green center">
                                                <span><?php echo $nxt_kin_progress.'%';?></span>
                                                <div class="slice">
                                                    <div class="bar"></div>
                                                    <div class="fill"></div>
                                                </div>
                                            </div>
                                            <p class="text-center c100-text">Next Of Kin Detals</p>
                                        </div>
                                        <div class="col-md-2 col-xs-6">
                                            <div class="c100 p<?php echo $emp_progress;?> small green center">
                                                <span><?php echo $emp_progress.'%';?></span>
                                                <div class="slice">
                                                    <div class="bar"></div>
                                                    <div class="fill"></div>
                                                </div>
                                            </div>
                                            <p class="text-center c100-text">Employment Details</p>
                                        </div>
                                        <div class="col-md-2 col-xs-6">
                                            <div class="c100 p<?php echo $biz_progress;?> small green center">
                                                <span><?php echo $biz_progress.'%';?></span>
                                                <div class="slice">
                                                    <div class="bar"></div>
                                                    <div class="fill"></div>
                                                </div>
                                            </div>
                                            <p class="text-center c100-text">Business Details</p>
                                        </div>
                                        <div class="col-md-2 col-xs-6">
                                            <div class="c100 p<?php echo $bk_progress;?> small green center">
                                                <span><?php echo $bk_progress.'%';?></span>
                                                <div class="slice">
                                                    <div class="bar"></div>
                                                    <div class="fill"></div>
                                                </div>
                                            </div>
                                            <p class="text-center c100-text">Bank Informaton</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="securityProgress">
                                    <div class="row">
                                        <div class="col-md-3 col-xs-6">
                                            <div class="c100 p<?php echo $main_sec_progress;?> small green center">
                                                <span><?php echo $main_sec_progress.'%';?></span>
                                                <div class="slice">
                                                    <div class="bar"></div>
                                                    <div class="fill"></div>
                                                </div>
                                            </div>
                                            <p class="text-center c100-text">Personal Details</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6">
                                            <div class="c100 p<?php echo $sec_vehicle_progress;?> small green center">
                                                <span><?php echo $sec_vehicle_progress.'%';?></span>
                                                <div class="slice">
                                                    <div class="bar"></div>
                                                    <div class="fill"></div>
                                                </div>
                                            </div>
                                            <p class="text-center c100-text">Motor Vehicles</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6">
                                            <div class="c100 p<?php echo $sec_property_progress;?> small green center">
                                                <span><?php echo $sec_property_progress.'%';?></span>
                                                <div class="slice">
                                                    <div class="bar"></div>
                                                    <div class="fill"></div>
                                                </div>
                                            </div>
                                            <p class="text-center c100-text">Property</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6">
                                            <div class="c100 p<?php echo $sec_chattles_progress;?> small green center">
                                                <span><?php echo $sec_chattles_progress.'%';?></span>
                                                <div class="slice">
                                                    <div class="bar"></div>
                                                    <div class="fill"></div>
                                                </div>
                                            </div>
                                            <p class="text-center c100-text">Chattels</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="custom-tabs-line tabs-line-bottom left-aligned">
                                <ul class="nav" role="tablist" id="three_tabs">
                                    <li><a href="#attachedFiles" role="tab" data-toggle="tab">Attached Files</a></li>
                                    <li><a href="#checkList" role="tab" data-toggle="tab">Check List</a></li>
                                    <li><a href="#profileNotes" role="tab" data-toggle="tab">Profile Notes</a></li>
                                </ul>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane fade" id="attachedFiles">
                                    <div class="table-responsive">
                                        <table class="table table-striped dataTable js-pointstable display">
                                            <thead>
                                                <tr>
                                                    <th>File Name</th>
                                                    <th>File Description</th>
                                                    <th style="width: 130px;"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if ($user_files) { ?>
                                                    <?php $user_files_arr = json_decode($user_files) ?>
                                                    <?php foreach ($user_files_arr as $user_file) { ?>
                                                        <tr>
                                                            <td><?php echo $user_file->file_name ?></td>
                                                            <td><?php echo $user_file->file_description ?></td>
                                                            <td>
                                                                <a href="<?php echo base_url('uploads/file_attachments/' . $single_user->client_id . '/' . $user_file->file_id) ?>" class="btn btn-fefault" title="View"><i class="fa fa-eye"></i></a> &nbsp; 
                                                                <a href="assets/img/login-bg.jpg" class="btn btn-fefault" title="Download" download><i class="fa fa-download"></i></a>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>   
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="checkList">
                                    <form action="" method="post" id="chk_list" name="chk_list">
                                        <ul class="list-unstyled todo-list">
                                            <?php if ($check_list) { ?>
                                                <?php foreach ($check_list as $check_list_val) { ?>
                                                    <li>
                                                        <label class="control-inline fancy-checkbox">
                                                            <input type="checkbox" name="chk[]" id="<?php echo $check_list_val->checklist_id ?>" value="<?php echo $check_list_val->checklistclient_id ?>"<?php echo $check_list_val->is_checked=='1'?'checked':'' ?>><span></span>
                                                        </label>
                                                        <p><span class="short-description"><?php echo $check_list_val->description ?></span></p>
                                                    </li>
                                                <?php } ?>
                                            <?php } ?>
                                            <!--                                    <li>
                                                                                    <label class="control-inline fancy-checkbox">
                                                                                        <input type="checkbox"><span></span>
                                                                                    </label>
                                                                                    <p>
                                                                                        <span class="short-description">Compellingly implement clicks-and-mortar relationships without highly efficient metrics.</span>
                                                                                    </p>
                                                                                    <div class="controls">
                                                                                        <a href="#" title="Delete"><i class="lnr lnr-cross"></i></a>
                                                                                    </div>
                                                                                </li>-->
                                        </ul>
                                    </form>
                                </div>
                                <div class="tab-pane fade" id="profileNotes">
                                    <div class="table-responsive">
                                        <table class="table table-striped dataTable js-pointstable display">
                                            <thead>
                                                <tr>
                                                    <th>User</th>
                                                    <th>Comment</th>
                                                    <th>Date</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if ($user_comments != '') { ?>
                                                    <?php $user_comments_arr = json_decode($user_comments) ?>
                                                    <?php foreach ($user_comments_arr as $single_comment) { ?>
                                                        <?php //var_dump($single_comment);?>
                                                        <tr>
                                                            <td><?php echo $single_comment->user_details->first_name . ' ' . $single_comment->user_details->last_name ?></td>
                                                            <td><?php echo $single_comment->comment ?></td>
                                                            <td><?php echo $single_comment->date_create ?></td>
                                                        </tr>
                                                    <?php } ?>   
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>

                                    <hr>

                                    <form action="<?php echo base_url('add_comment/' . $this->encrypt->encode($single_user->user_id) . '/' . $this->encrypt->encode($user_type)) ?>" method="post" id="comment" name="comment">
                                        <div class="input-group">
                                            <input type="hidden" name="client_ref" id="client_ref" value="<?php echo $this->encrypt->encode($single_user->client_id) ?>">
                                            <input class="form-control input-lg" type="text" placeholder="Type your comment" name="cmnt" id="cmnt" data-validation="required" data-validation-error-msg="The field comment is mandatory, please check again!">
                                            <span class="input-group-btn"><button class="btn btn-primary btn-lg" type="submit" id="add_comment">+ Comment</button></span>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<script>
    //localStorage.removeItem("tab_val");
    $(document).ready(function () {
        $.validate({
            form: '#comment',
            modules: 'security'
        });

        //----------------------------------------------------------------------
        //localStorage.removeItem("tab_val");
        var selected_tab = localStorage.getItem("tab_val");
        console.log(selected_tab);
        if (selected_tab != "") {
            $('a[href="' + selected_tab + '"]').parents("li").addClass("active");
            $(selected_tab).addClass('active in');
        } else {
            $('#three_tabs li:first-child').addClass("active");
            $($('#three_tabs li:first-child a').get(0).hash).addClass('active in');
        }

        //----------------------------------------------------------------------

        $('#three_tabs li a').on('click', function () {
            localStorage.removeItem("tab_val");
            localStorage.setItem("tab_val", $(this).get(0).hash);
        });

        //----------------------------------------------------------------------

        $('#chk_list :checkbox').on('click', function () {
            var uid = $(this).val();
            var stat = $(this).is(':checked');
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('toggle_checklist'); ?>",
                data: {'uid': uid,'chk':stat},
                success: function (results) {
                    console.log(results);
                }
            });
        });
    });
</script>