<?php $user_type = $this->encrypt->decode(end($this->uri->segments)); ?>
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                Users <i class="lnr lnr-chevron-right"></i> <?php echo (($user_type == '2') ? 'Admin' : (($user_type == '3') ? 'Accountant' : (($user_type == '4') ? 'Coach' : 'Client'))) ?> <i class="lnr lnr-chevron-right"></i> Edit
            </h3>
            <?php //var_dump($single_user)?>
            <form action="<?php echo base_url('edit/' . $this->encrypt->encode($single_user->user_id) . '/' . $this->encrypt->encode($user_type)); ?>" method="post" name="user_edit" id="user_edit" enctype="multipart/form-data">
                <input type="hidden" name="current_prof_img" id="current_prof_img" value="<?php echo $single_user->image_link; ?>">
<!--                <input type="hidden" name="user_info_id" id="user_info_id" value="<?php echo $data->u_id; ?>">-->
                <!--<input type="hidden" name="fileName" id="fileName" value="<?php echo $data->imageURL; ?>">-->
                <div class="panel">
                    <div class="panel-heading">
                        <p>Login Details</p>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="profile-header" style="margin-bottom: 10px;">
                                <div class="overlay"></div>
                                <div class="profile-main">
                                    <?php if ($single_user->image_link) { ?>
                                        <img src="<?php echo base_url('uploads/profile_image/' . $single_user->image_link) ?>" class="img-circle img-responsive center-block" alt="">
                                    <?php } ?>
                                    <h3 class="name"><?php echo $single_user->first_name . ' ' . $single_user->last_name; ?></h3>
                                    Status: <?php echo $single_user->status == '1' ? '<span class="text-success">Active</span>' : '<span class="text-danger">Inactive</span>' ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="username" name="username" placeholder="User Name" maxlength="45" data-validation="required" data-validation-error-msg="The field user name is mandatory, please check again!" value="<?php echo $single_user->user_name; ?>" readonly="true">
                                    <div id="infoMessage"><?php echo form_error('username'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="email" name="email" placeholder="Email" maxlength="100" data-validation="required email" data-validation-error-msg-required="The field user name is mandatory, please check again!" data-validation-error-msg-email="You have not given a correct e-mail address" value="<?php echo $single_user->user_email; ?>">
                                    <div id="infoMessage"><?php echo form_error('email'); ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="password" class="form-control" id="pwd" name="pwd" placeholder="Change Password" maxlength="10" data-validation="custom" data-validation-error-msg-required="The field password is mandatory, please check again!" data-validation-regexp="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$" data-validation-error-msg-custom="The  provided password does not fit according to the rules required, please check again!" value="<?php echo set_value('pwd'); ?>" data-validation-optional="true">
                                    <div id="infoMessage"><?php echo form_error('pwd'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="password" class="form-control" id="confirm_pwd" name="confirm_pwd" placeholder="Confirm Password" maxlength="10" data-validation="confirmation" data-validation-confirm="pwd" data-validation-error-msg="The password does not match, please try again!" value="<?php echo set_value('confirm_pwd'); ?>" data-validation-optional="true">
                                    <div id="infoMessage"><?php echo form_error('confirm_pwd'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <ul list-unstyled activity-list style="list-style: none;">
                                    <li>The Password must contain between 6 and 10 characters</li>
                                    <li>At least 1 uppercase letter(s) (ABCDEFGHIJKLMNOPQRSTUVWXYZ)</li>
                                    <li>At least 1 lowercase letter(s) (abcdefghijklmnopqrstuvwxyz)</li>
                                    <li>At least 1 numeric character(s) (0123456789)</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <?php if($user_type == '5'){ ?>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select class="form-control" name="client_category" id="client_category" data-validation="required">
                                       <option value="">Client Category</option> 
                                       <option value="loan-client" <?php echo $single_user->client_category == 'loan-client' ? "selected" : "" ?>>Loan client</option> 
                                       <option value="cocheee" <?php echo $single_user->client_category == 'cocheee' ? "selected" : "" ?>>Coachee</option> 
                                       <option value="both" <?php echo $single_user->client_category == 'both' ? "selected" : "" ?>>Both</option> 
                                    </select>
                                     <div id="infoMessage"><?php echo form_error('client_category'); ?></div>
                                </div>
                            </div>
                            <?php } ?>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <p style="padding: 6px 0;">
                                        <strong>Status: &nbsp; </strong>
                                        <label class="radio radio-inline" style="margin: 0">
                                            <input name="status" id="stat_active" value="1" type="radio" <?php echo $single_user->status == '1' ? "checked" : "" ?>>
                                            <span>Active</span>
                                        </label>
                                        <label class="radio radio-inline">
                                            <input name="status" id="stat_inactive" value="0" type="radio" <?php echo $single_user->status == '0' ? "checked" : "" ?>>
                                            <span>Inactive</span>
                                        </label>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel">
                    <div class="panel-heading">
                        <p>User Information</p>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" maxlength="50" value="<?php echo $single_user->first_name; ?>" data-validation="required" data-validation-error-msg="The field first name is mandatory, please check again!">
                                    <div id="infoMessage"><?php echo form_error('fname'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="laname" name="lname" placeholder="Last Name" maxlength="50" value="<?php echo $single_user->last_name; ?>" data-validation="required" data-validation-error-msg="The field last name is mandatory, please check again!">
                                    <div id="infoMessage"><?php echo form_error('lname'); ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control date" id="dob" name="dob" placeholder="Birthday (dd/mm/yyyy)" value="<?php echo date('d/m/Y', strtotime($single_user->dob)); ?>">
                                    <div id="infoMessage"><?php echo form_error('dob'); ?></div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <p style="padding: 6px 0;">
                                        <strong>Gender: &nbsp; </strong>
                                        <label class="radio radio-inline" style="margin: 0">
                                            <input name="gender" value="male" type="radio" data-validation="required" data-validation-error-msg="The field gender is mandatory, please check again!" <?php echo $single_user->gender == 'male' ? "checked" : "" ?>>
                                            <span>Male</span>
                                        </label>
                                        <label class="radio radio-inline">
                                            <input name="gender" value="female" type="radio" data-validation="required" data-validation-error-msg="The field gender is mandatory, please check again!" <?php echo $single_user->gender == 'female' ? "checked" : "" ?>>
                                            <span>Female</span>
                                        </label>
                                    </p>
                                    <div id="infoMessage"><?php echo form_error('gender'); ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control phone" id="phone" name="phone" placeholder="Land Phone 091234567" value="<?php echo $single_user->phone; ?>">
                                    <div id="infoMessage"><?php echo form_error('phone'); ?></div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control phone" id="mobile" name="mobile" placeholder="Mobile 0211234567" value="<?php echo $single_user->mobile; ?>">
                                    <div id="infoMessage"><?php echo form_error('mobile'); ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="address" id="address" name="address" onFocus="geolocate()" placeholder="Address" value="<?php echo $single_user->address; ?>" data-validation="required" data-validation-error-msg="The field address is mandatory, please check again!" <?php echo $user_type != '5' ? 'data-validation-optional="true"' : '' ?>>
                                    <div id="infoMessage"><?php echo form_error('address'); ?></div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="street_number" id="street_number" name="street_number" placeholder="Number" readonly value="<?php echo $single_user->add_street_no; ?>">
                                    <div id="infoMessage"><?php echo form_error('street_number'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="route" id="route" name="route" placeholder="Street" readonly value="<?php echo $single_user->add_street_name; ?>">
                                    <div id="infoMessage"><?php echo form_error('route'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="postal_code" id="postal_code" id="postal_code" placeholder="Postal code" readonly value="<?php echo $single_user->add_post_code; ?>">
                                    <div id="infoMessage"><?php echo form_error('postal_code'); ?></div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="locality" id="locality" id="locality" placeholder="City" readonly value="<?php echo $single_user->add_city; ?>">
                                    <div id="infoMessage"><?php echo form_error('locality'); ?></div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="administrative_area_level_1" id="administrative_area_level_1" name="administrative_area_level_1" placeholder="Region" readonly value="<?php echo $single_user->add_region; ?>">
                                    <div id="infoMessage"><?php echo form_error('administrative_area_level_1'); ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-bottom: 15px;"><hr></div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <i class="fa fa-info-circle pull-right" style="margin-top: 10px;" data-toggle="tooltip" data-placement="top" title="Accept Formats: jpg, jpeg and png"></i>
                                    <label for="prof_image" class="filupp form-control" style="width: 96%;">
                                        <span class="filupp-file-name js-value">Profile Image</span>
                                        <input type="file" name="prof_image" value="" id="prof_image" accept="image/*" data-validation="mime size" data-validation-allowing="jpg, jpeg and png" data-validation-max-size="10M" data-validation-error-msg-size="You can not upload images larger than 10MB" data-validation-error-msg-mime="The filetype you are attempting to upload is not allowed">
                                    </label>
                                    <div id="infoMessage"><?php echo form_error('prof_image'); ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Description" rows="4" maxlength="1000" name="description" id="description"><?php echo $single_user->description; ?></textarea>
                                    <div id="infoMessage"><?php echo form_error('description'); ?></div>
                                </div>
                            </div>
                        </div>
                        <?php if ($user_type == '5') { ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="industry" name="industry" placeholder="Industry" maxlength="50" value="<?php echo $single_user->industry; ?>" data-validation="required" data-validation-error-msg="The field industry is mandatory, please check again!">
                                        <div id="infoMessage"><?php echo form_error('industry'); ?></div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <p><br>
                            <a href="javascript: history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; Back</a> &nbsp; 
                            <button type="submit" class="btn btn-primary">UPDATE USER</button>
                        </p>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<?php if ($this->session->flashdata('create_success')) { ?>
    <script>swal({type: 'success', title: 'success', text: 'The user has been created', timer: 3000});</script>
<?php } else if ($this->session->flashdata('update_success')) { ?>
    <script>swal({type: 'success', title: 'success', text: 'The user has been updated', timer: 3000});</script>
<?php } else if ($this->session->flashdata('error_msg')) { ?>
    <script>swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000});</script>
<?php } ?>
<script>
    $(document).ready(function () {
        $.validate({
            form: '#user_edit',
            modules: 'security,date,file'
        });

        $('#dob').mask('00/00/0000', {placeholder: "Birthday DD/MM/YYYY"});
        $('#phone').mask('000000000000', {placeholder: "Land Phone 091234567"});
        $('#mobile').mask('000000000000', {placeholder: "Mobile 0211234567"});
    });
</script>
<script>
// This example displays an address form, using the autocomplete feature
    // of the Google Places API to help users fill in the information.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    var placeSearch, autocomplete;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        postal_code: 'short_name'
    };
    function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
                /** @type {!HTMLInputElement} */(document.getElementById('address')),
                {types: ['geocode'], componentRestrictions: {country: "nz"}});
        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        for (var component in componentForm) {
            document.getElementById(component).value = '';
            //document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
            }
        }
    }

    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCU5pwsCrj8K5Sf7dvZ1fmq2VRfG4CMg64&libraries=places&callback=initAutocomplete"
async defer></script>