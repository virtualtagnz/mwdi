<?php $user_type = $this->encrypt->decode(end($this->uri->segments)); ?>
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <?php if ($this->session->userdata('role') == '1') { ?>
                    <a href="<?php echo base_url('create_user/' . $this->encrypt->encode($user_type)) ?>" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> &nbsp; Create User Account</a>
                <?php } ?>
                Users <i class="lnr lnr-chevron-right"></i> <?php echo (($user_type == '2') ? 'Admin' : (($user_type == '3') ? 'Accountant' : (($user_type == '4') ? 'Coach' : 'Client'))) ?>
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <?php if ($users) { ?>
                        <div class="table-responsive">
                            <table id="user_tab" class="table table-striped table-bordered dataTable js-pointstable display">
                                <thead>
                                    <tr>
                                        <th>User</th>
                                        <th>Status</th>
                                        <?php if($user_type=='5'){?>
                                            <th>Category</th>
                                        <?php } ?>
                                        <?php if ($this->session->userdata('role') == '4') {?>
                                            <th>Client - Coach engagement time</th>
                                        <?php } ?>
                                        <th style="width: 130px;" data-orderable="false"></th>
                                    </tr>
                                    <tr id="filterrow">
                                        <th></th>
                                        <th>Status</th>
                                        <?php if($user_type=='5'){?>
                                            <th>Category</th>
                                        <?php } ?>
                                        <?php if ($this->session->userdata('role') == '4') {?>
                                            <th></th>
                                        <?php } ?>
                                        <th style="width: 130px;" data-orderable="false"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $ext_users = json_decode($users) ?>
                                    <?php //var_dump($ext_users);?>
                                    <?php foreach ($ext_users as $user) { ?>
                                        <tr>
                                            <td>
                                                <a href="<?php echo base_url('view_profile/' . $this->encrypt->encode($user->user_id) . '/' . $this->encrypt->encode($user->role_id)) ?>">
                                                    <?php if ($user->image_link) { ?>
                                                        <img src="<?php echo base_url('uploads/profile_image/' . $user->image_link) ?>" alt="" class="img-circle img-thumbnail" style="height: 50px;">
                                                    <?php } ?>
                                                    &nbsp; <?php echo $user->first_name . ' ' . $user->last_name; ?>
                                                </a>
                                            </td>
                                            <td><?php echo $user->status == '1' ? 'Active' : 'Inactive' ?></td>
                                            <?php if($user_type=='5'){?>
                                                <td><?php echo readable_client_category($user->client_category); ?></td>
                                            <?php } ?>
                                            <?php if ($this->session->userdata('role') == '4') {?>
                                                <td><?php echo $user->strat_work == 1 ? $user->start_date : '-' ?></td>
                                            <?php } ?>
                                            <td>
                                                <?php if ($this->session->userdata('role') == '1' || $this->session->userdata('role') == '2' || $this->session->userdata('role') == '4') { ?>
                                                    <a href="<?php echo base_url('edit_user/' . $this->encrypt->encode($user->user_id) . '/' . $this->encrypt->encode($user->role_id)); ?>" class="btn btn-fefault" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a> &nbsp;
                                                <?php } ?>
                                                <?php if ($this->session->userdata('role') == '4') {?>
                                                    <?php if ($user->strat_work == 0) {?>
                                                        <a href="javascript:void(0)" onclick="mark_as_start_work(<?php echo $user->tab_id; ?>);" class="btn btn-fefault" data-toggle="tooltip" data-placement="top" title="Engage with client"><i class="fa fa-check-square-o"></i></a>
                                                    <?php } ?> 
                                                <?php } ?>   
                                                <?php if ($this->session->userdata('role') == '1') { ?>
                                                    <?php if($user->status == 1) { ?>
                                                    <a id="<?php echo $this->encrypt->encode($user->user_id) ?>" class="btn btn-fefault btn_deactive" data-toggle="tooltip" data-placement="top" title="Deactivate"><i class="fa fa-ban"></i></a>
                                                    <?php } else { ?>
                                                        <a id="<?php echo $this->encrypt->encode($user->user_id) ?>" class="btn btn-fefault btn_active" data-toggle="tooltip" data-placement="top" title="Activate"><i class="fa fa-check"></i></a>
                                                    <?php } ?>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
<!--                                <tfoot>
                                    <tr>
                                        <th>User</th>
                                        <th>Status</th>
                                        <th style="width: 130px;" data-orderable="false"></th>
                                    </tr>
                                </tfoot>-->
                            </table>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script type="text/javascript">
    $(document).ready(function () {
        var u_type = '<?php echo $user_type;?>';
        var column_numbers=[];
        if(u_type=="5"){
            column_numbers=[1,2];
        }else{
            column_numbers=[1];
        }
        $('#user_tab').dataTable({
            "order":[],
            "pageLength": 10,
            "searching": true,
            "info": true,
            "ordering": true,
            initComplete: function () {
                
                console.log(column_numbers);
                this.api().columns(column_numbers).every( function () {
                    var column = this;
                    var select = $('<select><option value=""></option></select>')
                        .appendTo( $(column.header()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );
                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                } );
            }
        });

        $('#user_tab').on('click', '.btn_deactive', function () {
            var uid = $(this).attr('id');
            swal({
                title: 'Do you really want to deactivate this user?',
                confirmButtonText: 'Deactivate',
                text: 'User will be deactive permanently',
                showLoaderOnConfirm: true,
                showCancelButton: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajax({
                            url: '<?php echo base_url('deactivate/'); ?>' + uid,
                            type: 'POST',
                        })
                                .done(function (response) {
                                    var rslt = JSON.parse(response);
                                    console.log(rslt.status);
                                    if (rslt.status == 1) {
                                        swal({type: 'success', title: 'success', text: 'The user has been deactivated', timer: 3000}).catch(function (timeout) { });
                                        window.setTimeout(function () {
                                            location.reload();
                                        }, 3000);
                                    } else {
                                        resolve()
                                        //swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000}) 
                                    }
                                })
                                .fail(function () {
                                    swal('Oops...', 'Something went wrong !', 'error');
                                });
                    })
                }
            }).catch(swal.noop);
        });

        $('#user_tab').on('click', '.btn_active', function () {
            var uid = $(this).attr('id');
            swal({
                title: 'Do you really want to activate this user?',
                confirmButtonText: 'Activate',
                text: 'User will be activate permanently',
                showLoaderOnConfirm: true,
                showCancelButton: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajax({
                            url: '<?php echo base_url('activate/'); ?>' + uid,
                            type: 'POST',
                        })
                                .done(function (response) {
                                    var rslt = JSON.parse(response);
                                    console.log(rslt.status);
                                    if (rslt.status == 1) {
                                        swal({type: 'success', title: 'success', text: 'The user has been activate', timer: 3000}).catch(function (timeout) { });
                                        window.setTimeout(function () {
                                            location.reload();
                                        }, 3000);
                                    } else {
                                        resolve()
                                        //swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000}) 
                                    }
                                })
                                .fail(function () {
                                    swal('Oops...', 'Something went wrong !', 'error');
                                });
                    })
                }
            }).catch(swal.noop);
        });
    });
    
    function mark_as_start_work(uid) {
        console.log(uid);
        swal({
            title: 'Do you confirm that you start work with this client?',
            confirmButtonText: 'Confirm',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            preConfirm: function () {
                return new Promise(function (resolve) {
                    $.ajax({
                        type: "POST",
                        url: '<?php echo base_url('mark_as_start_work')?>',
                        data: {'uid':uid}
                    }).done(function (response) {
                        if(JSON.parse(response)=='success'){
                            swal({type: 'success', title: 'success'}).then(function(){location.reload();});
                        }
                    });
                });
            }
        }).catch(swal.noop);
    } 
</script>