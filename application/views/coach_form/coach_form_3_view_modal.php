<?php
$ch_form2_details = (json_decode($details));
?>
<style>
    td{
        border: 1px solid #9D8526;
        padding: 5px;
    }
    tr.bg{
        background-color: #252c35;
        color: #ffffff;
    }
    .title{
        text-align: center;
    }
</style>
<div class="title">
    <h2>Client Form Engagement 2</h2>
</div>
<?php if ($ch_form2_details) { ?>
<table cellspacing="0" cellpadding="5" border="1">
        <tr class="bg">
            <td colspan="2">
                <h4 class="">ENGAGEMENT 2 (Building on the Relationship)</h4>
            </td>
        </tr>
        <tr>
            <td>Client Name</td><td><?php echo $ch_form2_details->first_name . ' ' . $ch_form2_details->last_name ?></td>
        </tr>
        <tr>
            <td>Where did you meet</td><td><?php echo $ch_form2_details->chp3_f24 ?></td>
        </tr>
        <tr>
            <td>Start Time</td><td><?php echo $ch_form2_details->chp3_f25 ?></td>
        </tr>
        <tr>
            <td>End Time</td><td><?php echo $ch_form2_details->chp3_f26 ?></td>
        </tr>
        <tr>
            <td>General overview of the Client</td><td><?php echo $ch_form2_details->chp3_f27 ?></td>
        </tr>
        <tr>
            <td>What information has been provided to the client</td>
            <td>
                <?php
                $selected_ck_box_arr = $ch_form2_details->chp3_f28 != '' ? explode(",", $ch_form2_details->chp3_f28) : array();
                if (in_array('1', $selected_ck_box_arr)) {
                    echo '- Business Audit' . '<br>';
                }
                if (in_array('2', $selected_ck_box_arr)) {
                    echo '  - Financial Audit' . '<br>';
                }
                if (in_array('3', $selected_ck_box_arr)) {
                    echo '  - Business Plan' . '<br>';
                }
                if (in_array('4', $selected_ck_box_arr)) {
                    echo '  - Financial Plan' . '<br>';
                }
                if (in_array('5', $selected_ck_box_arr)) {
                    echo '  - Loan Application' . '<br>';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>Relevant points found in the business/financial audit</td><td><?php echo $ch_form2_details->chp3_f29 ?></td>
        </tr>
        <tr>
            <td>If they haven’t filled in the link or template please note why</td><td><?php echo $ch_form2_details->chp3_f30 ?></td>
        </tr>
        <br pagebreak="true"/>
        <tr class="bg">
            <td colspan="2">
                <h4 class="">Loan Questions if applicable</h4>
            </td>
        </tr>
        <tr>
            <td>Does this client wish to progress with a loan application and do they require assistance to complete all the requirements</td><td><?php echo $ch_form2_details->chp3_f31 == '1' ? 'Yes' : 'No' ?></td>
        </tr>
        <tr>
            <td>Has the money invested by MWDI been in line with their original application/Business plan submitted</td><td><?php echo $ch_form2_details->chp3_f32 ?></td>
        </tr>
        <tr>
            <td>Will the client consider increasing repayments to MWDI? If so how much/by when</td><td><?php echo $ch_form2_details->chp3_f33 ?></td>
        </tr>
    </table>
    <div style="page-break-inside: avoid;"></div>
    <table cellspacing="0" cellpadding="5" border="1">
        <tr class="bg">
            <td colspan="2">
                <h4 class="">Financial Literacy</h4>
            </td>
        </tr>
        <tr>
            <td>What are the key financial issues impacting the client at a personal level</td><td><?php echo $ch_form2_details->chp3_f34 ?></td>
        </tr>
        <tr>
            <td>What are the key financial issues impacting the client at a business level</td><td><?php echo $ch_form2_details->chp3_f35 ?></td>
        </tr>
        <tr>
            <td>What topics did you explore together</td><td><?php echo $ch_form2_details->chp3_f36 ?></td>
        </tr>
    </table>
    <div style="page-break-inside: avoid;"></div>
    <table cellspacing="0" cellpadding="5" border="1">
        <tr class="bg">
            <td colspan="2">
                <h4 class="">Training</h4>
            </td>
        </tr>
        <tr>
            <td>Has the client identified any specific training needs</td><td><?php echo $ch_form2_details->chp3_f37 ?></td>
        </tr>
        <tr>
            <td>Did you refer the clients onto any of our training programs</td><td><?php echo $ch_form2_details->chp3_f38 ?></td>
        </tr>
    </table>
    <br pagebreak="true"/>
    <table cellspacing="0" cellpadding="5" border="1">
        <tr class="bg">
            <td colspan="2">
                <h4 class="">Information</h4>
            </td>
        </tr>
        <tr>
            <td>What information been provided to the client (ie Business Plan templates/Financial Plan Templates, Links to information)</td><td><?php echo $ch_form2_details->chp3_f39 ?></td>
        </tr>
        <tr>
            <td>Has the client identified any specific training needs</td><td><?php echo $ch_form2_details->chp3_f40 ?></td>
        </tr>
        <tr>
            <td>Did you refer the clients onto any of our training programmes</td><td><?php echo $ch_form2_details->chp3_f41 ?></td>
        </tr>
    </table>
    <div style="page-break-inside: avoid;"></div>
    <table cellspacing="0" cellpadding="5" border="1">
        <tr class="bg">
            <td colspan="2">
                <h4 class="">Business Development</h4>
            </td>
        </tr>
        <tr>
            <td>What are the key challenges facing this client</td><td><?php echo $ch_form2_details->chp3_f41 ?></td>
        </tr>
        <tr>
            <td>What resources/support does this client require</td><td><?php echo $ch_form2_details->chp3_f42 ?></td>
        </tr>
        <tr>
            <td>What would support this client to grow and expand their business</td><td><?php echo $ch_form2_details->chp3_f43 ?></td>
        </tr>
        <tr>
            <td>Key highlights/Breakthroughs</td><td><?php echo $ch_form2_details->chp3_f44 ?></td>
        </tr>
        <tr>
            <td>Did you refer the client onto any other service/organisation/website? If so who/what</td><td><?php echo $ch_form2_details->chp3_f45 ?></td>
        </tr>
        <tr>
            <td>Any points for follow up from other Coaches</td><td><?php echo $ch_form2_details->chp3_f46 ?></td>
        </tr>
<!--        <tr>
            <td>What would support this client to grow and expand their business</td><td><?php echo $ch_form2_details->chp3_f47 ?></td>
        </tr>-->
        <tr>
            <td>Confirmation of next appointment</td><td><?php echo $ch_form2_details->chp3_f48?></td>
        </tr>
        <tr>
            <td>Date for the next engagements</td><td><?php echo $ch_form2_details->chp3_f49?></td>
        </tr>
        <tr>
            <td>Invoice sent by the 15th of the month</td><td><?php echo $ch_form2_details->chp3_f50?></td>
        </tr>
    </table>
<?php } ?>

