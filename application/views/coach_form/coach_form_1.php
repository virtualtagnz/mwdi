<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
<!--                <a href="<?php echo base_url('view_single_form/5/FALSE'); ?>" class="btn btn-primary pull-right"><i class="fa fa-print"></i> &nbsp; Print Form</a>-->
                Forms <i class="lnr lnr-chevron-right"></i> Create <i class="lnr lnr-chevron-right"></i> Client Form
            </h3>
            <?php //var_dump($details) ?>
            <form action="<?php echo base_url('create_coach_form_1') ?>" id="coach_frm_1" name="coach_frm_1" method="post" enctype="multipart/form-data">
                <div class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <h4>CLIENT INFORMATION</h4>
                            </div>
                            <input type="hidden" name="ch1_start_time" id="ch1_start_time" value="<?php echo set_value('ch1_start_time'); ?>">
                            <input type="hidden" name="sub_stat" id="sub_stat" value="N">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Client Name</label>
                                    <select class="form-control" id="sel_client" name="sel_client" data-validation="required" data-validation-error-msg="The field select client is mandatory, please check again!">
                                        <option selected="selected" value="">Client Name</option>
                                        <?php if ($my_clients) { ?>
                                            <?php $clients = json_decode($my_clients) ?>
                                            <?php foreach ($clients as $client) { ?>
                                                <option value="<?php echo $client->client_id ?>"<?php echo set_select('sel_client', $client->client_id); ?>><?php echo $client->first_name . ' ' . $client->last_name ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                    <div id="infoMessage"><?php echo form_error('sel_client'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>New/Existing Client</label>
                                    <select class="form-control" id="sel_type" name="sel_type" data-validation="required" data-validation-error-msg="The field new/existing Client is mandatory, please check again!">
                                        <option value="" selected="selected">New/Existing Client</option>
                                        <option value="1" <?php echo set_select('sel_type', '1'); ?>>New</option>
                                        <option value="2" <?php echo set_select('sel_type', '2'); ?>>Existing</option>
                                    </select>
                                    <div id="infoMessage"><?php echo form_error('sel_type'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Region of Delivery</label>
                                    <input type="text" class="form-control" id="region" name="region" placeholder="Region of Delivery" data-validation="required" data-validation-error-msg="The field region of delivery is mandatory, please check again!" value="<?php echo set_value('username'); ?>">
                                    <div id="infoMessage"><?php echo form_error('region'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Referred by</label>
                                    <input type="text" class="form-control" id="referred" name="referred" placeholder="Referred by" value="<?php echo set_value('username'); ?>" data-validation="required" data-validation-error-msg="The field referred by is mandatory, please check again!">
                                    <div id="infoMessage"><?php echo form_error('referred'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-4">
<!--                                <div class="form-group">
                                    <label>Name of Client Referred</label>
                                    <select class="form-control" id="intro_client" name="intro_client" data-validation="required" data-validation-error-msg="The field name of client referred is mandatory, please check again!">
                                        <option value="" selected="selected">Name of Client Referred</option>
                                        <?php if ($my_all_clients) { ?>
                                            <?php $clients = json_decode($my_all_clients) ?>
                                            <?php foreach ($clients as $client) { ?>
                                                <option value="<?php echo $client->client_id ?>"<?php echo set_select('intro_client', $client->client_id); ?> ><?php echo $client->first_name . ' ' . $client->last_name ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                    <div id="infoMessage"><?php echo form_error('intro_client'); ?></div>
                                </div>-->
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Address</label>
                                    <textarea class="form-control" name="address" id="address" placeholder="Address" rows="3" data-validation="required" data-validation-error-msg="The field address is mandatory, please check again!"><?php echo set_value('address'); ?></textarea>
                                    <div id="infoMessage"><?php echo form_error('address'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Purpose of Visit</label>
                                    <i class="fa fa-info-circle pull-right" style="margin-top: 25px;" data-toggle="tooltip" data-placement="top" title="Please write down the key intention of this visit, indicate if this is the first, second or third visit and what you hope to achieve in the visit with the Coachee."></i>
                                    <textarea class="form-control" name="purpose" id="purpose" placeholder="Purpose of Visit" rows="3" style="width: 96%;" data-validation="required" data-validation-error-msg="The field purpose of visit is mandatory, please check again!"><?php echo set_value('purpose'); ?></textarea>
                                    <div id="infoMessage"><?php echo form_error('purpose'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <input type="text" class="form-control phone" id="phone" name="phone" placeholder="Phone Number" maxlength="20" data-validation="required" data-validation-error-msg="The field phone number is mandatory, please check again!" value="<?php echo set_value('phone'); ?>">
                                    <div id="infoMessage"><?php echo form_error('phone'); ?></div>
                                </div>
                            </div>
<!--                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Emergency Contact Person</label>
                                    <input type="text" class="form-control" id="nxt_kin" name="nxt_kin" placeholder="Emergency Contact Person" value="<?php echo set_value('nxt_kin'); ?>">
                                    <div id="infoMessage"><?php echo form_error('nxt_kin'); ?></div>
                                </div>
                            </div>-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Mobile Number</label>
                                    <input type="text" class="form-control phone" id="mobile" name="mobile" placeholder="Mobile Number" maxlength="20" value="<?php echo set_value('mobile'); ?>">
                                    <div id="infoMessage"><?php echo form_error('mobile'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" data-validation="required" data-validation-error-msg="The field email is mandatory, please check again!" value="<?php echo set_value('email'); ?>">
                                    <div id="infoMessage"><?php echo form_error('email'); ?></div>
                                </div>
                            </div>
                            <!--                            <div class="col-md-12" style="padding-top: 20px; padding-bottom: 20px;">
                                                            <a href="javascript: history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; Back</a> &nbsp; 
                                                            <button type="submit" id="save_ch_frm_1" class="btn btn-default">SAVE FORM &nbsp; <i class="fa fa-save"></i></button> &nbsp;
                                                            <button type="button" id="go_nxt" class="btn btn-primary">NEXT &nbsp; <i class="fa fa-arrow-right"></i></button>
                                                        </div>-->
                        </div>
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <h4>ENGAGEMENT 1 (Whanaungatanga/Relationship Building)</h4>
                            </div>
                            <input type="hidden" name="start_time" id="start_time" value="<?php echo set_value('start_time'); ?>">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Where did you meet?</label>
                                    <input type="text" class="form-control" id="ch2_place" name="ch2_place" placeholder="Where did you meet?" data-validation="required" data-validation-error-msg="The field where did you meet is mandatory, please check again!" value="<?php echo set_value('ch2_place'); ?>">
                                    <div id="infoMessage"><?php echo form_error('ch2_place'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Start Time</label>
                                    <input type="text" class="form-control" id="ch2_st_time" name="ch2_st_time" placeholder="Start Time" data-validation="required time custom" data-validation-help="HH:mm in 24 hours" data-validation-error-msg-required="The field start time is mandatory, please check again!" data-validation-error-msg-time="You have not given a correct time" data-validation-regexp="^([01]\d|2[0-4]):?([0-5]\d)$" data-validation-error-msg-time="Time format should be 24 hours format" value="<?php echo set_value('ch2_st_time'); ?>">
                                    <div id="infoMessage"><?php echo form_error('ch2_st_time'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>End Time</label>
                                    <input type="text" class="form-control" id="ch2_ed_time" name="ch2_ed_time" placeholder="End Time" data-validation="required time custom" data-validation-help="HH:mm in 24 hours" data-validation-error-msg-required="The field end time is mandatory, please check again!" data-validation-error-msg-time="You have not given a correct time" value="<?php echo set_value('ch2_ed_time'); ?>">
                                    <div id="infoMessage"><?php echo form_error('ch2_ed_time'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Provide your general overview of the Client, any key issues/findings/areas of importance</label>
                                    <textarea class="form-control" name="ch2_oview" id="ch2_oview" placeholder="Provide your general overview of the Client, any key issues/findings/areas of importance" rows="3" data-validation="required" data-validation-error-msg="The field general overview is mandatory, please check again!"><?php echo set_value('ch2_oview'); ?></textarea>
                                    <div id="infoMessage"><?php echo form_error('ch2_oview'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>What are your key actions from this engagement</label>
                                    <textarea class="form-control" name="ch2_goals" id="ch2_goals" placeholder="What are your key actions from this engagement" rows="3" data-validation="required" data-validation-error-msg="The field key actions is mandatory, please check again!"><?php echo set_value('ch2_goals'); ?></textarea>
                                    <div id="infoMessage"><?php echo form_error('ch2_goals'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-12">                                        
                                <label>Tick what information has been provided to the client</label>
                                <ul class="list-unstyled activity-list">
                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_1" value="1" <?php echo set_checkbox('info_list', '1'); ?>>
                                            <span>
                                                History of MWDI – Services we provide
                                                &nbsp; ( <a href="https://drive.google.com/open?id=0BxyqeL8pjMojUDZvU0daN0cxdHc" target="blank">View History of MWDI & Services we provide</a> )
                                            </span> 
                                        </label>
                                    </li>
                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_2" value="2" <?php echo set_checkbox('info_list', '2'); ?>>
                                            <span>
                                                Loan Information/Application
                                                &nbsp; ( <a href="<?php echo base_url('downloads/Business Criteria.pdf')?>" target="blank" download>View/Download Loan Information</a> &nbsp; | &nbsp; <a href="<?php echo base_url('downloads/Application.pdf')?>" target="blank" download>View/Download Loan Application</a> )
                                            </span> 
                                        </label>
                                    </li>
                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_3" value="3" <?php echo set_checkbox('info_list', '3'); ?>>
                                            <span>
                                                Contact Information for Coach
                                            </span> 
                                        </label>
                                    </li>
<!--                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_4" value="4" <?php echo set_checkbox('info_list', '4'); ?>>
                                            <span>
                                                Coaching Ethics/Contract signed and scanned to projects@mwdi.co.nz 
                                                &nbsp; ( <a href="http://coachfederation.org/about/ethics.aspx?ItemNumber=854" target="blank">View Coaching Ethics</a> &nbsp; | &nbsp; <a href="https://docs.google.com/document/d/15vlOnkqj_YngTocuQsBnag4mOra1JjQwEiyX2C_Y9Vk/edit" target="blank">View Coaching Contract</a> )
                                            </span> 
                                        </label>
                                    </li>-->
                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_5" value="5" <?php echo set_checkbox('info_list', '5'); ?>> 
                                            <span>
                                                Copy of complaints process
                                            </span> 
                                        </label>
                                    </li>
<!--                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_6" value="6" <?php echo set_checkbox('info_list', '6'); ?>>
                                            <span>
                                                Link to Business Audit/Needs Survey 
                                                &nbsp; ( <a href="http://goo.gl/forms/PywghBcHwO" target="blank">Business Audit/Needs Survey</a> )
                                            </span> 
                                        </label>
                                    </li>-->
<!--                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_7" value="7" <?php echo set_checkbox('info_list', '7'); ?>>
                                            <span>
                                                Personal and Business Questionaire
                                                &nbsp; ( <a href="https://drive.google.com/open?id=1Sg797kipK0H-9PFyJ_jhSvrkFy7flUXcSZSP_fvTkVk" target="blank">Link to Personal and Business Questionaire</a> )
                                            </span> 
                                        </label>
                                    </li>-->
                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_8" value="8" <?php echo set_checkbox('info_list', '8'); ?>>
                                            <span>
                                                <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="This is a NZ Trade and Enterprise Link and will require you to sit with the Coachee to complete or for them to work through the template until the end where they can download the Business Plan in its entirety"></i>&nbsp; 
                                                Link to Business Plan Template 
                                                &nbsp; ( <a href="http://m.business.govt.nz/starting-and-stopping/entering-a-business/before-you-start-a-business/business-plan-template-tool" target="blank">Link to Business Plan Template</a> )
                                            </span> 
                                        </label>
                                    </li>
<!--                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_9" value="9" <?php echo set_checkbox('info_list', '9'); ?>>
                                            <span>
                                                Link to Financial Plan Template
                                            </span> 
                                        </label>
                                    </li>-->
<!--                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_10" value="10" <?php echo set_checkbox('info_list', '10'); ?>>
                                            <span>
                                                Did you attempt to meet the 11 ICF Core competencies in your Client Engagement
                                                &nbsp; ( <a href="https://drive.google.com/open?id=1L3n6rV15u-TwuSDIeajrktZuxLVg6fHwfGrn024d57E" target="blank">Please Click</a> )
                                            </span> 
                                        </label>
                                    </li>-->
                                    <li style="padding: 10px 0;">
                                        <div class="col-md-3" style="padding: 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_11" value="11">
                                            <span>
                                                Upload non-disclosure agreement
                                            </span> 
                                        </label>
                                            </div>
                                        <div class="col-md-9">
                                            <input type="file" name="nd_upload" id="nd_upload" class="form-control" style="visibility: hidden;">
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-12">
                                <hr><br>
                            </div>
                            <div class="col-md-6">
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="nxt_app" id="nxt_app" value="Yes" <?php echo set_checkbox('nxt_app', 'Yes'); ?>>
                                    <span><strong>Yes</strong> - Confirmation of next appointment</span>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>When is the date confirmed for the next engagements</label>
                                    <input type="text" class="form-control" id="next_date" name="next_date" placeholder="When is the date/s confirmed for the next engagements" data-validation="date" data-validation-format="dd/mm/yyyy"  data-validation-optional="true" value="<?php echo set_value('next_date'); ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Any points for follow up from other Coaches</label>
                                    <textarea class="form-control" name="other_ch_flow" id="other_ch_flow" placeholder="Any points for follow up from other Coaches" rows="3"><?php echo set_value('other_ch_flow'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Any points for MWDI to consider</label>
                                    <textarea class="form-control" name="any_consider" id="any_consider" placeholder="Any points for MWDI to consider" rows="3"><?php echo set_value('any_consider'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="invo_sent" id="invo_sent" value="Yes" <?php echo set_checkbox('invo_sent', 'Yes'); ?>>
                                    <span><strong>Yes</strong> - Invoice sent by the 15th of the month to accountspayable@mwdi.co.nz including Kilometres and any costs incurred</span>
                                </label>
                            </div>
                            <div class="col-md-12" style="padding-top: 20px; padding-bottom: 20px;">
                                <a href="javascript: history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; Back</a> &nbsp; 
                                <button type="submit" class="btn btn-default" name="btnaction" value="frm_save" id="chf1_save">SAVE FORM &nbsp; <i class="fa fa-save"></i></button> &nbsp;
                                <button type="submit" class="btn btn-primary" name="btnaction" value="frm_submit" id="chf1_submit">SUBMIT</button>
                                <input type="hidden" name="button_type" id="button_type" value="frm_save">
                            </div> 
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<?php if ($this->session->flashdata('create_success')) { ?>
    <script>swal({type: 'success', title: 'success', text: 'The form has been successfully saved', timer: 3000});</script>
<?php } else if ($this->session->flashdata('pdf_error')) { ?>
    <script>swal({type: 'error', title: 'PDF Error', text: 'No information to print', timer: 3000});</script>
<?php } else if ($this->session->flashdata('error_msg')) { ?>
    <script>swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000});</script>
<?php } ?>
<script>
    $(document).ready(function () {
        var s1 = $('#sel_client,#intro_client').select2();
        $('#sel_client').on("select2:select", function(e) { 
            $('#sel_client').validate({ validateHiddenInputs: false });//Selected - Hide errors message
        });
        
        //----------------------------------------------------------------------
        
        $("#coach_frm_1 :input").change(function () {
            if (!$("#ch1_start_time").val()) {
               $('#ch1_start_time').val(timer());
            }
        });

        //----------------------------------------------------------------------

        $.validate({
            form: '#coach_frm_1',
            modules: 'security,date,file'
        });

        //----------------------------------------------------------------------

        $('#ch2_st_time').mask('00:00', {placeholder: "Start Time"});
        $('#ch2_ed_time').mask('00:00', {placeholder: "End Time"});

        $('#next_date').mask('00/00/0000', {placeholder: "When is the date confirmed for the next engagements DD/MM/YYYY"});
        $('#phone').mask('000000000000', {placeholder: "091234567"});
        $('#mobile').mask('000000000000', {placeholder: "0211234567"});

        //----------------------------------------------------------------------

        $('#next_date').keyup(function () {
            if (e.keyCode == 8 || e.keyCode == 46) {
                $(this).attr("data-validation-optional", "true");
            } else {
                $(this).removeAttr('data-validation-optional');
            }
        });

        //----------------------------------------------------------------------

        $('#sel_client').on('change', function () {
            var uid = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('Coach_form/check_main_form_available_p1'); ?>",
                data: {'uid': uid},
                success: function (results) {
                    var frm_id = $.parseJSON(results);
                    console.log(frm_id);
                    if (frm_id != null) {
                        location.href = '<?php echo base_url('view_engagement/5/') ?>' + frm_id.main_id;
                    } else {
                        call_user_address(uid);
                    }
                }
            });
        });

        //----------------------------------------------------------------------

        function call_user_address(uid) {
            var usr_type = '<?php echo $this->encrypt->encode('5') ?>';
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('Coach_form/load_single_user/'); ?>",
                data: {'usr_type': usr_type, 'uid': uid},
                success: function (results) {
                    var user_details = $.parseJSON(results);
                    //console.log(user_details);
                    $('#region').val(user_details.add_region);
                    $('#address').val(user_details.address);
                    $('#phone').val(user_details.phone);
                    $('#mobile').val(user_details.mobile);
                    $('#email').val(user_details.user_email);
                }
            });
        }

        //----------------------------------------------------------------------

        $('#chf1_submit').on('click', function (e) {
            e.preventDefault();
            if($('#coach_frm_1').isValid()){
                swal({
                    title: 'Do you want submit this form?',
                    confirmButtonText: 'Submit',
                    html: '<span style="color:red;">You cannot edit this form after submit</span>',
                    showLoaderOnConfirm: true,
                    showCancelButton: true
                }).then((result) => {
                    $("#button_type").val('frm_submit');
                    $("#coach_frm_1").submit();
                });
            }
        });
        
        //----------------------------------------------------------------------
        
        if($('#info_list_11').is(":checked")){
            $('#nd_upload').css({'visibility':'visible'});
        }else{
            $('#nd_upload').css({'visibility':'hidden'});
        }
        
        //----------------------------------------------------------------------
        
        $('#info_list_11').change(function (){
            if(this.checked){
                $('#nd_upload').css({'visibility':'visible','border-color':'#eaeaea'});
                $('#nd_upload').removeAttr('data-validation-optional');
                $('#nd_upload').attr({'data-validation':'required size','accept':'.pdf','data-validation-max-size':'2M','data-validation-error-msg':'Please upload non-disclosure agreement!','data-validation-error-msg-size':'Maximum upload size (2MB) exceeded, please try again!'});
            }else{
                $('#nd_upload').css({'visibility':'hidden'});
                $('#nd_upload').parent().removeClass('has-error');
                $('#nd_upload').next('span').remove();
                $('#nd_upload').attr('data-validation-optional','true');
                $('#nd_upload').val("");
            }
        });
    });
</script>
