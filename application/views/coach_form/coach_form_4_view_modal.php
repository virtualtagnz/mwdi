<?php
$ch_form3_details = (json_decode($details));
?>
<style>
    td{
        border: 1px solid #9D8526;
        padding: 5px;
    }
    tr.bg{
        background-color: #252c35;
        color: #ffffff;
    }
    .title{
        text-align: center;
    }
</style>
<div class="title">
    <h2>Client Form Engagement <?php echo $ch_form3_details->frequency ?></h2>
</div>
<?php if ($ch_form3_details) { ?>
    <table cellspacing="0" cellpadding="5" border="1">
        <tr class="bg">
            <td colspan="2">
                <h4 class="">ENGAGEMENT <?php echo $ch_form3_details->frequency ?> (Building on the Relationship)</h4>
            </td>
        </tr>
        <tr>
            <td>Client Name</td><td><?php echo $ch_form3_details->first_name . ' ' . $ch_form3_details->last_name ?></td>
        </tr>
        <tr>
            <td>Where did you meet</td><td><?php echo $ch_form3_details->chp4_f52 ?></td>
        </tr>
        <tr>
            <td>Start Time</td><td><?php echo $ch_form3_details->chp4_f53 ?></td>
        </tr>
        <tr>
            <td>End Time</td><td><?php echo $ch_form3_details->chp4_f54 ?></td>
        </tr>
        <tr>
            <td>General overview of the Client</td><td><?php echo $ch_form3_details->chp4_f55 ?></td>
        </tr>
        <tr>
            <td>Key actions from this engagement</td><td><?php echo $ch_form3_details->chp4_f56 ?></td>
        </tr>
        <tr>
            <td>Has the client filled in all required information from the last engagement session</td><td><?php echo $ch_form3_details->chp4_f57 == '1' ? 'Yes' : 'No' ?></td>
        </tr>
    </table>
    <div style="page-break-inside: avoid;"></div>
    <table cellspacing="0" cellpadding="5" border="1">
        <tr class="bg">
            <td colspan="2">
                <h4 class="">Loan Questions if applicable</h4>
            </td>
        </tr>
        <tr>
            <td>Is the client proceeding with an application, if not, please note reasons</td><td><?php echo $ch_form3_details->chp4_f58?></td>
        </tr>
        <tr>
            <td>Existing clients Would the client consider increased repayments to MWDI or looked at ways to pay off loan faster? If so how much/by when</td><td><?php echo $ch_form3_details->chp4_f59 ?></td>
        </tr>
        <tr>
            <td>What information has been provided to the client (ie Business Plan templates/Financial Plan Templates, Links to information)</td><td><?php echo $ch_form3_details->chp4_f60 ?></td>
        </tr>
        <tr>
            <td>What actions has the client taken since the last engagement</td><td><?php echo $ch_form3_details->chp4_f61 ?></td>
        </tr>
        <tr>
            <td>What challenges are still facing this client</td><td><?php echo $ch_form3_details->chp4_f62 ?></td>
        </tr>
        <tr>
            <td>What resources/support does this client require</td><td><?php echo $ch_form3_details->chp4_f63 ?></td>
        </tr>
        <tr>
            <td>What would support this client to grow and expand their business</td><td><?php echo $ch_form3_details->chp4_f64 ?></td>
        </tr>
        <tr>
            <td>Key highlights/Breakthroughs</td><td><?php echo $ch_form3_details->chp4_f65 ?></td>
        </tr>
    </table>
    <div style="page-break-inside: avoid;"></div>
    <table cellspacing="0" cellpadding="5" border="1">
        <tr class="bg">
            <td colspan="2">
                <h4 class="">Financial Literacy</h4>
            </td>
        </tr>
        <tr>
            <td>What are the key financial issues impacting the client at a personal level</td><td><?php echo $ch_form3_details->chp4_f66?></td>
        </tr>
        <tr>
            <td>What are the key financial issues impacting the client at a business level</td><td><?php echo $ch_form3_details->chp4_f67 ?></td>
        </tr>
        <tr>
            <td>What topics did you explore together</td><td><?php echo $ch_form3_details->chp4_f68 ?></td>
        </tr>
        <tr>
            <td>Email confirmation sent for next appointment</td><td><?php echo $ch_form3_details->chp4_f69 ?></td>
        </tr>
        <tr>
            <td>Date for the next engagements</td><td><?php echo $ch_form3_details->chp4_f70 ?></td>
        </tr>
        <tr>
            <td>Any points for follow up from other Coaches</td><td><?php echo $ch_form3_details->chp4_f71 ?></td>
        </tr>
        <tr>
            <td>New Clients Application form checked that all relevant details have been completed, everything is signed ready to send to MWDI for processing</td><td><?php echo $ch_form3_details->chp4_f72 ?></td>
        </tr>
        <tr>
            <td>Invoice sent by the 15th of the month</td><td><?php echo $ch_form3_details->chp4_f73 ?></td>
        </tr>
    </table>
<?php } ?>


