<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
<!--                <a href="<?php echo base_url('view_single_form/8/FALSE'); ?>" class="btn btn-primary pull-right"><i class="fa fa-print"></i> &nbsp; Print Form</a>-->
                Forms <i class="lnr lnr-chevron-right"></i> Client Form
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <form action="<?php echo base_url('create_coach_form_3'); ?>" method="post" name="coach_frm_3" id="coach_frm_3" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <h4>ENGAGEMENT 2 (Building on the Relationship)</h4>
                            </div>
                            <input type="hidden" name="ch3_start_time" id="ch3_start_time" value="<?php echo set_value('ch3_start_time'); ?>">
                            <input type="hidden" name="main_form_id" id="main_form_id" value="<?php echo set_value('main_form_id'); ?>">
                            <input type="hidden" name="sub_stat" id="sub_stat" value="">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Client Name</label>
                                    <select class="form-control" id="ch3_client" name="ch3_client" data-validation="required" data-validation-error-msg="The field select client is mandatory, please check again!">
                                        <option selected="selected" value="">Client Name</option>
                                        <?php if ($my_clients) { ?>
                                            <?php $clients = json_decode($my_clients) ?>
                                            <?php foreach ($clients as $client) { ?>
                                                <option value="<?php echo $client->client_id ?>"<?php echo set_select('ch3_client', $client->client_id); ?>><?php echo $client->first_name . ' ' . $client->last_name ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                    <div id="infoMessage"><?php echo form_error('ch3_client'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Where did you meet?</label>
                                    <input type="text" class="form-control" id="ch3_place" name="ch3_place" placeholder="Where did you meet?" data-validation="required" data-validation-error-msg="The field where did you meet is mandatory, please check again!" value="<?php echo set_value('ch3_place'); ?>">
                                    <div id="infoMessage"><?php echo form_error('ch3_place'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Start Time</label>
                                    <input type="text" class="form-control" id="ch3_st_time" name="ch3_st_time" placeholder="Start Time" data-validation="required time" data-validation-help="HH:mm in 24 hours" data-validation-error-msg-required="The field start time is mandatory, please check again!" data-validation-error-msg-time="You have not given a correct time" value="<?php echo set_value('ch3_st_time'); ?>">
                                    <div id="infoMessage"><?php echo form_error('ch3_st_time'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>End Time</label>
                                    <input type="text" class="form-control" id="ch3_ed_time" name="ch3_ed_time" placeholder="End Time" data-validation="required time" data-validation-help="HH:mm in 24 hours" data-validation-error-msg-required="The field end time is mandatory, please check again!" data-validation-error-msg-time="You have not given a correct time" value="<?php echo set_value('ch3_ed_time'); ?>">
                                    <div id="infoMessage"><?php echo form_error('ch3_ed_time'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Provide your general overview of the Client, any key issues/findings/areas of importance</label>
                                    <textarea class="form-control" name="ch3_oview" id="ch3_oview" placeholder="Provide your general overview of the Client, any key issues/findings/areas of importance" rows="3" data-validation="required" data-validation-error-msg="The field general overview is mandatory, please check again!"><?php echo set_value('ch3_oview'); ?></textarea>
                                    <div id="infoMessage"><?php echo form_error('ch3_oview'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label>Tick what information has been provided to the client</label>
                                <ul class="list-unstyled activity-list">
<!--                                    <li style="padding: 10px 0; width: 33%; float: left;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="chk_list[]" id="chk_list_1" value="1" <?php echo set_checkbox('chk_list', '1'); ?>>
                                            <span>Business Audit</span> 
                                        </label>
                                    </li>
                                    <li style="padding: 10px 0; width: 33%; float: left;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="chk_list[]" id="chk_list_1" value="2" <?php echo set_checkbox('chk_list', '1'); ?>>
                                            <span>Financial Audit</span> 
                                        </label>
                                    </li>-->
                                    <li style="padding: 10px 0; width: 33%; float: left;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="chk_list[]" id="chk_list_1" value="3" <?php echo set_checkbox('chk_list', '1'); ?>>
                                            <span>Business Plan</span> 
                                        </label>
                                    </li>
                                    <li style="padding: 10px 0; width: 33%; float: left;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="chk_list[]" id="chk_list_1" value="4" <?php echo set_checkbox('chk_list', '1'); ?>>
                                            <span>Financial Plan</span> 
                                        </label>
                                    </li>
                                    <li style="padding: 10px 0; width: 33%; float: left;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="chk_list[]" id="chk_list_1" value="5" <?php echo set_checkbox('chk_list', '1'); ?>>
                                            <span>Loan Application</span> 
                                        </label>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Please fill in relevant points found in the business/financial audit</label>
                                    <textarea class="form-control" name="rel_points" id="rel_points" placeholder="Please fill in relevant points found in the business/financial audit" rows="3" data-validation="required" data-validation-error-msg-required="The field relevant points number is mandatory, please check again!" data-validation-depends-on="chk_list[]" data-validation-depends-on-value="1,2"><?php echo set_value('rel_points'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>If they haven&#39t filled in the link or template please note why</label>
                                    <textarea class="form-control" name="note" id="note" placeholder="If they haven&#39t filled in the link or template please note why" rows="3"><?php echo set_value('note'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <hr>
                                <h4>Loan Questions if applicable</h4>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>New Clients: Does this client wish to progress with a loan application and do they require assistance to complete all the requirements</label>
                                    <select class="form-control" name="lq_1" id="lq_1">
                                        <option value="0" selected="selected">- Please Select -</option>
                                        <option value="1" <?php echo set_select('lq_1', '1'); ?>>Yes</option>
                                        <option value="2" <?php echo set_select('lq_1', '2'); ?>>No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Existing clients: Has the money invested by MWDI been in line with their original application/Business plan submitted</label>
                                    <textarea class="form-control" name="lq_2" id="lq_2" placeholder="Existing clients: Has the money invested by MWDI been in line with their original application/Business plan submitted" rows="3"><?php echo set_value('lq_2'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Existing clients: Will the client consider increasing repayments to MWDI? If so how much/by when</label>
                                    <textarea class="form-control" name="lq_3" id="lq_3" placeholder="Existing clients: Will the client consider increasing repayments to MWDI? If so how much/by when" rows="3"><?php echo set_value('lq_3'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <hr>
                                <h4>Financial Literacy</h4>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>What are the key financial issues impacting the client at a personal level</label>
                                    <textarea class="form-control" name="ft_1" id="ft_1" placeholder="What are the key financial issues impacting the client at a personal level" rows="3"><?php echo set_value('ft_1'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>What are the key financial issues impacting the client at a business level</label>
                                    <textarea class="form-control" name="ft_2" id="ft_2" placeholder="What are the key financial issues impacting the client at a business level" rows="3"><?php echo set_value('ft_2'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>What topics did you explore together</label>
                                    <textarea class="form-control" name="ft_3" id="ft_3" placeholder="What topics did you explore together" rows="3"><?php echo set_value('ft_3'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <hr>
                                <h4>Training</h4>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Has the client identified any specific training needs</label>
                                    <textarea class="form-control" name="tr_1" id="tr_1" placeholder="Has the client identified any specific training needs" rows="3"><?php echo set_value('tr_1'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Did you refer the clients onto any of our training programs</label>
                                    <textarea class="form-control" name="tr_2" id="tr_2" placeholder="Did you refer the clients onto any of our training programs" rows="3"><?php echo set_value('tr_2'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <hr>
                                <h4>Information</h4>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>What information been provided to the client (ie Business Plan templates/Financial Plan Templates, Links to information)</label>
                                    <textarea class="form-control" name="info_1" id="info_1" placeholder="What information been provided to the client (ie Business Plan templates/Financial Plan Templates, Links to information)" rows="3"><?php echo set_value('info_1'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Has the client identified any specific training needs</label>
                                    <textarea class="form-control" name="info_2" id="info_2" placeholder="Has the client identified any specific training needs" rows="3"><?php echo set_value('info_2'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Did you refer the clients onto any of our training programmes</label>
                                    <textarea class="form-control" name="info_3" id="info_3" placeholder="Did you refer the clients onto any of our training programmes" rows="3"><?php echo set_value('info_3'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <hr>
                                <h4>Business Development</h4>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>What are the key challenges facing this client</label>
                                    <textarea class="form-control" name="bd_1" id="bd_1" placeholder="What are the key challenges facing this client" rows="3"><?php echo set_value('bd_1'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>What resources/support does this client require</label>
                                    <textarea class="form-control" name="bd_2" id="bd_2" placeholder="What resources/support does this client require" rows="3"><?php echo set_value('bd_2'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>What would support this client to grow and expand their business</label>
                                    <textarea class="form-control" name="bd_3" id="bd_3" placeholder="What would support this client to grow and expand their business" rows="3"><?php echo set_value('bd_3'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Key highlights/Breakthroughs</label>
                                    <textarea class="form-control" name="bd_4" id="bd_4" placeholder="Key highlights/Breakthroughs" rows="3"><?php echo set_value('bd_4'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Did you refer the client onto any other service/organisation/website? If so who/what</label>
                                    <textarea class="form-control" name="bd_5" id="bd_5" placeholder="Did you refer the client onto any other service/organisation/website? If so who/what" rows="3"><?php echo set_value('bd_5'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Any points for follow up from other Coaches</label>
                                    <textarea class="form-control" name="bd_6" id="bd_6" placeholder="Any points for follow up from other Coaches" rows="3"><?php echo set_value('bd_6'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="ch3_nxt_app" id="ch3_nxt_app" value="Yes" <?php echo set_checkbox('ch3_nxt_app', 'Yes'); ?>>
                                    <span><strong>Yes</strong> - Confirmation of next appointment</span>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>When is the date confirmed for the next engagements</label>
                                    <input type="text" class="form-control" id="ch3_next_date" name="ch3_next_date" placeholder="When is the date/s confirmed for the next engagements" data-validation="date" data-validation-format="dd/mm/yyyy"  data-validation-optional="true" value="<?php echo set_value('ch3_next_date'); ?>">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="ch3_invo_sent" id="ch3_invo_sent" value="Yes" <?php echo set_checkbox('ch3_invo_sent', 'Yes'); ?>>
                                    <span><strong>Yes</strong> - Invoice sent by the 15th of the month to accountspayable@mwdi.co.nz including Kilometres and any costs incurred</span>
                                </label>
                            </div>
                            <div class="col-md-12" style="padding-top: 20px; padding-bottom: 20px;">
                                <a href="javascript:history.back()" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; BACK</a> &nbsp; 
                                <button type="submit" class="btn btn-default" name="btnaction" value="frm_save" id="chf3_save">SAVE FORM &nbsp; <i class="fa fa-save"></i></button> &nbsp;
                                <button type="submit" class="btn btn-primary" name="btnaction" value="frm_submit" id="chf3_submit">SUBMIT</button>
                                <input type="hidden" name="button_type" id="button_type" value="frm_save">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<?php if ($this->session->flashdata('create_success')) { ?>
    <script>swal({type: 'success', title: 'success', text: 'The form has been successfully saved', timer: 3000});</script>
<?php } else if ($this->session->flashdata('pdf_error')) { ?>
    <script>swal({type: 'error', title: 'PDF Error', text: 'No information to print', timer: 3000});</script>
<?php } else if ($this->session->flashdata('error_msg')) { ?>
    <script>swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000});</script>
<?php } ?>
<script>
    $(document).ready(function () {
        $('#ch3_client').select2();
        $('#ch3_client').on("select2:select", function(e) { 
            $('#ch3_client').validate({ validateHiddenInputs: false });//Selected - Hide errors message
        });
        
        //----------------------------------------------------------------------

        $.validate({
            form: '#coach_frm_3',
            modules: 'security,date,logic'
        });

        //----------------------------------------------------------------------
        
        $('#ch3_st_time').mask('00:00', {placeholder: "Start Time"});
        $('#ch3_ed_time').mask('00:00', {placeholder: "End Time"});

        $('#ch3_next_date').mask('00/00/0000', {placeholder: "When is the date/s confirmed for the next engagements DD/MM/YYYY"});
        
        //----------------------------------------------------------------------
        
        $('#ch3_next_date').keyup(function () {
            if (e.keyCode == 8 || e.keyCode == 46) {
                $(this).attr("data-validation-optional", "true");
            } else {
                $(this).removeAttr('data-validation-optional');
            }
        });

        //----------------------------------------------------------------------
        
        $("#coach_frm_3 :input").change(function () {
            if (!$("#ch3_start_time").val()) {
               $('#ch3_start_time').val(timer());
            }
        });

        //----------------------------------------------------------------------
        
        $('#ch3_client').on('change', function () {
            var frm_id = '<?php echo end($this->uri->segments);?>';
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('Coach_form/check_main_form_available_p2'); ?>",
                data: {'uid': $(this).val(),'form_id':frm_id},
                success: function (results) {
                    var frm_id = $.parseJSON(results);
                    console.log(frm_id);
                    if (frm_id == null) {
                        swal({title: 'No main form',
                            text: "Please fill Coach Form Engagement 1!",
                            type: 'error',
                            confirmButtonText: 'Go to my form section!',
                        }).then(function (result) {
                            location.href = '<?php echo base_url('my_forms/' . $this->encrypt->encode($this->session->userdata('user_level_id')) . '/' . $this->encrypt->encode($this->session->userdata('role'))) ?>'
                        });
                    } else if (frm_id.form_2_id) {
                        location.href = '<?php echo base_url('view_edit_forms/8/') ?>'+frm_id.form_2_id;
                    } else {
                        $('#main_form_id').val(frm_id.main_id);
                    }
                }
            });
        });
        
        //----------------------------------------------------------------------
        
        $('#chf3_submit').on('click', function (e) {
            e.preventDefault();
            if($('#coach_frm_3').isValid()){
                swal({
                    title: 'Do you want submit this form?',
                    confirmButtonText: 'Submit',
                    html: '<span style="color:red;">You cannot edit this form after submit</span>',
                    showLoaderOnConfirm: true,
                    showCancelButton: true
                }).then((result) => {
                    $("#button_type").val('frm_submit');
                    $("#coach_frm_3").submit();
                });
            }
        });

    });
</script>
