<?php
$frm_id = end($this->uri->segments);
$secondLastKey = count($this->uri->segment_array())-1;
$frm_type = $this->uri->segment($secondLastKey);
?>
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <a href="<?php echo base_url('view_single_form/' . $frm_type . '/' . $frm_id); ?>" class="btn btn-primary pull-right"><i class="fa fa-print"></i> &nbsp; Print Form</a>
                Forms <i class="lnr lnr-chevron-right"></i> Client Form
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <form action="<?php echo base_url('edit_coach_form_5/' . end($this->uri->segments)); ?>" method="post" name="coach_frm_5_edit" id="coach_frm_5_edit" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <h4>FINAL ENGAGEMENT (Final Sponsored Session – Follow Up)</h4>
                            </div>
                            <?php //var_dump($details)?>
                            <input type="hidden" name="start_time" id="start_time" value="<?php echo set_value('start_time'); ?>">
                            <input type="hidden" name="main_form_id" id="main_form_id" value="<?php echo set_value('main_form_id'); ?>">
                            <input type="hidden" name="sub_stat" id="sub_stat" value="<?php echo $details->is_submit?>">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="ch3_place" value="<?php echo $details->first_name . ' ' . $details->last_name; ?>" readonly="true">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Where did you meet?</label>
                                    <input type="text" class="form-control" id="ch5_place" name="ch5_place" placeholder="Where did you meet?" data-validation="required" data-validation-error-msg="The field where did you meet is mandatory, please check again!" value="<?php echo $details->chp5_f75; ?>">
                                    <div id="infoMessage"><?php echo form_error('ch5_place'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Start Time</label>
                                    <input type="text" class="form-control" id="ch5_st_time" name="ch5_st_time" placeholder="Start Time" data-validation="required time" data-validation-help="HH:mm" data-validation-error-msg-required="The field start time is mandatory, please check again!" data-validation-error-msg-time="You have not given a correct time" value="<?php echo $details->chp5_f77; ?>">
                                    <div id="infoMessage"><?php echo form_error('ch5_st_time'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>End Time</label>
                                    <input type="text" class="form-control" id="ch5_ed_time" name="ch5_ed_time" placeholder="End Time" data-validation="required time" data-validation-error-msg-required="The field end time is mandatory, please check again!" data-validation-error-msg-time="You have not given a correct time" value="<?php echo $details->chp5_f78; ?>">
                                    <div id="infoMessage"><?php echo form_error('ch5_ed_time'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Provide your general overview of the Client, any key issues/findings/areas of importance</label>
                                    <textarea class="form-control" name="ch5_oview" id="ch5_oview" placeholder="Provide your general overview of the Client, any key issues/findings/areas of importance" rows="3" data-validation="required" data-validation-error-msg="The field general overview is mandatory, please check again!"><?php echo $details->chp5_f79; ?></textarea>
                                    <div id="infoMessage"><?php echo form_error('ch5_oview'); ?></div>
                                </div>
                            </div>
                            <?php $selected_ck_box_arr = $details->chp5_f80 != '' ? explode(",", $details->chp5_f80) : array(); ?>
                            <div class="col-md-12">
                                <label>Has the client completed any of the following</label>
                                <ul class="list-unstyled activity-list">
                                    <?php if(in_array('1', $selected_ck_box_arr)):?>
                                    <li style="padding: 10px 0; width: 33%; float: left;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="completed_list[]" id="completed_list_1" value="1" <?php echo in_array('1', $selected_ck_box_arr) ? 'checked' : ''; ?>>
                                            <span>Business Audit</span> 
                                        </label>
                                    </li>
                                    <?php endif;?>
                                    <?php if(in_array('2', $selected_ck_box_arr)):?>
                                    <li style="padding: 10px 0; width: 33%; float: left;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="completed_list[]" id="completed_list_1" value="2" <?php echo in_array('2', $selected_ck_box_arr) ? 'checked' : ''; ?>>
                                            <span>Financial Audit</span> 
                                        </label>
                                    </li>
                                    <?php endif;?>
                                    <li style="padding: 10px 0; width: 33%; float: left;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="completed_list[]" id="completed_list_1" value="3" <?php echo in_array('3', $selected_ck_box_arr) ? 'checked' : ''; ?>>
                                            <span>Business Plan</span> 
                                        </label>
                                    </li>
                                    <li style="padding: 10px 0; width: 33%; float: left;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="completed_list[]" id="completed_list_1" value="4" <?php echo in_array('4', $selected_ck_box_arr) ? 'checked' : ''; ?>>
                                            <span>Financial Plan</span> 
                                        </label>
                                    </li>
                                    <li style="padding: 10px 0; width: 33%; float: left;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="completed_list[]" id="completed_list_1" value="5" <?php echo in_array('5', $selected_ck_box_arr) ? 'checked' : ''; ?>>
                                            <span>Loan Application</span> 
                                        </label>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Has the client engaged in any training or development</label>
                                    <textarea class="form-control" name="tr_dev" id="tr_dev" placeholder="Has the client engaged in any training or development" rows="3"><?php echo $details->chp5_f81; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ny points for follow up needed from other Coaches</label>
                                    <textarea class="form-control" name="follow_up" id="follow_up" placeholder="Any points for follow up needed from other Coaches" rows="3"><?php echo $details->chp5_f82; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Does the client wish to engage you privately for ongoing Coaching services</label>
                                    <select class="form-control" name="sel_list" id="sel_list">
                                        <option selected="selected">- Please Select -</option>
                                        <option value="1" <?php echo $details->chp5_f83 == '1' ? 'selected' : ''; ?>>Yes</option>
                                        <option value="2" <?php echo $details->chp5_f83 == '2' ? 'selected' : ''; ?>>No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>If Yes, please indicate the nature of the engagement moving forward (including contracted rates/potential dates)</label>
                                    <textarea class="form-control" name="eng_nature" id="eng_nature" placeholder="If Yes, please indicate the nature of the engagement moving forward (including contracted rates/potential dates)" rows="3"><?php echo $details->chp5_f84; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Any other points/concerns/reflections</label>
                                    <textarea class="form-control" name="other_pnts" id="other_pnts" placeholder="Any other points/concerns/reflections" rows="3"><?php echo $details->chp5_f85; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>If required have you provided a closing process for the client</label>
                                    <select class="form-control" id="sel_list2" name="sel_list2">
                                        <option elected="selected">- Please Select -</option>
                                        <option value="1" <?php echo $details->chp5_f86 == '1' ? 'selected' : ''; ?>>Yes</option>
                                        <option value="2" <?php echo $details->chp5_f86 == '2' ? 'selected' : ''; ?>>No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="ch5_invo_sent" id="ch5_invo_sent" value="Yes" <?php echo $details->chp5_f87 == 'Yes' ? 'checked' : ''; ?>>
                                    <span><strong>Yes</strong> - Invoice sent by the 15th of the month to accountspayable@mwdi.co.nz including Kilometres and any costs incurred</span>
                                </label>
                            </div>
                            <div class="col-md-12" style="padding-top: 20px; padding-bottom: 20px;">
                                <a href="javascript:history.back()" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; BACK</a> &nbsp; 
                                <button type="submit" class="btn btn-default" name="btnaction" value="frm_edit" id="chf5_edit">SAVE FORM &nbsp; <i class="fa fa-save"></i></button> &nbsp;
                                <?php if($details->is_submit=='N'){?>
                                <button type="submit" class="btn btn-primary" name="btnaction" value="frm_submit" id="chf5_edit_submit">SUBMIT</button>
                                <?php }?>
                                <input type="hidden" name="button_type" id="button_type" value="frm_edit">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<?php if ($this->session->flashdata('create_success')) { ?>
    <script>swal({type: 'success', title: 'success', text: 'The form has been successfully saved', timer: 3000});</script>
<?php } else if ($this->session->flashdata('error_msg')) { ?>
    <script>swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000});</script>
<?php } ?>
<script>
    $(document).ready(function () {
        $('#ch5_client').select2();
        
        //----------------------------------------------------------------------
        
        $.validate({
            form: '#coach_frm_5_edit',
            modules: 'security,date'
        });
        
        //----------------------------------------------------------------------

        $('#ch5_st_time').mask('00:00', {placeholder: "Start Time"});
        $('#ch5_ed_time').mask('00:00', {placeholder: "End Time"});

        $('#ch5_next_date').mask('00/00/0000', {placeholder: "When is the date confirmed for the next engagements DD/MM/YYYY"});
        
        //----------------------------------------------------------------------
        
        $('#ch5_next_date').keyup(function () {
            if (e.keyCode == 8 || e.keyCode == 46) {
                $(this).attr("data-validation-optional", "true");
            } else {
                $(this).removeAttr('data-validation-optional');
            }
        });

        //----------------------------------------------------------------------
        
//        $('#ch5_submit').on('click', function () {
//            swal({
//                title: 'Do you want submit this form?',
//                confirmButtonText: 'Submit',
//                html: '<span style="color:red;">You cannot edit this form after submit</span>',
//                showLoaderOnConfirm: true,
//                showCancelButton: true,
//                preConfirm: function () {
//                    return new Promise(function (resolve) {
//                        $.ajax({
//                            type: 'POST',
//                            url: '<?php echo base_url('submit_form/'.$frm_type.'/'.$frm_id); ?>',
//                        })
//                                .done(function (response) {
//                                    var rslt = JSON.parse(response);
//                                    console.log(rslt);
//                                    if (rslt) {
//                                        swal({type: 'success', title: 'success', text: 'The form has been submited', timer: 3000}).catch(function (timeout) { });
//                                        window.setTimeout(function () {
//                                            location.href = '<?php echo base_url('submited_coach_form') ?>';
//                                        }, 3000);
//                                    } else {
//                                        resolve()
//                                        //swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000}) 
//                                    }
//                                })
//                                .fail(function () {
//                                    swal('Oops...', 'Something went wrong !', 'error');
//                                });
//                    })
//                }
//            }).catch(swal.noop);
//        });

        //----------------------------------------------------------------------
        
        $('#chf5_edit_submit').on('click', function (e) {
            e.preventDefault();
            if($('#coach_frm_5_edit').isValid()){
                swal({
                    title: 'Do you want submit this form?',
                    confirmButtonText: 'Submit',
                    html: '<span style="color:red;">You cannot edit this form after submit</span>',
                    showLoaderOnConfirm: true,
                    showCancelButton: true
                }).then((result) => {
                    $("#button_type").val('frm_submit');
                    $("#coach_frm_5_edit").submit();
                });
            }
        });

    });
</script>
