<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <a href="#x" class="btn btn-primary pull-right"><i class="fa fa-print"></i> &nbsp; Print Form</a>
                Forms <i class="lnr lnr-chevron-right"></i> Create <i class="lnr lnr-chevron-right"></i> Client Form
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <form action="<?php echo base_url('create_coach_form_2/' . end($this->uri->segments)); ?>" method="post" id="coach_frm_2" name="coach_frm_2" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <h4>ENGAGEMENT 1 (Whanaungatanga/Relationship Building)</h4>
                            </div>
                            <input type="hidden" name="start_time" id="start_time" value="<?php echo set_value('start_time'); ?>">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="ch2_place" name="ch2_place" placeholder="Where did you meet?" data-validation="required" data-validation-error-msg="The field where did you meet is mandatory, please check again!" value="<?php echo $details->chp2_f1; ?>">
                                    <div id="infoMessage"><?php echo form_error('ch2_place'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="ch2_st_time" name="ch2_st_time" placeholder="Start Time" data-validation="required time" data-validation-help="HH:mm" data-validation-error-msg-required="The field start time is mandatory, please check again!" data-validation-error-msg-time="You have not given a correct time" value="<?php echo $details->chp2_f2; ?>">
                                    <div id="infoMessage"><?php echo form_error('ch2_st_time'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="ch2_ed_time" name="ch2_ed_time" placeholder="End Time" data-validation="required time" data-validation-error-msg-required="The field end time is mandatory, please check again!" data-validation-error-msg-time="You have not given a correct time" value="<?php echo $details->chp2_f3; ?>">
                                    <div id="infoMessage"><?php echo form_error('ch2_ed_time'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" name="ch2_oview" id="ch2_oview" placeholder="Provide your general overview of the Client, any key issues/findings/areas of importance" rows="3" data-validation="required" data-validation-error-msg="The field general overview is mandatory, please check again!"><?php echo $details->chp2_f4; ?></textarea>
                                    <div id="infoMessage"><?php echo form_error('ch2_oview'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" name="ch2_goals" id="ch2_goals" placeholder="What are your key actions from this engagement" rows="3" data-validation="required" data-validation-error-msg="The field key actions is mandatory, please check again!"><?php echo $details->chp2_f5; ?></textarea>
                                    <div id="infoMessage"><?php echo form_error('ch2_goals'); ?></div>
                                </div>
                            </div>
                            <?php $selected_ck_box_arr = $details->chp2_f6!=''?explode(",", $details->chp2_f6):array();?>
                            <div class="col-md-12">                                        
                                <label>Tick what information has been provided to the client</label>
                                <ul class="list-unstyled activity-list">
                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_1" value="1" <?php echo in_array('1',$selected_ck_box_arr)?'checked':''; ?>>
                                            <span>
                                                History of MWDI – Services we provide
                                                &nbsp; ( <a href="">View History of MWDI & Services we provide</a> )
                                            </span> 
                                        </label>
                                    </li>
                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_2" value="2" <?php echo in_array('2',$selected_ck_box_arr)?'checked':''; ?>>
                                            <span>
                                                Loan Information/Application
                                                &nbsp; ( <a href="">View Loan Information</a> &nbsp; | &nbsp; <a href="">View Loan Application</a> )
                                            </span> 
                                        </label>
                                    </li>
                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_3" value="3" <?php echo in_array('3',$selected_ck_box_arr)?'checked':''; ?>>
                                            <span>
                                                Contact Information for Coach
                                            </span> 
                                        </label>
                                    </li>
                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_4" value="4" <?php echo set_checkbox('info_list', '4'); ?>>
                                            <span>
                                                Coaching Ethics/Contract signed and scanned to projects@mwdi.co.nz 
                                                &nbsp; ( <a href="">View Coaching Ethics</a> &nbsp; | &nbsp; <a href="">View Coaching Contract</a> )
                                            </span> 
                                        </label>
                                    </li>
                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_5" value="5" <?php echo in_array('5',$selected_ck_box_arr)?'checked':''; ?>> 
                                            <span>
                                                Copy of complaints process
                                            </span> 
                                        </label>
                                    </li>
                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_6" value="6" <?php echo in_array('6',$selected_ck_box_arr)?'checked':''; ?>>
                                            <span>
                                                Link to Business Audit/Needs Survey 
                                                &nbsp; ( <a href="">Business Audit/Needs Survey</a> )
                                            </span> 
                                        </label>
                                    </li>
                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_7" value="7" <?php echo in_array('7',$selected_ck_box_arr)?'checked':''; ?>>
                                            <span>
                                                Personal and Business Questionaire
                                                &nbsp; ( <a href="">Link to Personal and Business Questionaire</a> )
                                            </span> 
                                        </label>
                                    </li>
                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_8" value="8" <?php echo in_array('8',$selected_ck_box_arr)?'checked':''; ?>>
                                            <span>
                                                <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="This is a NZ Trade and Enterprise Link and will require you to sit with the Coachee to complete or for them to work through the template until the end where they can download the Business Plan in its entirety"></i>&nbsp; 
                                                Link to Business Plan Template 
                                                &nbsp; ( <a href="">Link to Business Plan Template</a> )
                                            </span> 
                                        </label>
                                    </li>
                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_9" value="9" <?php echo in_array('9',$selected_ck_box_arr)?'checked':''; ?>>
                                            <span>
                                                Link to Financial Plan Template
                                            </span> 
                                        </label>
                                    </li>
                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_10" value="10" <?php echo in_array('10',$selected_ck_box_arr)?'checked':''; ?>>
                                            <span>
                                                Did you attempt to meet the 11 ICF Core competencies in your Client Engagement
                                                &nbsp; ( <a href="">Please Click</a> )
                                            </span> 
                                        </label>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-12">
                                <hr><br>
                            </div>
                            <div class="col-md-6">
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="nxt_app" id="nxt_app" value="Yes" <?php echo $details->chp2_f7?'checked':''; ?>>
                                    <span><strong>Yes</strong> - Confirmation of next appointment</span>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="next_date" name="next_date" placeholder="When is the date confirmed for the next engagements" data-validation="date" data-validation-format="dd/mm/yyyy"  data-validation-optional="true" value="<?php echo $details->chp2_f8=='0000-00-00'?'':$details->chp2_f8; ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" name="other_ch_flow" id="other_ch_flow" placeholder="Any points for follow up from other Coaches" rows="3"><?php echo $details->chp2_f9; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" name="any_consider" id="any_consider" placeholder="Any points for MWDI to consider" rows="3"><?php echo $details->chp2_f10; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="invo_sent" id="invo_sent" value="Yes" <?php echo $details->chp2_f11?'checked':''; ?>>
                                    <span><strong>Yes</strong> - Invoice sent by the 15th of the month to accountspayable@mwdi.co.nz including Kilometres and any costs incurred</span>
                                </label>
                            </div>
                            <div class="col-md-12" style="padding-top: 20px; padding-bottom: 20px;">
                                <a href="javascript: history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; Back</a> &nbsp; 
                                <button type="submit" class="btn btn-default">EDIT FORM &nbsp; <i class="fa fa-save"></i></button> &nbsp;
                                <button type="button" class="btn btn-primary">SUBMIT</button>
                            </div> 
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script>
    $(document).ready(function () {
        $.validate({
            form: '#coach_frm_2',
            modules: 'security,date'
        });

        $('#ch2_st_time').mask('00:00', {placeholder: "Start Time"});
        $('#ch2_ed_time').mask('00:00', {placeholder: "End Time"});

        $('#next_date').mask('00/00/0000', {placeholder: "When is the date/s confirmed for the next engagements DD/MM/YYYY"});

        $('#ch2_place').change(function () {
            $('#start_time').val(new Date());
        });

        $('#next_date').keyup(function () {
            if (e.keyCode == 8 || e.keyCode == 46) {
                $(this).attr("data-validation-optional", "true");
            } else {
                $(this).removeAttr('data-validation-optional');
            }
        });
    });
</script>