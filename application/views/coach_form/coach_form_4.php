<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
<!--                <a href="<?php echo base_url('view_single_form/9/FALSE'); ?>" class="btn btn-primary pull-right"><i class="fa fa-print"></i> &nbsp; Print Form</a>-->
                Forms <i class="lnr lnr-chevron-right"></i> Client Form
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <form action="<?php echo base_url('create_coach_form_4'); ?>" method="post" name="coach_frm_4" id="coach_frm_4" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <h4>ENGAGEMENT <span id="freq"></span> (Building on the Relationship)</h4>
                            </div>
                            <input type="hidden" name="ch4_start_time" id="ch4_start_time" value="<?php echo set_value('ch4_start_time'); ?>">
                            <input type="hidden" name="main_form_id" id="main_form_id" value="<?php echo set_value('main_form_id'); ?>">
                            <input type="hidden" name="sub_stat" id="sub_stat" value="">
                            <input type="hidden" name="eng_freq" id="eng_freq" value="">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Client Name</label>
                                    <select class="form-control" id="ch4_client" name="ch4_client" data-validation="required" data-validation-error-msg="The field select client is mandatory, please check again!">
                                        <option selected="selected" value="">Client Name</option>
                                        <?php if ($my_clients) { ?>
                                            <?php $clients = json_decode($my_clients) ?>
                                            <?php foreach ($clients as $client) { ?>
                                                <option value="<?php echo $client->client_id ?>"<?php echo set_select('ch4_client', $client->client_id); ?>><?php echo $client->first_name . ' ' . $client->last_name ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                    <div id="infoMessage"><?php echo form_error('ch4_client'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Where did you meet?</label>
                                    <input type="text" class="form-control" id="ch4_place" name="ch4_place" placeholder="Where did you meet?" data-validation="required" data-validation-error-msg="The field where did you meet is mandatory, please check again!" value="<?php echo set_value('ch4_place'); ?>">
                                    <div id="infoMessage"><?php echo form_error('ch4_place'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Start Time</label>
                                    <input type="text" class="form-control" id="ch4_st_time" name="ch4_st_time" placeholder="Start Time" data-validation="required time" data-validation-help="HH:mm" data-validation-error-msg-required="The field start time is mandatory, please check again!" data-validation-error-msg-time="You have not given a correct time" value="<?php echo set_value('ch4_st_time'); ?>">
                                    <div id="infoMessage"><?php echo form_error('ch4_st_time'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">End Time</label>
                                    <input type="text" class="form-control" id="ch4_ed_time" name="ch4_ed_time" placeholder="End Time" data-validation="required time" data-validation-error-msg-required="The field end time is mandatory, please check again!" data-validation-error-msg-time="You have not given a correct time" value="<?php echo set_value('ch4_ed_time'); ?>">
                                    <div id="infoMessage"><?php echo form_error('ch4_ed_time'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Provide your general overview of the Client, any key issues/findings/areas of importance</label>
                                    <textarea class="form-control" name="ch4_oview" id="ch4_oview" placeholder="Provide your general overview of the Client, any key issues/findings/areas of importance" rows="3" data-validation="required" data-validation-error-msg="The field general overview is mandatory, please check again!"><?php echo set_value('ch4_oview'); ?></textarea>
                                    <div id="infoMessage"><?php echo form_error('ch4_oview'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">What are your key actions from this engagement</label>
                                    <textarea class="form-control" name="ch4_actions" id="ch4_actions" placeholder="What are your key actions from this engagement" rows="3"><?php echo set_value('ch4_actions'); ?></textarea>
                                    <div id="infoMessage"><?php echo form_error('ch4_actions'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Has the client filled in all required information from the last engagement session</label>
                                    <select class="form-control" id="lst_eng" name="lst_eng">
                                        <option value="" selected="selected">- Please Select -</option>
                                        <option value="1" <?php echo set_select('lst_eng', '1'); ?>>Yes</option>
                                        <option value="2" <?php echo set_select('lst_eng', '2'); ?>>No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <hr>
                                <h4>Loan Questions if applicable</h4>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">New clients: Is the client proceeding with an application, if not, please note reasons</label>
                                    <textarea class="form-control" name="ch4_lq_1" id="ch4_lq_1" placeholder="New clients: Is the client proceeding with an application, if not, please note reasons" rows="3"><?php echo set_value('ch4_lq_1'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Existing clients Would the client consider increased repayments to MWDI or looked at ways to pay off loan faster? If so how much/by when</label>
                                    <textarea class="form-control" name="ch4_lq_2" id="ch4_lq_2" placeholder="Existing clients Would the client consider increased repayments to MWDI or looked at ways to pay off loan faster? If so how much/by when" rows="3"><?php echo set_value('ch4_lq_2'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">What information has been provided to the client (ie Business Plan templates/Financial Plan Templates, Links to information)</label>
                                    <textarea class="form-control" name="ch4_lq_3" id="ch4_lq_3" placeholder="What information has been provided to the client (ie Business Plan templates/Financial Plan Templates, Links to information)" rows="3"><?php echo set_value('ch4_lq_3'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">What actions has the client taken since the last engagement</label>
                                    <textarea class="form-control" name="ch4_lq_4" id="ch4_lq_4" placeholder="What actions has the client taken since the last engagement" rows="3"><?php echo set_value('ch4_lq_4'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">What challenges are still facing this client</label>
                                    <textarea class="form-control" name="ch4_lq_5" id="ch4_lq_5" placeholder="What challenges are still facing this client" rows="3"><?php echo set_value('ch4_lq_5'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">What resources/support does this client require</label>
                                    <textarea class="form-control" name="ch4_lq_6" id="ch4_lq_6" placeholder="What resources/support does this client require" rows="3"><?php echo set_value('ch4_lq_6'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">What would support this client to grow and expand their business</label>
                                    <textarea class="form-control" name="ch4_lq_7" id="ch4_lq_7" placeholder="What would support this client to grow and expand their business" rows="3"><?php echo set_value('ch4_lq_7'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Key highlights/Breakthroughs</label>
                                    <textarea class="form-control" name="ch4_lq_8" id="ch4_lq_8" placeholder="Key highlights/Breakthroughs" rows="3"><?php echo set_value('ch4_lq_8'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <hr>
                                <h4>Financial Literacy</h4>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">What are the key financial issues impacting the client at a personal level</label>
                                    <textarea class="form-control" name="ch4_ft_1" id="ch4_ft_1" placeholder="What are the key financial issues impacting the client at a personal level" rows="3"><?php echo set_value('ch4_ft_1'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">What are the key financial issues impacting the client at a business level</label>
                                    <textarea class="form-control" name="ch4_ft_2" id="ch4_ft_2" placeholder="What are the key financial issues impacting the client at a business level" rows="3"><?php echo set_value('ch4_ft_2'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">What topics did you explore together</label>
                                    <textarea class="form-control" name="ch4_ft_3" id="ch4_ft_3" placeholder="What topics did you explore together" rows="3"><?php echo set_value('ch4_ft_3'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="ch4_nxt_app" id="ch4_nxt_app" value="Yes" <?php echo set_checkbox('ch4_nxt_app', 'Yes'); ?>>
                                    <span><strong>Yes</strong> - Email confirmation sent for next appointment</span>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">When is the date confirmed for the next engagements</label>
                                    <input type="text" class="form-control" id="ch4_next_date" name="ch4_next_date" placeholder="When is the date/s confirmed for the next engagements" data-validation="date" data-validation-format="dd/mm/yyyy"  data-validation-optional="true" value="<?php echo set_value('ch3_next_date'); ?>">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Any points for follow up from other Coaches</label>
                                    <textarea class="form-control" name="ch4_flw_other" id="ch4_flw_other" placeholder="Any points for follow up from other Coaches" rows="3"><?php echo set_value('ch4_flw_other'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="ch4_app_chk" id="ch4_app_chk" value="Yes" <?php echo set_checkbox('ch4_app_chk', 'Yes'); ?>>
                                    <span><strong>Yes</strong> - New Clients Application form checked that all relevant details have been completed, everything is signed ready to send to MWDI for processing</span>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="ch3_invo_sent" id="ch4_nxt_app" value="Yes" <?php echo set_checkbox('ch3_invo_sent', 'Yes'); ?>>
                                    <span><strong>Yes</strong> - Invoice sent by the 15th of the month to accountspayable@mwdi.co.nz including Kilometres and any costs incurred</span>
                                </label>
                            </div>
                            <div class="col-md-12" style="padding-top: 20px; padding-bottom: 20px;">
                                <a href="javascript:history.back()" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; BACK</a> &nbsp; 
                                <button type="submit" class="btn btn-default" name="btnaction" value="frm_save" id="chf4_save">SAVE FORM &nbsp; <i class="fa fa-save"></i></button> &nbsp;
                                <button type="submit" class="btn btn-primary" name="btnaction" value="frm_submit" id="chf4_submit">SUBMIT</button>
                                <input type="hidden" name="button_type" id="button_type" value="frm_save">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<?php if ($this->session->flashdata('create_success')) { ?>
    <script>swal({type: 'success', title: 'success', text: 'The form has been successfully saved', timer: 3000});</script>
<?php } else if ($this->session->flashdata('pdf_error')) { ?>
    <script>swal({type: 'error', title: 'PDF Error', text: 'No information to print', timer: 3000});</script>
<?php } else if ($this->session->flashdata('error_msg')) { ?>
    <script>swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000});</script>
<?php } ?>
<script>
    $(document).ready(function () {
        $('#ch4_client').select2();
        $('#ch4_client').on("select2:select", function(e) { 
            $('#ch4_client').validate({ validateHiddenInputs: false });//Selected - Hide errors message
        });

        //----------------------------------------------------------------------
        
        $.validate({
            form: '#coach_frm_4',
            modules: 'security,date'
        });
        
        //----------------------------------------------------------------------
        
        $("#coach_frm_4 :input").change(function () {
            if (!$("#ch4_start_time").val()) {
               $('#ch4_start_time').val(timer());
            }
        });

        //----------------------------------------------------------------------
        
        $('#ch4_st_time').mask('00:00', {placeholder: "Start Time"});
        $('#ch4_ed_time').mask('00:00', {placeholder: "End Time"});

        $('#ch4_next_date').mask('00/00/0000', {placeholder: "When is the date/s confirmed for the next engagements DD/MM/YYYY"});

        //----------------------------------------------------------------------
        
        $('#ch4_next_date').keyup(function () {
            if (e.keyCode == 8 || e.keyCode == 46) {
                $(this).attr("data-validation-optional", "true");
            } else {
                $(this).removeAttr('data-validation-optional');
            }
        });

        //----------------------------------------------------------------------
        
        $('#ch4_client').on('change', function () {
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('Coach_form/check_main_form_available_p3'); ?>",
                data: {'uid': $(this).val()},
                success: function (results) {
                    var frm_obj = $.parseJSON(results);
                    console.log(frm_obj);
                    if (frm_obj.main_id == null) {
                        swal({title: 'No main form',
                            text: "Please fill Coach Form Engagement 1!",
                            type: 'error',
                            confirmButtonText: 'Go to my form section!',
                        }).then(function (result) {
                            location.href = '<?php echo base_url('my_forms/' . $this->encrypt->encode($this->session->userdata('user_level_id')) . '/' . $this->encrypt->encode($this->session->userdata('role'))) ?>'
                        });
                    } else if (frm_obj.form_3_id && frm_obj.submit_stat=='N') {
                        location.href = '<?php echo base_url('view_engagement/9/') ?>'+frm_obj.form_3_id;
                    }  else {
                        $('#main_form_id').val(frm_obj.main_id);
                        if(frm_obj.cnt == 0){
                            $('#freq').text('3')
                            $('#eng_freq').val('3')
                        }else{
                            $('#freq').text(3+parseInt(frm_obj.cnt));
                            $('#eng_freq').val(3+parseInt(frm_obj.cnt))
                        }
                    }
                }
            });
        });
        
        //----------------------------------------------------------------------
        
        $('#chf4_submit').on('click', function (e) {
            e.preventDefault();
            if($('#coach_frm_4').isValid()){
                swal({
                    title: 'Do you want submit this form?',
                    confirmButtonText: 'Submit',
                    html: '<span style="color:red;">You cannot edit this form after submit</span>',
                    showLoaderOnConfirm: true,
                    showCancelButton: true
                }).then((result) => {
                    $("#button_type").val('frm_submit');
                    $("#coach_frm_4").submit();
                });
            }
        });

    });
</script>