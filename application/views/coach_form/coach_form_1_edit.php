<?php
$frm_id = end($this->uri->segments);
$secondLastKey = count($this->uri->segment_array())-1;
$frm_type = $this->uri->segment($secondLastKey);
?>
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <a href="<?php echo base_url('view_single_form/' . $frm_type . '/' . $frm_id); ?>" class="btn btn-primary pull-right"><i class="fa fa-print"></i> &nbsp; Print Form</a>
                Forms <i class="lnr lnr-chevron-right"></i> Edit <i class="lnr lnr-chevron-right"></i> Client Form
            </h3>
            <?php //var_dump($details); ?>
            <form action="<?php echo base_url('edit_coach_form_1/'.end($this->uri->segments)) ?>" id="edit_coach_frm_1" name="edit_coach_frm_1" method="post" enctype="multipart/form-data">
                <div class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <h4>EDIT CLIENT INFORMATION</h4>
                            </div>
                            <input type="hidden" name="start_time" id="start_time" value="">
                            <input type="hidden" name="sub_stat" id="sub_stat" value="<?php echo $details->is_submit?>">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Client Name</label>
                                    <input type="text" class="form-control" id="sel_client" name="sel_client" value="<?php echo $details->first_name . ' ' . $details->last_name; ?>" readonly="true">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>New/Existing Client</label>
                                    <select class="form-control" id="sel_type" name="sel_type" data-validation="required" data-validation-error-msg="The field new/existing Client is mandatory, please check again!">
                                        <option value="" selected="selected">New/Existing Client</option>
                                        <option value="1" <?php echo $details->chf_cl_type == '1' ? 'selected' : ''; ?>>New</option>
                                        <option value="2" <?php echo $details->chf_cl_type == '2' ? 'selected' : ''; ?>>Existing</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Region of Delivery</label>
                                    <input type="text" class="form-control" id="region" name="region" placeholder="Region of Delivery" data-validation="required" data-validation-error-msg="The field region of delivery is mandatory, please check again!" value="<?php echo $details->chf_reg; ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Referred by</label>
                                    <input type="text" class="form-control" id="referred" name="referred" placeholder="Referred by" value="<?php echo $details->chf_ref; ?>" data-validation="required" data-validation-error-msg="The field referred by is mandatory, please check again!">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <?php if($details->chf_ref_client){?>
                                <div class="form-group">
                                    <label>Name of Client Referred</label>
                                    <select class="form-control" id="intro_client" name="intro_client" data-validation="required" data-validation-error-msg="The field name of client referred is mandatory, please check again!">
                                        <option value="" selected="selected">Name of Client Referred</option>
                                        <?php if ($my_all_clients) { ?>
                                            <?php $clients = json_decode($my_all_clients) ?>
                                            <?php foreach ($clients as $client) { ?>
                                                <option value="<?php echo $client->client_id ?>"<?php echo $client->client_id == $details->chf_ref_client ? 'selected' : '' ?> ><?php echo $client->first_name . ' ' . $client->last_name ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                                <?php }?>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Address</label>
                                    <textarea class="form-control" name="address" id="address" placeholder="Address" rows="3" data-validation="required" data-validation-error-msg="The field address is mandatory, please check again!"><?php echo $details->chf_add; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Purpose of Visit</label>
                                    <i class="fa fa-info-circle pull-right" style="margin-top: 25px;" data-toggle="tooltip" data-placement="top" title="Please write down the key intention of this visit, indicate if this is the first, second or third visit and what you hope to achieve in the visit with the Coachee."></i>
                                    <textarea class="form-control" name="purpose" id="purpose" placeholder="Purpose of Visit" rows="3" style="width: 96%;" data-validation="required" data-validation-error-msg="The field purpose of visit is mandatory, please check again!"><?php echo $details->chf_purpose; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <input type="text" class="form-control phone" id="phone" name="phone" placeholder="Phone Number" maxlength="20" data-validation="required" data-validation-error-msg="The field phone number is mandatory, please check again!" value="<?php echo $details->chf_phn; ?>">
                                </div>
                            </div>
                            <?php if($details->chf_nxt_contact){?>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Emergency Contact Person</label>
                                    <input type="text" class="form-control" id="nxt_kin" name="nxt_kin" placeholder="Emergency Contact Person" value="<?php echo $details->chf_nxt_contact; ?>">
                                </div>
                            </div>
                            <?php }?>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Mobile Number</label>
                                    <input type="text" class="form-control phone" id="mobile" name="mobile" placeholder="Mobile Number" maxlength="20" value="<?php echo $details->chf_nxt_phn; ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" data-validation="required" data-validation-error-msg="The field email is mandatory, please check again!" value="<?php echo $details->chf_email; ?>">
                                </div>
                            </div>
<!--                            <div class="col-md-12" style="padding-top: 20px; padding-bottom: 20px;">
                                <a href="javascript: history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; Back</a> &nbsp; 
                                <button type="submit" id="save_ch_frm_1" class="btn btn-default">SAVE FORM &nbsp; <i class="fa fa-save"></i></button> &nbsp;
                                <button type="button" id="go_nxt" class="btn btn-primary">NEXT &nbsp; <i class="fa fa-arrow-right"></i></button>
                            </div>-->
                        </div>
                    </div>
                </div>

                <div class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <h4>ENGAGEMENT 1 (Whanaungatanga/Relationship Building)</h4>
                            </div>
                            <input type="hidden" name="start_time" id="start_time" value="<?php echo set_value('start_time'); ?>">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Where did you meet?</label>
                                    <input type="text" class="form-control" id="ch2_place" name="ch2_place" placeholder="Where did you meet?" data-validation="required" data-validation-error-msg="The field where did you meet is mandatory, please check again!" value="<?php echo $details->chp2_f1; ?>">
                                    <div id="infoMessage"><?php echo form_error('ch2_place'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Start Time</label>
                                    <input type="text" class="form-control" id="ch2_st_time" name="ch2_st_time" placeholder="Start Time" data-validation="required time" data-validation-help="HH:mm in 24 hours" data-validation-error-msg-required="The field start time is mandatory, please check again!" data-validation-error-msg-time="You have not given a correct time" value="<?php echo $details->chp2_f2; ?>">
                                    <div id="infoMessage"><?php echo form_error('ch2_st_time'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>End Time</label>
                                    <input type="text" class="form-control" id="ch2_ed_time" name="ch2_ed_time" placeholder="End Time" data-validation="required time" data-validation-help="HH:mm in 24 hours" data-validation-error-msg-required="The field end time is mandatory, please check again!" data-validation-error-msg-time="You have not given a correct time" value="<?php echo $details->chp2_f3; ?>">
                                    <div id="infoMessage"><?php echo form_error('ch2_ed_time'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Provide your general overview of the Client, any key issues/findings/areas of importance</label>
                                    <textarea class="form-control" name="ch2_oview" id="ch2_oview" placeholder="Provide your general overview of the Client, any key issues/findings/areas of importance" rows="3" data-validation="required" data-validation-error-msg="The field general overview is mandatory, please check again!"><?php echo $details->chp2_f4; ?></textarea>
                                    <div id="infoMessage"><?php echo form_error('ch2_oview'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>What are your key actions from this engagement</label>
                                    <textarea class="form-control" name="ch2_goals" id="ch2_goals" placeholder="What are your key actions from this engagement" rows="3" data-validation="required" data-validation-error-msg="The field key actions is mandatory, please check again!"><?php echo $details->chp2_f5; ?></textarea>
                                    <div id="infoMessage"><?php echo form_error('ch2_goals'); ?></div>
                                </div>
                            </div>
                            <?php $selected_ck_box_arr = $details->chp2_f6 != '' ? explode(",", $details->chp2_f6) : array(); ?>
                            <div class="col-md-12">                                        
                                <label>Tick what information has been provided to the client</label>
                                <ul class="list-unstyled activity-list">
                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_1" value="1" <?php echo in_array('1', $selected_ck_box_arr) ? 'checked' : ''; ?>>
                                            <span>
                                                History of MWDI – Services we provide
                                                &nbsp; ( <a href="https://drive.google.com/open?id=0BxyqeL8pjMojUDZvU0daN0cxdHc" target="blank">View History of MWDI & Services we provide</a> )
                                            </span> 
                                        </label>
                                    </li>
                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_2" value="2" <?php echo in_array('2', $selected_ck_box_arr) ? 'checked' : ''; ?>>
                                            <span>
                                                Loan Information/Application
                                                &nbsp; ( <a href="<?php echo base_url('downloads/Business Criteria.pdf')?>" target="blank" download>View/Download Loan Information</a> &nbsp; | &nbsp; <a href="<?php echo base_url('downloads/Application.pdf')?>" target="blank" download>View/Download Loan Application</a> )
                                            </span> 
                                        </label>
                                    </li>
                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_3" value="3" <?php echo in_array('3', $selected_ck_box_arr) ? 'checked' : ''; ?>>
                                            <span>
                                                Contact Information for Coach
                                            </span> 
                                        </label>
                                    </li>
                                    <?php if(in_array('4', $selected_ck_box_arr)){?>
                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_4" value="4" <?php echo in_array('4', $selected_ck_box_arr) ? 'checked' : ''; ?>>
                                            <span>
                                                Coaching Ethics/Contract signed and scanned to projects@mwdi.co.nz 
                                                &nbsp; ( <a href="http://coachfederation.org/about/ethics.aspx?ItemNumber=854" target="blank">View Coaching Ethics</a> &nbsp; | &nbsp; <a href="https://docs.google.com/document/d/15vlOnkqj_YngTocuQsBnag4mOra1JjQwEiyX2C_Y9Vk/edit" target="blank">View Coaching Contract</a> )
                                            </span> 
                                        </label>
                                    </li>
                                    <?php } ?>
                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_5" value="5" <?php echo in_array('5', $selected_ck_box_arr) ? 'checked' : ''; ?>> 
                                            <span>
                                                Copy of complaints process
                                            </span> 
                                        </label>
                                    </li>
                                    <?php if(in_array('6', $selected_ck_box_arr)){?>
                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_6" value="6" <?php echo in_array('6', $selected_ck_box_arr) ? 'checked' : ''; ?>>
                                            <span>
                                                Link to Business Audit/Needs Survey 
                                                &nbsp; ( <a href="http://goo.gl/forms/PywghBcHwO" target="blank">Business Audit/Needs Survey</a> )
                                            </span> 
                                        </label>
                                    </li>
                                    <?php } ?>
                                    <?php if(in_array('7', $selected_ck_box_arr)){?>
                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_7" value="7" <?php echo in_array('7', $selected_ck_box_arr) ? 'checked' : ''; ?>>
                                            <span>
                                                Personal and Business Questionaire
                                                &nbsp; ( <a href="https://drive.google.com/open?id=1Sg797kipK0H-9PFyJ_jhSvrkFy7flUXcSZSP_fvTkVk" target="blank">Link to Personal and Business Questionaire</a> )
                                            </span> 
                                        </label>
                                    </li>
                                    <?php } ?>
                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_8" value="8" <?php echo in_array('8', $selected_ck_box_arr) ? 'checked' : ''; ?>>
                                            <span>
                                                <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="This is a NZ Trade and Enterprise Link and will require you to sit with the Coachee to complete or for them to work through the template until the end where they can download the Business Plan in its entirety"></i>&nbsp; 
                                                Link to Business Plan Template 
                                                &nbsp; ( <a href="http://m.business.govt.nz/starting-and-stopping/entering-a-business/before-you-start-a-business/business-plan-template-tool" target="blank">Link to Business Plan Template</a> )
                                            </span> 
                                        </label>
                                    </li>
                                    <?php if(in_array('9', $selected_ck_box_arr)){?>
                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_9" value="9" <?php echo in_array('9', $selected_ck_box_arr) ? 'checked' : ''; ?>>
                                            <span>
                                                Link to Financial Plan Template
                                            </span> 
                                        </label>
                                    </li>
                                    <?php } ?>
                                    <?php if(in_array('10', $selected_ck_box_arr)){?>
                                    <li style="padding: 10px 0;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="info_list[]" id="info_list_10" value="10" <?php echo in_array('10', $selected_ck_box_arr) ? 'checked' : ''; ?>>
                                            <span>
                                                Did you attempt to meet the 11 ICF Core competencies in your Client Engagement
                                                &nbsp; ( <a href="https://drive.google.com/open?id=1L3n6rV15u-TwuSDIeajrktZuxLVg6fHwfGrn024d57E" target="blank">Please Click</a> )
                                            </span> 
                                        </label>
                                    </li>
                                    <?php } ?>
                                    <li style="padding: 10px 0;">
                                        <div class="col-md-3" style="padding: 0;">
                                            <label class="fancy-checkbox">
                                                <input type="checkbox" name="info_list[]" id="info_list_11" value="11" <?php echo in_array('11', $selected_ck_box_arr) ? 'checked' : ''; ?>>
                                                <span>
                                                    Upload non-disclosure agreement
                                                </span> 
                                                <?php if($details->nd_link){?>
                                                <hr>
                                                <span>
                                                    <a href="<?php echo base_url('uploads/non_disclosure_agreements/'.$details->nd_link)?>" target="_blank">View uploaded non-disclosure agreement</a>
                                                </span>
                                                <?php }?>
                                            </label>
                                        </div>
                                        <div class="col-md-9">
                                            <input type="file" name="nd_upload" id="nd_upload" class="form-control">
                                            <input type="hidden" name="uploaded_link" id="uploaded_link" value="<?php echo $details->nd_link?>">
                                            <input type="hidden" name="uploaded_name" id="uploaded_name" value="<?php echo $details->nd_name?>">
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-12">
                                <hr><br>
                            </div>
                            <div class="col-md-6">
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="nxt_app" id="nxt_app" value="Yes" <?php echo $details->chp2_f7 ? 'checked' : ''; ?>>
                                    <span><strong>Yes</strong> - Confirmation of next appointment</span>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>When is the date confirmed for the next engagements</label>
                                    <input type="text" class="form-control" id="next_date" name="next_date" placeholder="When is the date/s confirmed for the next engagements" data-validation="date" data-validation-format="dd/mm/yyyy"  data-validation-optional="true" value="<?php echo $details->chp2_f8 == '0000-00-00' ? '' : $details->chp2_f8; ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Any points for follow up from other Coaches</label>
                                    <textarea class="form-control" name="other_ch_flow" id="other_ch_flow" placeholder="Any points for follow up from other Coaches" rows="3"><?php echo $details->chp2_f9; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Any points for MWDI to consider</label>
                                    <textarea class="form-control" name="any_consider" id="any_consider" placeholder="Any points for MWDI to consider" rows="3"><?php echo $details->chp2_f10; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="invo_sent" id="invo_sent" value="Yes" <?php echo $details->chp2_f11 ? 'checked' : ''; ?>>
                                    <span><strong>Yes</strong> - Invoice sent by the 15th of the month to accountspayable@mwdi.co.nz including Kilometres and any costs incurred</span>
                                </label>
                            </div>
                            <div class="col-md-12" style="padding-top: 20px; padding-bottom: 20px;">
                                <a href="javascript: history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; Back</a> &nbsp; 
                                <button type="submit" class="btn btn-default" name="btnaction" value="frm_edit" id="chf1_save">SAVE FORM &nbsp; <i class="fa fa-save"></i></button> &nbsp;
                                <?php if($details->is_submit=='N'){?>
                                <button type="submit" class="btn btn-primary" name="btnaction" value="frm_submit" id="chf1_submit">SUBMIT</button>
                                <?php }?>
                                <input type="hidden" name="button_type" id="button_type" value="frm_edit">
                            </div> 
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<?php if ($this->session->flashdata('create_success')) { ?>
    <script>swal({type: 'success', title: 'success', text: 'The form has been successfully saved', timer: 3000});</script>
<?php } else if ($this->session->flashdata('error_msg')) { ?>
    <script>swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000});</script>
<?php } ?>
<script>
    $(document).ready(function () {
        $.validate({
            form: '#edit_coach_frm_1',
            modules: 'security,date,file'
        });
        
        //----------------------------------------------------------------------
        
        $('#ch2_st_time').mask('00:00', {placeholder: "Start Time"});
        $('#ch2_ed_time').mask('00:00', {placeholder: "End Time"});

        $('#next_date').mask('00/00/0000', {placeholder: "When is the date confirmed for the next engagements DD/MM/YYYY"});
        $('#phone').mask('000000000000',{placeholder: "091234567"});
        $('#mobile').mask('000000000000',{placeholder: "0211234567"});

        //----------------------------------------------------------------------
        
        $('#next_date').keyup(function () {
            if (e.keyCode == 8 || e.keyCode == 46) {
                $(this).attr("data-validation-optional", "true");
            } else {
                $(this).removeAttr('data-validation-optional');
            }
        });
        
        //----------------------------------------------------------------------
        
//        $('#ch1_submit').on('click', function () {
//            swal({
//                title: 'Do you want submit this form?',
//                confirmButtonText: 'Submit',
//                html: '<span style="color:red;">You cannot edit this form after submit</span>',
//                showLoaderOnConfirm: true,
//                showCancelButton: true,
//                preConfirm: function () {
//                    return new Promise(function (resolve) {
//                        $.ajax({
//                            type: 'POST',
//                            url: '<?php echo base_url('submit_form/'.$frm_type.'/'.$frm_id); ?>',
//                        })
//                                .done(function (response) {
//                                    var rslt = JSON.parse(response);
//                                    console.log(rslt);
//                                    if (rslt) {
//                                        swal({type: 'success', title: 'success', text: 'The form has been submited', timer: 3000}).catch(function (timeout) { });
//                                        window.setTimeout(function () {
//                                            location.href = '<?php echo base_url('submited_coach_form') ?>';
//                                        }, 3000);
//                                    } else {
//                                        resolve()
//                                        //swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000}) 
//                                    }
//                                })
//                                .fail(function () {
//                                    swal('Oops...', 'Something went wrong !', 'error');
//                                });
//                    })
//                }
//            }).catch(swal.noop);
//        });

        //----------------------------------------------------------------------
        
        $('#chf1_submit').on('click', function (e) {
            e.preventDefault();
            if($('#edit_coach_frm_1').isValid()){
                swal({
                    title: 'Do you want submit this form?',
                    confirmButtonText: 'Submit',
                    html: '<span style="color:red;">You cannot edit this form after submit</span>',
                    showLoaderOnConfirm: true,
                    showCancelButton: true
                }).then((result) => {
                    $("#button_type").val('frm_submit');
                    $("#edit_coach_frm_1").submit();
                });
            }
        });
        
        //----------------------------------------------------------------------
        
        if($('#info_list_11').is(":checked")){
            $('#nd_upload').css({'visibility':'visible'});
            if($('#uploaded_link').val()){
                $('#nd_upload').attr('data-validation-optional','true');
            }
        }else{
            $('#nd_upload').css({'visibility':'hidden'});
        }
        
        //----------------------------------------------------------------------
        
        $('#info_list_11').change(function (){
            if(this.checked){
                $('#nd_upload').css({'visibility':'visible','border-color':'#eaeaea'});
                $('#nd_upload').removeAttr('data-validation-optional');
                $('#nd_upload').attr({'data-validation':'required size','accept':'.pdf','data-validation-max-size':'2M','data-validation-error-msg':'Please upload non-disclosure agreement!','data-validation-error-msg-size':'Maximum upload size (2MB) exceeded, please try again!'});
            }else{
                $('#nd_upload').css({'visibility':'hidden'});
                $('#nd_upload').parent().removeClass('has-error');
                $('#nd_upload').next('span').remove();
                $('#nd_upload').attr('data-validation-optional','true');
                $('#nd_upload').val("");
            }
        });

    });
</script>



