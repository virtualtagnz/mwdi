<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <?php if ($this->session->userdata('role') == '1') { ?>
<!--                    <a href="form_coach.php" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> &nbsp; Create Coach Form</a>-->
                <?php } ?>
                Forms <i class="lnr lnr-chevron-right"></i> Coach
            </h3>
            <?php //var_dump($archives); ?>
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="archive_frm_tab" class="table table-striped table-bordered dataTable js-pointstable display">
                            <thead>
                                <tr>
                                    <th>Client Name</th>
                                    <th>Form Name</th>
                                    <th>Status</th>
                                    <th>Edit/ View</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $all_archives = json_decode($archives) ?>
                                <?php foreach ($all_archives as $archive) { ?>
                                    <?php //var_dump($archive); ?>
                                    <tr>
                                        <td><?php echo $archive->fname . ' ' . $archive->lname ?></td>
                                        <td><?php echo $archive->form_name ?></td>
                                        <td><?php echo $archive->stat == 'Y' ? 'Submited' : 'Draft' ?></td>
                                        <?php if ($archive->stat != 'Y') { ?>
                                            <?php if ($this->session->userdata('role') == '1'||$this->session->userdata('role') == '2'||$this->session->userdata('role') == '4') {?>
                                            <td><a href="<?php echo base_url('view_engagement/' . $archive->form_type . '/' . $this->encrypt->encode($archive->form_id)); ?>" class="btn btn-fefault" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a> &nbsp;</td>
                                            <?php }?>
                                        <?php } else { ?>
                                            <td>
<!--                                            <a rel="<?php echo $archive->form_type; ?>" id="<?php echo $this->encrypt->encode($archive->form_id); ?>" class="btn btn-fefault view_form" data-toggle="tooltip" data-placement="top" title="View"><i class="fa fa-eye"></i></a> &nbsp;-->
                                                <a href="<?php echo base_url('view_single_form/' . $archive->form_type . '/' . $this->encrypt->encode($archive->form_id)); ?>" target="blank" class="btn btn-fefault" data-toggle="tooltip" data-placement="top" title="Print"><i class="fa fa-print"></i></a> &nbsp;
                                            </td>        
                                        <?php } ?>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<?php if ($this->session->flashdata('submit_success')) { ?>
    <script>swal({type: 'success', title: 'success', text: 'The form has been successfully submitted', timer: 3000});</script>
<?php } else if ($this->session->flashdata('error_msg')) { ?>
    <script>swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000});</script>
<?php } ?>
<div id="modalView"></div>
<script>
    $(document).ready(function () {
        $('#archive_frm_tab').dataTable({
            "pageLength": 10,
            "searching": true,
            "info": true,
            "ordering": true
        });

        $('body').delegate('.view_form', 'click', (function () {
            var formType = $(this).get(0).rel;
            var id = $(this).get(0).id;
            console.log(id);
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('view_single_form/') ?>"+formType+'/'+id,
                success: function (results) {
                    //console.log(results);
                    switch(formType){
                        case '5':
                            $('#modalView').html(results);
                            $('#coach_form_1_modal').modal('show');
                            break;
                    }
                }
            })
        }));
    });
</script>