<?php
$ch_form1_details = (json_decode($details));
?>
<style>
    td{
        border: 1px solid #9D8526;
        padding: 5px;
    }
    tr.bg{
        background-color: #252c35;
        color: #ffffff;
    }
    .title{
        text-align: center;
    }
</style>
<div class="title">
    <h2>Client Form Engagement 1</h2>
</div>
<?php if ($ch_form1_details) { ?>
    <table cellspacing="0" cellpadding="5" border="1">
        <tr class="bg">
            <td colspan="2">
                <h4 class="">CLIENT INFORMATION</h4>
            </td>
        </tr>
        <tr>
            <td>Name</td><td><?php echo $ch_form1_details->first_name . ' ' . $ch_form1_details->last_name ?></td>
        </tr>
        <tr>
            <td>New/Existing Client</td><td><?php echo $ch_form1_details->chf_cl_type == '1' ? 'New' : 'Existing' ?></td>
        </tr>
        <tr>
            <td>Region of Delivery</td><td><?php echo $ch_form1_details->chf_reg ?></td>
        </tr>
        <tr>
            <td>Referred by</td><td><?php echo $ch_form1_details->chf_ref ?></td>
        </tr>
        <?php if($ch_form1_details->ref_first_name){?>
        <tr>
            <td>Name of Client Referred</td><td><?php echo $ch_form1_details->ref_first_name . ' ' . $ch_form1_details->ref_last_name ?></td>
        </tr>
        <?php }?>
        <tr>
            <td>Address</td><td><?php echo $ch_form1_details->chf_add ?></td>
        </tr>
        <tr>
            <td>Purpose of Visit</td><td><?php echo $ch_form1_details->chf_purpose ?></td>
        </tr>
        <tr>
            <td>phone</td><td><?php echo $ch_form1_details->chf_phn ?></td>
        </tr>
        <tr>
            <td>mobile</td><td><?php echo $ch_form1_details->chf_nxt_phn ?></td>
        </tr>
        <?php if($ch_form1_details->chf_nxt_contact){ ?>
        <tr>
            <td>Emergency Contact Person</td><td><?php echo $ch_form1_details->chf_nxt_contact ?></td>
        </tr>
        <?php }?>
        <tr>
            <td>Email</td><td><?php echo $ch_form1_details->chf_email ?></td>
        </tr>
    </table>
    <div style="page-break-inside: avoid;"></div>
    <table cellspacing="0" cellpadding="5" border="1">
        <tr class="bg">
            <td colspan="2">
                <h4 class="">ENGAGEMENT 1 (Whanaungatanga/Relationship Building)</h4>
            </td>
        </tr>
        <tr>
            <td>Where did you meet</td><td><?php echo $ch_form1_details->chp2_f1 ?></td>
        </tr>
        <tr>
            <td>Start Time</td><td><?php echo $ch_form1_details->chp2_f2 ?></td>
        </tr>
        <tr>
            <td>End Time</td><td><?php echo $ch_form1_details->chp2_f3 ?></td>
        </tr>
        <tr>
            <td>General overview of the Client</td><td><?php echo $ch_form1_details->chp2_f4 ?></td>
        </tr>
        <tr>
            <td>Key actions from this engagement</td><td><?php echo $ch_form1_details->chp2_f5 ?></td>
        </tr>
        <br pagebreak="true"/>
        <tr>
            <td>What information has been provided to the client</td>
            <td>
                <?php
                $selected_ck_box_arr = $ch_form1_details->chp2_f6 != '' ? explode(",", $ch_form1_details->chp2_f6) : array();
                if (in_array('1', $selected_ck_box_arr)) {
                    echo '- History of MWDI – Services we provide' . '<br>';
                }
                if (in_array('2', $selected_ck_box_arr)) {
                    echo '  - Loan Information/Application' . '<br>';
                }
                if (in_array('3', $selected_ck_box_arr)) {
                    echo '  - Contact Information for Coach' . '<br>';
                }
                if (in_array('4', $selected_ck_box_arr)) {
                    echo '  - Coaching Ethics/Contract signed and scanned to projects@mwdi.co.nz' . '<br>';
                }
                if (in_array('5', $selected_ck_box_arr)) {
                    echo '  - Copy of complaints process' . '<br>';
                }
                if (in_array('6', $selected_ck_box_arr)) {
                    echo '  - Link to Business Audit/Needs Survey' . '<br>';
                }
                if (in_array('7', $selected_ck_box_arr)) {
                    echo '  - Personal and Business Questionaire' . '<br>';
                }
                if (in_array('8', $selected_ck_box_arr)) {
                    echo '  - Link to Business Plan Template' . '<br>';
                }
                if (in_array('9', $selected_ck_box_arr)) {
                    echo '  - Link to Financial Plan Template' . '<br>';
                }
                if (in_array('10', $selected_ck_box_arr)) {
                    echo '  - Did you attempt to meet the 11 ICF Core competencies in your Client Engagement' . '<br>';
                }
                if(in_array('11', $selected_ck_box_arr)) {
                    echo '  - Upload non-disclosure agreement'.' '.'<a href="'.base_url('uploads/non_disclosure_agreements/'.$ch_form1_details->nd_link).'">View</a>';
                    
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>Confirmation of next appointment</td><td><?php echo $ch_form1_details->chp2_f7 ?></td>
        </tr>
        <tr>
            <td>Date for the next engagements</td><td><?php echo $ch_form1_details->chp2_f8 ?></td>
        </tr>
        <tr>
            <td>Any points for follow up from other Coaches</td><td><?php echo $ch_form1_details->chp2_f9 ?></td>
        </tr>
        <tr>
            <td>Any points for MWDI to consider</td><td><?php echo $ch_form1_details->chp2_f10 ?></td>
        </tr>
        <tr>
            <td>Invoice sent by the 15th of the month</td><td><?php echo $ch_form1_details->chp2_f11 ?></td>
        </tr>
    </table>
    <?php
}?>