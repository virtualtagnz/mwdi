 <?php
$ch_form4_details = (json_decode($details));
?>
<style>
    td{
        border: 1px solid #9D8526;
        padding: 5px;
    }
    tr.bg{
        background-color: #252c35;
        color: #ffffff;
    }
    .title{
        text-align: center;
    }
</style>
<div class="title">
    <h2>FINAL ENGAGEMENT</h2>
</div>
<?php if ($ch_form4_details) { ?>
    <table cellspacing="0" cellpadding="5" border="1">
        <tr class="bg">
            <td colspan="2">
                <h4 class="">ENGAGEMENT 4 (Final Sponsored Session – Follow Up)</h4>
            </td>
        </tr>
        <tr>
            <td>Client Name</td><td><?php echo $ch_form4_details->first_name . ' ' . $ch_form4_details->last_name ?></td>
        </tr>
        <tr>
            <td>Where did you meet</td><td><?php echo $ch_form4_details->chp5_f75 ?></td>
        </tr>
        <tr>
            <td>Start Time</td><td><?php echo $ch_form4_details->chp5_f77 ?></td>
        </tr>
        <tr>
            <td>End Time</td><td><?php echo $ch_form4_details->chp5_f78 ?></td>
        </tr>
        <tr>
            <td>General overview of the Client</td><td><?php echo $ch_form4_details->chp5_f79 ?></td>
        </tr>
        <tr>
            <td>What information has been provided to the client</td>
            <td>
                <?php
                $selected_ck_box_arr = $ch_form4_details->chp5_f80 != '' ? explode(",", $ch_form4_details->chp5_f80) : array();
                if (in_array('1', $selected_ck_box_arr)) {
                    echo '- Business Audit' . '<br>';
                }
                if (in_array('2', $selected_ck_box_arr)) {
                    echo '  - Financial Audit' . '<br>';
                }
                if (in_array('3', $selected_ck_box_arr)) {
                    echo '  - Business Plan' . '<br>';
                }
                if (in_array('4', $selected_ck_box_arr)) {
                    echo '  - Financial Plan' . '<br>';
                }
                if (in_array('5', $selected_ck_box_arr)) {
                    echo '  - Loan Application' . '<br>';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>Has the client engaged in any training or development</td><td><?php echo $ch_form4_details->chp5_f81 ?></td>
        </tr>
        <tr>
            <td>Any points for follow up needed from other Coaches</td><td><?php echo $ch_form4_details->chp5_f82 ?></td>
        </tr>
        <tr>
            <td>Does the client wish to engage you privately for ongoing Coaching services</td><td><?php echo $ch_form4_details->chp5_f83=='1'?'Yes':'No' ?></td>
        </tr>
        <tr>
            <td>If Yes, please indicate the nature of the engagement moving forward (including contracted rates/potential dates)</td><td><?php echo $ch_form4_details->chp5_f84?></td>
        </tr>
        <tr>
            <td>Any other points/concerns/reflections</td><td><?php echo $ch_form4_details->chp5_f85?></td>
        </tr>
        <tr>
            <td>If required have you provided a closing process for the client</td><td><?php echo $ch_form4_details->chp5_f86=='1'?'Yes':'No'?></td>
        </tr>
        <tr>
            <td>Invoice sent by the 15th of the month</td><td><?php echo $ch_form4_details->chp5_f87 ?></td>
        </tr>
    </table>
<?php } ?>

