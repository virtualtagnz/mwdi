<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
<!--                <a href="<?php echo base_url('view_single_form/10/FALSE'); ?>" class="btn btn-primary pull-right"><i class="fa fa-print"></i> &nbsp; Print Form</a>-->
                Forms <i class="lnr lnr-chevron-right"></i> Client Form
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <form action="<?php echo base_url('create_coach_form_5'); ?>" method="post" name="coach_frm_5" id="coach_frm_5" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <h4>FINAL ENGAGEMENT (Final Sponsored Session – Follow Up)</h4>
                            </div>
                            <input type="hidden" name="ch5_start_time" id="ch5_start_time" value="<?php echo set_value('start_time'); ?>">
                            <input type="hidden" name="main_form_id" id="main_form_id" value="<?php echo set_value('main_form_id'); ?>">
                            <input type="hidden" name="sub_stat" id="sub_stat" value="">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Client Name</label>
                                    <select class="form-control" id="ch5_client" name="ch5_client" data-validation="required" data-validation-error-msg="The field select client is mandatory, please check again!">
                                        <option selected="selected" value="">Client Name</option>
                                        <?php if ($my_clients) { ?>
                                            <?php $clients = json_decode($my_clients) ?>
                                            <?php foreach ($clients as $client) { ?>
                                                <option value="<?php echo $client->client_id ?>"<?php echo set_select('ch5_client', $client->client_id); ?>><?php echo $client->first_name . ' ' . $client->last_name ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                    <div id="infoMessage"><?php echo form_error('ch5_client'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Where did you meet?</label>
                                    <input type="text" class="form-control" id="ch5_place" name="ch5_place" placeholder="Where did you meet?" data-validation="required" data-validation-error-msg="The field where did you meet is mandatory, please check again!" value="<?php echo set_value('ch5_place'); ?>">
                                    <div id="infoMessage"><?php echo form_error('ch5_place'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Start Time</label>
                                    <input type="text" class="form-control" id="ch5_st_time" name="ch5_st_time" placeholder="Start Time" data-validation="required time" data-validation-help="HH:mm" data-validation-error-msg-required="The field start time is mandatory, please check again!" data-validation-error-msg-time="You have not given a correct time" value="<?php echo set_value('ch5_st_time'); ?>">
                                    <div id="infoMessage"><?php echo form_error('ch5_st_time'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>End Time</label>
                                    <input type="text" class="form-control" id="ch5_ed_time" name="ch5_ed_time" placeholder="End Time" data-validation="required time" data-validation-error-msg-required="The field end time is mandatory, please check again!" data-validation-error-msg-time="You have not given a correct time" value="<?php echo set_value('ch5_ed_time'); ?>">
                                    <div id="infoMessage"><?php echo form_error('ch5_ed_time'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Provide your general overview of the Client, any key issues/findings/areas of importance</label>
                                    <textarea class="form-control" name="ch5_oview" id="ch5_oview" placeholder="Provide your general overview of the Client, any key issues/findings/areas of importance" rows="3" data-validation="required" data-validation-error-msg="The field general overview is mandatory, please check again!"><?php echo set_value('ch5_oview'); ?></textarea>
                                    <div id="infoMessage"><?php echo form_error('ch5_oview'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label>Has the client completed any of the following</label>
                                <ul class="list-unstyled activity-list">
<!--                                    <li style="padding: 10px 0; width: 33%; float: left;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="completed_list[]" id="completed_list_1" value="1" <?php echo set_checkbox('completed_list', '1'); ?>>
                                            <span>Business Audit</span> 
                                        </label>
                                    </li>
                                    <li style="padding: 10px 0; width: 33%; float: left;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="completed_list[]" id="completed_list_1" value="2" <?php echo set_checkbox('completed_list', '2'); ?>>
                                            <span>Financial Audit</span> 
                                        </label>
                                    </li>-->
                                    <li style="padding: 10px 0; width: 33%; float: left;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="completed_list[]" id="completed_list_1" value="3" <?php echo set_checkbox('completed_list', '3'); ?>>
                                            <span>Business Plan</span> 
                                        </label>
                                    </li>
                                    <li style="padding: 10px 0; width: 33%; float: left;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="completed_list[]" id="completed_list_1" value="4" <?php echo set_checkbox('completed_list', '4'); ?>>
                                            <span>Financial Plan</span> 
                                        </label>
                                    </li>
                                    <li style="padding: 10px 0; width: 33%; float: left;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="completed_list[]" id="completed_list_1" value="5" <?php echo set_checkbox('completed_list', '5'); ?>>
                                            <span>Loan Application</span> 
                                        </label>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Has the client engaged in any training or development</label>
                                    <textarea class="form-control" name="tr_dev" id="tr_dev" placeholder="Has the client engaged in any training or development" rows="3"><?php echo set_value('tr_dev'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Any points for follow up needed from other Coaches</label>
                                    <textarea class="form-control" name="follow_up" id="follow_up" placeholder="Any points for follow up needed from other Coaches" rows="3"><?php echo set_value('follow_up'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Does the client wish to engage you privately for ongoing Coaching services</label>
                                    <select class="form-control" name="sel_list" id="sel_list">
                                        <option selected="selected">- Please Select -</option>
                                        <option value="1" <?php echo set_select('sel_list', '1'); ?>>Yes</option>
                                        <option value="2" <?php echo set_select('sel_list', '2'); ?>>No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>If Yes, please indicate the nature of the engagement moving forward (including contracted rates/potential dates)</label>
                                    <textarea class="form-control" name="eng_nature" id="eng_nature" placeholder="If Yes, please indicate the nature of the engagement moving forward (including contracted rates/potential dates)" rows="3"><?php echo set_value('eng_nature'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Any other points/concerns/reflections</label>
                                    <textarea class="form-control" name="other_pnts" id="other_pnts" placeholder="Any other points/concerns/reflections" rows="3"><?php echo set_value('other_pnts'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>If required have you provided a closing process for the client</label>
                                    <select class="form-control" id="sel_list2" name="sel_list2">
                                        <option elected="selected">- Please Select -</option>
                                        <option value="1" <?php echo set_select('sel_list2', '1'); ?>>Yes</option>
                                        <option value="2" <?php echo set_select('sel_list2', '2'); ?>>No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="ch5_invo_sent" id="ch5_invo_sent" value="Yes" <?php echo set_checkbox('ch5_invo_sent', 'Yes'); ?>>
                                    <span><strong>Yes</strong> - Invoice sent by the 15th of the month to accountspayable@mwdi.co.nz including Kilometres and any costs incurred</span>
                                </label>
                            </div>
                            <div class="col-md-12" style="padding-top: 20px; padding-bottom: 20px;">
                                <a href="javascript:history.back()" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; BACK</a> &nbsp; 
                                <button type="submit" class="btn btn-default" name="btnaction" value="frm_save" id="chf5_save">SAVE FORM &nbsp; <i class="fa fa-save"></i></button> &nbsp;
                                <button type="submit" class="btn btn-primary" name="btnaction" value="frm_submit" id="chf5_submit">SUBMIT</button>
                                <input type="hidden" name="button_type" id="button_type" value="frm_save">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<?php if ($this->session->flashdata('create_success')) { ?>
    <script>swal({type: 'success', title: 'success', text: 'The form has been successfully saved', timer: 3000});</script>
<?php } else if ($this->session->flashdata('pdf_error')) { ?>
    <script>swal({type: 'error', title: 'PDF Error', text: 'No information to print', timer: 3000});</script>
<?php } else if ($this->session->flashdata('error_msg')) { ?>
    <script>swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000});</script>
<?php } ?>
<script>
    $(document).ready(function () {
        $('#ch5_client').select2();
        $('#ch5_client').on("select2:select", function(e) { 
            $('#ch5_client').validate({ validateHiddenInputs: false });//Selected - Hide errors message
        });
        
        //----------------------------------------------------------------------
        
        $.validate({
            form: '#coach_frm_5',
            modules: 'security,date'
        });
        
        //----------------------------------------------------------------------
        
        $("#coach_frm_5 :input").change(function () {
            if (!$("#ch5_start_time").val()) {
               $('#ch5_start_time').val(timer());
            }
        });
        
        //----------------------------------------------------------------------

        $('#ch5_st_time').mask('00:00', {placeholder: "Start Time"});
        $('#ch5_ed_time').mask('00:00', {placeholder: "End Time"});

        $('#ch5_next_date').mask('00/00/0000', {placeholder: "When is the date confirmed for the next engagements DD/MM/YYYY"});

        $('#ch5_next_date').keyup(function () {
            if (e.keyCode == 8 || e.keyCode == 46) {
                $(this).attr("data-validation-optional", "true");
            } else {
                $(this).removeAttr('data-validation-optional');
            }
        });
        
        //----------------------------------------------------------------------

        $('#ch5_client').on('change', function () {
            var frm_id = '<?php echo end($this->uri->segments);?>';
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('Coach_form/check_main_form_available_p2'); ?>",
                data: {'uid': $(this).val(),'form_id':frm_id},
                success: function (results) {
                    var frm_id = $.parseJSON(results);
                    console.log(frm_id);
                    if (frm_id == null) {
                        swal({title: 'No main form',
                            text: "Please fill Coach Form Engagement 1!",
                            type: 'error',
                            confirmButtonText: 'Go to my form section!',
                        }).then(function (result) {
                            location.href = '<?php echo base_url('my_forms/' . $this->encrypt->encode($this->session->userdata('user_level_id')) . '/' . $this->encrypt->encode($this->session->userdata('role'))) ?>'
                        });
                    } else if (frm_id.form_2_id) {
                        location.href = '<?php echo base_url('view_edit_forms/10/') ?>'+frm_id.form_2_id;
                    } else {
                        $('#main_form_id').val(frm_id.main_id);
                    }
                }
            });
        });
        
        //----------------------------------------------------------------------
        
        $('#chf5_submit').on('click', function (e) {
            e.preventDefault();
            if($('#coach_frm_5').isValid()){
                swal({
                    title: 'Do you want submit this form?',
                    confirmButtonText: 'Submit',
                    html: '<span style="color:red;">You cannot edit this form after submit</span>',
                    showLoaderOnConfirm: true,
                    showCancelButton: true
                }).then((result) => {
                    $("#button_type").val('frm_submit');
                    $("#coach_frm_5").submit();
                });
            }
        });

    });
</script>
