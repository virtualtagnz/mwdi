<?php
$frm_id = end($this->uri->segments);
$secondLastKey = count($this->uri->segment_array())-1;
$frm_type = $this->uri->segment($secondLastKey);
?>
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <a href="<?php echo base_url('view_single_form/' . $frm_type . '/' . $frm_id); ?>" class="btn btn-primary pull-right"><i class="fa fa-print"></i> &nbsp; Print Form</a>
                Forms <i class="lnr lnr-chevron-right"></i> Client Form
            </h3>
            <?php //var_dump($details); ?>
            <div class="panel">
                <div class="panel-body">
                    <form action="<?php echo base_url('edit_coach_form_3/' . end($this->uri->segments)); ?>" method="post" name="coach_frm_3" id="coach_frm_3" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <h4>ENGAGEMENT 2 (Building on the Relationship)</h4>
                            </div>
                            <input type="hidden" name="start_time" id="start_time" value="<?php echo set_value('start_time'); ?>">
                            <input type="hidden" name="sub_stat" id="sub_stat" value="<?php echo $details->is_submit?>">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" class="form-control" id="ch3_place" value="<?php echo $details->first_name . ' ' . $details->last_name; ?>" readonly="true">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Where did you meet?</label>
                                    <input type="text" class="form-control" id="ch3_place" name="ch3_place" placeholder="Where did you meet?" data-validation="required" data-validation-error-msg="The field where did you meet is mandatory, please check again!" value="<?php echo $details->chp3_f24; ?>">
                                    <div id="infoMessage"><?php echo form_error('ch3_place'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Start Time</label>
                                    <input type="text" class="form-control" id="ch3_st_time" name="ch3_st_time" placeholder="Start Time" data-validation="required time" data-validation-help="HH:mm in 24 hours" data-validation-error-msg-required="The field start time is mandatory, please check again!" data-validation-error-msg-time="You have not given a correct time" value="<?php echo $details->chp3_f25; ?>">
                                    <div id="infoMessage"><?php echo form_error('ch3_st_time'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>End Time</label>
                                    <input type="text" class="form-control" id="ch3_ed_time" name="ch3_ed_time" placeholder="End Time" data-validation="required time" data-validation-help="HH:mm in 24 hours" data-validation-error-msg-required="The field end time is mandatory, please check again!" data-validation-error-msg-time="You have not given a correct time" value="<?php echo $details->chp3_f26; ?>">
                                    <div id="infoMessage"><?php echo form_error('ch3_ed_time'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Provide your general overview of the Client, any key issues/findings/areas of importance</label>
                                    <textarea class="form-control" name="ch3_oview" id="ch3_oview" placeholder="Provide your general overview of the Client, any key issues/findings/areas of importance" rows="3" data-validation="required" data-validation-error-msg="The field general overview is mandatory, please check again!"><?php echo $details->chp3_f27; ?></textarea>
                                    <div id="infoMessage"><?php echo form_error('ch3_oview'); ?></div>
                                </div>
                            </div>
                            <?php $selected_ck_box_arr = $details->chp3_f28 != '' ? explode(",", $details->chp3_f28) : array(); ?>
                            <?php //var_dump($selected_ck_box_arr);?>
                            <div class="col-md-12">
                                <label>Tick what information has been provided to the client</label>
                                <ul class="list-unstyled activity-list">
                                    <?php if(in_array('1', $selected_ck_box_arr)):?>
                                    <li style="padding: 10px 0; width: 33%; float: left;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="chk_list[]" id="chk_list_1" value="1" <?php echo in_array('1', $selected_ck_box_arr) ? 'checked' : ''; ?>>
                                            <span>Business Audit</span> 
                                        </label>
                                    </li>
                                    <?php endif;?>
                                    <?php if(in_array('2', $selected_ck_box_arr)):?>
                                    <li style="padding: 10px 0; width: 33%; float: left;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="chk_list[]" id="chk_list_1" value="2" <?php echo in_array('2', $selected_ck_box_arr) ? 'checked' : ''; ?>>
                                            <span>Financial Audit</span> 
                                        </label>
                                    </li>
                                    <?php endif;?>
                                    <li style="padding: 10px 0; width: 33%; float: left;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="chk_list[]" id="chk_list_1" value="3" <?php echo in_array('3', $selected_ck_box_arr) ? 'checked' : ''; ?>>
                                            <span>Business Plan</span> 
                                        </label>
                                    </li>
                                    <li style="padding: 10px 0; width: 33%; float: left;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="chk_list[]" id="chk_list_1" value="4" <?php echo in_array('4', $selected_ck_box_arr) ? 'checked' : ''; ?>>
                                            <span>Financial Plan</span> 
                                        </label>
                                    </li>
                                    <li style="padding: 10px 0; width: 33%; float: left;">
                                        <label class="fancy-checkbox">
                                            <input type="checkbox" name="chk_list[]" id="chk_list_1" value="5" <?php echo in_array('5', $selected_ck_box_arr) ? 'checked' : ''; ?>>
                                            <span>Loan Application</span> 
                                        </label>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Please fill in relevant points found in the business/financial audit</label>
                                    <textarea class="form-control" name="rel_points" id="rel_points" placeholder="Please fill in relevant points found in the business/financial audit" rows="3" data-validation="required" data-validation-error-msg-required="The field relevant points number is mandatory, please check again!" data-validation-depends-on="chk_list[]" data-validation-depends-on-value="1,2"><?php echo $details->chp3_f29; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>If they haven&#39t filled in the link or template please note why</label>
                                    <textarea class="form-control" name="note" id="note" placeholder="If they haven&#39t filled in the link or template please note why" rows="3"><?php echo $details->chp3_f30; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <hr>
                                <h4>Loan Questions if applicable</h4>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>New Clients: Does this client wish to progress with a loan application and do they require assistance to complete all the requirements</label>
                                    <select class="form-control" name="lq_1" id="lq_1">
                                        <option value="0" selected="selected">- Please Select -</option>
                                        <option value="1" <?php echo $details->chp3_f31 == '1' ? 'selected' : ''; ?>>Yes</option>
                                        <option value="2" <?php echo $details->chp3_f31 == '2' ? 'selected' : ''; ?>>No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Existing clients: Has the money invested by MWDI been in line with their original application/Business plan submitted</label>
                                    <textarea class="form-control" name="lq_2" id="lq_2" placeholder="Existing clients: Has the money invested by MWDI been in line with their original application/Business plan submitted" rows="3"><?php echo $details->chp3_f32; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Existing clients: Will the client consider increasing repayments to MWDI? If so how much/by when</label>
                                    <textarea class="form-control" name="lq_3" id="lq_3" placeholder="Existing clients: Will the client consider increasing repayments to MWDI? If so how much/by when" rows="3"><?php echo $details->chp3_f33; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <hr>
                                <h4>Financial Literacy</h4>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>What are the key financial issues impacting the client at a personal level</label>
                                    <textarea class="form-control" name="ft_1" id="ft_1" placeholder="What are the key financial issues impacting the client at a personal level" rows="3"><?php echo $details->chp3_f34; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>What are the key financial issues impacting the client at a business level</label>
                                    <textarea class="form-control" name="ft_2" id="ft_2" placeholder="What are the key financial issues impacting the client at a business level" rows="3"><?php echo $details->chp3_f35; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>What topics did you explore together</label>
                                    <textarea class="form-control" name="ft_3" id="ft_3" placeholder="What topics did you explore together" rows="3"><?php echo $details->chp3_f36; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <hr>
                                <h4>Training</h4>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Has the client identified any specific training needs</label>
                                    <textarea class="form-control" name="tr_1" id="tr_1" placeholder="Has the client identified any specific training needs" rows="3"><?php echo $details->chp3_f37; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Did you refer the clients onto any of our training programs</label>
                                    <textarea class="form-control" name="tr_2" id="tr_2" placeholder="Did you refer the clients onto any of our training programs" rows="3"><?php echo $details->chp3_f38; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <hr>
                                <h4>Information</h4>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>What information been provided to the client (ie Business Plan templates/Financial Plan Templates, Links to information)</label>
                                    <textarea class="form-control" name="info_1" id="info_1" placeholder="What information been provided to the client (ie Business Plan templates/Financial Plan Templates, Links to information)" rows="3"><?php echo $details->chp3_f39; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Has the client identified any specific training needs</label>
                                    <textarea class="form-control" name="info_2" id="info_2" placeholder="Has the client identified any specific training needs" rows="3"><?php echo $details->chp3_f40; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Did you refer the clients onto any of our training programmes</label>
                                    <textarea class="form-control" name="info_3" id="info_3" placeholder="Did you refer the clients onto any of our training programmes" rows="3"><?php echo $details->chp3_f41; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <hr>
                                <h4>Business Development</h4>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>What are the key challenges facing this client</label>
                                    <textarea class="form-control" name="bd_1" id="bd_1" placeholder="What are the key challenges facing this client" rows="3"><?php echo $details->chp3_f42; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>What resources/support does this client require</label>
                                    <textarea class="form-control" name="bd_2" id="bd_2" placeholder="What resources/support does this client require" rows="3"><?php echo $details->chp3_f43; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>What would support this client to grow and expand their business</label>
                                    <textarea class="form-control" name="bd_3" id="bd_3" placeholder="What would support this client to grow and expand their business" rows="3"><?php echo $details->chp3_f44; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Key highlights/Breakthroughs</label>
                                    <textarea class="form-control" name="bd_4" id="bd_4" placeholder="Key highlights/Breakthroughs" rows="3"><?php echo $details->chp3_f45; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Did you refer the client onto any other service/organisation/website? If so who/what</label>
                                    <textarea class="form-control" name="bd_5" id="bd_5" placeholder="Did you refer the client onto any other service/organisation/website? If so who/what" rows="3"><?php echo $details->chp3_f46; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Any points for follow up from other Coaches</label>
                                    <textarea class="form-control" name="bd_6" id="bd_6" placeholder="Any points for follow up from other Coaches" rows="3"><?php echo $details->chp3_f47; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="ch3_nxt_app" id="ch3_nxt_app" value="Yes" <?php echo $details->chp3_f48 == 'Yes' ? 'checked' : ''; ?>>
                                    <span><strong>Yes</strong> - Confirmation of next appointment</span>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>When is the date confirmed for the next engagements</label>
                                    <input type="text" class="form-control" id="ch3_next_date" name="ch3_next_date" placeholder="When is the date/s confirmed for the next engagements" data-validation="date" data-validation-format="dd/mm/yyyy"  data-validation-optional="true" value="<?php echo $details->chp3_f49; ?>">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="ch3_invo_sent" id="ch3_invo_sent" value="Yes" <?php echo $details->chp3_f50 == 'Yes' ? 'checked' : ''; ?>>
                                    <span><strong>Yes</strong> - Invoice sent by the 15th of the month to accountspayable@mwdi.co.nz including Kilometres and any costs incurred</span>
                                </label>
                            </div>
                            <div class="col-md-12" style="padding-top: 20px; padding-bottom: 20px;">
                                <a href="javascript:history.back()" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; BACK</a> &nbsp; 
                                <button type="submit" class="btn btn-default" name="btnaction" value="frm_edit" id="chf3_save">SAVE FORM &nbsp; <i class="fa fa-save"></i></button> &nbsp;
                                <?php if($details->is_submit=='N'){?>
                                <button type="submit" class="btn btn-primary" name="btnaction" value="frm_submit" id="chf3_submit">SUBMIT</button>
                                <?php }?>
                                <input type="hidden" name="button_type" id="button_type" value="frm_edit">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<?php if ($this->session->flashdata('create_success')) { ?>
    <script>swal({type: 'success', title: 'success', text: 'The form has been successfully saved', timer: 3000});</script>
<?php } else if ($this->session->flashdata('error_msg')) { ?>
    <script>swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000});</script>
<?php } ?>
<script>
    $(document).ready(function () {
        $('#ch3_client').select2();
        
        //----------------------------------------------------------------------
        
        $.validate({
            form: '#coach_frm_3',
            modules: 'security,date'
        });

        //----------------------------------------------------------------------
        
        $('#ch3_st_time').mask('00:00', {placeholder: "Start Time"});
        $('#ch3_ed_time').mask('00:00', {placeholder: "End Time"});

        $('#ch3_next_date').mask('00/00/0000', {placeholder: "When is the date/s confirmed for the next engagements DD/MM/YYYY"});

        //----------------------------------------------------------------------
        
        $('#ch3_next_date').keyup(function () {
            if (e.keyCode == 8 || e.keyCode == 46) {
                $(this).attr("data-validation-optional", "true");
            } else {
                $(this).removeAttr('data-validation-optional');
            }
        });

        //----------------------------------------------------------------------
        
        $('#ch3_place').change(function () {
            $('#start_time').val(new Date());
        });

//        $('#frm_submit').on('click', function () {
//            swal({
//                title: 'Do you want submit this form?',
//                confirmButtonText: 'Submit',
//                html: '<span style="color:red;">You cannot edit this form after submit</span>',
//                showLoaderOnConfirm: true,
//                showCancelButton: true,
//                preConfirm: function () {
//                    return new Promise(function (resolve) {
//                        $.ajax({
//                            type: 'POST',
//                            url: '<?php echo base_url('submit_form/'.$frm_type.'/'.$frm_id); ?>',
//                        })
//                                .done(function (response) {
//                                    var rslt = JSON.parse(response);
//                                    console.log(rslt);
//                                    if (rslt) {
//                                        swal({type: 'success', title: 'success', text: 'The form has been submited', timer: 3000}).catch(function (timeout) { });
//                                        window.setTimeout(function () {
//                                            location.href = '<?php echo base_url('submited_coach_form') ?>';
//                                        }, 3000);
//                                    } else {
//                                        resolve()
//                                        //swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000}) 
//                                    }
//                                })
//                                .fail(function () {
//                                    swal('Oops...', 'Something went wrong !', 'error');
//                                });
//                    })
//                }
//            }).catch(swal.noop);
//        });

        //----------------------------------------------------------------------
        
        $('#chf3_submit').on('click', function (e) {
            e.preventDefault();
            if($('#coach_frm_3').isValid()){
                swal({
                    title: 'Do you want submit this form?',
                    confirmButtonText: 'Submit',
                    html: '<span style="color:red;">You cannot edit this form after submit</span>',
                    showLoaderOnConfirm: true,
                    showCancelButton: true
                }).then((result) => {
                    $("#button_type").val('frm_submit');
                    $("#coach_frm_3").submit();
                });
            }
        });
    });
</script>
