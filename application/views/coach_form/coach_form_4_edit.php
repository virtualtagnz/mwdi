<?php
$frm_id = end($this->uri->segments);
$secondLastKey = count($this->uri->segment_array())-1;
$frm_type = $this->uri->segment($secondLastKey);
?>
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <a href="<?php echo base_url('view_single_form/' . $frm_type . '/' . $frm_id); ?>" class="btn btn-primary pull-right"><i class="fa fa-print"></i> &nbsp; Print Form</a>
                Forms <i class="lnr lnr-chevron-right"></i> Client Form
            </h3>
            <?php //var_dump($details);?>
            <div class="panel">
                <div class="panel-body">
                    <form action="<?php echo base_url('edit_coach_form_4/'.end($this->uri->segments)); ?>" method="post" name="coach_frm_4_edit" id="coach_frm_4_edit" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <h4>ENGAGEMENT <?php echo $details->frequency?> (Building on the Relationship)</h4>
                            </div>
                            <input type="hidden" name="start_time" id="start_time" value="<?php echo set_value('start_time'); ?>">
                            <input type="hidden" name="main_form_id" id="main_form_id" value="<?php echo set_value('main_form_id'); ?>">
                            <input type="hidden" name="sub_stat" id="sub_stat" value="<?php echo $details->is_submit?>">
                            <input type="hidden" name="eng_freq" id="eng_freq" value="<?php echo $details->frequency?>">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Client Name</label>
                                    <input type="text" class="form-control" id="ch3_place" value="<?php echo $details->first_name . ' ' . $details->last_name; ?>" readonly="true">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Where did you meet?</label>
                                    <input type="text" class="form-control" id="ch4_place" name="ch4_place" placeholder="Where did you meet?" data-validation="required" data-validation-error-msg="The field where did you meet is mandatory, please check again!" value="<?php echo $details->chp4_f52; ?>">
                                    <div id="infoMessage"><?php echo form_error('ch4_place'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Start Time</label>
                                    <input type="text" class="form-control" id="ch4_st_time" name="ch4_st_time" placeholder="Start Time" data-validation="required time" data-validation-help="HH:mm" data-validation-error-msg-required="The field start time is mandatory, please check again!" data-validation-error-msg-time="You have not given a correct time" value="<?php echo $details->chp4_f53; ?>">
                                    <div id="infoMessage"><?php echo form_error('ch4_st_time'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>End Time</label>
                                    <input type="text" class="form-control" id="ch4_ed_time" name="ch4_ed_time" placeholder="End Time" data-validation="required time" data-validation-error-msg-required="The field end time is mandatory, please check again!" data-validation-error-msg-time="You have not given a correct time" value="<?php echo $details->chp4_f54; ?>">
                                    <div id="infoMessage"><?php echo form_error('ch4_ed_time'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Provide your general overview of the Client, any key issues/findings/areas of importance</label>
                                    <textarea class="form-control" name="ch4_oview" id="ch4_oview" placeholder="Provide your general overview of the Client, any key issues/findings/areas of importance" rows="3" data-validation="required" data-validation-error-msg="The field general overview is mandatory, please check again!"><?php echo $details->chp4_f55; ?></textarea>
                                    <div id="infoMessage"><?php echo form_error('ch4_oview'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>What are your key actions from this engagement</label>
                                    <textarea class="form-control" name="ch4_actions" id="ch4_actions" placeholder="What are your key actions from this engagement" rows="3"><?php echo $details->chp4_f56; ?></textarea>
                                    <div id="infoMessage"><?php echo form_error('ch4_actions'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Has the client filled in all required information from the last engagement session</label>
                                    <select class="form-control" id="lst_eng" name="lst_eng">
                                        <option value="" selected="selected">- Please Select -</option>
                                        <option value="1" <?php echo $details->chp4_f57 == '1' ? 'selected' : ''; ?>>Yes</option>
                                        <option value="2" <?php echo $details->chp4_f57 == '2' ? 'selected' : ''; ?>>No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <hr>
                                <h4>Loan Questions if applicable</h4>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>New clients: Is the client proceeding with an application, if not, please note reasons</label>
                                    <textarea class="form-control" name="ch4_lq_1" id="ch4_lq_1" placeholder="New clients: Is the client proceeding with an application, if not, please note reasons" rows="3"><?php echo $details->chp4_f58; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Existing clients Would the client consider increased repayments to MWDI or looked at ways to pay off loan faster? If so how much/by when</label>
                                    <textarea class="form-control" name="ch4_lq_2" id="ch4_lq_2" placeholder="Existing clients Would the client consider increased repayments to MWDI or looked at ways to pay off loan faster? If so how much/by when" rows="3"><?php echo $details->chp4_f59; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>What information has been provided to the client (ie Business Plan templates/Financial Plan Templates, Links to information)</label>
                                    <textarea class="form-control" name="ch4_lq_3" id="ch4_lq_3" placeholder="What information has been provided to the client (ie Business Plan templates/Financial Plan Templates, Links to information)" rows="3"><?php echo $details->chp4_f60; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>What actions has the client taken since the last engagement</label>
                                    <textarea class="form-control" name="ch4_lq_4" id="ch4_lq_4" placeholder="What actions has the client taken since the last engagement" rows="3"><?php echo $details->chp4_f61; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>What challenges are still facing this client</label>
                                    <textarea class="form-control" name="ch4_lq_5" id="ch4_lq_5" placeholder="What challenges are still facing this client" rows="3"><?php echo $details->chp4_f62; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>What resources/support does this client require</label>
                                    <textarea class="form-control" name="ch4_lq_6" id="ch4_lq_6" placeholder="What resources/support does this client require" rows="3"><?php echo $details->chp4_f63; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>What would support this client to grow and expand their business</label>
                                    <textarea class="form-control" name="ch4_lq_7" id="ch4_lq_7" placeholder="What would support this client to grow and expand their business" rows="3"><?php echo $details->chp4_f64; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Key highlights/Breakthroughs</label>
                                    <textarea class="form-control" name="ch4_lq_8" id="ch4_lq_8" placeholder="Key highlights/Breakthroughs" rows="3"><?php echo $details->chp4_f65; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <hr>
                                <h4>Financial Literacy</h4>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>What are the key financial issues impacting the client at a personal level</label>
                                    <textarea class="form-control" name="ch4_ft_1" id="ch4_ft_1" placeholder="What are the key financial issues impacting the client at a personal level" rows="3"><?php echo $details->chp4_f66; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>What are the key financial issues impacting the client at a business level</label>
                                    <textarea class="form-control" name="ch4_ft_2" id="ch4_ft_2" placeholder="What are the key financial issues impacting the client at a business level" rows="3"><?php echo $details->chp4_f67; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>What topics did you explore together</label>
                                    <textarea class="form-control" name="ch4_ft_3" id="ch4_ft_3" placeholder="What topics did you explore together" rows="3"><?php echo $details->chp4_f68; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="ch4_nxt_app" id="ch4_nxt_app" value="Yes" <?php echo $details->chp4_f69 == 'Yes' ? 'checked' : ''; ?>>
                                    <span><strong>Yes</strong> - Email confirmation sent for next appointment</span>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>When is the date confirmed for the next engagements</label>
                                    <input type="text" class="form-control" id="ch4_next_date" name="ch4_next_date" placeholder="When is the date/s confirmed for the next engagements" data-validation="date" data-validation-format="dd/mm/yyyy"  data-validation-optional="true" value="<?php echo $details->chp4_f70; ?>">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Any points for follow up from other Coaches</label>
                                    <textarea class="form-control" name="ch4_flw_other" id="ch4_flw_other" placeholder="Any points for follow up from other Coaches" rows="3"><?php echo $details->chp4_f71; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="ch4_app_chk" id="ch4_app_chk" value="Yes" <?php echo $details->chp4_f72 == 'Yes' ? 'checked' : ''; ?>>
                                    <span><strong>Yes</strong> - New Clients Application form checked that all relevant details have been completed, everything is signed ready to send to MWDI for processing</span>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="ch3_invo_sent" id="ch4_nxt_app" value="Yes" <?php echo $details->chp4_f73 == 'Yes' ? 'checked' : ''; ?>>
                                    <span><strong>Yes</strong> - Invoice sent by the 15th of the month to accountspayable@mwdi.co.nz including Kilometres and any costs incurred</span>
                                </label>
                            </div>
                            <div class="col-md-12" style="padding-top: 20px; padding-bottom: 20px;">
                                <a href="javascript:history.back()" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; BACK</a> &nbsp; 
                                <button type="submit" class="btn btn-default" name="btnaction" value="frm_edit" id="chf4_edit_save">SAVE FORM &nbsp; <i class="fa fa-save"></i></button> &nbsp;
                                <?php if($details->is_submit=='N'){?>
                                <button type="submit" class="btn btn-primary" name="btnaction" value="frm_submit" id="chf4_edit_submit">SUBMIT</button>
                                <?php }?>
                                <input type="hidden" name="button_type" id="button_type" value="frm_edit">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<?php if ($this->session->flashdata('create_success')) { ?>
    <script>swal({type: 'success', title: 'success', text: 'The form has been successfully saved', timer: 3000});</script>
<?php } else if ($this->session->flashdata('error_msg')) { ?>
    <script>swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000});</script>
<?php } ?>
<script>
    $(document).ready(function () {
        $('#ch4_client').select2();

        //----------------------------------------------------------------------
        
        $.validate({
            form: '#coach_frm_4_edit',
            modules: 'security,date'
        });

        //----------------------------------------------------------------------
        
        $('#ch4_st_time').mask('00:00', {placeholder: "Start Time"});
        $('#ch4_ed_time').mask('00:00', {placeholder: "End Time"});

        $('#ch4_next_date').mask('00/00/0000', {placeholder: "When is the date/s confirmed for the next engagements DD/MM/YYYY"});

        $('#ch4_next_date').keyup(function () {
            if (e.keyCode == 8 || e.keyCode == 46) {
                $(this).attr("data-validation-optional", "true");
            } else {
                $(this).removeAttr('data-validation-optional');
            }
        });

        //----------------------------------------------------------------------
        
        $('#ch4_client').change(function () {
            $('#start_time').val(new Date());
        });
        
        //----------------------------------------------------------------------
        
//        $('#ch4_submit').on('click', function () {
//            swal({
//                title: 'Do you want submit this form?',
//                confirmButtonText: 'Submit',
//                html: '<span style="color:red;">You cannot edit this form after submit</span>',
//                showLoaderOnConfirm: true,
//                showCancelButton: true,
//                preConfirm: function () {
//                    return new Promise(function (resolve) {
//                        $.ajax({
//                            type: 'POST',
//                            url: '<?php echo base_url('submit_form/'.$frm_type.'/'.$frm_id); ?>',
//                        })
//                                .done(function (response) {
//                                    var rslt = JSON.parse(response);
//                                    console.log(rslt);
//                                    if (rslt) {
//                                        swal({type: 'success', title: 'success', text: 'The form has been submited', timer: 3000}).catch(function (timeout) { });
//                                        window.setTimeout(function () {
//                                            location.href = '<?php echo base_url('submited_coach_form') ?>';
//                                        }, 3000);
//                                    } else {
//                                        resolve()
//                                        //swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000}) 
//                                    }
//                                })
//                                .fail(function () {
//                                    swal('Oops...', 'Something went wrong !', 'error');
//                                });
//                    })
//                }
//            }).catch(swal.noop);
//        });

        //----------------------------------------------------------------------
        
        $('#chf4_edit_submit').on('click', function (e) {
            e.preventDefault();
            if($('#coach_frm_4_edit').isValid()){
                swal({
                    title: 'Do you want submit this form?',
                    confirmButtonText: 'Submit',
                    html: '<span style="color:red;">You cannot edit this form after submit</span>',
                    showLoaderOnConfirm: true,
                    showCancelButton: true
                }).then((result) => {
                    $("#button_type").val('frm_submit');
                    $("#coach_frm_4_edit").submit();
                });
            }
        });

    });
</script>