<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                File Attachment <i class="lnr lnr-chevron-right"></i> New Attachment
            </h3>
            <div class="panel">
                <?php //var_dump($clients) ?>
                <div class="panel-body">
                    <form action="<?php echo base_url('attach_file') ?>" method="post" name="file_attachment_form" id="file_attachment_form" enctype="multipart/form-data"  style="padding-top: 20px;">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Client Name</label>
                                    <?php if ($this->session->userdata('role') == '5') { ?>
                                        <input type="text" class="form-control" id="fa_client_name" name="fa_client_name" placeholder="Client Name" value="<?php echo $this->session->userdata('full_name') ?>" readonly="true">
                                    <?php } else { ?>
                                        <select class="form-control" name="fa_client" id="fa_client" data-validation="required" data-validation-error-msg="The field client name is mandatory, please check again!">
                                            <option value="" selected="selected">Client Name</option>
                                            <?php if ($clients) { ?>
                                                <?php $ext_users = json_decode($clients) ?>
                                                <?php foreach ($ext_users as $user) { ?>
                                                    <option value="<?php echo $this->encrypt->encode($user->client_id) ?>"><?php echo $user->first_name . ' ' . $user->last_name ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    <?php } ?>
                                    <div id="infoMessage"><?php echo form_error('fa_client'); ?></div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>File Name</label>
                                    <input type="text" class="form-control" id="fa_name" name="fa_name" placeholder="File Name" data-validation="required" data-validation-error-msg="The field file name is mandatory, please check again!" value="<?php echo set_value('fa_name'); ?>">
                                    <div id="infoMessage"><?php echo form_error('fa_name'); ?></div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>File Description</label>
                                    <textarea class="form-control" name="fa_des" id="fa_des" placeholder="File Description" rows="4" data-validation-error-msg="The field file description is mandatory, please check again!"><?php echo set_value('fa_des'); ?></textarea>
                                    <div id="infoMessage"><?php echo form_error('fa_des'); ?></div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <i class="fa fa-info-circle pull-right" style="margin-top: 10px;" data-toggle="tooltip" data-placement="top" title="Maximum size 5 MB"></i>
                                    <label for="fa_file" class="filupp form-control" style="width: 98%;">
                                        <span class="filupp-file-name js-value">File Attachment</span>
                                        <input type="file" name="fa_file" id="fa_file" accept=".xlsx,.xls,.doc, .docx,.txt,.pdf,.jpg,.png,.jpeg" data-validation="required mime size" data-validation-allowing="application/pdf, application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel,text/plain, image/jpeg, image/png" data-validation-max-size="5M" data-validation-error-msg-required="The field file is mandatory, please check again!" data-validation-error-msg-size="You can not upload files larger than 5MB" data-validation-error-msg-mime="The filetype you are attempting to upload is not allowed">
                                    </label>
                                    <div id="infoMessage"><?php echo form_error('fa_file'); ?></div>
                                </div>
                            </div>
                        </div>

                        <p>
                            <a href="javascript: history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; Back</a> &nbsp; 
                            <button type="submit" class="btn btn-primary">SUBMIT</button>
                        </p>
                    </form>
                    <br>
                </div>
            </div>
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable js-pointstable display" id="my_files_tab">
                            <thead>
                                <tr>
                                    <th>File Name</th>
                                    <th>File Description</th>
                                    <th>Create Date</th>
                                    <th>File Id</th>
                                    <th>View</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<?php if ($this->session->flashdata('create_success')) { ?>
    <script>swal({type: 'success', title: 'success', text: 'The form has been successfully saved', timer: 3000});</script>
<?php } else if ($this->session->flashdata('img_error')) { ?>
    <script>swal({type: 'error', title: 'File Upload Error', html: '<?php echo $this->session->flashdata('img_error'); ?>', timer: 3000});</script>
<?php } ?>
<script type="text/javascript">
    $(document).ready(function () {
        var table;
        var base_url = '<?php echo base_url('uploads/file_attachments') ?>';
        var user_type = '<?php echo $this->session->userdata('role') ?>';
        $('#fa_client').select2();
        $('#fa_client').on("select2:select", function (e) {
            $('#fa_client').validate({validateHiddenInputs: false});//Selected - Hide errors message
        });

        //--------------------------------------------------------------------------

        $.validate({
            form: '#file_attachment_form',
            modules: 'security,date,file'
        });

        //----------------------------------------------------------------------

        function create_datatable(data) {
            table = $('#my_files_tab').DataTable({
                "destroy": true,
                "aaData": $.parseJSON(data),
                "aoColumns": [
                    {"mData": "file_name"},
                    {"mData": "file_description"},
                    {"mData": "create_date"},
                    {"mData": "uid", "visible": false},
                    {"mData": "", "mRender": function (data, type, row) {
                            return '<a href="' + base_url + '/' + row.client_id + '/' + row.file_id + '" class="btn btn-fefault" data-toggle="tooltip" data-placement="top" title="View" target="blank"><i class="fa fa-eye"></i></a> &nbsp'
                                    + '<a href="' + base_url + '/' + row.client_id + '/' + row.file_id + '" class="btn btn-fefault" title="Download" data-toggle="tooltip" data-placement="top" download><i class="fa fa-download"></i></a> &nbsp;'
                                    + '<a id="' + row.uid + '" class="btn btn-fefault btn_delete" data-toggle="tooltip" data-placement="top" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></a>';
                        }}
                ]
            });
        }

        //----------------------------------------------------------------------

        if (user_type == '5') {
            var user_id = '<?php echo $this->encrypt->encode($this->session->userdata('user_level_id')); ?>'
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('load_my_files/'); ?>" + user_id,
                success: function (results) {
                    console.log(results);
                    create_datatable(results);
                }
            });
        }

        //----------------------------------------------------------------------

        $('#fa_client').on('change', function () {
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('load_my_files/'); ?>" + $(this).val(),
                success: function (results) {
                    console.log(results);
                    create_datatable(results);
                }
            });
        });

        //----------------------------------------------------------------------

        $('#my_files_tab').on('click', 'a.btn_delete', function () {
            var row_obj = table.row($(this).parents('tr')).data();
            var row_index = table.row($(this).parents('tr')).index();
            console.log(row_index);
            swal({
                title: 'Do you want delete this file?',
                confirmButtonText: 'Delete',
                showLoaderOnConfirm: true,
                showCancelButton: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajax({
                            type: 'POST',
                            url: "<?php echo base_url('delete_file'); ?>",
                            data: {'file_id': row_obj.uid, 'file_link': row_obj.file_id, 'cid': row_obj.client_id},
                        }).done(function (response) {
                            if (response == '1') {
                                //swal({type: 'success', title: 'Success!', text: 'The file has been deleted', timer: 3000})
                                table.row(row_index).remove().draw(false);
                            } else {
                                swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000})
                            }
                            resolve();
                        });
                    });
                }
            }).catch(swal.noop);
        });


    });
</script>
