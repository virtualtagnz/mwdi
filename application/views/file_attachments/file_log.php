
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <a href="<?php echo base_url('new_attachment');?>" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> &nbsp; New Attachment</a>
                File Attachment <i class="lnr lnr-chevron-right"></i> Files
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable js-pointstable display">
                            <thead>
                                <tr>
                                    <th>Client Name</th>
                                    <th>File Name</th>
                                    <th>Created By</th>
                                    <th>Created Date</th>
                                    <th>Deleted By</th>
                                    <th>Deleted Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($files_info) { ?>
                                    <?php $files = json_decode($files_info); ?>
                                    <?php foreach ($files as $file) { ?>
                                    <?php //var_dump($file) ?>
                                        <tr>
                                            <td><?php echo $file->first_name.' '.$file->last_name?></td>
                                            <td>
                                                <?php if($file->is_deleted=='N'){?>
                                                    <a href="<?php echo base_url('uploads/file_attachments/'.$file->client_id.'/'.$file->file_id)?>" target="blank"><?php echo $file->file_name?></a>
                                                <?php } else {?>
                                                    <?php echo $file->file_name?>
                                                <?php } ?>
                                            </td>
                                            <td><?php echo $file->created_by?></td>
                                            <td><?php echo $file->create_date?></td>
                                            <td><?php echo  $file->is_deleted=='N'?'-':$file->delete_by?></td>
                                            <td><?php echo  $file->is_deleted=='N'?'-':$file->delete_date?></td>
                                            <td><?php echo $file->is_deleted=='N'?'Upload':'Deleted'?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<?php if ($this->session->flashdata('success_msg')) { ?>
    <script>swal({type: 'success', title: 'Success', text: 'The file has been successfully attached', timer: 3000});</script>
<?php } else if ($this->session->flashdata('error_msg')) { ?>
    <script>swal({type: 'error', title: 'File Upload Error', html: '<?php echo $this->session->flashdata('img_error');?>', timer: 3000});</script>
<?php } ?>
<script type="text/javascript">
    $(function () {
        $('.js-pointstable').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            aaSorting: [[3, "desc"]]
        });
    });
</script>

</body>
</html>