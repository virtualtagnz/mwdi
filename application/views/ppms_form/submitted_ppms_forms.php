<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                Forms <i class="lnr lnr-chevron-right"></i> Professional Payment Management Service
            </h3>
            <?php //var_dump($ppms_submitted); ?>
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="archive_frm_tab" class="table table-striped table-bordered dataTable js-pointstable display">
                            <thead>
                                <tr>
                                    <th>Client Id</th>
                                    <th>Client Name</th>
                                    <th>Status</th>
                                    <th>Edit/ View</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($ppms_submitted) { ?>
                                    <?php $submitted_ppms = json_decode($ppms_submitted) ?>
                                    <?php foreach ($submitted_ppms as $ppms) { ?>
                                        <tr>
                                            <td><?php echo $ppms->ppms_client_ref; ?></td>
                                            <td><?php echo $ppms->first_name . ' ' . $ppms->last_name; ?></td>
                                            <td><?php echo $ppms->sub_stat == 'Y' ? 'Submitted' : 'Draft'; ?></td>
                                            <td>
                                                <?php if ($ppms->sub_stat == 'Y') { ?>
                                                    <a href="<?php echo base_url('uploads/ppms_forms/' . $ppms->file_link); ?>" target="blank" class="btn btn-fefault" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="fa fa-eye"></i></a>
                                                <?php } else { ?>
                                                    PPMS Form     
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<div id="modalView"></div>
<script>
    $(document).ready(function () {
        $('#archive_frm_tab').dataTable({
            "pageLength": 10,
            "searching": true,
            "info": true,
            "ordering": true
        });

        $('body').delegate('.view_form', 'click', (function () {
            var formType = $(this).get(0).rel;
            var id = $(this).get(0).id;
            console.log(id);
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('view_single_form/') ?>" + formType + '/' + id,
                success: function (results) {
                    //console.log(results);
                    switch (formType) {
                        case '5':
                            $('#modalView').html(results);
                            $('#coach_form_1_modal').modal('show');
                            break;
                    }
                }
            })
        }));
    });
</script>
