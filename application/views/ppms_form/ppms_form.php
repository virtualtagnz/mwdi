<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <span id="dln_btn" class="pull-right"><i class="fa fa-info-circle" style="font-size: 15px;" data-toggle="tooltip" data-placement="top" title="Once you have downloaded the PPMS form, complete all details, upload and submit"></i> &nbsp; <a href="<?php echo base_url('downloads/PPMS_Disclosure.pdf') ?>" target="blank" download class="btn btn-primary"><i class="fa fa-download"></i> &nbsp; Download PPMS Form</a></span>
                Forms <i class="lnr lnr-chevron-right"></i> Create <i class="lnr lnr-chevron-right"></i> Professional Payment Management Service
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <form action="<?php echo base_url('upload_ppms') ?>" method="post" name="ppms_form" id="ppms_form" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <h4>PPMS</h4>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="custom-file-upload" class="filupp form-control">
                                        <span class="filupp-file-name ppms_form"> Upload Signed PPMS Form (PDF)</span>
                                        <input type="file" name="custom-file-upload" value="" id="custom-file-upload" accept=".pdf" data-validation="mime size required" data-validation-allowing="pdf" data-validation-max-size="5M" data-validation-error-msg-size="You can not upload files larger than 5MB" data-validation-error-msg-mime="The filetype you are attempting to upload is not allowed" data-validation-error-msg-required="The field is mandatory, please check again!">
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12" style="padding-top: 20px; padding-bottom: 20px;">
                                <a href="javascript:history.back()" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; BACK</a> &nbsp; 
                                <button type="submit" class="btn btn-primary">UPLOAD &nbsp; <i class="fa fa-upload"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<?php if ($this->session->flashdata('no_result_found')) { ?>
    <script>swal({type: 'error', title: 'Error!', text: 'Please download PPMS form and upload again', timer: 3000});</script>
<?php } elseif ($this->session->flashdata('file_upload_error')) { ?>
    <script>swal({type: 'error', title: 'Error!', text: '<?php echo $this->session->flashdata('file_upload_error')?>', timer: 3000});</script>
<?php } elseif ($this->session->flashdata('upload_success')) { ?>
    <script>swal({type: 'success', title: 'Success!', text: 'The PPMS Form has been successfully uploaded', timer: 3000});</script>
<?php } elseif ($this->session->flashdata('error_msg')) { ?>
    <script>swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!', timer: 3000});</script>    
<?php } ?>
<script>
    $(document).ready(function () {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('is_submitted'); ?>",
            success: function (results) {
                console.log(results)
                if (results > 0) {
                    swal({
                        title: "Warning!",
                        text: "You have submitted PPMS form",
                        type: "warning"
                    }).then(function () {
                        location.href="<?php echo base_url('client_forms') ?>"
                    });
                }
            }
        });

        $('#dln_btn').on('click', function (e) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('create_ppms'); ?>",
                success: function (results) {
                    console.log(results)
                }
            });
        });

        $.validate({
            form: '#ppms_form',
            modules: 'security,file'
        });
        
        $('input[type="file"]').change(function () {
            $('.ppms_form').text(this.files[0].name);
        });
    });
</script>
