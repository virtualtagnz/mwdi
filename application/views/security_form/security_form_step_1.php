<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <a href="<?php echo base_url('view_security_form/' . end($this->uri->segments));?>" target="blank" class="btn btn-primary pull-right"><i class="fa fa-print"></i> &nbsp; Print Form</a>
                Forms <i class="lnr lnr-chevron-right"></i> Create <i class="lnr lnr-chevron-right"></i> Instrument by Way of Security
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <form action="<?php echo base_url('save_security_form_step1/' . end($this->uri->segments)); ?>" method="post" name="sec_form1" id="sec_form1" enctype="multipart/form-data">
                        <input type="hidden" name="start_time" id="start_time" value="<?php echo set_value('start_time'); ?>">
                        <div class="row">
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <h4>DETAILS OF THE SECURITY: CHATTELS</h4>
                            </div>
                            <div class="col-md-4">
                                <!--                                <div class="form-group">-->
                                <label>Schedule of Business Property</label>
                                <label for="sec1_file_1" class="filupp form-control">
                                    <span class="filupp-file-name sec1_file_1_name">Upload</span>
                                    <input type="file" name="sec1_file_1" id="sec1_file_1" accept=".xlsx,.xls,.doc,.docx,.txt,.pdf" data-validation="size" data-validation-max-size="10M" data-validation-error-msg-size="Maximum upload size (10MB) exceeded, please try again!">
                                </label>
                                <!--                                </div>-->
                            </div>
                            <!--custom-file-upload-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Schedule of Personal Property</label>
                                    <label for="sec1_file_2" class="filupp form-control">
                                        <span class="filupp-file-name sec1_file_2_name">Upload</span>
                                        <input type="file" name="sec1_file_2" id="sec1_file_2" accept=".xlsx,.xls,.doc,.docx,.txt,.pdf" data-validation="size" data-validation-max-size="10M" data-validation-error-msg-size="Maximum upload size (10MB) exceeded, please try again!">
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Copy of Insurance Cover</label>
                                    <label for="sec1_file_3" class="filupp form-control">
                                        <span class="filupp-file-name sec1_file_3_name">Upload</span>
                                        <input type="file" name="sec1_file_3" id="sec1_file_3" accept=".xlsx,.xls,.doc,.docx,.txt,.pdf" data-validation="size" data-validation-max-size="10M" data-validation-error-msg-size="Maximum upload size (10MB) exceeded, please try again!">
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <hr>
                                <h4 style="margin-top: 30px;">MORTGAGE DETAILS &nbsp; <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Upload a copy of all supporting information in relation to property as your listed security.(refer to Business Criteria)."></i></h4>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Certificate of Title</label>
                                    <label for="sec1_file_4" class="filupp form-control">
                                        <span class="filupp-file-name sec1_file_4_name">Upload</span>
                                        <input type="file" name="sec1_file_4" value="" id="sec1_file_4" accept=".xlsx,.xls,.doc,.docx,.txt,.pdf" data-validation="size" data-validation-max-size="10M" data-validation-error-msg-size="Maximum upload size (10MB) exceeded, please try again!">
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Evidence of Equity</label>
                                    <label for="sec1_file_5" class="filupp form-control">
                                        <span class="filupp-file-name sec1_file_5_name">Upload</span>
                                        <input type="file" name="sec1_file_5" value="" id="sec1_file_5" accept=".xlsx,.xls,.doc,.docx,.txt,.pdf" data-validation="size" data-validation-max-size="10M" data-validation-error-msg-size="Maximum upload size (10MB) exceeded, please try again!">
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Copy of Insurance Cover as per The Policy No</label>
                                    <label for="sec1_file_6" class="filupp form-control">
                                        <span class="filupp-file-name sec1_file_6_name">Upload</span>
                                        <input type="file" name="sec1_file_6" value="" id="sec1_file_6" accept=".xlsx,.xls,.doc,.docx,.txt,.pdf" data-validation="size" data-validation-max-size="10M" data-validation-error-msg-size="Maximum upload size (10MB) exceeded, please try again!">
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <hr>
                                <h4 style="margin-top: 30px;"> PERSONAL DETAILS &nbsp; <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="If you are not using a guarantor then enter your own personal details below.  Save your form before proceeding."></i></h4>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" class="form-control" id="sec1_cline_name" name="sec1_cline_name" placeholder="Name" data-validation="required" data-validation-error-msg="The field name is mandatory, please check again!" value="<?php echo set_value('sec1_cline_name'); ?>">
                                    <div id="infoMessage"><?php echo form_error('sec1_cline_name'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Birthday</label>
                                    <input type="text" class="form-control date" id="sec1_dob" name="sec1_dob" placeholder="Birthday (mm/dd/yyyy)" data-validation="required birthdate" data-validation-error-msg-required="The field date of birth is mandatory, please check again!" data-validation-format="dd/mm/yyyy" data-validation-error-msg-birthdate="Invalid Date (dd/mm/yyyy)" value="<?php echo set_value('sec1_dob'); ?>">
                                    <div id="infoMessage"><?php echo form_error('sec1_dob'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Client No</label>
                                    <input type="text" class="form-control" id="sec1_client_no" name="sec1_client_no" placeholder="Client No" value="<?php echo set_value('sec1_client_no'); ?>">
                                    <div id="infoMessage"><?php echo form_error('sec1_client_no'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Business Name</label>
                                    <input type="text" class="form-control" id="sec1_business_name" name="sec1_business_name" placeholder="Business Name" data-validation="required" data-validation-error-msg="The field business name is mandatory, please check again!" value="<?php echo set_value('sec1_business_name'); ?>">
                                    <div id="infoMessage"><?php echo form_error('sec1_business_name'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Address</label>
                                    <input type="text" class="form-control autocomplete" id="sec1_address" name="sec1_address" placeholder="Address" data-validation="required" data-validation-error-msg="The field address is mandatory, please check again!" value="<?php echo set_value('sec1_address'); ?>">
                                    <div id="infoMessage"><?php echo form_error('sec1_address'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Business Address</label>
                                    <input type="text" class="form-control autocomplete" id="sec1_business_address" name="sec1_business_address" placeholder="Business Address" data-validation="required" data-validation-error-msg="The field business address is mandatory, please check again!" value="<?php echo set_value('sec1_business_address'); ?>">
                                    <div id="infoMessage"><?php echo form_error('sec1_business_address'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Home Telephone</label>
                                    <input type="text" class="form-control phone" id="sec1_home_tel" name="sec1_home_tel" placeholder="Home Telephone" maxlength="20" data-validation="number" data-validation-optional="true" value="<?php echo set_value('sec1_home_tel'); ?>">
                                    <div id="infoMessage"><?php echo form_error('sec1_home_tel'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Business Telephone</label>
                                    <input type="text" class="form-control phone" id="sec1_business_tel" name="sec1_business_tel" placeholder="Business Telephone" data-validation="number" data-validation-optional="true" maxlength="20" value="<?php echo set_value('sec1_business_tel'); ?>">
                                    <div id="infoMessage"><?php echo form_error('sec1_business_tel'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Mobile</label>
                                    <input type="text" class="form-control phone" id="sec1_mobile" name="sec1_mobile" placeholder="Mobile" maxlength="20" data-validation="number" data-validation-optional="true" value="<?php echo set_value('sec1_mobile'); ?>">
                                    <div id="infoMessage"><?php echo form_error('sec1_mobile'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" class="form-control" id="sec1_email" name="sec1_email" placeholder="Email" data-validation="email" data-validation-error-msg="You have not given a correct e-mail address" data-validation-optional="true" value="<?php echo set_value('sec1_email'); ?>">
                                    <div id="infoMessage"><?php echo form_error('sec1_email'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Facsimile</label>
                                    <label for="sec1_file_7" class="filupp form-control">
                                        <span class="filupp-file-name sec1_file_7_name">Upload</span>
                                        <input type="file" name="sec1_file_7" id="sec1_file_7" accept=".xlsx,.xls,.doc,.docx,.txt,.pdf" data-validation="size" data-validation-max-size="10M" data-validation-error-msg-size="Maximum upload size (10MB) exceeded, please try again!">
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12" style="padding-top: 20px; padding-bottom: 20px;">
                                <a href="javascript:history.back()" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; BACK</a> &nbsp; 
                                <button type="submit" class="btn btn-default" name="btnaction" value="save">SAVE FORM &nbsp; <i class="fa fa-save"></i></button> &nbsp;
                                <button type="submit" class="btn btn-primary" name="btnaction" value="next">NEXT &nbsp; <i class="fa fa-arrow-right"></i></button>
                                &nbsp; [1/4]
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<?php if ($this->session->flashdata('pdf_error')) { ?>
    <script>swal({type: 'error', title: 'PDF Error', text: 'No information to print', timer: 3000});</script>
<?php } ?>
<script>
    $(document).ready(function () {
        $("#sec_form1 :input").change(function () {
            if (!$("#start_time").val()) {
               $('#start_time').val(timer());
            }
        });

        $.validate({
            form: '#sec_form1',
            modules: 'security,date,file'
        });

        $('#sec1_dob').mask('00/00/0000', {placeholder: "Birthday (DD/MM/YYYY)"});
        $('#sec1_home_tel,#sec1_business_tel').mask('000000000000',{placeholder: "091234567"});
        $('#sec1_mobile').mask('000000000000',{placeholder: "0211234567"});
        $('#sec1_client_no').mask('000000000000',{placeholder: "Client No"});

        $('#sec1_home_tel,#sec1_business_tel,#sec1_mobile,#sec1_email').keyup(function (e) {
            if (e.keyCode == 8 || e.keyCode == 46) {
                $(this).attr("data-validation-optional", "true");
                $('#confirm_pwd').attr("data-validation-optional", "true");
            } else {
                $(this).removeAttr("data-validation-optional", "true");
                $('#confirm_pwd').removeAttr("data-validation-optional", "true");
            }

        });

        $('input[type="file"]').change(function () {
            for (var i = 0; i < this.files.length; i++)
            {
                switch ($(this).attr('id')) {
                    case 'sec1_file_1':
                        $('.sec1_file_1_name').text(this.files[i].name);
                        break;
                    case 'sec1_file_2':
                        $('.sec1_file_2_name').text(this.files[i].name);
                        break;
                    case 'sec1_file_3':
                        $('.sec1_file_3_name').text(this.files[i].name);
                        break;
                    case 'sec1_file_4':
                        $('.sec1_file_4_name').text(this.files[i].name);
                        break;
                    case 'sec1_file_5':
                        $('.sec1_file_5_name').text(this.files[i].name);
                        break;
                    case 'sec1_file_6':
                        $('.sec1_file_6_name').text(this.files[i].name);
                        break;
                    case 'sec1_file_7':
                        $('.sec1_file_7_name').text(this.files[i].name);
                        break;
                }
            }
        });

        var user_level = '<?php echo $this->session->userdata('role') ?>'
        if (user_level == '5') {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('sec_submit_status/' . end($this->uri->segments)); ?>",
                success: function (results) {
                    console.log(results)
                    if (results == 'Y') {
                        swal({
                            title: "Warning!",
                            text: "You have already submitted security form",
                            type: "warning"
                        }).then(function () {
                            location.href = "<?php echo base_url('client_forms') ?>"
                        });
                    }
                }
            });
        }

    });
</script>
<script>
    function initAutocomplete() {
        var acInputs = document.getElementsByClassName("autocomplete");
        for (var i = 0; i < acInputs.length; i++) {
            var autocomplete = new google.maps.places.Autocomplete(acInputs[i], {types: ['geocode'], componentRestrictions: {country: "nz"}});
            autocomplete.inputId = acInputs[i].id;
        }
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCU5pwsCrj8K5Sf7dvZ1fmq2VRfG4CMg64&libraries=places&callback=initAutocomplete"
async defer></script>