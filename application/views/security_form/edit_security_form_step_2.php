<?php if ($data) { ?>
    <?php $vehical_list = json_decode($data); ?>
<?php } ?>
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <a href="<?php echo base_url('view_security_form/' . end($this->uri->segments));?>" target="blank" class="btn btn-primary pull-right"><i class="fa fa-print"></i> &nbsp; Print Form</a>
                Forms <i class="lnr lnr-chevron-right"></i> Create <i class="lnr lnr-chevron-right"></i> Instrument by Way of  <i class="lnr lnr-chevron-right"></i> Item Info
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <form action="<?php echo base_url('edit_security_form_step2/' . end($this->uri->segments)) ?>" method="post" name="sec_form2" id="sec_form2">
                        <div class="row">
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <h4>MOTOR VEHICLES (No more than 5 years of age)</h4>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Item Description</label>
                                    <input type="text" class="form-control" id="sec2_des" name="sec2_des" placeholder="Item Description" data-validation="required" data-validation-error-msg="The field item description is mandatory, please check again!">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Make/Model/CT</label>
                                    <input type="text" class="form-control" id="sec2_model" name="sec2_model" placeholder="Make/Model/CT" data-validation="required" data-validation-error-msg="The field make/model is mandatory, please check again!">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Year</label>
                                    <select class="form-control" id="sec2_year" name="sec2_year" data-validation="required" data-validation-error-msg="The field year is mandatory, please check again!">
                                        <option disabled selected="selected">Year</option>
                                        <?php
                                            for($i=date('Y');$i>=2010;$i--) {
                                                echo '<option value="'.$i.'">'.$i.'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputAmount">Amount (in dollars)</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <input type="text" class="form-control" id="sec2_amt" placeholder="Amount" name="sec2_amt" data-validation="required" data-validation-error-msg="The field amount is mandatory, please check again!">
                                        <div class="input-group-addon">.00</div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <button type="button" class="btn btn-primary" id="set_table">SET VEHICLE DETAILS</button>
                                <hr style="margin-top: 35px;">

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered" id="sec2_tab">
                                        <thead>
                                            <tr>
                                                <th>Item Description</th>
                                                <th>Make/Model/CT</th>
                                                <th>Year</th>
                                                <th>Amount</th>
                                                <th>Delete</th>
                                            </tr>
                                        </thead>
                                        <?php if ($vehical_list) { ?>
                                            <tbody>
                                                <?php $i = 0; $amt = 0; ?>
                                                <?php foreach ($vehical_list as $vehical) { ?>
                                                    <tr>
                                                        <td><?php echo $vehical->vcl_item_des ?></td>
                                                        <td><?php echo $vehical->vcl_mod ?></td>
                                                        <td><?php echo $vehical->vcl_year ?></td>
                                                        <td><?php echo number_format($vehical->vcl_amt) . '.00' ?></td>
                                                        <td><a class="btn btn-fefault" id="<?php echo $vehical->vcl_id ?>"><i class="fa fa-trash"></i></a></td>
                                                    </tr>
                                                    <?php
                                                    $json_arr[$i]['col1'] = $vehical->vcl_item_des;
                                                    $json_arr[$i]['col2'] = $vehical->vcl_mod;
                                                    $json_arr[$i]['col3'] = $vehical->vcl_year;
                                                    $json_arr[$i]['col4'] = number_format($vehical->vcl_amt) . '.00';
                                                    $json_str = json_encode($json_arr);
                                                    $amt += $vehical->vcl_amt;
                                                    $i++;
                                                    ?>
                                                <?php } ?>
                                            </tbody>
                                        <?php } ?>
                                        <div id="infoMessage"><?php echo form_error('tab_validator'); ?></div>
                                    </table>
                                </div>
                                <input type="hidden" name="start_time" id="start_time" value="<?php echo $vehical_list[0]->vcl_start_date?$vehical_list[0]->vcl_start_date:date('Y-m-d H:i:s'); ?>">
                                <input type="hidden" name="end_time" id="end_time" value="<?php echo $vehical_list[0]->vcl_end_date?$vehical_list[0]->vcl_end_date:date('Y-m-d H:i:s'); ?>">
                                <div class="col-md-3 pull-right" style="margin-top: 10px;">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <input type="text" class="form-control" id="sec2_tot_amt" placeholder="Total" name="sec2_tot_amt" value="<?php echo $amt?>">
                                        <div class="input-group-addon">.00</div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <input type="hidden" name="json_str" id="json_str" value="">
                            <div class="col-md-12" style="padding-top: 20px; padding-bottom: 20px;">
                                <a href="<?php echo base_url('security_form/step1/' . end($this->uri->segments)) ?>" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; BACK</a> &nbsp;
                                <button type="submit" class="btn btn-default" name="btnaction" value="save">SAVE FORM &nbsp; <i class="fa fa-save"></i></button> &nbsp;
                                <button type="submit" class="btn btn-primary" name="btnaction" value="next">NEXT &nbsp; <i class="fa fa-arrow-right"></i></button>
                                &nbsp; [2/4]
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<?php if ($this->session->flashdata('create_success')) { ?>
    <script>swal({type: 'success', title: 'success', text: 'The form has been successfully saved', timer: 3000});</script>
<?php } else if ($this->session->flashdata('update_success')) { ?>
    <script>swal({type: 'success', title: 'success', text: 'The form has been successfully updated', timer: 3000});</script>
<?php } else if ($this->session->flashdata('error_msg')) { ?>
    <script>swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000});</script>
<?php } ?>
<script>
    $(document).ready(function () {
        $('#sec2_amt,#sec2_tot_amt').mask('000,000,000,000,000', {reverse: true});

        $('#json_str').val('<?php echo $json_str ?>');

        /*
         * Check main form availability
         */
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('is_main_form_available/' . end($this->uri->segments)); ?>",
            success: function (results) {
                if (results == '') {
                    swal({title: 'No main form',
                        text: "Plese fill Security form step 1",
                        type: 'error',
                        confirmButtonText: 'Go to my form section!',
                    }).then(function () {
                        location.href = '<?php echo base_url('my_forms/' . $this->encrypt->encode($this->session->userdata('user_level_id')) . '/' . $this->encrypt->encode($this->session->userdata('role'))) ?>'
                    });
                }
            }
        })
        /*
         * Create datatable
         */
        var tab = $('#sec2_tab').DataTable({
            "paging": false,
            "searching": false,
            "info": false
        });

        $('#set_table').click(function () {
            var c1 = $('#sec2_des').val();
            var c2 = $('#sec2_model').val();
            var c3 = $('#sec2_year option:selected').text();
            var c4 = $('#sec2_amt').val() + '.00';
            var c5 = '<a class="btn btn-fefault"><i class="fa fa-trash"></i></a>';
            if (c1.length !== 0 && c2.length !== 0 && c3.length !== 0 && c4.length !== 0) {
                tab.row.add([
                    c1,
                    c2,
                    c3,
                    c4,
                    c5
                ]).draw(true);
                createJson();
                clearTest();
                add_total(c4,'add');
                $('#infoMessage').empty();
            } else {
                //validations
                $.validate({
                    form: '#sec_form2',
                    modules: 'security'
                });
                $('#sec2_des,#sec2_model,#sec2_year,#sec2_amt').validate(); 
            }
        });

        /*
         * Create JSON
         */
        function createJson() {
            var rowData = tab.data();
            var jsonObj = [];
            for (var i = 0; i < tab.data().length; i++) {
                var item = {};
                item["col1"] = rowData[i][0];
                item["col2"] = rowData[i][1];
                item["col3"] = rowData[i][2];
                item["col4"] = rowData[i][3];
                jsonObj.push(item);
            }
            var json_str = JSON.stringify(jsonObj);
            console.log(json_str);
            //$('#json_str').val('');
            $('#json_str').val(json_str);
        }

        /*
         * Clear all test boxes
         */
        var clearTest = function () {
            $('#sec2_des').val('');
            $('#sec2_model').val('');
            $('#sec2_year').val($("#sec2_year option:first").val());
            $('#sec2_amt').val('');
        };

        $('#sec2_tab').on('click', 'a', function () {
            add_total(tab.row($(this).parents('tr')).data()[3],'sub');
            var id = $(this).get(0).id;
            console.log(id);
            if(id!=""){
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('delete_single_vehicle'); ?>",
                    data:{"vahicle_id":id},
                    success: function (results) {
                        console.log(results);
                    }
                });
            }
            tab.row($(this).parents('tr')).remove().draw(false);
            createJson();
        });
        
        /*
         * Add 
         */
        function add_total(amount,operator){
            var current_amt;
            if($('#sec2_tot_amt').val()==''){
                current_amt = '0';
            }else{
                current_amt = $('#sec2_tot_amt').val();
            } 
            var tot_amt;
            if(operator=='add'){
                tot_amt = Number(current_amt.replace(/,/g, ''))+Number(amount.replace(/,/g, ''));
            }else{
                tot_amt = Number(current_amt.replace(/,/g, ''))-Number(amount.replace(/,/g, ''));
            }
            $('#sec2_tot_amt').unmask().val();
            $('#sec2_tot_amt').val(tot_amt).mask('000,000,000,000,000', {reverse: true});
        }
        
        $('#sec_form2').on("submit", function (event) {
            $(this).find('*[data-validation]').attr("data-validation-optional","true");
        });


    });
</script>