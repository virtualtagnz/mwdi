<?php
$sec_form1_details = (json_decode($details));
?>
<style>
    td{
        border: 1px solid #9D8526;
        padding: 5px;
    }
    tr.bg{
        background-color: #252c35;
        color: #ffffff;
    }
    .title{
        text-align: center;
    }
</style>

<div class="title">
    <h2>Instrument By Way Of Security</h2>
</div>
<?php if ($sec_form1_details) { ?>
    <table cellspacing="0" cellpadding="5" border="1">
        <tr class="bg">
            <td colspan="2">
                <h4 class="">Details of the security : Chattels</h4>
            </td>
        </tr>
        <tr>
            <td>Schedule of Business Property</td><td><?php echo $sec_form1_details->sbp_link?'<a href="'.base_url('uploads/security_forms/'.$sec_form1_details->sbp_link).'">Schedule of Business Property</a>':'-' ?></td>
        </tr>
        <tr>
            <td>Schedule of Personal Property</td><td><?php echo $sec_form1_details->spp_link?'<a href="'.base_url('uploads/security_forms/'.$sec_form1_details->spp_link).'">Schedule of Personal Property</a>':'-' ?></td>
        </tr>
        <tr>
            <td>Copy of Insurance Cover</td><td><?php echo $sec_form1_details->cis_link?'<a href="'.base_url('uploads/security_forms/'.$sec_form1_details->cis_link).'">Copy of Insurance Cover</a>':'-' ?></td>
        </tr>
        <tr class="bg">
            <td colspan="2">
                <h4 class="">Mortgage Details</h4>
            </td>
        </tr>
        <tr>
            <td>Certificate of Title</td><td><?php echo $sec_form1_details->cot_link?'<a href="'.base_url('uploads/security_forms/'.$sec_form1_details->cot_link).'">Certificate of Title</a>':'-' ?></td>
        </tr>
        <tr>
            <td>Evidence of Equity</td><td><?php echo $sec_form1_details->eoe_link?'<a href="'.base_url('uploads/security_forms/'.$sec_form1_details->eoe_link).'">Evidence of Equity</a>':'-' ?></td>
        </tr>
        <tr>
            <td>Copy of Insurance Cover as per The Policy No</td><td><?php echo $sec_form1_details->cof_link?'<a href="'.base_url('uploads/security_forms/'.$sec_form1_details->cof_link).'">Copy of Insurance Cover</a>':'-' ?></td>
        </tr>
        <tr class="bg">
            <td colspan="2">
                <h4 class="">Personal Details</h4>
            </td>
        </tr>
        <tr>
            <td>Name</td><td><?php echo $sec_form1_details->sec_name;?></td>
        </tr>
        <tr>
            <td>Birthday</td><td><?php echo $sec_form1_details->sec_dob;?></td>
        </tr>
        <tr>
            <td>Client No</td><td><?php echo $sec_form1_details->sec_cli_no;?></td>
        </tr>
        <tr>
            <td>Business Name</td><td><?php echo $sec_form1_details->sec_biz_name;?></td>
        </tr>
        <tr>
            <td>Address</td><td><?php echo $sec_form1_details->sec_add;?></td>
        </tr>
        <tr>
            <td>Business Address</td><td><?php echo $sec_form1_details->sec_biz_add;?></td>
        </tr>
        <tr>
            <td>Home Telephone</td><td><?php echo $sec_form1_details->sec_hm_tel;?></td>
        </tr>
        <tr>
            <td>Business Telephone</td><td><?php echo $sec_form1_details->sec_biz_tel;?></td>
        </tr>
        <tr>
            <td>Mobile</td><td><?php echo $sec_form1_details->sec_mob;?></td>
        </tr>
        <tr>
            <td>Email</td><td><?php echo $sec_form1_details->sec_email;?></td>
        </tr>
        <tr>
            <td>Facsimile</td><td><?php echo $sec_form1_details->sec_fac?'file':'-';?></td>
        </tr>
    </table>
<!--    <br pagebreak="true" />-->
<?php } ?>


