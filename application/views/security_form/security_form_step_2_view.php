<?php
$sec_form2_details = (json_decode($details));
?>
<style>
    td{
        border: 1px solid #9D8526;
        padding: 5px;
    }
    tr.bg{
        background-color: #252c35;
        color: #ffffff;
    }
    .title{
        text-align: center;
    }
</style>
<div class="title">
    <h2>Instrument By Way Of Security - Item Info</h2>
</div>
<?php if ($sec_form2_details) { ?>
    <table cellspacing="0" cellpadding="5" border="1">
        <tr class="bg">
            <td colspan="2">
                <h4 class="">Motor Vehicles</h4>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="5" border="1">
        <thead>
            <tr>
                <td>Item Description</td>
                <td>Make/Model/CT</td>
                <td>Year</td>
                <td>Amount</td>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($sec_form2_details as $vehicle) { ?>
            <tr>
                <td><?php echo $vehicle->vcl_item_des?></td>
                <td><?php echo $vehicle->vcl_mod?></td>
                <td><?php echo $vehicle->vcl_year?></td>
                <td><?php echo 'NZ$'.number_format($vehicle->vcl_amt)?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<!--<br pagebreak="true" />-->
<?php } ?>

