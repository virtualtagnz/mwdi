<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                Forms <i class="lnr lnr-chevron-right"></i> Security
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="archive_frm_tab" class="table table-striped table-bordered dataTable js-pointstable display">
                            <thead>
                                <tr>
                                    <th>Client Id</th>
                                    <th>Client Name</th>
                                    <th>Status</th>
                                    <th>Edit/ View</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($sec_forms_submitted) { ?>
                                    <?php $submitted_sec_forms = json_decode($sec_forms_submitted) ?>
                                <?php //var_dump($submitted_sec_forms); ?>
                                    <?php foreach ($submitted_sec_forms as $sec) { ?>
                                        <tr>
                                            <td><?php echo $sec->sec_client_ref; ?></td>
                                            <td><?php echo $sec->first_name . ' ' . $sec->last_name; ?></td>
                                            <td><?php echo $sec->sub_stat == 'Y' ? 'Submitted' : 'Draft'; ?></td>
                                            <td>
                                                <?php if ($sec->sub_stat == 'Y') { ?>
                                                    <?php if($this->session->userdata('role') == '1'||$this->session->userdata('role') == '2'){?>
                                                    <a href="<?php echo base_url('security_form/step1/'.$this->encrypt->encode($sec->sec_client_ref))?>" class="btn btn-fefault" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a> 
                                                    <?php } ?>
                                                    <a href="<?php echo base_url('view_security_form/' . $this->encrypt->encode($sec->sec_client_ref)); ?>" target="blank" class="btn btn-fefault" data-toggle="tooltip" data-placement="top" title="Print"><i class="fa fa-print"></i></a>
                                                <?php } else { ?>
                                                    <?php if($this->session->userdata('role') == '1'||$this->session->userdata('role') == '2'){?>
                                                    <a href="<?php echo base_url('security_form/step1/'.$this->encrypt->encode($sec->sec_client_ref))?>" class="btn btn-fefault" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>  
                                                    <?php } ?>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<div id="modalView"></div>
<script>
    $(document).ready(function () {
        $('#archive_frm_tab').dataTable({
            "pageLength": 10,
            "searching": true,
            "info": true,
            "ordering": true
        });
    });
</script>


