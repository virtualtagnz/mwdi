<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <a href="<?php echo base_url('view_security_form/' . end($this->uri->segments));?>" target="blank" class="btn btn-primary pull-right"><i class="fa fa-print"></i> &nbsp; Print Form</a>
                Forms <i class="lnr lnr-chevron-right"></i> Create <i class="lnr lnr-chevron-right"></i> Instrument by Way of  <i class="lnr lnr-chevron-right"></i> Item Info
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <form action="<?php echo base_url('save_security_form_step4/' . end($this->uri->segments)) ?>" method="post" name="sec_form4" id="sec_form4">
                        <input type="hidden" name="start_time" id="start_time" value="<?php echo set_value('start_time'); ?>">
                        <div class="row">
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <h4>CHATTELS</h4>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Item Description</label>
                                    <input type="text" class="form-control" id="sec4_des" name="sec4_des" placeholder="Item Description" data-validation="required" data-validation-error-msg="The field item description is mandatory, please check again!">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputAmount">Amount (in dollars)</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <input type="text" class="form-control" id="sec4_amt" name="sec4_amt" placeholder="Amount" data-validation="required" data-validation-error-msg="The field amount is mandatory, please check again!">
                                        <div class="input-group-addon">.00</div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <button type="button" class="btn btn-primary" id="set_tab">ADD TABLE</button>
                                <hr style="margin-top: 35px;">

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered" id="sec4_tab">
                                        <thead>
                                            <tr>
                                                <th>Item Description</th>
                                                <th>Amount</th>
                                                <th>Delete</th>
                                            </tr>
                                        </thead>
                                        <div id="infoMessage"><?php echo form_error('tab_validator'); ?></div>
                                    </table>
                                </div>
                            </div>
                            <input type="hidden" name="json_str" id="json_str" value="">
                            <div class="col-md-12" style="padding-top: 20px; padding-bottom: 20px;">
                                <a href="<?php echo base_url('security_form/step3/'.end($this->uri->segments))?>" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; BACK</a> &nbsp;
                                <button type="submit" class="btn btn-default" name="btnaction" value="save">SAVE FORM &nbsp; <i class="fa fa-save"></i></button> &nbsp;
                                <button type="button" class="btn btn-primary" name="btnaction" value="submit" id="btn_submit">SUBMIT</button>
                                &nbsp; [4/4]
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script>
    $(document).ready(function () { 
        $('#sec4_amt').mask('000,000,000,000,000', {reverse: true});

        $("#sec_form4 :input").change(function () {
            if (!$("#start_time").val()) {
               $('#start_time').val(timer());
            }
        });

        /*
         * Check main form availability
         */
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('is_main_form_available/' . end($this->uri->segments)); ?>",
            success: function (results) {
                if (results == '') {
                    swal({title: 'No main form',
                        text: "Plese fill Security form step 1",
                        type: 'error',
                        confirmButtonText: 'Go to my form section!',
                    }).then(function () {
                        location.href = '<?php echo base_url('my_forms/' . $this->encrypt->encode($this->session->userdata('user_level_id')) . '/' . $this->encrypt->encode($this->session->userdata('role'))) ?>'
                    });
                }
            }
        });

        /*
         * Create datatable
         */
        var tab = $('#sec4_tab').DataTable({
            "paging": false,
            "searching": false,
            "info": false
        });

        $('#set_tab').click(function () {
            var c1 = $('#sec4_des').val();
            var c2 = $('#sec4_amt').val() + '.00';
            var c3 = '<a class="btn btn-default"><i class="fa fa-trash"></i></a>';
            if (c1.length !== 0 && c2.length) {
                tab.row.add([
                    c1,
                    c2,
                    c3
                ]).draw(true);
                createJson();
                clearTest();
                $('#infoMessage').empty();
            } else {
                //validations
                $.validate({
                    form: '#sec_form4'
                });
                $('#sec4_des,#sec4_amt').validate();
            }
        });

        /*
         * Create JSON
         */
        function createJson() {
            var rowData = tab.data();
            var jsonObj = [];
            for (var i = 0; i < tab.data().length; i++) {
                var item = {};
                item["col1"] = rowData[i][0];
                item["col2"] = rowData[i][1];
                jsonObj.push(item);
            }
            var json_str = JSON.stringify(jsonObj);
            $('#json_str').val(json_str);
        }

        /*
         * Clear all test boxes
         */
        var clearTest = function () {
            $('#sec4_des').val('');
            $('#sec4_amt').val('');
        };

        $('#sec4_tab').on('click', 'a', function () {
            tab.row($(this).parents('tr')).remove().draw(false);
            createJson();
        });
        
        $('#btn_submit').on('click', function () {
            swal({
                title: 'Do you want submit this form?',
                confirmButtonText: 'Submit',
                html: '<span style="color:red;">You cannot edit this form after submit</span>',
                showLoaderOnConfirm: true,
                showCancelButton: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        var tab_data = $('#json_str').val();
                        var start_time = $('#start_time').val();
                        var end_time = $('#end_time').val();
                        $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url('sumbit_security_form/'.end($this->uri->segments)); ?>',
                            data:{'json_str':tab_data,'start_time':start_time,'end_time':end_time,'stat':'1'}
                        }).done(function (response) {
                            //var rslt = JSON.parse(response);
                            console.log(response);
                            if(response=='error'){
                                $('#infoMessage').html('Please enter chattels details');
                            }else{
                                if(response=='success'){
                                    swal({type: 'success', title: 'success', text: 'The form has been submited'}).then(function(){location.href = '<?php echo $this->session->userdata('role') == '5'?base_url('client_forms'):base_url('submitted_security_forms') ?>';});
                                }else{
                                    swal({type: 'success', title: 'success', text: 'The form has been submited'}).then(function(){location.href = '<?php echo base_url('security_form/step4/'.end($this->uri->segments)) ?>';});
                                }
                            }
                            //resolve();
                        });
                    });
                }
            }).catch(swal.noop);
        });
        
        $('#sec_form4').on("submit", function (event) {
            $(this).find('*[data-validation]').attr("data-validation-optional","true");
        });
        
    });
</script>