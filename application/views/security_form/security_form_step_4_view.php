<?php
$sec_form4_details = (json_decode($details));
?>
<style>
    td{
        border: 1px solid #9D8526;
        padding: 5px;
    }
    tr.bg{
        background-color: #252c35;
        color: #ffffff;
    }
    .title{
        text-align: center;
    }
</style>
<div class="title">
    <h2>Instrument By Way Of Security - Item Info</h2>
</div>
<?php if ($sec_form4_details) { ?>
    <table cellspacing="0" cellpadding="5" border="1">
        <tr class="bg">
            <td colspan="2">
                <h4 class="">Chattels</h4>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="5" border="1">
        <thead>
            <tr>
                <td>Item Description</td>
                <td>Amount</td>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($sec_form4_details as $chattel) { ?>
            <tr>
                <td><?php echo $chattel->cat_des?></td>
                <td><?php echo 'NZ$'.number_format($chattel->cat_amt)?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<!--<br pagebreak="true" />-->
<?php } ?>

