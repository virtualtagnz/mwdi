<?php if ($data) { $chattles_list = json_decode($data); } ?>
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <a href="<?php echo base_url('view_security_form/' . end($this->uri->segments));?>" target="blank" class="btn btn-primary pull-right"><i class="fa fa-print"></i> &nbsp; Print Form</a>
                Forms <i class="lnr lnr-chevron-right"></i> Create <i class="lnr lnr-chevron-right"></i> Instrument by Way of  <i class="lnr lnr-chevron-right"></i> Item Info
            </h3>
            <?php //var_dump($chattles_list) ?>
            <div class="panel">
                <div class="panel-body">
                    <form action="<?php echo base_url('edit_security_form_step4/' . end($this->uri->segments)) ?>" method="post" name="sec_form4" id="sec_form4">
                        <div class="row">
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <h4>CHATTELS</h4>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Item Description</label>
                                    <input type="text" class="form-control" id="sec4_des" name="sec4_des" placeholder="Item Description">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputAmount">Amount (in dollars)</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <input type="text" class="form-control" id="sec4_amt" name="sec4_amt" placeholder="Amount">
                                        <div class="input-group-addon">.00</div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <button type="button" class="btn btn-primary" id="set_tab">ADD TABLE</button>
                                <hr style="margin-top: 35px;">

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered" id="sec4_tab">
                                        <thead>
                                            <tr>
                                                <th>Item Description</th>
                                                <th>Amount</th>
                                                <th>Delete</th>
                                            </tr>
                                        </thead>
                                        <?php if ($chattles_list) { ?>
                                            <tbody>
                                                <?php $i = 0 ?>
                                                <?php foreach ($chattles_list as $chattles) { ?>
                                                    <tr>
                                                        <td><?php echo $chattles->cat_des ?></td>
                                                        <td><?php echo number_format($chattles->cat_amt) . '.00' ?></td>
                                                        <td><a class="btn btn-fefault" id="<?php echo $chattles->cat_id ?>"><i class="fa fa-trash"></i></a></td>
                                                    </tr>
                                                    <?php
                                                    $json_arr[$i]['col1'] = $chattles->cat_des;
                                                    $json_arr[$i]['col2'] = number_format($chattles->cat_amt) . '.00';
                                                    $json_str = json_encode($json_arr);
                                                    $i++;
                                                    ?>
                                                <?php } ?>
                                            </tbody>
                                        <?php } ?>
                                        <div id="infoMessage"><?php echo form_error('tab_validator'); ?></div>
                                    </table>
                                </div>
                            </div>
                            <input type="hidden" name="start_time" id="start_time" value="<?php echo $chattles_list[0]->cat_start_date?$chattles_list[0]->cat_start_date:date('Y-m-d H:i:s'); ?>">
                            <input type="hidden" name="end_time" id="end_time" value="<?php echo $chattles_list[0]->cat_end_date?$chattles_list[0]->cat_end_date:date('Y-m-d H:i:s'); ?>">
                            <input type="hidden" name="json_str" id="json_str" value="">
                            <div class="col-md-12" style="padding-top: 20px; padding-bottom: 20px;">
                                <a href="<?php echo base_url('security_form/step3/'.end($this->uri->segments))?>" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; BACK</a> &nbsp;
                                <button type="submit" class="btn btn-default" name="btnaction" value="save">SAVE FORM &nbsp; <i class="fa fa-save"></i></button> &nbsp;
                                <button type="button" class="btn btn-primary" name="btnaction" value="submit" id="btn_submit">SUBMIT</button>
                                &nbsp; [4/4]
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<?php if ($this->session->flashdata('create_success')) { ?>
    <script>swal({type: 'success', title: 'success', text: 'The form has been successfully saved', timer: 3000});</script>
<?php } else if ($this->session->flashdata('update_success')) { ?>
    <script>swal({type: 'success', title: 'success', text: 'The form has been successfully updated', timer: 3000});</script>
<?php } else if ($this->session->flashdata('error_msg')) { ?>
    <script>swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000});</script>
<?php } ?>
<script>
    $(document).ready(function () {
        $('#sec4_amt').mask('000,000,000,000,000', {reverse: true});

        $('#json_str').val('<?php echo $json_str ?>');
        /*
         * Check main form availability
         */
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('is_main_form_available/' . end($this->uri->segments)); ?>",
            success: function (results) {
                if (results == '') {
                    swal({title: 'No main form',
                        text: "Plese fill Security form step 1",
                        type: 'error',
                        confirmButtonText: 'Go to my form section!',
                    }).then(function () {
                        location.href = '<?php echo base_url('my_forms/' . $this->encrypt->encode($this->session->userdata('user_level_id')) . '/' . $this->encrypt->encode($this->session->userdata('role'))) ?>'
                    });
                }
            }
        })

        /*
         * Create datatable
         */
        var tab = $('#sec4_tab').DataTable({
            "paging": false,
            "searching": false,
            "info": false
        });

        $('#set_tab').click(function () {
            var c1 = $('#sec4_des').val();
            var c2 = $('#sec4_amt').val() + '.00';
            var c3 = '<a class="btn btn-default"><i class="fa fa-trash"></i></a>';
            if (c1.length !== 0 && c2.length) {
                tab.row.add([
                    c1,
                    c2,
                    c3
                ]).draw(true);
                createJson();
                clearTest();
                $('#infoMessage').empty();
            } else {
                //validations
                $.validate({
                    form: '#sec_form4'
                });
                $('#sec4_des,#sec4_amt').validate();
            }
        });

        /*
         * Create JSON
         */
        function createJson() {
            var rowData = tab.data();
            var jsonObj = [];
            for (var i = 0; i < tab.data().length; i++) {
                var item = {};
                item["col1"] = rowData[i][0];
                item["col2"] = rowData[i][1];
                jsonObj.push(item);
            }
            var json_str = JSON.stringify(jsonObj);
            console.log(json_str);
            $('#json_str').val(json_str);
        }

        /*
         * Clear all test boxes
         */
        var clearTest = function () {
            $('#sec4_des').val('');
            $('#sec4_amt').val('');
        };

        $('#sec4_tab').on('click', 'a', function () {
            var id = $(this).get(0).id;
            if(id!=""){
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('delete_single_chattel'); ?>",
                    data:{"chattel_id":id},
                    success: function (results) {
                        console.log(results);
                    }
                });
            }
            tab.row($(this).parents('tr')).remove().draw(false);
            createJson();
        });

        $('#btn_submit').on('click', function () {
            swal({
                title: 'Do you want submit this form?',
                confirmButtonText: 'Submit',
                html: '<span style="color:red;">You cannot edit this form after submit</span>',
                showLoaderOnConfirm: true,
                showCancelButton: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        var tab_data = $('#json_str').val();
                        var start_time = $('#start_time').val();
                        var end_time = $('#end_time').val();
                        console.log(start_time);
                        $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url('sumbit_security_form/'.end($this->uri->segments)); ?>',
                            data:{'json_str':tab_data,'start_time':start_time,'end_time':end_time,'stat':'2'}
                        }).done(function (response) {
                            //var rslt = JSON.parse(response);
                            console.log(response);
                            if(response=='error'){
                                $('#infoMessage').html('Please enter chattels details');
                                resolve();
                            }else{
                                if(response=='success'){
                                    swal({type: 'success', title: 'success', text: 'The form has been submited'}).then(function(){location.href = '<?php echo $this->session->userdata('role') == '5'?base_url('client_forms'):base_url('submitted_security_forms') ?>';});
                                }else{
                                    swal({type: 'success', title: 'success', text: 'The form has been submited'}).then(function(){location.href = '<?php echo base_url('security_form/step4/'.end($this->uri->segments)) ?>';});
                                }
                            }
                        });
                    });
                }
            }).catch(swal.noop);
        });
        
        $('#sec_form4').on("submit", function (event) {
            $(this).find('*[data-validation]').attr("data-validation-optional","true");
        });
    });
</script>