<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <?php $notification_arr = (json_decode($notifications)); ?>
    <?php //var_dump($this->session->userdata()); ?>
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">Notifications</h3>
            <div class="panel">
                <button id="read_all" type="button" class="btn btn-primary btn-bottom">Mark as read (All Notifications)</button>
                <button id="read_selected" type="button" class="btn btn-primary btn-bottom">Mark as read (Selected Notification(s))</button>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable js-pointstable display" id="notification_tab">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Notification By</th>
                                    <th>Notification</th>
                                    <th>Notify Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($notification_arr) { ?>
                                    <?php foreach ($notification_arr as $notification) { ?>
                                        <tr>
                                            <td><input type="checkbox" class="checkbox" id="<?php echo $notification->nf_id ?>"/></td>
                                            <td>
                                                <?php if ($notification->user_details ? $notification->user_details->image_link : '') { ?>
                                                    <img src="<?php echo base_url('uploads/profile_image/' . $notification->user_details->image_link) ?>" alt="" class="img-circle img-thumbnail" style="height: 50px;">
                                                <?php } ?>
                                                &nbsp; <?php echo $notification->user_details ? $notification->user_details->first_name . ' ' . $notification->user_details->last_name : '' ?>
                                            </td>
                                            <td><?php echo $notification->notification_txt; ?></td>
                                            <td><?php echo date('d/m/Y', strtotime($notification->notify_date)) ?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>

<!--                    <p style="padding: 20px 0 10px 0;"><button id="loadMore" type="button" class="btn btn-primary btn-bottom center-block">More</button></p>-->
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script>
    $(document).ready(function () {

        var notify_tab = $('#notification_tab').dataTable({
            "pageLength": 10,
            "searching": true,
            "info": true,
            "responsive": true,
            "order": []
        });

        //Make selected notifications array

        var rows_selected = [];
        $('#notification_tab tbody').on('click', 'input[type="checkbox"]', function () {
            var id = $(this).get(0).id;
            if (this.checked) {
                rows_selected.push(id);
            } else {
                var index = rows_selected.indexOf(id);
                rows_selected.splice(index, 1);
            }
            $(this).closest('tr').toggleClass('selected');
        });

        //Mark as read only selected notifications items

        $('#read_selected').on('click', function () {
            if (rows_selected.length > 0) {
                swal({
                    text: 'Do you want to mark the selected notification as read',
                    confirmButtonText: 'Yes',
                    showLoaderOnConfirm: true,
                    showCancelButton: true,
                    preConfirm: function () {
                        return new Promise(function (resolve) {
                            var selected_json_str = JSON.stringify(rows_selected);
                            $.ajax({
                                type: 'POST',
                                url: '<?php echo base_url('mark_selected_as_read'); ?>',
                                data:{'json_str':selected_json_str},
                            }).done(function (response) {
                                 console.log(response);
                                 if(response){
                                     change_current_notification("<?php echo base_url('increase_current_notification_count'); ?>");
                                     swal({type: 'success', title: 'Success', text: 'The selected notifications to set as read'}).then(function(){location.reload();});
                                 }
                            });
                        });
                    }
                }).catch(swal.noop);
            } else {
                swal({type: 'warning', text: 'There are no any selected notifications to set as read'});
            }
        });
        
        //Mark all as read
        
        $('#read_all').on('click',function(){
            swal({
                    text: 'Do you want to mark the all notifications as read',
                    confirmButtonText: 'Yes',
                    showLoaderOnConfirm: true,
                    showCancelButton: true,
                    preConfirm: function () {
                        return new Promise(function (resolve) {
                            $.ajax({
                                type: 'POST',
                                url: '<?php echo base_url('mark_all_as_read'); ?>',
                            }).done(function (response) {
                                 console.log(response);
                                 if(response){
                                     change_current_notification("<?php echo base_url('increase_current_notification_count'); ?>");
                                     swal({type: 'success', title: 'Success', text: 'The all notifications mark as read'}).then(function(){location.reload();});
                                 }
                            });
                        });
                    }
                }).catch(swal.noop);
        });
        
    });
</script>