<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title"> Resources <i class="lnr lnr-chevron-right"></i>Edit Resource</h3>
            <?php //var_dump($single_resource);?>
            <div class="panel">
                <div class="panel-body">
                    <form action="<?php echo base_url('edit_resource/' . $this->encrypt->encode($single_resource->res_id)) ?>" method="post" name="resource_create_form" id="resource_create_form" enctype="multipart/form-data" style="padding-top: 20px;">
                        <input type="hidden" name="current_file" id="current_file" value="<?php echo $single_resource->res_linq; ?>">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                   <p><i class="fa fa-cloud-upload activity-icon-blue"></i>Uploaded new file <a href="<?php echo base_url('uploads/resources/' . $single_resource->res_linq); ?>" target="blank"><?php echo $single_resource->res_name; ?></a></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="res_name" name="res_name" placeholder="Resource Name" data-validation="required" data-validation-error-msg="The field resource name is mandatory, please check again!" value="<?php echo $single_resource->res_name; ?>">
                                    <div id="infoMessage"><?php echo form_error('res_name'); ?></div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <i class="fa fa-info-circle pull-right" style="margin-top: 10px;" data-toggle="tooltip" data-placement="top" title="Accept Formats: PDF, DOC and DOCX"></i>
                                    <label for="res_file" class="filupp form-control" style="width: 96%;">
                                        <span class="filupp-file-name js-value">Upload File</span>
                                        <input type="file" name="res_file" value="<?php echo set_value('res_file'); ?>" id="res_file" accept=".xlsx,.xls,.doc, .docx,.txt,.pdf" data-validation="required mime size" data-validation-allowing="pdf, doc" data-validation-max-size="1M" data-validation-error-msg-required="The field file is mandatory, please check again!" data-validation-error-msg-size="You can not upload images larger than 1MB" data-validation-error-msg-mime="The filetype you are attempting to upload is not allowed" <?php echo $single_resource->res_linq?'data-validation-optional="true"':'';?>>
                                    </label>
                                    <div id="infoMessage"><?php echo form_error('res_file'); ?></div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea class="form-control" name="des" id="des" placeholder="Resource Description" rows="4" data-validation="required" data-validation-error-msg="The field resource description is mandatory, please check again!"><?php echo $single_resource->res_des; ?></textarea>
                                    <div id="infoMessage"><?php echo form_error('des'); ?></div>
                                </div>
                            </div>
                        </div>

                        <p>
                            <a href="javascript: history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; Back</a> &nbsp; 
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<?php if ($this->session->flashdata('create_success')) { ?>
    <script>swal({type: 'success', title: 'success', text: 'The resource has been created', timer: 3000});</script>
<?php } else if ($this->session->flashdata('update_success')) { ?>
    <script>swal({type: 'success', title: 'success', text: 'The resource has been updated', timer: 3000});</script>
<?php } else if ($this->session->flashdata('error_msg')) { ?>
    <script>swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000});</script>
<?php } ?>
<script>
    $(document).ready(function () {
        $.validate({
            form: '#resource_create_form',
            modules: 'security,date,file'
        });
        
        
    });
</script>

