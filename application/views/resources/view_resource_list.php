<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <a href="<?php echo base_url('go_create_resource') ?>" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> &nbsp; Create Resource </a>
                Resources
            </h3>
            <?php if ($all_resources) { ?>
                <div class="panel">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="user_tab" class="table table-striped table-bordered dataTable js-pointstable display">
                                <thead>
                                    <tr>
                                        <th>Resource Name</th>
                                        <th>Description</th>
                                        <th style="width: 130px;" data-orderable="false"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $resources = json_decode($all_resources) ?>
                                    <?php foreach ($resources as $resource) { ?>
                                        <tr>
                                            <td><a href="<?php echo base_url('uploads/resources/' . $resource->res_linq); ?>" target="blank"><?php echo $resource->res_name ?></a></td>
                                            <td><?php echo $resource->res_des ?></td>
                                            <td>
                                                <a href="<?php echo base_url('go_edit_resource/' . $this->encrypt->encode($resource->res_id)); ?>" class="btn btn-fefault" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a> &nbsp; 
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

