<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">Dashboard</h3>
            <?php //var_dump($coach); ?>
            <?php if ($this->session->userdata('role') == '1' || $this->session->userdata('role') == '2' || $this->session->userdata('role') == '3') { ?>
                <div class="panel panel-headline">
                    <div class="panel-heading">
                        <h3 class="panel-title">Client Performances</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <form action="#" method="post" name="frm_client_performence" id="frm_client_performence" enctype="multipart/form-data">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Select Form Type</label>
                                        <select class="form-control" id="frm_type" name="frm_type">
                                            <option value="" selected="selected">-- Select Form Type --</option>
                                            <option value="1" <?php echo set_select('frm_type', '1'); ?>>BA Form</option>
                                            <option value="2" <?php echo set_select('frm_type', '2'); ?>>PPMS Form</option>
                                            <option value="4" <?php echo set_select('frm_type', '4'); ?>>Security Form</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="metric">
                                    <span class="icon"><i class="lnr lnr-select"></i></span>
                                    <p>
                                        <span class="number" id="tot_forms"><?php echo $tot_forms_assigned_client; ?></span>
                                        <span class="title" id="tot_forms_title">Number of Forms Assigned</span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="metric">
                                    <span class="icon"><i class="lnr lnr-text-align-justify"></i></span>
                                    <p>
                                        <span class="number" id="submitted_form"><?php echo $tot_submitted_forms; ?></span>
                                        <span class="title" id="submitted_form_title">Completed Forms</span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="metric">
                                    <span class="icon"><i class="lnr lnr-checkmark-circle"></i></span>
                                    <div id="success_rate_class" class="c100 small green p<?php echo round($client_success_rate); ?>" style="margin: 6px 0 0 12px;">
                                        <span id="success_rate_inner"><?php echo $client_success_rate . '%'; ?></span>
                                        <div class="slice">
                                            <div class="bar"></div>
                                            <div class="fill"></div>
                                        </div>
                                    </div>
                                    <p>
                                        <span class="number" id="success_rate"><?php echo $client_success_rate . '%'; ?></span>
                                        <span class="title">Success Rate</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php if ($this->session->userdata('role') == '4') { ?>
                <div class="panel panel-headline">
                    <div class="panel-heading">
                        <h3 class="panel-title">Client form Progress</h3>
                        <p class="panel-subtitle">Total Number of Clients Assigned : <span class="label label-primary"><?php echo $tot_number_of_client ?></span></p>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <?php foreach ($coach as $name => $inner_val){?>
                            <?php //var_dump($inner_val)?>
                            <div class="col-md-3">
                                <div class="metric">
                                    <div id="success_rate_class" class="c100 small green p<?php echo round($inner_val['success_rate']); ?>" style="margin: 6px 0 0 12px;">
                                        <span id="success_rate_inner"><?php echo $inner_val['success_rate'] . '%'; ?></span>
                                        <div class="slice">
                                            <div class="bar"></div>
                                            <div class="fill"></div>
                                        </div>
                                    </div>
                                    <p>
                                        <span class="number" id="success_rate"><?php echo $inner_val['submit_cnt']; ?></span>
                                        <span class="title"><?php echo $name?></span>
                                    </p>
                                </div>
                            </div>
                            <?php }?>
                                
                            
<!--                            <div class="col-md-3">
                                <div class="metric">
                                    <div id="success_rate_class" class="c100 small green p<?php echo round($success_rate_ch1); ?>" style="margin: 6px 0 0 12px;">
                                        <span id="success_rate_inner"><?php echo $success_rate_ch1 . '%'; ?></span>
                                        <div class="slice">
                                            <div class="bar"></div>
                                            <div class="fill"></div>
                                        </div>
                                    </div>
                                    <p>
                                        <span class="number" id="success_rate"><?php echo $submitted_ch1; ?></span>
                                        <span class="title">Engagement 1</span>
                                    </p>
                                </div>
                            </div>-->

<!--                            <div class="col-md-3">
                                <div class="metric">
                                    <div id="success_rate_class" class="c100 small green p<?php echo round($success_rate_ch2); ?>" style="margin: 6px 0 0 12px;">
                                        <span id="success_rate_inner"><?php echo $success_rate_ch2 . '%'; ?></span>
                                        <div class="slice">
                                            <div class="bar"></div>
                                            <div class="fill"></div>
                                        </div>
                                    </div>
                                    <p>
                                        <span class="number" id="success_rate"><?php echo $submitted_ch2; ?></span>
                                        <span class="title">Engagement 2</span>
                                    </p>
                                </div>
                            </div>-->

<!--                            <div class="col-md-3">
                                <div class="metric">
                                    <div id="success_rate_class" class="c100 small green p<?php echo round($success_rate_ch3); ?>" style="margin: 6px 0 0 12px;">
                                        <span id="success_rate_inner"><?php echo $success_rate_ch3 . '%'; ?></span>
                                        <div class="slice">
                                            <div class="bar"></div>
                                            <div class="fill"></div>
                                        </div>
                                    </div>
                                    <p>
                                        <span class="number" id="success_rate"><?php echo $submitted_ch3; ?></span>
                                        <span class="title">Engagement 3</span>
                                    </p>
                                </div>
                            </div>-->

<!--                            <div class="col-md-3">
                                <div class="metric">
                                    <div id="success_rate_class" class="c100 small green p<?php echo round($success_rate_ch4); ?>" style="margin: 6px 0 0 12px;">
                                        <span id="success_rate_inner"><?php echo $success_rate_ch4 . '%'; ?></span>
                                        <div class="slice">
                                            <div class="bar"></div>
                                            <div class="fill"></div>
                                        </div>
                                    </div>
                                    <p>
                                        <span class="number" id="success_rate"><?php echo $submitted_ch4; ?></span>
                                        <span class="title">Engagement 4</span>
                                    </p>
                                </div>
                            </div>-->
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php if ($this->session->userdata('role') == '5') { ?>
                <div class="row">
                    <div class="col-md-7">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Business Application Progress</h3>
                                <div class="right">
                                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                                </div>
                            </div>
                            <div class="panel-body">
                                <br>
                                <div class="row">
                                    <div class="col-md-2 col-xs-6">
                                        <div class="c100 p<?php echo $main_ba_progress; ?> small green center">
                                            <span><?php echo $main_ba_progress . '%'; ?></span>
                                            <div class="slice">
                                                <div class="bar"></div>
                                                <div class="fill"></div>
                                            </div>
                                        </div>
                                        <p class="text-center c100-text">Personal Details</p>
                                    </div>
                                    <div class="col-md-2 col-xs-6">
                                        <div class="c100 p<?php echo $edu_progress; ?> small green center">
                                            <span><?php echo $edu_progress . '%'; ?></span>
                                            <div class="slice">
                                                <div class="bar"></div>
                                                <div class="fill"></div>
                                            </div>
                                        </div>
                                        <p class="text-center c100-text">Educational Details</p>
                                    </div>
                                    <div class="col-md-2 col-xs-6">
                                        <div class="c100 p<?php echo $nxt_kin_progress; ?> small green center">
                                            <span><?php echo $nxt_kin_progress . '%'; ?></span>
                                            <div class="slice">
                                                <div class="bar"></div>
                                                <div class="fill"></div>
                                            </div>
                                        </div>
                                        <p class="text-center c100-text">Next Of Kin Detals</p>
                                    </div>
                                    <div class="col-md-2 col-xs-6">
                                        <div class="c100 p<?php echo $emp_progress; ?> small green center">
                                            <span><?php echo $emp_progress . '%'; ?></span>
                                            <div class="slice">
                                                <div class="bar"></div>
                                                <div class="fill"></div>
                                            </div>
                                        </div>
                                        <p class="text-center c100-text">Employment Details</p>
                                    </div>
                                    <div class="col-md-2 col-xs-6">
                                        <div class="c100 p<?php echo $biz_progress; ?> small green center">
                                            <span><?php echo $biz_progress . '%'; ?></span>
                                            <div class="slice">
                                                <div class="bar"></div>
                                                <div class="fill"></div>
                                            </div>
                                        </div>
                                        <p class="text-center c100-text">Business Details</p>
                                    </div>
                                    <div class="col-md-2 col-xs-6">
                                        <div class="c100 p<?php echo $bk_progress; ?> small green center">
                                            <span><?php echo $bk_progress . '%'; ?></span>
                                            <div class="slice">
                                                <div class="bar"></div>
                                                <div class="fill"></div>
                                            </div>
                                        </div>
                                        <p class="text-center c100-text">Bank Informaton</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-5">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Security Application Progress</h3>
                                <div class="right">
                                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                                </div>
                            </div>
                            <div class="panel-body">
                                <br>
                                <div class="row">
                                    <div class="col-md-3 col-xs-6">
                                        <div class="c100 p<?php echo $main_sec_progress; ?> small green center">
                                            <span><?php echo $main_sec_progress . '%'; ?></span>
                                            <div class="slice">
                                                <div class="bar"></div>
                                                <div class="fill"></div>
                                            </div>
                                        </div>
                                        <p class="text-center c100-text">Personal Details</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6">
                                        <div class="c100 p<?php echo $sec_vehicle_progress; ?> small green center">
                                            <span><?php echo $sec_vehicle_progress . '%'; ?></span>
                                            <div class="slice">
                                                <div class="bar"></div>
                                                <div class="fill"></div>
                                            </div>
                                        </div>
                                        <p class="text-center c100-text">Motor Vehicles</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6">
                                        <div class="c100 p<?php echo $sec_property_progress; ?> small green center">
                                            <span><?php echo $sec_property_progress . '%'; ?></span>
                                            <div class="slice">
                                                <div class="bar"></div>
                                                <div class="fill"></div>
                                            </div>
                                        </div>
                                        <p class="text-center c100-text">Property</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6">
                                        <div class="c100 p<?php echo $sec_chattles_progress; ?> small green center">
                                            <span><?php echo $sec_chattles_progress . '%'; ?></span>
                                            <div class="slice">
                                                <div class="bar"></div>
                                                <div class="fill"></div>
                                            </div>
                                        </div>
                                        <p class="text-center c100-text">Chattels</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="row">
                <?php if ($this->session->userdata('role') != '4') { ?>
                    <div class="<?php echo $this->session->userdata('role') == '5' ? 'col-md-6' : 'col-md-4' ?>">
                        <div class="panel panel-scrolling">
                            <div class="panel-heading">
                                <h3 class="panel-title"><?php echo ($this->session->userdata('role') == '1' || $this->session->userdata('role') == '2' || $this->session->userdata('role') == '3' ? 'Average Time to Complete Forms(HH:MM:SS)' : ($this->session->userdata('role') == '4' ? 'My Progress' : 'Time to Complete Forms(HH:MM:SS)')) ?> </h3>
                                <div class="right">
                                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                                </div>
                            </div>
                            <div class="panel-body">
                                <ul class="list-unstyled list-basic">
                                    <?php if ($this->session->userdata('role') == '1' || $this->session->userdata('role') == '2' || $this->session->userdata('role') == '3') { ?>
                                        <li>
                                            <span class="pull-right text-primary"><?php echo gmdate("H:i:s", $ba_form_complete_time) ?></span>
                                            Business Application Form
                                        </li>
                                    <?php } elseif ($this->session->userdata('role') == '5') { ?>
                                        <li>
                                            <span class="pull-right text-primary"><?php echo ($is_ba_form_sumbit == 'Y' ? gmdate("H:i:s", $ba_form_complete_time) : ($is_ba_form_sumbit == 'N'?'In progress':'Not Started')) ?></span>
                                            Business Application Form
                                        </li>
                                    <?php } ?>

                                    <?php if ($this->session->userdata('role') == '1' || $this->session->userdata('role') == '2' || $this->session->userdata('role') == '3') { ?>
                                        <li>
                                            <span class="pull-right text-primary"><?php echo gmdate("H:i:s", $sec_form_complete_time) ?></span>
                                            Security Form
                                        </li>
                                    <?php } elseif ($this->session->userdata('role') == '5') { ?>
                                        <li>
                                            <span class="pull-right text-primary"><?php echo ($is_sec_form_sumbit == 'Y' ? gmdate("H:i:s", $sec_form_complete_time) : ($is_sec_form_sumbit == 'N'?'In progress':'Not Started')) ?></span>
                                            Security Form
                                        </li>
                                    <?php } ?>

                                    <?php if ($this->session->userdata('role') == '1' || $this->session->userdata('role') == '2' || $this->session->userdata('role') == '3') { ?>
                                        <li>
                                            <span class="pull-right text-primary"><?php echo gmdate("H:i:s", $ppms_form_complete_time) ?></span>
                                            PPMS Form
                                        </li>
                                    <?php } elseif ($this->session->userdata('role') == '5') { ?>
                                        <li>
                                            <span class="pull-right text-primary"><?php echo ($is_ppms_form_sumbit == 'Y' ? gmdate("H:i:s", $ppms_form_complete_time) : ($is_ppms_form_sumbit=='N'?'In progress':'Not Started')) ?></span>
                                            PPMS Form
                                        </li>
                                    <?php } ?> 

                                    <?php if ($this->session->userdata('role') == '1' || $this->session->userdata('role') == '2' || $this->session->userdata('role') == '3') { ?>
                                        <li>
                                            <span class="pull-right text-primary">19:02:23</span>
                                            Client Form
                                        </li>
                                    <?php } elseif ($this->session->userdata('role') == '4') { ?>
                                        <li>
                                            <span class="pull-right text-primary"><?php echo $tot_number_of_client ?></span>
                                            Total Number of Clients Assigned
                                        </li>
                                        <li>
                                            <span class="pull-right text-primary"><?php echo $submitted_ch1 . '(' . $success_rate_ch1 . '%)' ?></span>
                                            Submitted Engagement 1
                                            <div class="progress progress-xs">
                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $success_rate_ch1 ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $success_rate_ch1 . '%' ?>"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="pull-right text-primary"><?php echo $submitted_ch2 . '(' . $success_rate_ch2 . '%)' ?></span>
                                            Submitted Engagement 2
                                            <div class="progress progress-xs">
                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $success_rate_ch2 ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $success_rate_ch2 . '%' ?>"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="pull-right text-primary"><?php echo $submitted_ch3 . '(' . $success_rate_ch3 . '%)' ?></span>
                                            Submitted Engagement 3
                                            <div class="progress progress-xs">
                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $success_rate_ch3 ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $success_rate_ch3 . '%' ?>"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="pull-right text-primary"><?php echo $submitted_ch4 . '(' . $success_rate_ch4 . '%)' ?></span>
                                            Submitted Engagement 4
                                            <div class="progress progress-xs">
                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $success_rate_ch4 ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $success_rate_ch4 . '%' ?>"></div>
                                            </div>
                                        </li>
                                    <?php } ?> 
                                </ul>
                                <!--                            <button type="button" class="btn btn-primary btn-bottom center-block">More</button>-->
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($this->session->userdata('role') != '5') { ?>
                    <div class="<?php echo $this->session->userdata('role') == '4' ? 'col-md-6' : 'col-md-4' ?>">
                        <div class="panel panel-scrolling">
                            <div class="panel-heading">
                                <h3 class="panel-title"><?php echo $this->session->userdata('role') == '4' ? 'My Client Activities' : 'Client Activities' ?></h3>
                                <div class="right">
                                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                                </div>
                            </div>
                            <div class="panel-body">
                                <ul class="list-unstyled activity-list">
                                    <?php if ($all_cilent_activities) { ?>
                                        <?php $client_activity_arr = json_decode($all_cilent_activities); ?>
                                        <?php foreach ($client_activity_arr as $single_activity) { ?>
                                            <li>
                                                <img src="<?php echo $single_activity->image_link != NULL ? base_url('uploads/profile_image/' . $single_activity->image_link) : base_url('assets/img/user_default.jpg'); ?>" alt="" class="img-circle pull-left avatar">
                                                <p><strong><?php echo $single_activity->first_name . ' ' . $single_activity->last_name ?></strong> <?php echo $single_activity->act_txt ?> <span class="timestamp"><i class="fa fa-clock-o"></i>&nbsp;<?php echo $single_activity->act_date ?></span></p>
                                            </li>
                                        <?php } ?>
                                    <?php } ?>
                                </ul>
                                <!--                            <button type="button" class="btn btn-primary btn-bottom center-block">More</button>-->
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <div class="<?php echo $this->session->userdata('role') == '4' || $this->session->userdata('role') == '5' ? 'col-md-6' : 'col-md-4' ?>">
                    <div class="panel panel-scrolling">
                        <div class="panel-heading">
                            <h3 class="panel-title"><?php echo $this->session->userdata('role') == '4' || $this->session->userdata('role') == '5' ? 'My Activities' : 'Coach Activities' ?></h3>
                            <div class="right">
                                <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                            </div>
                        </div>
                        <div class="panel-body">
                            <ul class="list-unstyled activity-list">
                                <?php if ($all_coach_activities) { ?>
                                    <?php $coach_activity_arr = json_decode($all_coach_activities); ?>
                                    <?php foreach ($coach_activity_arr as $activity) { ?>
                                        <li>
                                            <img src="<?php echo $activity->image_link != NULL ? base_url('uploads/profile_image/' . $activity->image_link) : base_url('assets/img/user_default.jpg'); ?>" alt="" class="img-circle pull-left avatar">
                                            <p><strong><?php echo $activity->first_name . ' ' . $activity->last_name ?></strong> <?php echo $activity->act_txt ?> <span class="timestamp"><i class="fa fa-clock-o"></i>&nbsp;<?php echo $activity->act_date ?></span></p>
                                        </li>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                            <!--                            <button type="button" class="btn btn-primary btn-bottom center-block">More</button>-->
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<script>
    $(document).ready(function () {
        $('#frm_type').on('change', function () {
            var form_type = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('get_client_stat'); ?>",
                data: {'form_type': form_type},
                success: function (results) {
                    var data = JSON.parse(results);
                    $('#tot_forms').html(data.tot_forms_assigned_client);
                    $('#submitted_form').html(data.submitted_forms);
                    $('#success_rate_inner').html(data.success_rate + '%');
                    $('#success_rate').html(data.success_rate + '%');
                    var class_remove = $('#success_rate_class').attr('class').split(' ')[3];
                    $('#success_rate_class').removeClass(class_remove).addClass('p' + Math.round(data.success_rate));
                    //$('#success_rate_class').addClass('p' + data.success_rate);
                    switch (form_type) {
                        case '1':
                            $('#tot_forms_title').html('BA Forms Assigned');
                            $('#submitted_form_title').html('Completed BA Forms');
                            break;
                        case '2':
                            $('#tot_forms_title').html('PPMS Forms Assigned');
                            $('#submitted_form_title').html('Completed PPMS Forms');
                            break;
                        case '4':
                            $('#tot_forms_title').html('Security Forms Assigned');
                            $('#submitted_form_title').html('Completed Security Forms');
                            break;
                    }

                }
            });
        });
    });
</script>