<style>
    td{
        border: 1px solid #9D8526;
        padding: 5px;
    }
    tr.bg{
        background-color: #252c35;
        color: #ffffff;
    }
    .title{
        text-align: center;
    }
</style>

<div class="title">
    <h2>Coaching Enquire Form</h2>
</div>

<?php if ($form_details) { ?>   

    <table cellspacing="0" cellpadding="5" border="1">
        <tr class="bg">
            <td colspan="2">
                <h4 class="title">Details</h4>
            </td>
        </tr>     
        <tr>
            <th><strong>First Name</strong></th>
            <th><strong>Last Name</strong></th>
        </tr>                                    
        <tr>
            <td><?php echo $form_details->first_name; ?></td>
            <td><?php echo $form_details->last_name; ?></td>
        </tr>      
    </table>
    <table cellspacing="0" cellpadding="5" border="1">  
        <tr>
            <th><strong>City & Region within NZ</strong></th>
            <th><strong>Phone</strong></th>
            <th><strong>Email</strong></th>
        </tr>                                    
        <tr>
            <td><?php echo $form_details->city_region; ?></td>
            <td><?php echo $form_details->phone; ?></td>
            <td><?php echo $form_details->email; ?></td>
        </tr>      
    </table>    
    <table cellspacing="0" cellpadding="5" border="1">  
        <tr>
            <th><strong>Brief Description</strong></th>
        </tr>                                    
        <tr>
            <td><?php echo $form_details->business_description; ?></td>
        </tr>      
    </table>     

<?php } ?> 