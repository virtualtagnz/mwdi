<div class="title">
    <h2>Coaching Enquire Forms</h2>
</div>
<?php if ($templist) { ?>   

    <table cellpadding="2" cellspacing="0" border="1" >
        <tr>
            <th width="45" align="center"><strong>ID</strong></th>
            <th width="195"><strong>Client Name</strong></th>
            <th width="130" align="center"><strong>Created Date</strong></th>
            <th width="195"><strong>Answered By</strong></th>
            <th width="195"><strong>Answered Date</strong></th>
            <th width="195"><strong>Coach</strong></th>
        </tr>
        <?php if ($templist) { ?>
            <?php foreach ($templist as $list) { ?>                                
                <tr>
                    <td align="center"><?php echo $list->uid; ?></td>
                    <td><?php echo $list->cname; ?></td>
                    <td align="center"><?php echo date("d/m/Y - H:i", strtotime($list->created_date)); ?></td>
                    <td><?php echo $list->answered_by_name; ?></td>
                    <td><?php echo $list->answered_date!=''?date("d/m/Y - H:i", strtotime($list->answered_date)):''; ?></td>
                    <td><?php echo $list->coach_name; ?></td>
                </tr>
            <?php } ?>
        <?php } ?>                                        
    </table>   
<?php } ?> 