<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <a href="<?php echo base_url('print_coaching/'.$form_info->uid); ?>" class="btn btn-primary pull-right" target="_blank" ><i class="fa fa-print"></i> &nbsp; Print Form</a>
                Forms <i class="lnr lnr-chevron-right"></i> Coaching Enquire <i class="lnr lnr-chevron-right"></i> View
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <form action="<?php echo current_url(); ?>" method="post" enctype="multipart/form-data" style="padding-top: 20px;">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" class="form-control" id="" placeholder="First Nome" value="<?php echo $form_info->first_name; ?>" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control" id="" placeholder="Last Nome" value="<?php echo $form_info->last_name; ?>" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>City & Region within NZ</label>
                                    <input type="text" class="form-control" id="" placeholder="City & Region within NZ" value="<?php echo $form_info->city_region; ?>" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Phone</label>
                                    <input type="text" class="form-control" id="" placeholder="Phone" maxlength="20" value="<?php echo $form_info->phone; ?>" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label>Email</label>
                                <input type="email" class="form-control" id="" placeholder="Email" maxlength="50" value="<?php echo $form_info->email; ?>" readonly>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Brief Description</label>
                                    <textarea class="form-control" name="" id="" placeholder="Give a brief description of your business and what help you would like from working with a coach" rows="3" readonly><?php echo $form_info->business_description; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-12" style="padding-top: 20px; padding-bottom: 20px;">
                                <a href="javascript: history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; Back</a>                                    
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->