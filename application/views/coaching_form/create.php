<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                Forms <i class="lnr lnr-chevron-right"></i> Create <i class="lnr lnr-chevron-right"></i> Coaching Enquire Form
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <form action="<?php echo current_url(); ?>" method="post" enctype="multipart/form-data" style="padding-top: 20px;" name="temp_form" id="temp_form">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name" data-validation="required" data-validation-error-msg="The field first name is mandatory, please check again!">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name" data-validation="required" data-validation-error-msg="The field last name is mandatory, please check again!">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>City & Region within NZ</label>
                                    <input type="text" class="form-control" name="city_region" id="city_region" placeholder="City & Region within NZ" data-validation="required" data-validation-error-msg="The field city is mandatory, please check again!">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Phone</label>
                                    <input type="text" class="form-control phone" name="phone" id="phone" placeholder="Phone" maxlength="20" data-validation="required" data-validation-error-msg="The field phone is mandatory, please check again!">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email" maxlength="50" value="" data-validation="required email" data-validation-error-msg-required="The field email is mandatory, please check again!" data-validation-error-msg-email="You have not given a correct e-mail address">
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Brief Description</label>
                                    <textarea class="form-control" name="comments" id="comments" placeholder="Give a brief description of your business and what help you would like from working with a coach" rows="3" data-validation="required" data-validation-error-msg="Please give a brief description of your business"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12" style="padding-top: 20px; padding-bottom: 20px;">
                                <a href="<?php echo base_url('coaching_form'); ?>" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; Back</a> &nbsp; 
                                <button type="submit" class="btn btn-primary" id="reg_me">SUBMIT</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN --> 
<script>
    $(document).ready(function () {
        $('.phone').mask('0000000000');

        //----------------------------------------------------------------------

        $.validate({
            form: '#temp_form',
            modules: 'security'
        });

        //----------------------------------------------------------------------
        
        $('#reg_me').on('click', function (e) {
            e.preventDefault();
            if($('#temp_form').isValid()){
                swal({
                    title: 'Do you want to submit this form?',
                    confirmButtonText: 'OK',
                    showLoaderOnConfirm: true,
                    showCancelButton: true
                }).then((result) => {
                    //$("#coach_frm_1").val('1');
                    $("#temp_form").submit();
                });
            }
        });
    });
</script>