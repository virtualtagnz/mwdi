<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <?php if($this->session->userdata('role') == '1'){?><a href="<?php echo base_url('create_coaching'); ?>" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> &nbsp; Create Coaching Enquire Form</a><?php } ?>
                <a href="<?php echo base_url('print_coaching_full_form'); ?>" class="btn btn-primary pull-right" style="margin-right: 10px;"><i class="fa fa-print"></i> &nbsp; Print</a>
                Forms <i class="lnr lnr-chevron-right"></i> Coaching Enquire
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable js-pointstable display">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Client Name</th>
                                    <th>Created Date</th>
                                    <th>Answered By</th>
                                    <th>Answered Date</th>
                                    <th>Coach</th>
                                    <th data-orderable="false"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($templist) { ?>
                                    <?php foreach ($templist as $list) { ?>                                
                                        <tr>
                                            <td><?php echo $list->uid; ?></td>
                                            <td><?php echo $list->cname; ?></td>
                                            <td><?php echo $list->created_date; ?></td>
                                            <td><?php echo $list->answered_by_name; ?></td>
                                            <td><?php echo $list->answered_date!=''?date("d/m/Y - H:i", strtotime($list->answered_date)):''; ?></td>
                                            <td><?php echo $list->coach_name; ?></td>
                                            <td class="text-right">
                                                <?php if (!($list->answered_by>0)) { ?>
                                                    <a href="javascript:void(0)" onclick="mark_as_answered(<?php echo $list->uid; ?>);" class="btn btn-fefault" style="margin: 0 6px;" data-toggle="tooltip" data-placement="top" title="Mark as answered"><i class="fa fa-check-square-o"></i></a>
                                                <?php } ?>
                                                <?php if($this->session->userdata('role') == '4'){?>
                                                    <?php if (!($list->coach>0)) { ?>
                                                        <a href="javascript:void(0)" onclick="become_coach(<?php echo $list->uid; ?>);" class="btn btn-fefault" style="margin: 0 6px;" data-toggle="tooltip" data-placement="top" title="Become Coach"><i class="fa fa-handshake-o"></i></a>
                                                    <?php } ?>
                                                <?php } ?>
                                                <a href="<?php echo base_url('edit_coaching/'.$list->uid); ?>" class="btn btn-fefault" style="margin: 0 6px;" data-toggle="tooltip" data-placement="top" title="View"><i class="fa fa-eye"></i></a>
                                                <?php if($this->session->userdata('role') == '1'){?>
                                                <a href="<?php echo base_url('convert_client/'.$this->encrypt->encode($list->uid).'/'.$this->encrypt->encode('5')); ?>" class="btn btn-fefault" style="margin: 0 6px;" data-toggle="tooltip" data-placement="top" title="Convert as client"><i class="fa fa-link"></i></a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>                                        
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<?php if ($this->session->flashdata('submit')) { ?>
    <script>swal({type: 'success', title: 'success', text: 'The form has been successfully saved', timer: 3000});</script>
<?php } else if ($this->session->flashdata('error')) { ?>
    <script>swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000});</script>
<?php } ?>
<script type="text/javascript">
    $(function () {
        $('.js-pointstable').DataTable({
            "responsive": true,
            "pageLength": 10,
            "searching": true,
            "info": true,
            "ordering": [[ 0, "desc" ]]
        });
    });

    function mark_as_answered(form_uid) {
        swal({
            title: 'Do you confirm that you answered that client?',
            confirmButtonText: 'Confirm',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            preConfirm: function () {
                return new Promise(function (resolve) {
                    $.ajax({
                        url: '<?php echo base_url('mark_as_answered/')?>'+form_uid,
                        dataType: 'JSON'
                    }).done(function (response) {
                        console.log(response);
                        if(response=='success'){
                            swal({type: 'success', title: 'success', text: 'The status answered has been placed for this client'}).then(function(){location.reload();});
                        }
                    });
                });
            }
        }).catch(swal.noop);
//        if (confirm('Confirm that form #' + form_uid + ' was answered?')) {
//            $.ajax({
//                url: '<?php echo base_url('mark_as_answered/')?>'+form_uid,
//                dataType: 'JSON'
//            });
//            setTimeout(function() {location.reload();}, 500);
//        }
    }  

    function become_coach(form_uid) {
        swal({
            title: 'Do you confirm to become a coach for this client?',
            confirmButtonText: 'Confirm',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            preConfirm: function () {
                return new Promise(function (resolve) {
                    $.ajax({
                        url: '<?php echo base_url('become_coach/')?>'+form_uid,
                        dataType: 'JSON'
                    }).done(function (response) {
                        console.log(response);
                        if(response=='success'){
                            swal({type: 'success', title: 'success', text: 'You are now the coach of this client'}).then(function(){location.reload();});
                        }
                    });
                });
            }
        }).catch(swal.noop);
//        if (confirm('Become Coach. Confirm?')) {
//            $.ajax({
//                url: '<?php echo base_url('become_coach/')?>'+form_uid,
//                dataType: 'JSON'
//            });
//            setTimeout(function() {location.reload();}, 500);
//        }
    }  
</script>        