<div class="vertical-align-wrap">
    <div class="vertical-align-middle">
        <div class="auth-box ">
            <div class="left">
                <div class="content">
                    <?php if ($this->session->flashdata('success')) { ?>
                        <div class="text-center" style="color: #629f5c;">
                            <div>The form has been successfully submitted!</div>
                        </div>
                    <?php } else { ?>  
                        <form class="form-auth-small" action="<?php echo current_url(); ?>" method="post" autocomplete="off" name="create_out" id="create_out">
                            <div class="form-group">
                                <label for="" class="control-label sr-only">First Name</label>
                                <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name" data-validation="required" data-validation-error-msg="The field first name is mandatory, please check again!">
                            </div>
                            <div class="form-group">
                                <label for="" class="control-label sr-only">Last Name</label>
                                <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name" data-validation="required" data-validation-error-msg="The field last name is mandatory, please check again!">
                            </div>
                            <div class="form-group">
                                <label for="" class="control-label sr-only">City / Region</label>
                                <input type="text" class="form-control" name="city_region" id="city_region" placeholder="City & Region within NZ" data-validation="required" data-validation-error-msg="The field city is mandatory, please check again!">
                            </div>
                            <div class="form-group">
                                <label for="" class="control-label sr-only">Phone</label>
                                <input type="num" class="form-control phone" name="phone" id="phone" placeholder="Phone" maxlength="20" data-validation="required" data-validation-error-msg="The field phone is mandatory, please check again!">
                            </div>
                            <div class="form-group">
                                <label for="" class="control-label sr-only">Email</label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email" maxlength="50" value="" data-validation="required email" data-validation-error-msg-required="The field email is mandatory, please check again!" data-validation-error-msg-email="You have not given a correct e-mail address">
                            </div>
                            <div class="form-group">
                                <label for="" class="control-label sr-only">Brief Description</label>
                                <textarea class="form-control" name="comments" id="comments" placeholder="Give a brief description of your business and what help you would like from working with a coach" rows="3" data-validation="required" data-validation-error-msg="Please give a brief description of your business"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary btn-lg btn-block" id="reg_submit">SUBMIT</button>
                        </form>
                        <div class="text-center" style="color: #a94442;">
                            <?php if ($this->session->flashdata('result')) { ?>
                                <div> <?= $this->session->flashdata('result') ?> </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="right">
                <div class="overlay"></div>
                <div class="content text">
                    <h1 class="heading">Welcome to Māori Womens Development Inc</h1>
                    <p>Hei Manaaki i Te Mana Wāhine Māori</p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.phone').mask('0000000000');
        
        //----------------------------------------------------------------------
        
        $.validate({
            form: '#create_out',
            modules: 'security'
        });
        
        //----------------------------------------------------------------------
        
        $('#reg_submit').on('click', function (e) {
            e.preventDefault();
            if($('#create_out').isValid()){
                swal({
                    title: 'Do you want to submit this form?',
                    confirmButtonText: 'OK',
                    showLoaderOnConfirm: true,
                    showCancelButton: true
                }).then((result) => {
                    //$("#coach_frm_1").val('1');
                    $("#create_out").submit();
                });
            }
        });
        
    });
</script>