<div class="vertical-align-wrap">
    <div class="vertical-align-middle">
        <div class="auth-box" style="height: 350px">
            <div class="left">
                <div class="content">
                    <?php if ($this->session->flashdata('success')) { ?>
                        <div class="text-center" style="color: #629f5c;">
                            <div>The form has been successfully submitted!</div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <a href="<?php echo base_url('downloads/pdf_forms.zip'); ?>" download><button type="button" class="btn btn-primary">Download forms</button></a>
                        </div>
                    <?php } else { ?>  
                        <form class="form-auth-small" action="<?php echo current_url(); ?>" method="post" autocomplete="off" name="create_out" id="create_out">
                            <div class="form-group">
                                <label for="" class="control-label sr-only">Name</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Name" data-validation="required" data-validation-error-msg="The field Name is mandatory, please check again!">
                            </div>
                            <div class="form-group">
                                <label for="" class="control-label sr-only">Email Address</label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email" maxlength="100" value="" data-validation="required email" data-validation-error-msg-required="The field email is mandatory, please check again!" data-validation-error-msg-email="You have not given a correct e-mail address">
                            </div>

                            <div class="form-group">
                                <label for="" class="control-label sr-only">Industry</label>
                                <select class="form-control" style="color:#999" name="region" id="region" data-validation="required" data-validation-error-msg="The field Region is mandatory, please check again!">
                                    <option value="">Region</option>
                                    <?php
                                    if ($regions) {
                                        foreach ($regions as $region) {
                                            if (strlen($region['add_region']) > 0) {
                                                echo '<option value="' . $region['add_region'] . '">' . $region['add_region'] . '</option>';
                                            }
                                        }
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="" class="control-label sr-only">Industry</label>
                                <input type="text" class="form-control" name="industry" id="industry" placeholder="Industry" data-validation="required" data-validation-error-msg="The field Industry is mandatory, please check again!">
                            </div>

                            <div class="form-group">
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="attach_forms" id="attach_forms">
                                    <span>
                                        Email forms to myself
                                    </span>
                                </label>
                            </div>

                            <button type="submit" class="btn btn-primary btn-lg btn-block" id="reg_submit">SUBMIT</button>
                        </form>
                        <div class="text-center" style="color: #a94442;">
                            <?php if ($this->session->flashdata('result')) { ?>
                                <div> <?= $this->session->flashdata('result') ?> </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="right">
                <div class="overlay"></div>
                <div class="content text">
                    <h1 class="heading">Welcome to Māori Womens Development Inc</h1>
                    <p>Hei Manaaki i Te Mana Wāhine Māori</p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.phone').mask('0000000000');
        //----------------------------------------------------------------------

        $.validate({
            form: '#create_out',
            modules: 'security'
        });
        //----------------------------------------------------------------------

        $('#reg_submit').on('click', function (e) {
            e.preventDefault();
            if($('#create_out').isValid()){
                swal({
                    title: 'Do you want to submit this form?',
                    confirmButtonText: 'OK',
                    showLoaderOnConfirm: true,
                    showCancelButton: true
                }).then((result) => {
                    //$("#coach_frm_1").val('1');
                    $("#create_out").submit();
                });
            }
        });
    });
</script>