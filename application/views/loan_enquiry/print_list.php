<div class="title">
    <h2>Loan Enquires Forms</h2>
</div>
<?php if ($templist) { ?>   

    <table cellpadding="2" cellspacing="0" border="1" >
        <tr>
            <th width="45" align="center"><strong>ID</strong></th>
            <th width="170"><strong>Client Name</strong></th>
            <th width="170"><strong>Email</strong></th>
            <th width="170"><strong>Region</strong></th>
            <th width="170"><strong>Industry</strong></th>
            <th width="120" align="center"><strong>Answered Date</strong></th>
            <th width="120" align="center"><strong>Created Date</strong></th>
        </tr>
        <?php if ($templist) { ?>
            <?php foreach ($templist as $list) { ?>                                
                <tr>
                    <td align="center"><?php echo $list->uid; ?></td>
                    <td><?php echo $list->name; ?></td>
                    <td><?php echo $list->email; ?></td>
                    <td><?php echo $list->region; ?></td>
                    <td><?php echo $list->industry; ?></td>
                    <td align="center"><?php echo $list->date_answered!=''?date("d/m/Y - H:i", strtotime($list->date_answered)):'-'; ?></td>
                    <td align="center"><?php echo date("d/m/Y - H:i", strtotime($list->created_date)); ?></td>
                </tr>
            <?php } ?>
        <?php } ?>                                        
    </table>   
<?php } ?> 