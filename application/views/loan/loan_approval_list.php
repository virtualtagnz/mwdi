<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                Loans <i class="lnr lnr-chevron-right"></i> List to approval
            </h3>
            <?php //var_dump($clients); ?>
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="app_tab" class="table table-striped table-bordered dataTable js-pointstable display">
                            <thead>
                                <tr>
                                    <th>Client Name</th>
                                    <th>Forms</th>
                                    <th>Approve/Reject</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $all_clients = json_decode($clients) ?>
                                <?php foreach ($all_clients as $client) { ?>
                                    <?php if (($client->BA != NULL) && ($client->PPMS != NULL) && ($client->SEC != NULL)) { ?>
                                    <?php //var_dump($client); ?>
                                        <tr>
                                            <td><?php echo $client->first_name . ' ' . $client->last_name ?></td>
                                            <td>
                                                <a href="<?php echo base_url('baform/pdf/'.$this->encrypt->encode($client->BA))?>" target="blank" class="btn btn-fefault" data-toggle="tooltip" data-placement="top" title="Print"><i class="fa fa-print"></i>&nbsp; - BA FORM</a>&nbsp;
                                                <a href="<?php echo base_url('uploads/ppms_forms/' . $client->PPMS); ?>" target="blank" class="btn btn-fefault" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="fa fa-print"></i>&nbsp; - PPMS FORM</a>&nbsp;
                                                <a href="<?php echo base_url('view_security_form/' . $this->encrypt->encode($client->client_id)); ?>" target="blank" class="btn btn-fefault" data-toggle="tooltip" data-placement="top" title="Print"><i class="fa fa-print"></i>&nbsp; - SECURITY FORM</a>&nbsp;
                                            </td>
                                            <td><a href="<?php echo base_url('loan_approval_link/' . $this->encrypt->encode($client->client_id)); ?>" class="btn btn-fefault" data-toggle="tooltip" data-placement="top" title="Approve/ Reject Loan"><i class="fa fa-thumbs-up"></i></a> &nbsp;</td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<?php if ($this->session->flashdata('success_msg')) { ?>
    <script>swal({type: 'success', title: 'success', text: 'The loan status successfully changed', timer: 3000});</script>
<?php } else if ($this->session->flashdata('error_msg')) { ?>
    <script>swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000});</script>
<?php } ?>
<script>
    $(document).ready(function () {
        $('#app_tab').dataTable({
            "pageLength": 10,
            "searching": true,
            "info": true,
            "ordering": true
        });
    });
</script>