<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                Loan Approve / Reject
            </h3>
            <form action="<?php echo base_url('approve/'.end($this->uri->segments)) ?>" id="loan_approve_frm" name="loan_approve_frm" method="post" enctype="multipart/form-data">
                <div class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Client Name</label>
                                    <input type="text" class="form-control" id="app_client" name="app_client" placeholder="Client Name" data-validation="required" data-validation-error-msg="The field client name is mandatory, please check again!" value="<?php echo $client_info->first_name.' '.$client_info->last_name; ?>" disabled="true">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Approve/Reject</label>
                                    <select class="form-control" id="app_stat" name="app_stat" data-validation="required" data-validation-error-msg="The field loan approve/reject is mandatory, please check again!">
                                        <option value="" selected="selected">-- Select --</option>
                                        <option value="1" <?php echo set_select('app_stat', '1'); ?>>Approve</option>
                                        <option value="2" <?php echo set_select('app_stat', '2'); ?>>Reject</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Loan Amount</label>
                                    <input type="text" class="form-control" id="loan_amt" name="loan_amt" placeholder="Loan Amount" data-validation="required" data-validation-error-msg="The field loan amount is mandatory, please check again!" data-validation-depends-on="app_stat" data-validation-depends-on-value="1" value="<?php echo set_value('loan_amt'); ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Proposed end date for final payment</label>
                                    <input type="text" class="form-control" id="end_date" name="end_date" placeholder="Proposed end date for final payment" data-validation="required date gt_currnt_date" data-validation-format="dd/mm/yyyy" data-validation-error-msg-required="The field proposed end date is mandatory, please check again!" data-validation-error-msg-gt_currnt_date="Please enter date in the future!" data-validation-depends-on="app_stat" data-validation-depends-on-value="1" value="<?php echo set_value('end_date'); ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" name="app_des" id="app_des" placeholder="Description" rows="3" ><?php echo set_value('app_des'); ?></textarea> 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="padding-top: 20px; padding-bottom: 20px;">
                                <a href="javascript: history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; Back</a> &nbsp; 
                                <button type="submit" class="btn btn-primary" name="btn_approve" id="btn_approve">SUBMIT</button>
                            </div> 
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<?php if ($this->session->flashdata('success_msg')) { ?>
    <script>swal({type: 'success', title: 'success', text: 'The form has been successfully saved', timer: 3000});</script>
<?php } else if ($this->session->flashdata('error_msg')) { ?>
    <script>swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000});</script>
<?php } ?>
<script>
    $(document).ready(function () {
        $.validate({
            form: '#loan_approve_frm',
            modules: 'security,date,logic'
        });
        
        //----------------------------------------------------------------------
        
        // Add custom validation rule
        $.formUtils.addValidator({
            name : 'gt_currnt_date',
            validatorFunction : function(value, $el, config, language, $form) {
                var today = new Date(); 
                var sel_date = value.split("/")
                var myDate = new Date(sel_date[2],sel_date[1]-1,sel_date[0]);
                if( myDate <= today){
                    return false;
                }else{
                    return true;
                }
        },
            errorMessage : 'Please enter date in the future',
            errorMessageKey: 'baddate'
        });
        
        //----------------------------------------------------------------------
        
        $('#loan_amt').mask('000,000,000,000,000.00', {reverse: true});
        $('#end_date').mask('00/00/0000', {placeholder: "Proposed end date for final payment (DD/MM/YYYY)"});
        
        //----------------------------------------------------------------------

        $('#btn_approve').on('click', function (e) {
            e.preventDefault();
            if($('#loan_approve_frm').isValid()){
                var stat = $('#app_stat').val();
                var print_stat;
                if(stat=='1'){
                    print_stat = "approve";
                }else{
                    print_stat = "reject";
                }
                swal({
                    title: 'Do you want to '+print_stat+' this loan',
                    confirmButtonText: 'Submit',
                    showLoaderOnConfirm: true,
                    showCancelButton: true
                }).then((result) => {
                    $("#loan_approve_frm").submit();
                });
            }
        });
    });
</script>
