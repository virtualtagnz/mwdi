<style>
    td{
        border: 1px solid #9D8526;
        padding: 5px;
    }
    tr.bg{
        background-color: #252c35;
        color: #ffffff;
    }
    .title{
        text-align: center;
    }
</style>

<div class="title">
    <h2>Client - Coach engagement time</h2>
</div>

<table cellspacing="0" cellpadding="5" border="1">
    <tr class="bg">
        <td colspan="4">
            <h4 class="title">List of coaches start work on clients</h4>
        </td>
    </tr>     
    <tr>
        <th><strong>Coach Name</strong></th>
        <th><strong>Client Name</strong></th>
        <th><strong>Assigned Date</strong></th>
        <th><strong>Date Start Work With Client</strong></th>
    </tr>
    <?php if ($coach_client_list) { ?>
        <?php $users_list = json_decode($coach_client_list) ?>
        <?php foreach ($users_list as $list) { ?>                                        
            <tr>
                <td><?php echo $list->coach_fname . ' ' . $list->coach_lname; ?></td>
                <td><?php echo $list->client_fname . ' ' . $list->client_lname; ?></td>
                <td><?php echo $list->assign_date; ?></td>
                <td><?php echo $list->start_date; ?></td>
            </tr>
        <?php } ?>
    <?php } ?>                      
</table>