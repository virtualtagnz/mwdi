<?php
$industry_list = $report_list;
?>
<style>
    td{
        border: 1px solid #9D8526;
        padding: 5px;
    }
    tr.bg{
        background-color: #252c35;
        color: #ffffff;
    }
    .title{
        text-align: center;
    }
</style>

<div class="title">
    <h2>Industry Report</h2>
</div>

<table cellspacing="0" cellpadding="5" border="1">
    <tr class="bg">
        <td colspan="2">
            <h4 class="title">Industry List</h4>
        </td>
    </tr>     
    <tr>
        <th><strong>Industry</strong></th>
        <th><strong>Number of Clients</strong></th>
    </tr>
    <?php if ($industry_list) { ?>
        <?php foreach ($industry_list as $list) { ?>                                        
            <tr>
                <td><?php echo $list->industry; ?></td>
                <td><?php echo $list->cnt; ?></td>
            </tr>
        <?php } ?>
    <?php } ?>                
</table>