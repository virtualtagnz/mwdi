<?php
$stats        = $report_stats;
$applications = $report_list;
?>
<style>
    td{
        border: 1px solid #9D8526;
        padding: 5px;
    }
    tr.bg{
        background-color: #252c35;
        color: #ffffff;
    }
    .title{
        text-align: center;
    }
</style>

<div class="title">
    <h2>Loan Applications Report</h2>
</div>

<table cellspacing="0" cellpadding="5" border="1">
    <tr class="bg">
        <td colspan="3">
            <h4 class="title">Stats</h4>
        </td>
    </tr>    
    <tr>
        <td class="title"><?php echo $stats['forms_assigned']; ?></td>
        <td class="title"><?php echo $stats['completed_forms']; ?></td>
        <td class="title"><?php echo round($stats['success_rate'],1); ?>%</td>
    </tr>
    <tr>
        <td class="title">NUMBER OF FORMS ASSIGNED</td>
        <td class="title">COMPLETED FORMS</td>
        <td class="title">SUCCES RATE</td>
    </tr>
</table>

<div style="page-break-inside: avoid;"></div>

<table cellspacing="0" cellpadding="5" border="1">
    <tr class="bg">
        <td colspan="3">
            <h4 class="title">Loan Applications List</h4>
        </td>
    </tr>     
    <tr>
        <th><strong>Client Name</strong></th>
        <th><strong>Submit Status</strong></th>
        <th><strong>Submit Date</strong></th>
    </tr>
        <?php if ($applications) { ?>
            <?php foreach ($applications as $list) { ?>
                <tr>
                    <td><?php echo $list->cname; ?></td>
                    <td><?php echo $list->sub_stat; ?></td>
                    <td><?php echo $list->submit_date; ?></td>
                </tr>
            <?php } ?>
        <?php } ?>               
</table>