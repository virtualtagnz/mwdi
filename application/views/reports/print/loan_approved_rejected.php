<?php
$stats        = $report_stats;
$applications = $report_list;
?>
<style>
    td{
        border: 1px solid #9D8526;
        padding: 5px;
    }
    tr.bg{
        background-color: #252c35;
        color: #ffffff;
    }
    .title{
        text-align: center;
    }
</style>

<div class="title">
    <h2>Loan Approved / Rejected Report</h2>
</div>

<table cellspacing="0" cellpadding="5" border="1">
    <tr class="bg">
        <td colspan="2">
            <h4 class="title">Stats</h4>
        </td>
    </tr>    
    <tr>
        <td class="title"><?php echo $stats['loan_approved']; ?></td>
        <td class="title"><?php echo $stats['loan_rejected']; ?></td>
    </tr>
    <tr>
        <td class="title">LOAN APPROVED</td>
        <td class="title">LOAN REJECTED</td>
    </tr>
</table>

<div style="page-break-inside: avoid;"></div>

<table cellspacing="0" cellpadding="5" border="1">
    <tr class="bg">
        <td colspan="4">
            <h4 class="title">Loan Approved / Rejected List</h4>
        </td>
    </tr>     
    <tr>
        <th><strong>Client Name</strong></th>
        <th><strong>Loan Amount</strong></th>
        <th><strong>Approve/Reject</strong></th>
        <th><strong>Date Approved/Rejected</strong></th>
    </tr>
    <?php if ($applications) { ?>
        <?php foreach ($applications as $list) { ?>
            <tr>
                <td><?php echo $list->cname; ?></td>
                <td><?php echo $list->ln_amount; ?></td>
                <td><?php echo $list->app_stat; ?></td>
                <td><?php echo $list->app_date; ?></td>
            </tr>
        <?php } ?>
    <?php } ?>             
</table>

