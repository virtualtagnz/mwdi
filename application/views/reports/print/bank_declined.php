<?php
$stats     = $report_stats;
$bank_list = $report_list;
?>
<style>
    td{
        border: 1px solid #9D8526;
        padding: 5px;
    }
    tr.bg{
        background-color: #252c35;
        color: #ffffff;
    }
    .title{
        text-align: center;
    }
</style>

<div class="title">
    <h2>Bank Declined Report</h2>
</div>

<table cellspacing="0" cellpadding="5" border="1">
    <tr class="bg">
        <td colspan="4">
            <h4 class="title">Stats</h4>
        </td>
    </tr>    
    <tr>
        <td class="title"><?php echo $stats['security']; ?></td>
        <td class="title"><?php echo $stats['cashflow']; ?></td>
        <td class="title"><?php echo $stats['credit_rating']; ?></td>
        <td class="title"><?php echo $stats['other']; ?></td>
    </tr>
    <tr>
        <td class="title">SECURITY</td>
        <td class="title">CASHFLOW</td>
        <td class="title">CREDIT RATING</td>
        <td class="title">OTHER</td>
    </tr>
</table>

<div style="page-break-inside: avoid;"></div>

<table cellspacing="0" cellpadding="5" border="1">
    <tr class="bg">
        <td colspan="3">
            <h4 class="title">Loan Applications List</h4>
        </td>
    </tr>     
    <tr>
        <th><strong>Client Name</strong></th>
        <th><strong>Reason(s)</strong></th>
        <th><strong>Description</strong></th>
    </tr>
    <?php if ($bank_list) { ?>
        <?php foreach ($bank_list as $list) { 
            $temp = array();
            $ops = '';
            $temp = $list->try_ops;
            $pieces = explode(",", $temp);
            for ($k = 0; $k < count($pieces); $k++) {
                if ($pieces[$k] == '1') $ops = $ops . ' Security';
                if ($pieces[$k] == '2') $ops = $ops . ' Cashflow';
                if ($pieces[$k] == '3') $ops = $ops . ' Credit rating';
                if ($pieces[$k] == '4') $ops = $ops . ' Other';
            }
        ?>                                        
            <tr>
                <td><?php echo $list->cname; ?></td>
                <td><?php echo $ops; ?></td>
                <td><?php echo $list->try_result; ?></td>
            </tr>
        <?php } ?>
    <?php } ?>               
</table>