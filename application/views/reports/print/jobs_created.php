<?php
$stats     = $report_stats;
$jobs_list = $report_list;
?>
<style>
    td{
        border: 1px solid #9D8526;
        padding: 5px;
    }
    tr.bg{
        background-color: #252c35;
        color: #ffffff;
    }
    .title{
        text-align: center;
    }
</style>

<div class="title">
    <h2>Jobs Created Report</h2>
</div>

<table cellspacing="0" cellpadding="5" border="1">
    <tr class="bg">
        <td colspan="3">
            <h4 class="title">Stats</h4>
        </td>
    </tr>    
    <tr>
        <td class="title"><?php echo $stats[0]->tot_positions; ?></td>
        <td class="title"><?php echo $stats[0]->full_time; ?></td>
        <td class="title"><?php echo $stats[0]->part_time; ?></td>
    </tr>
    <tr>
        <td class="title">JOBS CREATED</td>
        <td class="title">FULL TIME POSITIONS</td>
        <td class="title">PART TIME POSITIONS</td>
    </tr>
</table>

<div style="page-break-inside: avoid;"></div>

<table cellspacing="0" cellpadding="5" border="1">
    <tr class="bg">
        <td colspan="4">
            <h4 class="title">Loan Applications List</h4>
        </td>
    </tr>     
    <tr>
        <th><strong>Client Name</strong></th>
        <th><strong>Full Time Positions</strong></th>
        <th><strong>Part Time Positions</strong></th>
        <th><strong>Total Positions</strong></th>
    </tr>
    <?php if ($jobs_list) { ?>
        <?php foreach ($jobs_list as $list) { ?>                                        
            <tr>
                <td><?php echo $list->cname; ?></td>
                <td><?php echo $list->full_time; ?></td>
                <td><?php echo $list->part_time; ?></td>
                <td><?php echo ($list->full_time+$list->part_time); ?></td>
            </tr>
        <?php } ?>
    <?php } ?>               
</table>