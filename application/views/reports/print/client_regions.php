<?php
$client_region_list = $report_list;
?>
<style>
    td{
        border: 1px solid #9D8526;
        padding: 5px;
    }
    tr.bg{
        background-color: #252c35;
        color: #ffffff;
    }
    .title{
        text-align: center;
    }
</style>

<div class="title">
    <h2>Client Region Report</h2>
</div>

<table cellspacing="0" cellpadding="5" border="1">
    <tr class="bg">
        <td colspan="3">
            <h4 class="title">Client Region List</h4>
        </td>
    </tr>     
    <tr>
        <th><strong>Client Name</strong></th>
        <th><strong>Category</strong></th>
        <th><strong>Region</strong></th>
    </tr>
    <?php if ($client_region_list) { ?>
        <?php foreach ($client_region_list as $list) { ?>                                        
            <tr>
                <td><?php echo $list->cname; ?></td>
                <td><?php echo readable_client_category($list->client_category); ?></td>
                <td><?php echo $list->add_region; ?></td>
            </tr>
        <?php } ?>
    <?php } ?>                
</table>