<?php
$stats    = (json_decode($report_stats));
$enquires = $report_list;
$srate = 0;
if (intval($stats->completed_forms) > 0 && intval($stats->forms_assigned)) $srate = (intval($stats->completed_forms) / intval($stats->forms_assigned)) * 100;
?>
<style>
    td{
        border: 1px solid #9D8526;
        padding: 5px;
    }
    tr.bg{
        background-color: #252c35;
        color: #ffffff;
    }
    .title{
        text-align: center;
    }
</style>

<div class="title">
    <h2>Enquires Report</h2>
</div>

<table cellspacing="0" cellpadding="5" border="1">
    <tr class="bg">
        <td colspan="4">
            <h4 class="title">Stats</h4>
        </td>
    </tr>    
    <tr>
        <td class="title"><?php echo $stats->forms_assigned; ?></td>
        <td class="title"><?php echo $stats->completed_forms; ?></td>
        <td class="title"><?php echo $stats->temporary_enquiry; ?></td>
        <td class="title"><?php echo $srate; ?>%</td>
    </tr>
    <tr>
        <td class="title">FORMS ASSIGNED</td>
        <td class="title">TEMPORARY ENQUIRY</td>
        <td class="title">COMPLETED FORMS</td>
        <td class="title">SUCCESS RATE</td>
    </tr>
</table>

<div style="page-break-inside: avoid;"></div>

<table cellspacing="0" cellpadding="5" border="1">
    <tr class="bg">
        <td colspan="4">
            <h4 class="title">Enquires List</h4>
        </td>
    </tr>     
    <tr>
        <th><strong>Client Name</strong></th>
        <th><strong>Submit Status</strong></th>
        <th><strong>Submit Date</strong></th>
        <th><strong>User Type</strong></th>
    </tr>
    <?php if ($enquires) { ?>
        <?php foreach ($enquires as $list) { ?>    
            <tr>    
                <td><?php echo $list->cname; ?></td>
                <td><?php echo $list->sub_stat; ?></td>
                <td><?php echo $list->submit_date; ?></td>
                <td><?php echo $list->user_stat; ?></td>
            </tr>
        <?php } ?>
    <?php } ?>                
</table>