<?php
$stats    = $report_stats;
$enquires = $report_list;
?>
<style>
    td{
        border: 1px solid #9D8526;
        padding: 5px;
    }
    tr.bg{
        background-color: #252c35;
        color: #ffffff;
    }
    .title{
        text-align: center;
    }
</style>

<div class="title">
    <h2>Temporary Enquires Report</h2>
</div>

<table cellspacing="0" cellpadding="5" border="1">
    <tr class="bg">
        <td>
            <h4 class="title">Stats</h4>
        </td>
    </tr>    
    <tr>
        <td class="title"><?php echo $stats[0]->cnt; ?></td>
    </tr>
    <tr>
        <td class="title">NUMBER OF CLIENTS</td>
    </tr>
</table>

<div style="page-break-inside: avoid;"></div>

<table cellspacing="0" cellpadding="5" border="1">
    <tr class="bg">
        <td colspan="5">
            <h4 class="title">Temporary Enquires List</h4>
        </td>
    </tr>     
    <tr>
        <th><strong>Client Name</strong></th>
        <th><strong>Email</strong></th>
        <th><strong>Phone</strong></th>
        <th><strong>Mobile</strong></th>
        <th><strong>Address</strong></th>
    </tr>
    <?php if ($enquires) { ?>
        <?php foreach ($enquires as $list) { ?>    
            <tr>    
                <td><?php echo $list->u_name; ?></td>
                <td><?php echo $list->enq_mail; ?></td>
                <td><?php echo $list->enq_phn; ?></td>
                <td><?php echo $list->enq_mob; ?></td>
                <td><?php echo $list->enq_hadd; ?></td>
            </tr>
        <?php } ?>
    <?php } ?>                
</table>