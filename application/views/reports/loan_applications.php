<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <a href="<?php echo base_url('reports/print/loan_applications'); ?>" class="btn btn-primary pull-right" id="loan_print"><i class="fa fa-print"></i> &nbsp; Print Report</a>
                Reports <i class="lnr lnr-chevron-right"></i> Loan
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <br>
                    <div class="row">
                        <div class="col-md-4 col-xs-6">
                            <div class="c100 small center green p<?php echo $all_stats['completed_forms']; ?>">
                                <span><?php echo $all_stats['completed_forms']; ?></span>
                                <div class="slice">
                                    <div class="bar"></div>
                                    <div class="fill"></div>
                                </div>
                            </div>
                            <p class="text-center c100-text"><strong>NUMBER OF FORMS ASSIGNED</strong></p>
                        </div>
                        <div class="col-md-4 col-xs-6">
                            <div class="c100 small center green p<?php echo $all_stats['forms_assigned']; ?>">
                                <span><?php echo $all_stats['forms_assigned']; ?></span>
                                <div class="slice">
                                    <div class="bar"></div>
                                    <div class="fill"></div>
                                </div>
                            </div>
                            <p class="text-center c100-text"><strong>COMPLETED FORMS</strong></p>
                        </div>
                        <div class="col-md-4 col-xs-6">
                            <div class="c100 small green center p<?php echo round($all_stats['success_rate']); ?>">
                                <span><?php echo round($all_stats['success_rate'], 1); ?>%</span>
                                <div class="slice">
                                    <div class="bar"></div>
                                    <div class="fill"></div>
                                </div>
                            </div>
                            <p class="text-center c100-text"><strong>SUCCESS RATE</strong></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable js-pointstable display">
                            <thead>
                                <tr>
                                    <th>Client Name</th>
                                    <th>Submit Status</th>
                                    <th>Submit Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($applications) { ?>
                                    <?php foreach ($applications as $list) { ?>
                                        <tr>
                                            <td><?php echo $list->cname; ?></td>
                                            <td><?php echo $list->sub_stat; ?></td>
                                            <td><?php echo $list->submit_date; ?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<script type="text/javascript">
    $(function () {
        $('.js-pointstable').DataTable({
            "responsive": true,
            "pageLength": 10,
            "searching": true,
            "info": true,
            "ordering": true
        });
    });
    
    $('#loan_print').on('click', function() {
            NProgress.start();
    });
</script>        