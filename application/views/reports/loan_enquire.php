<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <!--<a href="<?php echo base_url('reports/print/temporary_enquires'); ?>" class="btn btn-primary pull-right" target="_blank"><i class="fa fa-print"></i> &nbsp; Print Report</a>-->
                <a href="<?php echo base_url('loan/enquire/form'); ?>" target="_blank" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> &nbsp; Create Loan Enquiry Form</a>
                <a href="<?php echo base_url('loan/enquire/print/full'); ?>" class="btn btn-primary pull-right" style="margin-right: 10px;" id="btn_print"><i class="fa fa-print"></i> &nbsp; Print</a>

                Reports <i class="lnr lnr-chevron-right"></i> Loan Enquires
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <br>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="c100 small center green p<?php echo count($enquires); ?>">
                                <span><?php echo count($enquires); ?></span>
                                <div class="slice">
                                    <div class="bar"></div>
                                    <div class="fill"></div>
                                </div>
                            </div>
                            <p class="text-center c100-text"><strong>NUMBER OF CLIENTS</strong></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable js-pointstable display">
                            <thead>
                                <tr>
                                    <th>Client Name</th>
                                    <th>Email</th>
                                    <th>Region</th>
                                    <th>Industry</th>
                                    <th class="text-center">Type</th>
                                    <th class="text-center">Create Date</th>
                                    <th class="text-center">Answered Date</th>
                                    <th></th>
                                </tr>
                                <tr id="filterrow">
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th class="text-center">Type</th>
                                    <th class="text-center"></th>
                                    <th class="text-center"></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($enquires) { ?>
                                    <?php foreach ($enquires as $list) { ?>
                                        <tr>
                                            <td><?php echo $list->name; ?> </td>
                                            <td><?php echo $list->email; ?></td>
                                            <td><?php echo $list->region; ?></td>
                                            <td><?php echo $list->industry; ?></td>
                                            <td class="text-center"><?php echo ($list->old == 1?'Old register':'New register'); ?></td>
                                            <td class="text-center"><?php echo ($list->created_date? date("d/m/y h:i A", strtotime($list->created_date)):'-')?></td>
                                            <td class="text-center"><?php echo ($list->answered_by? date("d/m/y h:i A", strtotime($list->date_answered)):'-')?></td>
                                            <td>   
                                                <?php if (!($list->answered_by>0)) { ?>
                                                    <a href="javascript:void(0)" onclick="mark_as_answered(<?php echo $list->uid; ?>);" class="btn btn-fefault" style="margin: 0 6px;" data-toggle="tooltip" data-placement="top" title="Mark as answered"><i class="fa fa-check-square-o"></i></a>
                                                <?php } ?>

                                                <?php if($this->session->userdata('role') == '1'){?>
                                                <a href="<?php echo base_url('convert_client/'.$this->encrypt->encode($list->uid).'/'.$this->encrypt->encode('5')); ?>" class="btn btn-fefault" style="margin: 0 6px;" data-toggle="tooltip" data-placement="top" title="Convert as client"><i class="fa fa-link"></i></a>
                                                <?php } ?>

                                                <?php if($this->session->userdata('role') == '1'){?>
                                                    <a href="javascript:void(0)" class="btn btn-fefault btn-delete-loan" data-uid="<?php echo $list->uid; ?>" style="margin: 0 6px;" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></a>
                                                <?php } ?>    
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<script type="text/javascript">
    function mark_as_answered(form_uid) {
        swal({
            title: 'Do you confirm that you answered that client?',
            confirmButtonText: 'Confirm',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            preConfirm: function () {
                return new Promise(function (resolve) {
                    $.ajax({
                        url: '<?php echo base_url('mark_as_answered/')?>'+form_uid,
                        dataType: 'JSON'
                    }).done(function (response) {
                        console.log(response);
                        if(response=='success'){
                            swal({type: 'success', title: 'success', text: 'The status answered has been placed for this client'}).then(function(){location.reload();});
                        }
                    });
                });
            }
        }).catch(swal.noop);
    } 

    $(function () {
        $('.js-pointstable').DataTable({
            "order": [],
            initComplete: function () {
                this.api().columns([4]).every( function () {
                    var column = this;
                    var select = $('<select><option value=""></option></select>')
                        .appendTo( $(column.header()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );
     
                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                } );
            }
        });

        

        $('.btn-delete-loan').on('click', function (e) {
            e.preventDefault();
            var $this = $(this);
            var $uid = $this.attr('data-uid');
            swal({
                title: 'Do you want to delete<br>this loan enquire?',
                confirmButtonText: 'OK',
                showLoaderOnConfirm: true,
                showCancelButton: true,
                allowOutsideClick: () => !swal.isLoading(),
                preConfirm: () => {
                return new Promise((resolve) => {
                  setTimeout(() => {
                    $.ajax({
                      type: "POST",
                      dataType: 'json',
                      url:  '<?php echo base_url('loan/enquire/ajax');?>',
                      data: {'uid':$uid},
                      success: function(data){
                        resolve();
                      }
                    });
                  }, 100)
                })
              }
            }).then((result) => {
                console.log(result);
                swal({type: 'success', title: 'success', text: 'The Loan enquire has been deleted'}).then(function(){location.reload();});
            }).catch(swal.noop);
        });
        
        //----------------------------------------------------------------------
        
        $('#btn_print').on('click', function() {
            NProgress.start();
        });
    });
</script>  