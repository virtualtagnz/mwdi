<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <a href="<?php echo base_url('reports/print/temporary_enquires'); ?>" class="btn btn-primary pull-right" id="tem_enq_print"><i class="fa fa-print"></i> &nbsp; Print Report</a>
                Reports <i class="lnr lnr-chevron-right"></i> Temporary Enquiry
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <br>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="c100 small center green p<?php echo $stat[0]->cnt; ?>">
                                <span><?php echo $stat[0]->cnt; ?></span>
                                <div class="slice">
                                    <div class="bar"></div>
                                    <div class="fill"></div>
                                </div>
                            </div>
                            <p class="text-center c100-text"><strong>NUMBER OF CLIENTS</strong></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable js-pointstable display">
                            <thead>
                                <tr>
                                    <th>Client Name</th>
                                    <th>Email</th>
<!--                                    <th>Phone</th>
                                    <th>Mobile</th>
                                    <th>Address</th>-->
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($enquires) { ?>
                                    <?php foreach ($enquires as $list) { ?>
                                        <tr>
                                            <td><?php echo $list->u_name; ?></td>
                                            <td><?php echo $list->enq_mail; ?></td>
<!--                                            <td><?php //echo $list->enq_phn; ?></td>
                                            <td><?php //echo $list->enq_mob; ?></td>
                                            <td><?php //echo $list->enq_hadd; ?></td>-->
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<script type="text/javascript">
    $(function () {
        $('.js-pointstable').DataTable({
            "responsive": true,
            "pageLength": 10,
            "searching": true,
            "info": true,
            "ordering": true
        });
    });
    
    $('#tem_enq_print').on('click', function() {
            NProgress.start();
    });
</script>  