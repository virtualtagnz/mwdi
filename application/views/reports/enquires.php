<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">

            <h3 class="page-title">
                <a href="<?php echo base_url('reports/print/enquires'); ?>" class="btn btn-primary pull-right" id="enq_print"<i class="fa fa-print"></i> &nbsp; Print Report</a>
                Reports <i class="lnr lnr-chevron-right"></i> Enquiry
            </h3>

            <div class="panel">
                <div class="panel-body">
                    <br>
                    <div class="row">
                        <div class="col-md-3 col-xs-6">
                            <div class="c100 small green center p<?php echo $all_stats['forms_assigned'] ?>">
                                <span><?php echo $all_stats['forms_assigned']; ?></span>
                                <div class="slice">
                                    <div class="bar"></div>
                                    <div class="fill"></div>
                                </div>
                            </div>
                            <p class="text-center c100-text"><strong>FORMS ASSIGNED</strong></p>
                        </div>
                        <div class="col-md-3 col-xs-6">
                            <div class="c100 small green center p<?php echo $all_stats['completed_forms'] ?>">
                                <span><?php echo $all_stats['completed_forms']; ?></span>
                                <div class="slice">
                                    <div class="bar"></div>
                                    <div class="fill"></div>
                                </div>
                            </div>
                            <p class="text-center c100-text"><strong>COMPLETED FORMS</strong></p>
                        </div>
                        <div class="col-md-3 col-xs-6">
                            <div class="c100 small center green p<?php echo $all_stats['temporary_enquiry'] ?>">
                                <span><?php echo $all_stats['temporary_enquiry']; ?></span>
                                <div class="slice">
                                    <div class="bar"></div>
                                    <div class="fill"></div>
                                </div>
                            </div>
                            <p class="text-center c100-text"><strong>TEMPORARY ENQUIRY</strong></p>
                        </div>
                        <div class="col-md-3 col-xs-6">
                            <div class="c100 small green center p<?php echo round($all_stats['success_rate']) ?>">
                                <span><?php echo round($all_stats['success_rate'], 1) . '%'; ?></span>
                                <div class="slice">
                                    <div class="bar"></div>
                                    <div class="fill"></div>
                                </div>
                            </div>
                            <p class="text-center c100-text"><strong>SUCCESS RATE</strong></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable js-pointstable display">
                            <thead>
                                <tr>
                                    <th>Client Name</th>
                                    <th>Submit Status</th>
                                    <th>Submit Date</th>
                                    <th>User Type</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($enquires) { ?>
                                    <?php foreach ($enquires as $list) { ?>
                                        <tr>
                                            <td><?php echo $list->cname; ?></td>
                                            <td><?php echo $list->sub_stat; ?></td>
                                            <td><?php echo $list->submit_date; ?></td>
                                            <td><?php echo $list->user_stat; ?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- END MAIN CONTENT -->

    <script type="text/javascript">
        $(function () {
            $('.js-pointstable').DataTable({
                "responsive": true,
                "pageLength": 10,
                "searching": true,
                "info": true,
                "ordering": true
            });
        });
        
        $('#eng_print').on('click', function() {
            NProgress.start();
        });
    </script>            