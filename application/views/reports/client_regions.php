<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <form action="<?php echo base_url('reports/print/client_region'); ?>" id="client_reg_form" name="client_reg_form" method="post" enctype="multipart/form-data">
            <h3 class="page-title">
<!--                <a href="<?php echo base_url('reports/print/client_region'); ?>" class="btn btn-primary pull-right" id="clr_print"><i class="fa fa-print"></i> &nbsp; Print Report</a>-->
                <button type="submit" class="btn btn-primary pull-right" id="clr_print"><i class="fa fa-print"></i> &nbsp; Print Report</button>
                Reports <i class="lnr lnr-chevron-right"></i> Client Regions
            </h3>
            <input type="hidden" name="selected_category" id="selected_category" value="">
            <input type="hidden" name="selected_region" id="selected_region" value="">
            </form>
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable js-pointstable display">
                            <thead>
                                <tr>
                                    <th>Client Name</th>
                                    <th>Category</th>
                                    <th>Region</th>
                                </tr>
                                <tr id="filterrow">
                                    <th></th>
                                    <th>Category</th>
                                    <th>Region</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($client_region_list) { ?>
                                    <?php foreach ($client_region_list as $list) { ?>                                        
                                        <tr>
                                            <td><?php echo $list->cname; ?></td>
                                            <td><?php echo readable_client_category($list->client_category); ?></td>
                                            <td><?php echo $list->add_region; ?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>                                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="panel">
                        <div class="panel-body" style="line-height: 0; padding: 10px;" >
                            <div class="col-md-8" id="map" style="width: 100%; height: 450px;"></div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered dataTable js-pointstable display">
                                    <thead>
                                        <tr>
                                            <th>Region ID</th>
                                            <th>Region Name</th>
                                            <th>#Clients</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if ($client_region_summary) { ?>
                                            <?php foreach ($client_region_summary as $list) { ?>     
                                                <tr>
                                                    <td><?php echo $list['city_leter']; ?></td>
                                                    <td><?php echo $list['add_region']; ?></td>
                                                    <td><?php echo $list['cnt']; ?></td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>  
                                    </tbody>
                                </table>
                                <p>ND = Not Define</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<script type="text/javascript">
    $(function () {
        $('.js-pointstable').DataTable({
            "order": [],
            "responsive": true,
            "pageLength": 10,
            "searching": true,
            "info": true,
            "ordering": true,
            initComplete: function () {
                var i = 0;
                this.api().columns([1,2]).every( function () {
                    var column = this;
                    
                    var select = $('<select id="'+(i==0?"cat":"reg")+'"></select>')
                        .appendTo( $(column.header()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
                            column.search( val ? '^'+val+'$' : '', true, false ).draw();
                            console.log(column.selector.cols);
                            if(column.selector.cols=='1'){
                                var db_val = "";
                                switch (val){
                                    case "Loan client":
                                        db_val="loan-client";
                                        break;
                                    case "Coachee":
                                        db_val="cocheee";
                                        break;
                                    case "Both":
                                        db_val="both";
                                        break;
                                }
                                $("#selected_category").val(db_val);
                            }
                            if(column.selector.cols=='2'){
                                $("#selected_region").val(val);
                            }
                        } );
     
                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' );
                    } );
                    i++;
                } );
            }
        });
    });
    
    var citymap = {
        Northland: {
            center: {lat: -35.393, lng: 173.148},
            city_code: 'A'
        },
        Auckland: {
            center: {lat: -36.862, lng: 174.585},
            city_code: 'B'
        },
        Waikato: {
            center: {lat: -37.967, lng: 175.489},
            city_code: 'C'
        },
        BayPlenty: {
            center: {lat: -38.153, lng: 176.641},
            city_code: 'D'
        },
        Gisborne: {
            center: {lat: -38.623, lng: 178.024},
            city_code: 'E'
        },
        HawkBay: {
            center: {lat: -39.168, lng: 176.454},
            city_code: 'F'
        },
        Taranaki: {
            center: {lat: -39.184, lng: 174.779},
            city_code: 'G'
        },
        Manawatu: {
            center: {lat: -39.470, lng: 175.450},
            city_code: 'H'
        },
        Wellington: {
            center: {lat: -41.242, lng: 174.730},
            city_code: 'I'
        },
        Tasman: {
            center: {lat: -41.550, lng: 172.615},
            city_code: 'J'
        },
        Nelson: {
            center: {lat: -41.304, lng: 173.303},
            city_code: 'K'
        },
        Marlborough: {
            center: {lat: -42.194, lng: 173.052},
            city_code: 'L'
        },
        WestCoast: {
            center: {lat: -43.581, lng: 169.876},
            city_code: 'M'
        },
        Canterbury: {
            center: {lat: -43.784, lng: 171.160},
            city_code: 'N'
        },
        Otago: {
            center: {lat: -45.281, lng: 169.591},
            city_code: 'O'
        },
        Southland: {
            center: {lat: -46.250, lng: 167.992},
            city_code: 'P'
        },
        ChathamIslands: {
            center: {lat: -43.816, lng: -176.548},
            city_code: 'Q'
        }
    };

    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: -40.900, lng: 173.003},
            zoom: 6,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        for (var city in citymap) {
            var marker = new google.maps.Marker({
                position: citymap[city].center,
                map: map,
                label: citymap[city].city_code,
                labelClass: "labels",
                icon: {
                    path: google.maps.SymbolPath.CIRCLE,
                    scale: 10,
                    fillColor: 'red',
                    fillOpacity: 1,
                    strokeColor: 'red',
                    strokeWeight: 2,
                }
            });
        }
    }

    function circleSymbol(color) {
        return {
            path: 'M10,50a40,40 0 1,0 80,0a40,40 0 1,0 -80,0 Z',
            fillColor: color,
            fillOpacity: 1,
            strokeColor: '#951b81',
            strokeWeight: 2,
            scale: 0.2
        };
    }
    
    $('#clr_print').on('click', function() {
            NProgress.start();
    });
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCU5pwsCrj8K5Sf7dvZ1fmq2VRfG4CMg64&callback=initMap"
async defer></script>        
