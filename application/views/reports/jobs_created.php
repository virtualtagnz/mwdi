<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <a href="<?php echo base_url('reports/print/jobs_created'); ?>" class="btn btn-primary pull-right" id="job_print"><i class="fa fa-print"></i> &nbsp; Print Report</a>
                Reports <i class="lnr lnr-chevron-right"></i> Jobs Created
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <br>
                    <div class="row">
                        <div class="col-md-4 col-xs-6">
                            <div class="c100 small center green p<?php echo $all_stats[0]->tot_positions; ?>">
                                <span><?php echo $all_stats[0]->tot_positions; ?></span>
                                <div class="slice">
                                    <div class="bar"></div>
                                    <div class="fill"></div>
                                </div>
                            </div>
                            <p class="text-center c100-text"><strong>JOBS CREATED</strong></p>
                        </div>
                        <div class="col-md-4 col-xs-6">
                            <div class="c100 small center green p<?php echo $all_stats[0]->full_time; ?>">
                                <span><?php echo $all_stats[0]->full_time; ?></span>
                                <div class="slice">
                                    <div class="bar"></div>
                                    <div class="fill"></div>
                                </div>
                            </div>
                            <p class="text-center c100-text"><strong>FULL TIME POSITIONS</strong></p>
                        </div>
                        <div class="col-md-4 col-xs-6">
                            <div class="c100 small center green p<?php echo $all_stats[0]->part_time; ?>">
                                <span><?php echo $all_stats[0]->part_time; ?></span>
                                <div class="slice">
                                    <div class="bar"></div>
                                    <div class="fill"></div>
                                </div>
                            </div>
                            <p class="text-center c100-text"><strong>PART TIME POSITIONS</strong></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable js-pointstable display">
                            <thead>
                                <tr>
                                    <th>Client Name</th>
                                    <th>Full Time Positions</th>
                                    <th>Part Time Positions</th>
                                    <th>Total Positions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($jobs_list) { ?>
                                    <?php foreach ($jobs_list as $list) { ?>                                        
                                        <tr>
                                            <td><?php echo $list->cname; ?></td>
                                            <td><?php echo $list->full_time; ?></td>
                                            <td><?php echo $list->part_time; ?></td>
                                            <td><?php echo ($list->full_time + $list->part_time); ?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>                                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<script type="text/javascript">
    $(function () {
        $('.js-pointstable').DataTable({
            "responsive": true,
            "pageLength": 10,
            "searching": true,
            "info": true,
            "ordering": true
        });
    });
    
    $('#job_print').on('click', function() {
            NProgress.start();
    });
</script>        