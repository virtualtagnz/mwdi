<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <a href="<?php echo base_url('reports/print/industry'); ?>" class="btn btn-primary pull-right" id="ind_print"><i class="fa fa-print"></i> &nbsp; Print Report</a>
                Reports <i class="lnr lnr-chevron-right"></i> Industry
            </h3>

            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable js-pointstable display">
                            <thead>
                                <tr>
                                    <th>Industry</th>
                                    <th>Number of Clients</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($industry_list) { ?>
                                    <?php foreach ($industry_list as $list) { ?>                                        
                                        <tr>
                                            <td><?php echo $list->industry; ?></td>
                                            <td><?php echo $list->cnt; ?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>                                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<script type="text/javascript">
    $(function () {
        $('.js-pointstable').DataTable({
            "responsive": true,
            "pageLength": 10,
            "searching": true,
            "info": true,
            "ordering": true
        });
    });
    
    $('#ind_print').on('click', function() {
            NProgress.start();
    });
</script>