<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <a href="<?php echo base_url('reports/print/client_mark_as_start'); ?>" class="btn btn-primary pull-right" id="eng_print"><i class="fa fa-print"></i> &nbsp; Print Report</a>
                Reports <i class="lnr lnr-chevron-right"></i> Client - Coach engagement time
            </h3>

            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable js-pointstable display">
                            <thead>
                                <tr>
                                    <th>Coach Name</th>
                                    <th>Client Name</th>
                                    <th>Assigned Date</th>
                                    <th>Client - Coach engagement time</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($coach_client_list) { ?>
                                <?php $users_list = json_decode($coach_client_list) ?>
                                    <?php foreach ($users_list as $list) { ?>                                        
                                        <tr>
                                            <td><?php echo $list->coach_fname.' '.$list->coach_lname; ?></td>
                                            <td><?php echo $list->client_fname.' '.$list->client_lname; ?></td>
                                            <td><?php echo $list->assign_date; ?></td>
                                            <td><?php echo $list->start_date; ?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>                                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<script type="text/javascript">
    $(function () {
        $('.js-pointstable').DataTable({
            "responsive": true,
            "pageLength": 10,
            "searching": true,
            "info": true,
            "ordering": true
        });
    });
    
    $('#eng_print').on('click', function() {
            NProgress.start();
    });
</script>
