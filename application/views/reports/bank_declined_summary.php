<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <a href="<?php echo base_url('reports/print/bank_declined'); ?>" class="btn btn-primary pull-right" id="bds_print"><i class="fa fa-print"></i> &nbsp; Print Report</a>
                Reports <i class="lnr lnr-chevron-right"></i> Bank Declined Summary
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <br>
                    <div class="row">
                        <div class="col-md-3 col-xs-6">
                            <div class="c100 small green center p<?php echo $all_stats['security']; ?>">
                                <span><?php echo $all_stats['security']; ?></span>
                                <div class="slice">
                                    <div class="bar"></div>
                                    <div class="fill"></div>
                                </div>
                            </div>
                            <p class="text-center c100-text"><strong>SECURITY</strong></p>
                        </div>
                        <div class="col-md-3 col-xs-6">
                            <div class="c100 small center p<?php echo $all_stats['cashflow']; ?>">
                                <span><?php echo $all_stats['cashflow']; ?></span>
                                <div class="slice">
                                    <div class="bar"></div>
                                    <div class="fill"></div>
                                </div>
                            </div>
                            <p class="text-center c100-text"><strong>CASHFLOW</strong></p>
                        </div>
                        <div class="col-md-3 col-xs-6">
                            <div class="c100 small center p<?php echo $all_stats['credit_rating']; ?>">
                                <span><?php echo $all_stats['credit_rating']; ?></span>
                                <div class="slice">
                                    <div class="bar"></div>
                                    <div class="fill"></div>
                                </div>
                            </div>
                            <p class="text-center c100-text"><strong>CREDIT RATING</strong></p>
                        </div>
                        <div class="col-md-3 col-xs-6">
                            <div class="c100 small center p<?php echo $all_stats['other']; ?>">
                                <span><?php echo $all_stats['other']; ?></span>
                                <div class="slice">
                                    <div class="bar"></div>
                                    <div class="fill"></div>
                                </div>
                            </div>
                            <p class="text-center c100-text"><strong>OTHER</strong></p>
                        </div>                                
                    </div>
                </div>
            </div>

            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable js-pointstable display">
                            <thead>
                                <tr>
                                    <th>Client Name</th>
                                    <th>Reason(s)</th>
                                    <th>Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($bank_list) { ?>
                                    <?php
                                    foreach ($bank_list as $list) {
                                        $temp = array();
                                        $ops = '';
                                        $temp = $list->try_ops;
                                        $pieces = explode(",", $temp);
                                        for ($k = 0; $k < count($pieces); $k++) {
                                            if ($pieces[$k] == '1')
                                                $ops = $ops . 'Security' . '<br>';
                                            if ($pieces[$k] == '2')
                                                $ops = $ops . 'Cashflow' . '<br>';
                                            if ($pieces[$k] == '3')
                                                $ops = $ops . 'Credit rating' . '<br>';
                                            if ($pieces[$k] == '4')
                                                $ops = $ops . 'Other';
                                        }
                                        ?>                                        
                                        <tr>
                                            <td><?php echo $list->cname; ?></td>
                                            <td><?php echo $ops; ?></td>
                                            <td><?php echo $list->try_result; ?></td>
                                        </tr>
    <?php } ?>
<?php } ?>                                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<script type="text/javascript">
    $(function () {
        $('.js-pointstable').DataTable({
            "responsive": true,
            "pageLength": 10,
            "searching": true,
            "info": true,
            "ordering": true
        });
    });
    
    $('#bds_print').on('click', function() {
            NProgress.start();
    });
</script>        