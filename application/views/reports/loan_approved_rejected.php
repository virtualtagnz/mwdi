<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <a href="<?php echo base_url('reports/print/loan_ar'); ?>" class="btn btn-primary pull-right" id="loan_app_print"><i class="fa fa-print"></i> &nbsp; Print Report</a>
                Reports <i class="lnr lnr-chevron-right"></i> Loan Approved/Rejected
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <br>
                    <div class="row">
                        <div class="col-md-6 col-xs-6">
                            <div class="c100 small center green p<?php echo $all_stats['loan_approved']; ?>">
                                <span><?php echo $all_stats['loan_approved']; ?></span>
                                <div class="slice">
                                    <div class="bar"></div>
                                    <div class="fill"></div>
                                </div>
                            </div>
                            <p class="text-center c100-text"><strong>LOAN APPROVED</strong></p>
                        </div>
                        <div class="col-md-6 col-xs-6">
                            <div class="c100 small center green p<?php echo $all_stats['loan_rejected']; ?>">
                                <span><?php echo $all_stats['loan_rejected']; ?></span>
                                <div class="slice">
                                    <div class="bar"></div>
                                    <div class="fill"></div>
                                </div>
                            </div>
                            <p class="text-center c100-text"><strong>LOAN REJECTED</strong></p>
                        </div>
                    </div>
                </div>
            </div>


            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable js-pointstable display">
                            <thead>
                                <tr>
                                    <th>Client Name</th>
                                    <th>Loan Amount</th>
                                    <th>Approve/Reject</th>
                                    <th>Date Approved/Rejected</th>
                                    <th>Proposed end date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($ar_list) { ?>
                                    <?php foreach ($ar_list as $list) { ?>
                                        <tr>
                                            <td><?php echo $list->cname; ?></td>
                                            <td><?php echo $list->ln_amount; ?></td>
                                            <td><?php echo $list->app_stat; ?></td>
                                            <td><?php echo $list->app_date; ?></td>
                                            <td><?php echo $list->app_stat == 'REJECTED' ? '-' : $list->last_date; ?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<script type="text/javascript">
    $(function () {
        $('.js-pointstable').DataTable({
            "responsive": true,
            "pageLength": 10,
            "searching": true,
            "info": true,
            "ordering": true
        });
    });
    
    $('#loan_app_print').on('click', function() {
            NProgress.start();
    });
</script>        