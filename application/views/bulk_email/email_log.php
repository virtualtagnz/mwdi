<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <a href="<?php echo base_url('new_email') ?>" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> &nbsp; New Email</a>
                Emails Management <i class="lnr lnr-chevron-right"></i> Emails
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable js-pointstable display" id="emai_log_tab">
                            <thead>
                                <tr>
                                    <th>Email</th>
                                    <th>Recipients</th>
                                    <th>Create Date and Time</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $created_emails = json_decode($all_created_emails) ?>
                                <?php foreach ($created_emails as $email) { ?>
                                    <tr>
                                        <td><a href="<?php echo base_url('email_detail_view/' . $this->encrypt->encode($email->uid)) ?>"><?php echo $email->subject ?></a></td>
                                        <td><?php echo (($email->sending_type == '1') ? 'All clients' : (($email->sending_type == '2') ? 'All Loan Clients' : (($email->sending_type == '3') ? 'All Coachee' : 'Custom Selection'))) ?></td>
                                        <td><?php echo $email->create_date ?></td>
                                        <td><span class="<?php echo (($email->status == 'pending') ? 'label label-info' : (($email->status == 'processing') ? 'label label-warning' : (($email->status == 'closed') ? 'label label-success' : ''))) ?>"><?php echo $email->status ?></span></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<div id="modalView">
    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="single_email_modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content col-lg-12" style="background: #fff;">
                <?php var_dump($single_email); ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#emai_log_tab').dataTable({
            "pageLength": 10,
            "searching": true,
            "info": true,
            "ordering": true
        });
    });
</script>


