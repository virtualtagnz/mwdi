<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                Emails Management <i class="lnr lnr-chevron-right"></i> Send Email
            </h3>
            <?php //var_dump($temp_clients);
            $all_client_count = intval(count($clients))+intval(count($temp_clients));
            ?>
            <div class="panel">
                <div class="panel-body">
                    <form action="<?php echo base_url('composer') ?>" method="post" name="email_form" id="email_form" enctype="multipart/form-data" style="padding-top: 20px;">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
<!--                                    <input type="text" class="form-control" id="client_email" name="client_email" placeholder="To - Start typing client name(s)" data-validation="required" data-validation-error-msg="The field client name is mandatory, please check again!">-->
                                    <select class="form-control js-example-basic-multiple" name="client_email[]" id="client_email" multiple="true" data-validation="required" data-validation-error-msg="The field client name is mandatory, please check again!">
                                        <?php if ($all_clients) { ?>
                                            <?php $ext_clients = json_decode($all_clients) ?>
                                            <?php foreach ($ext_clients as $client) { ?>
                                                <option value="<?php echo $client->uid.':'.$client->first_name . ' ' . $client->last_name . ':' . $client->email ?>"><?php echo $client->first_name . ' ' . $client->last_name . ' (' . $client->email . ') ' ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                    <div id="infoMessage"><?php echo form_error('client_email'); ?></div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <p style="padding: 6px 0;">
                                        <label class="radio radio-inline" style="margin: 0">
                                            <input name="to" id="to1" value="1" type="radio" data-validation="required" data-validation-error-msg="The field gender is mandatory, please check again!">
                                            <span>To All Clients <?php echo '('.$all_client_count.')'?></span>
                                        </label>
                                    </p>
                                    <div id="infoMessage"><?php echo form_error('to'); ?></div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <p style="padding: 6px 0;">
                                        <label class="radio radio-inline" style="margin: 0">
                                            <input name="to" id="to2" value="2" type="radio" data-validation="required" data-validation-error-msg="The field gender is mandatory, please check again!">
                                            <span>To All Loan Clients <?php echo '('.count($temp_clients).')';?></span>
                                        </label>
                                    </p>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <p style="padding: 6px 0;">
                                        <label class="radio radio-inline" style="margin: 0" data-validation="required" data-validation-error-msg="The field gender is mandatory, please check again!">
                                            <input name="to" id="to3" value="3" type="radio">
                                            <span>To All Coachee <?php echo '('.count($clients).')';?></span>
                                        </label>
                                    </p>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="email_sub" name="email_sub" placeholder="Subject" maxlength="200" data-validation="required" data-validation-error-msg="The field subject is mandatory, please check again!">
                                </div>
                                <div id="infoMessage"><?php echo form_error('email_sub'); ?></div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" name="des" id="des" cols="4" data-validation="required" data-validation-error-msg="The field description is mandatory, please check again!"></textarea>
                                </div>
                                <div id="infoMessage"><?php echo form_error('des'); ?></div>
                            </div>
                        </div>

                        <p>
                            <a href="javascript: history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; Back</a> &nbsp; 
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script>
    $(document).ready(function () {
        $.validate({
            form: '#email_form',
            modules: 'security',
            validateHiddenInputs : true
        });

        $('#client_email').select2();

        $('input:radio[name="to"]').change(function () {
            $('#client_email').val(null).trigger('change');
            $('#client_email').removeAttr("data-validation");
        });

        $('#client_email').on('select2:select', function (e) {
            $('#to1,#to2,#to3').attr({'data-validation-optional': 'true'});
        });

        CKEDITOR.replace('des');
        
        CKEDITOR.on('instanceReady', function () {
        $.each(CKEDITOR.instances, function (instance) {
            CKEDITOR.instances[instance].on("change", function (e) {
                for (instance in CKEDITOR.instances)
                    CKEDITOR.instances[instance].updateElement();
            });
        });
    });
    });
</script>