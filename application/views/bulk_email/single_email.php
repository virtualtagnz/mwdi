<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                Emails Management <i class="lnr lnr-chevron-right"></i> Detail View
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%" >
                        <tr>
                            <td align="center" valign="top" bgcolor="#ffffff"  width="100%">

                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td style="background: #8c7722; height: 20px;">
                                    <center>
                                        <table cellspacing="0" cellpadding="0" width="500" class="w320">
                                            <tr><td style="color: #cccccc;"><a style="color: #cccccc;" href="<?php echo base_url('subscribe/auth') ?>"></a></td></tr>
                                        </table>
                                    </center> 
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom: 3px solid #8c7722; background-color: #000000" width="100%">
                        <center>
                            <table cellspacing="0" cellpadding="0" width="500" class="w320">
                                <tr>
                                    <td valign="top" style="padding:10px 0; text-align:left;" class="mobile-center">
                                        <a href="<?php echo base_url(); ?>" target="blank"><img width="250" height="62" src="<?php echo base_url('assets/img/logo.png'); ?>"/></a>
                                    </td>
                                </tr>
                            </table>
                        </center>
                        </td>
                        </tr>
                        <tr>
                            <td valign="top">
                        <center>
                            <table cellspacing="0" cellpadding="0" width="500" class="w320">
                                <tr>
                                    <td>

                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="mobile-padding" style="text-align:left;">
                                                    <br/>
                                                    <br/>
                                                    Dear User Name,<br/><br/>
                                                    <?php echo $single_email->content ?><br/>
                                                    <p>Team MWDI</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="mobile-padding">
                                        <br/>&nbsp;
                                        <br/>
                                    </td>
                                </tr>
                            </table>
                        </center>
                        </td>
                        </tr>
                        <tr>
                            <td style="background-color:#000000;border-top: 3px solid #8c7722;height: 50px; padding: 15px;"></td>
                        </tr>
                    </table>
                    </td>
                    </tr>
                    </table>

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable js-pointstable display" id="emai_log_tab">
                            <thead>
                                <tr>
                                    <th>Recipient Name</th>
                                    <th>Email</th>
                                    <th>Sent Date and Time</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $users = json_decode($user_list) ?>
                                <?php foreach ($users as $user) { ?>
                                    <tr>
                                        <td><?php echo $user->user_name ?></td>
                                        <td><?php echo $user->user_email ?></td>
                                        <td><?php echo $user->date_sent ?></td>
                                        <td><?php echo $user->email_sent_stat ?></span></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script type="text/javascript">
    $(function () {
        $('.js-pointstable').DataTable({
            "responsive": true,
            "pageLength": 10,
            "searching": true,
            "info": true,
            "ordering": true
        });
    });
</script> 

