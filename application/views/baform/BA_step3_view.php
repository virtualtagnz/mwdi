<?php
$ba_form3_details = (json_decode($details));
?>
<style>
    td{
        border: 1px solid #9D8526;
        padding: 5px;
    }
    tr.bg{
        background-color: #252c35;
        color: #ffffff;
    }
    .title{
        text-align: center;
    }
</style>

<div class="title">
    <h2>Next of Kin Details</h2>
</div>
<?php if ($ba_form3_details) { ?>
    <table cellspacing="0" cellpadding="5" border="1">
        <tr class="bg">
            <td colspan="2">
                <h4 class="">Next of Kin 1 Details</h4>
            </td>
        </tr>
        <tr>
            <td>Name</td><td><?php echo $ba_form3_details->nk1_name;?></td>
        </tr>
        <tr>
            <td>Relationship to you</td><td><?php echo $ba_form3_details->nk1_rel;?></td>
        </tr>
        <tr>
            <td>Home Phone</td><td><?php echo $ba_form3_details->nk1_hm_phn;?></td>
        </tr>
        <tr>
            <td>Work Phone</td><td><?php echo $ba_form3_details->nk1_wk_phn;?></td>
        </tr>
        <tr>
            <td>Mobile</td><td><?php echo $ba_form3_details->nk1_mob;?></td>
        </tr>
        <tr>
            <td>Email</td><td><?php echo $ba_form3_details->nk1_email;?></td>
        </tr>
        <tr class="bg">
            <td colspan="2">
                <h4 class="">Next of Kin 2 Details</h4>
            </td>
        </tr>
        <tr>
            <td>Name</td><td><?php echo $ba_form3_details->nk2_name;?></td>
        </tr>
        <tr>
            <td>Relationship to you</td><td><?php echo $ba_form3_details->nk2_rel;?></td>
        </tr>
        <tr>
            <td>Home Phone</td><td><?php echo $ba_form3_details->nk2_hm_phn;?></td>
        </tr>
        <tr>
            <td>Work Phone</td><td><?php echo $ba_form3_details->nk2_wk_phn;?></td>
        </tr>
        <tr>
            <td>Mobile</td><td><?php echo $ba_form3_details->nk2_mob;?></td>
        </tr>
        <tr>
            <td>Email</td><td><?php echo $ba_form3_details->nk2_email;?></td>
        </tr>
    </table>
<!--    <br pagebreak="true" />-->
<?php } ?>




