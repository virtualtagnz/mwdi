<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <?php echo (isset($ba_uid) ? '<a href="' . base_url('baform/pdf/' . $this->encrypt->encode($ba_uid)) . '" class="btn btn-primary pull-right" target="_blank"><i class="fa fa-print"></i> &nbsp; Print Form</a>' : false) ?>
                Forms  <i class="lnr lnr-chevron-right"></i> Business Application <i class="lnr lnr-chevron-right"></i> Employment Details
            </h3>
            <div class="panel">
                <div class="panel-heading">
                    <small class="pull-right timer-counter-started"></small>
                    <h3 class="panel-title">YOUR EMPLOYMENT DETAILS</h3>
                </div>
                <div class="panel-body">
                    <?php //echo response_messages(); ?>
                    <form id="baform" method="post">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Name of previous or present employer (or self-employed)?</label>
                                    <input type="text" class="form-control" id="emp_name" placeholder="Name of previous or present employer (or self-employed)?" maxlength="150" data-validation="required" data-validation-error-msg="The field name of previous or present employer is mandatory, please check again!">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>What is your role?</label>
                                    <input type="text" class="form-control" id="wrk" placeholder="What is your role?" maxlength="150" value="" data-validation="required" data-validation-error-msg="The field role is mandatory, please check again!">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>How long employed there?</label>
                                    <input type="text" class="form-control" id="long_wrk" placeholder="How long employed there?" maxlength="150" value="" data-validation="required" data-validation-error-msg="The field period is mandatory, please check again!">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">SET EMPLOYER DETAILS</button><br>

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered" style="margin-top: 25px;">
                                        <thead>
                                            <tr>
                                                <th>Name of previous or present employer (or self-employed)?</th>
                                                <th>What is your work?</th>
                                                <th>How long employed there?</th>
                                                <th style="width: 90px;"></th>
                                            </tr>
                                        </thead>
                                        <tbody class="body_table_employment_details">
                                            <?php
                                            if ($employment_details) {
                                                foreach ($employment_details as $string) {
                                                    echo '
                                                            <tr class="employment_details_' . $string->emp_id . '">
                                                                <td>' . $string->emp_name . '</td>
                                                                <td>' . $string->wrk . '</td>
                                                                <td>' . $string->long_wrk . '</td>
                                                                <td><a href="" class="btn btn-fefault delete-employment-details" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" data-detail_uid="' . $string->emp_id . '"><i class="fa fa-trash"></i></a></td>
                                                            </tr>
                                                        ';
                                                }
                                            } else {
                                                echo '<tr class="no_register_found"><td colspan="4" class="text-center">No registers found</td></tr>';
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </form>
                    <form id="baform_second" action="<?php echo base_url('baform/save/form/step4') ?>" method="post">
                        <div class="col-md-12" style="margin-bottom: 15px;">
                            <hr>
                            <h4 style="margin-top: 30px;">COPIES OF TESTIMONIALS (include two, attach details)</h4>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" id="tes_name1" name="tes_name1" placeholder="Name" maxlength="50" value="<?php echo (isset($testimonials) && $testimonials ? $testimonials->tes_name1 : false) ?>"  data-validation="required" data-validation-error-msg="The field name is mandatory, please check again!">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Phone</label>
                                <input type="text" class="form-control phone" id="tes_phn1" name="tes_phn1" placeholder="Phone" maxlength="12" value="<?php echo (isset($testimonials) && $testimonials ? $testimonials->tes_phn1 : false) ?>" data-validation="required number length" data-validation-length="min6" data-validation-allowing="float" data-validation-error-msg="The field phone is mandatory, please check again!">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" class="form-control autocomplete" name="tes_add1" id="tes_add1" placeholder="Address" value="<?php echo (isset($testimonials) && $testimonials ? $testimonials->tes_add1 : false) ?>" autocomplete="off" data-validation="required" data-validation-error-msg="The field address is mandatory, please check again!">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" id="tes_name2" name="tes_name2" placeholder="Name" maxlength="50" value="<?php echo (isset($testimonials) && $testimonials ? $testimonials->tes_name2 : false) ?>" data-validation="required" data-validation-error-msg="The field name is mandatory, please check again!">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Phone</label>
                                <input type="text" class="form-control phone" id="tes_phn2" name="tes_phn2" placeholder="Phone" maxlength="12" value="<?php echo (isset($testimonials) && $testimonials ? $testimonials->tes_phn2 : false) ?>" data-validation="required number length" data-validation-length="min6" data-validation-allowing="float" data-validation-error-msg="The field phone is mandatory, please check again!">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" class="form-control autocomplete" name="tes_add2" id="tes_add2" placeholder="Address" value="<?php echo (isset($testimonials) && $testimonials ? $testimonials->tes_add2 : false) ?>" autocomplete="off" data-validation="required" data-validation-error-msg="The field address is mandatory, please check again!">
                            </div>
                        </div>

                        <div class="col-md-12" style="padding-top: 20px; padding-bottom: 20px;">
                            <input type="hidden" name="timer_started" id="timer_started" value="">
                            <input type="hidden" name="ba_uid" id="ba_uid" name="ba_uid" value="<?php echo $ba_uid; ?>">
                            <a href="<?php echo base_url('baform/step3/' . $ba_uid_encrypt) ?>" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; BACK</a> &nbsp;
                            <button type="submit" class="btn btn-default" name="save_form" value="1">SAVE FORM &nbsp; <i class="fa fa-save"></i></button> &nbsp;
                            <button type="submit" class="btn btn-primary" name="next_form" value="1">NEXT &nbsp; <i class="fa fa-arrow-right"></i></button>
                            &nbsp; [4/6]
                        </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<?php if ($this->session->flashdata('success')) { ?>
    <script>swal({type: 'success', title: 'Success', text: 'The form has been successfully saved!', timer: 3000});</script>
<?php } else if ($this->session->flashdata('warning')) { ?>
    <script>swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000});</script>
<?php } ?>
<script type="text/javascript">
    $(function () {
        $.validate({
            form: '#baform_second',
            modules: 'security'
        });

        $.validate({
            form: '#baform',
            modules: 'security',
            onSuccess: function () {
                var emp_name = $("#emp_name").val();
                var wrk = $("#wrk").val();
                var long_wrk = $("#long_wrk").val();
                var ba_uid = $("#ba_uid").val();
                var timer_started = $("#timer_started").val();

                $.ajax({
                    url: config.base_url + 'baform/ajax/employment_details',
                    data: {emp_name: emp_name, wrk: wrk, long_wrk: long_wrk, ba_uid: ba_uid, timer_started: timer_started, action: 'add'},
                    method: 'POST',
                    dataType: 'json',
                    beforeSend: function () {
                        swal({
                            type: 'info',
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            title: 'Adding, please wait....'
                        });
                    }
                }).done(function (data) {
                    if (data.status == true) {
                        $(".no_register_found").remove();
                        $(".body_table_employment_details").append('<tr class="employment_details_' + data.uid + '"><td>' + emp_name + '</td><td>' + wrk + '</td><td>' + long_wrk + '</td><td><a href="" class="btn btn-fefault delete-employment-details" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" data-detail_uid="' + data.uid + '"><i class="fa fa-trash"></i></a></td></tr>');
                    }

                    $("#baform").trigger("reset");
                    swal.close();
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    swal.close();
                    console.log(errorThrown);
                });

                return false;
            }
        });

        $(document).on('click', '.delete-employment-details', function (e) {
            e.preventDefault();
            var uid = $(this).attr("data-detail_uid");
            if (uid) {
                if (confirm('You really want delete these details?')) {
                    $.ajax({
                        url: config.base_url + 'baform/ajax/employment_details',
                        data: {ba_uid: uid, action: 'delete'},
                        method: 'POST',
                        dataType: 'json',
                        beforeSend: function () {
                            swal({
                                type: 'info',
                                allowEscapeKey: false,
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                title: 'Deleting, please wait....'
                            });
                        }
                    }).done(function (data) {
                        if (data.status == true) {
                            $(".employment_details_" + uid).remove();
                        }
                        swal.close();
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        swal.close();
                        console.log(errorThrown);
                    });
                }
            }
        });

        $("#baform :input").change(function () {
            if (!$("#timer_started").val()) {
//                var datetime = new Date().toLocaleString();
//                $(".timer-counter-started").html('Form started at ' + datetime);
//                $("#timer_started").val(datetime);
                  $('#timer_started').val(timer());
            }
        });

        $("#baform_second :input").change(function () {
            if (!$("#timer_started").val()) {
                var datetime = new Date().toLocaleString();
                $(".timer-counter-started").html('Form started at ' + datetime);
                $("#timer_started").val(datetime);
            }
        });

        $('#tes_phn1,#tes_phn2').mask('000000000000', {placeholder: "Phone"});
        $('#tes_phn1,#tes_phn2').mask('000000000000', {placeholder: "Phone"});
        $('#tes_phn1,#tes_phn2').mask('000000000000', {placeholder: "Phone"});
    });
</script>
<script>
    function initAutocomplete() {
        var acInputs = document.getElementsByClassName("autocomplete");
        for (var i = 0; i < acInputs.length; i++) {
            var autocomplete = new google.maps.places.Autocomplete(acInputs[i], {types: ['geocode'], componentRestrictions: {country: "nz"}});
            autocomplete.inputId = acInputs[i].id;
        }
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCU5pwsCrj8K5Sf7dvZ1fmq2VRfG4CMg64&libraries=places&callback=initAutocomplete"
async defer></script>