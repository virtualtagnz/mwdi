<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <?php echo (isset($ba_uid) ? '<a href="' . base_url('baform/pdf/' . $this->encrypt->encode($ba_uid)) . '" class="btn btn-primary pull-right" target="_blank"><i class="fa fa-print"></i> &nbsp; Print Form</a>' : false) ?>
                Forms  <i class="lnr lnr-chevron-right"></i> Business Application <i class="lnr lnr-chevron-right"></i> Education Details
            </h3>
            <div class="panel">
                <div class="panel-heading">
                    <small class="pull-right timer-counter-started"></small>
                    <h3 class="panel-title">YOUR EDUCATIONAL DETAILS</h3>
                </div>
                <div class="panel-body">
                    <?php //echo response_messages(); ?>
                    <?php //var_dump($education_details); ?>
                    <form id="baform" method="post">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Educated At</label>
                                    <input type="text" class="form-control" id="educate_at" placeholder="Educated At" maxlength="150" value="" data-validation="required" data-validation-error-msg="The field educated at is mandatory, please check again!" name="educate_at">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Qualifications</label>
                                    <input type="text" class="form-control" id="qualifications" placeholder="Qualifications" maxlength="150" value="" data-validation="required" data-validation-error-msg="The field qualifications is mandatory, please check again!" name="qualifications">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Year Obtained</label>
                                    <select class="form-control" data-validation="required" data-validation-error-msg="The field year obtained is mandatory, please check again!" name="year_obtained" id="year_obtained">
                                        <option value="" selected="selected">Year Obtained</option>
                                        <?php
                                        $yearToday = date("Y");
                                        for ($i = 0; $i <= 100; $i++) {
                                            echo '<option value="' . ($yearToday - $i) . '">' . ($yearToday - $i) . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <input type="hidden" name="timer_started" id="timer_started" value="">
                                <input type="hidden" name="ba_uid" id="ba_uid" value="<?php echo $ba_uid; ?>">
                                <button type="submit" class="btn btn-primary">ADD QUALIFICATIONS</button>
                                <hr style="margin-top: 35px;">

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Educated At</th>
                                                <th>Qualifications</th>
                                                <th>Year Obtained</th>
                                                <th style="width: 90px;"></th>
                                            </tr>
                                        </thead>
                                        <tbody class="body_table_education_details">
                                            <?php
                                            if ($education_details) {
                                                foreach ($education_details as $string) {
                                                    echo '
                                                            <tr class="education_detail_' . $string->edu_id . '">
                                                                <td>' . $string->edu_place . '</td>
                                                                <td>' . $string->edu_qua . '</td>
                                                                <td>' . $string->edu_yr . '</td>
                                                                <td><a href="" class="btn btn-fefault delete-education-detail" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" data-detail_uid="' . $string->edu_id . '"><i class="fa fa-trash"></i></a></td>
                                                            </tr>
                                                        ';
                                                }
                                            } else {
                                                echo '<tr class="no_register_found"><td colspan="4" class="text-center">No registers found</td></tr>';
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                    </form>
                    <form id="baform" action="<?php echo base_url('baform/save/form/step2') ?>" method="post">
                        <input type="hidden" name="ba_uid" value="<?php echo $ba_uid; ?>">
                        <div class="col-md-12" style="padding-top: 20px; padding-bottom: 20px;">
                            <a href="<?php echo base_url('baform/step1/' . $ba_uid_encrypt) ?>" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; BACK</a> &nbsp;
                            <button type="submit" class="btn btn-default" name="save_form" value="1">SAVE FORM &nbsp; <i class="fa fa-save"></i></button> &nbsp;
                            <button type="submit" class="btn btn-primary" name="next_form" value="1">NEXT &nbsp; <i class="fa fa-arrow-right"></i></button>
                            &nbsp; [2/6]
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>      
</div>
<?php if ($this->session->flashdata('success')) { ?>
    <script>swal({type: 'success', title: 'Success', text: 'The form has been successfully saved!', timer: 3000});</script>
<?php } else if ($this->session->flashdata('warning')) { ?>
    <script>swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000});</script>
<?php } ?>
<script type="text/javascript">
    $(function () {
        $.validate({
            form: '#baform',
            modules: 'security',
            onSuccess: function () {
                var educate_at = $("#educate_at").val();
                var qualifications = $("#qualifications").val();
                var year_obtained = $("#year_obtained").val();
                var ba_uid = $("#ba_uid").val();
                var timer_started = $("#timer_started").val();

                $.ajax({
                    url: config.base_url + 'baform/ajax/education_details',
                    data: {educate_at: educate_at, qualifications: qualifications, year_obtained: year_obtained, ba_uid: ba_uid, timer_started: timer_started, action: 'add'},
                    method: 'POST',
                    dataType: 'json',
                    beforeSend: function () {
                        swal({
                            type: 'info',
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            title: 'Adding, please wait....'
                        });
                    }
                }).done(function (data) {
                    if (data.status == true) {
                        $(".no_register_found").remove();
                        $(".body_table_education_details").append('<tr class="education_detail_' + data.uid + '"><td>' + educate_at + '</td><td>' + qualifications + '</td><td>' + year_obtained + '</td><td><a href="" class="btn btn-fefault delete-education-detail" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" data-detail_uid="' + data.uid + '"><i class="fa fa-trash"></i></a></td></tr>');
                    }

                    $("#baform").trigger("reset");
                    swal.close();
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    swal.close();
                    console.log(errorThrown);
                });

                return false;
            }
        });

        $(document).on('click', '.delete-education-detail', function (e) {
            e.preventDefault();
            var uid = $(this).attr("data-detail_uid");
            if (uid) {
                if (confirm('You really want delete these details?')) {
                    $.ajax({
                        url: config.base_url + 'baform/ajax/education_details',
                        data: {ba_uid: uid, action: 'delete'},
                        method: 'POST',
                        dataType: 'json',
                        beforeSend: function () {
                            swal({
                                type: 'info',
                                allowEscapeKey: false,
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                title: 'Deleting, please wait....'
                            });
                        }
                    }).done(function (data) {
                        if (data.status == true) {
                            $(".education_detail_" + uid).remove();
                        }
                        swal.close();
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        swal.close();
                        console.log(errorThrown);
                    });
                }
            }
        });

        $("#baform :input").change(function () {
            if (!$("#timer_started").val()) {
//                var datetime = new Date().toLocaleString();
//                $(".timer-counter-started").html('Form started at ' + datetime);
//                $("#timer_started").val(datetime);
                  $('#timer_started').val(timer());
            }
        });
    });
</script>