<?php
$ba_form5_details = (json_decode($details));
$list_trading = array('1' => 'Limited', '2' => 'Sole Trader', '3' => 'Company', '4' => 'Trust', '5' => 'Other');
?>
<style>
    td{
        border: 1px solid #9D8526;
        padding: 5px;
    }
    tr.bg{
        background-color: #252c35;
        color: #ffffff;
    }
    .title{
        text-align: center;
    }
</style>

<div class="title">
    <h2>Business Details</h2>
</div>
<?php if ($ba_form5_details) { ?>
    <table cellspacing="0" cellpadding="5" border="1">
        <tr class="bg">
            <td colspan="2">
                <h4 class="">Tell Us About Your Business</h4>
            </td>
        </tr>
        <tr>
            <td>Is this new or existing business?</td><td><?php echo $ba_form5_details->biz=='1'?'New':'Existing';?></td>
        </tr>
        <tr>
            <td>Period operating</td><td><?php echo $ba_form5_details->biz_period;?></td>
        </tr>
        <tr>
            <td>Have you been in business before?</td><td><?php echo $ba_form5_details->biz_trading=='1'?'Yes':'No';?></td>
        </tr>
        <tr>
            <td>Trading As</td><td><?php echo $list_trading[$ba_form5_details->biz_bef];?></td>
        </tr>
        <tr>
            <td>What is your product/service?</td><td><?php echo $ba_form5_details->biz_link?'<a href="'.  base_url('uploads/business_application/'.$ba_form5_details->biz_link) .'">File Link</a>':'-';?></td>
        </tr>
        <tr>
            <td>No of Employees</td><td><?php echo $ba_form5_details->no_positions;?></td>
        </tr>
        <tr>
            <td>Employees Full Time</td><td><?php echo $ba_form5_details->full_time;?></td>
        </tr>
        <tr>
            <td>Employees Part Time</td><td><?php echo $ba_form5_details->part_time;?></td>
        </tr>
        <tr>
            <td>How did you hear about MWDI?</td><td><?php echo $ba_form5_details->abt_mdwi;?></td>
        </tr>
        <tr>
            <td>Have you previously applied for a loan with MWDI?</td><td><?php echo $ba_form5_details->pre_apply=='1'?'Yes':'No';?></td>
        </tr>
        <tr>
            <td>If Yes, what was the outcome?</td><td><?php echo $ba_form5_details->pre_outcome;?></td>
        </tr>
        <tr class="bg">
            <td colspan="2">
                <h4 class="">Security</h4>
            </td>
        </tr>
        <tr>
            <td>List security being provided for the loan</td><td><?php echo $ba_form5_details->sec_list;?></td>
        </tr>
    </table>
<!--    <br pagebreak="true" />-->
<?php } ?>


