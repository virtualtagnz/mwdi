<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                Forms <i class="lnr lnr-chevron-right"></i> Business Application
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable js-pointstable display">
                            <thead>
                                <tr>
                                    <th>Client Id</th>
                                    <th>Client Name</th>
                                    <th>Status</th>
                                    <th>Edit/ View</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($ba_submitted) { ?>
                                <?php $submitted_ba_forms = json_decode($ba_submitted) ?>
                                <?php foreach ($submitted_ba_forms as $ba_form) { ?>
                                <?php //var_dump($ba_form)?>
                                <tr>
                                    <td><?php echo $ba_form->ba_client_ref; ?></td>
                                    <td><?php echo $ba_form->first_name . ' ' . $ba_form->last_name; ?></td>
                                    <td><?php echo $ba_form->sub_status == 'Y' ? 'Submitted' : 'Draft'; ?></td>
                                    <td>
                                                <?php if ($ba_form->sub_status == 'Y') { ?>
                                                    <?php if($this->session->userdata('role') == '1'||$this->session->userdata('role') == '2'){?>
                                                    <a href="<?php echo base_url('baform/check/edit/'.$ba_form->ba_client_ref);?>" class="btn btn-fefault" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>
                                                    <?php } ?>
                                                    <a href="<?php echo base_url('baform/pdf/'.$this->encrypt->encode($ba_form->ba_id))?>" target="blank" class="btn btn-fefault" data-toggle="tooltip" data-placement="top" title="Print"><i class="fa fa-print"></i></a>
                                                <?php } else { ?>
                                                    <?php if($this->session->userdata('role') == '1'||$this->session->userdata('role') == '2'){?>
                                                    <a href="<?php echo base_url('baform/check/edit/'.$ba_form->ba_client_ref);?>" class="btn btn-fefault" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>   
                                                    <?php } ?>
                                                <?php } ?>
                                            </td>
                                </tr>
                                <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<script type="text/javascript">
    $(function () {
        $('.js-pointstable').DataTable({
            "pageLength": 10,
            "searching": true,
            "info": true,
            "order": [[ 1, "asc" ]],
            "responsive": true
        });
    });
</script>