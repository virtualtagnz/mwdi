<?php
$ba_form2_details = (json_decode($details));
?>
<style>
    td{
        border: 1px solid #9D8526;
        padding: 5px;
    }
    tr.bg{
        background-color: #252c35;
        color: #ffffff;
    }
    .title{
        text-align: center;
    }
</style>
<div class="title">
    <h2>Educational Details</h2>
</div>
<?php if ($ba_form2_details) { ?>
    <table cellspacing="0" cellpadding="5" border="1">
        <tr class="bg">
            <td colspan="2">
                <h4 class="">Educational Details</h4>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="5" border="1">
        <thead>
            <tr>
                <td>Educated At</td>
                <td>Qualifications</td>
                <td>Year Obtained</td>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($ba_form2_details as $edu) { ?>
            <tr>
                <td><?php echo $edu->edu_place?></td>
                <td><?php echo $edu->edu_qua?></td>
                <td><?php echo $edu->edu_yr?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<!--<br pagebreak="true" />-->
<?php } ?>

