<?php
$ba_form6_details = (json_decode($details));
$list_ops = array('1' => 'Security', '2' => 'Cashflow', '3' => 'Credit rating', '4' => 'Other');
$try_ops = array();
$tot_lbt = 0;
$tot_ast = 0;
$net_wrt = 0;
?>
<style>
    td{
        border: 1px solid #9D8526;
        padding: 5px;
    }
    tr.bg{
        background-color: #252c35;
        color: #ffffff;
    }
    .title{
        text-align: center;
    }
</style>

<div class="title">
    <h2>Business Details</h2>
</div>
<?php if ($ba_form6_details) { ?>
    <table cellspacing="0" cellpadding="5" border="1">
        <tr class="bg">
            <td colspan="2">
                <h4 class="">Bank Information</h4>
            </td>
        </tr>
        <tr>
            <td>Do you have a bank account open/operating?</td><td><?php echo $ba_form6_details->hv_bk == '1' ? 'New' : 'Existing'; ?></td>
        </tr>
        <tr>
            <td>Account number</td><td><?php echo $ba_form6_details->bk_no; ?></td>
        </tr>
        <tr>
            <td>Name Of Bank</td><td><?php echo $ba_form6_details->bk_name; ?></td>
        </tr>
        <tr>
            <td>Branch</td><td><?php echo $ba_form6_details->bk_branch; ?></td>
        </tr>
        <tr>
            <td>Telephone</td><td><?php echo $ba_form6_details->bk_tel; ?></td>
        </tr>
        <tr>
            <td>Name Of Accountant</td><td><?php echo $ba_form6_details->acc_name; ?></td>
        </tr>
        <tr>
            <td>Address</td><td><?php echo $ba_form6_details->acc_add; ?></td>
        </tr>
        <tr>
            <td>Telephone</td><td><?php echo $ba_form6_details->acc_tel; ?></td>
        </tr>
        <tr>
            <td>Name Of Lawyer</td><td><?php echo $ba_form6_details->lyr_name; ?></td>
        </tr>
        <tr>
            <td>Address</td><td><?php echo $ba_form6_details->lyr_add ?></td>
        </tr>
        <tr>
            <td>Telephone</td><td><?php echo $ba_form6_details->lyr_tel; ?></td>
        </tr>
        <tr>
            <td>Have you approached your bank for a loan?</td><td><?php echo $ba_form6_details->try_bk_loan == '1' ? 'Yes' : 'No'; ?></td>
        </tr>
        <tr>
            <td>Have you approached any other institution or organisation for a loan?</td><td><?php echo $ba_form6_details->try_other_ins == '1' ? 'Yes' : 'No'; ?></td>
        </tr>
        <tr>
            <td>Name of the institution(s)</td><td><?php echo $ba_form6_details->ins_names; ?></td>
        </tr>
        <tr>
            <td>Telephone of the institution(s)</td><td><?php echo $ba_form6_details->ins_tels; ?></td>
        </tr>
        <tr>
            <td>Reason for declined</td>
            <td>
                <?php
                if ($ba_form6_details->try_ops) {
                    $ops = '';
                    $try_ops = explode(',', $ba_form6_details->try_ops);
                    foreach ($try_ops as $string) {
                        $why_ops[] = $list_ops[$string];
                    }
                    $why_ops = implode(", ", $why_ops);
                    echo ltrim($why_ops);
                }
                ?>
            </td>
        </tr>
        <?php if (in_array(4, $try_ops)) { ?>
            <tr>
                <td>If other, please comment what the result was (if declined please state why)</td><td><?php echo $ba_form6_details->try_result; ?></td>
            </tr>
        <?php } ?>
        <tr class="bg">
            <td colspan="2">
                <h4 class="">Details Of Your Loan</h4>
            </td>
        </tr>
        <tr>
            <td>Purpose For Which The Loan</td><td><?php echo $ba_form6_details->loan_purps; ?></td>
        </tr>
        <tr>
            <td>Cost of purchase</td><td><?php echo number_format($ba_form6_details->cop,2); ?></td>
        </tr>
        <tr>
            <td>Less own cash contribtuion</td><td><?php echo number_format($ba_form6_details->own_cont,2); ?></td>
        </tr>
        <tr>
            <td>Loan</td><td><?php echo number_format($ba_form6_details->loan_amt_req,2); ?></td>
        </tr>
        
        <tr class="bg">
            <td colspan="2">
                <h4 class="">Business Mentoring</h4>
            </td>
        </tr>
        <tr>
            <td>Do you have a Business Mentor?</td><td><?php echo $ba_form6_details->hv_mentor=='1'?'Yes':'No'; ?></td>
        </tr>
        <tr>
            <td>Name</td><td><?php echo $ba_form6_details->ment_name; ?></td>
        </tr>
        <tr>
            <td>Telephone</td><td><?php echo $ba_form6_details->ment_tel; ?></td>
        </tr>
        <tr class="bg">
            <td colspan="2">
                <h4 class="">Your Financial Position</h4>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <h4 class="">What you owe (Total Amounts Outstanding)</h4>
            </td>
        </tr>
        <tr>
            <td>Amount</td><td><?php echo number_format($ba_form6_details->owe_amt,2); ?></td>
        </tr>
        <tr>
            <td>Term</td><td><?php echo $ba_form6_details->owe_terms; ?></td>
        </tr>
        <tr>
            <td>Years</td><td><?php echo $ba_form6_details->owe_yrs; ?></td>
        </tr>
        <tr>
            <td>Interest rate</td><td><?php echo number_format($ba_form6_details->owe_int_rate,2).'%'; ?></td>
        </tr>
        <tr>
            <td>Lender</td><td><?php echo $ba_form6_details->lender_name; ?></td>
        </tr>
        <tr>
            <td>Amount owing(total)</td><td><?php echo number_format($ba_form6_details->other_owe,2); ?></td>
        </tr>
        <tr>
            <?php $tot_lbt = intval($ba_form6_details->owe_amt)+intval($ba_form6_details->other_owe)?>
            <td><strong><i>TOTAL LIABILITIES</i></strong></td><td><?php echo number_format(strval($tot_lbt),2); ?></td>
        </tr>
        <tr>
            <td colspan="2">
                <h4 class="">Assets (what do you own)</h4>
            </td>
        </tr>
        <tr>
            <td>Cheque account</td><td><?php echo number_format($ba_form6_details->ast_chq_amt,2); ?></td>
        </tr>
        <tr>
            <td>Other bank accounts</td><td><?php echo number_format($ba_form6_details->other_bk_acnt,2); ?></td>
        </tr>
        <tr>
            <td>Life insurance cover</td><td><?php echo number_format($ba_form6_details->life_ins,2); ?></td>
        </tr>
        <tr>
            <td>Premiums paid to date (approx)</td><td><?php echo number_format($ba_form6_details->prem_paid,2); ?></td>
        </tr>
        <tr>
            <td>Government stock</td><td><?php echo number_format($ba_form6_details->go_stck,2); ?></td>
        </tr>
        <tr>
            <td>House and land (owners value)</td><td><?php echo number_format($ba_form6_details->house_land,2); ?></td>
        </tr>
        <tr>
            <td>Other property</td><td><?php echo number_format($ba_form6_details->othr_propty,2); ?></td>
        </tr>
        <tr>
            <td>Motor vehicle (value)</td><td><?php echo number_format($ba_form6_details->mot_val,2); ?></td>
        </tr>
        <tr>
            <td>Furniture and personal effects)</td><td><?php echo number_format($ba_form6_details->furniture_val,2); ?></td>
        </tr>
        <tr>
            <td>Other assets over $1,000 (total)</td><td><?php echo number_format($ba_form6_details->othr_asts,2); ?></td>
        </tr>
        <tr>
            <?php $tot_ast = intval($ba_form6_details->ast_chq_amt)+intval($ba_form6_details->other_bk_acnt)+intval($ba_form6_details->life_ins)+intval($ba_form6_details->prem_paid)+intval($ba_form6_details->go_stck)+intval($ba_form6_details->house_land)+intval($ba_form6_details->othr_propty)+intval($ba_form6_details->mot_val)+intval($ba_form6_details->furniture_val)+intval($ba_form6_details->othr_asts)?>
            <td><strong><i>TOTAL ASSETS</i></strong></td><td><?php echo number_format(strval($tot_ast),2); ?></td>
        </tr>
        <tr>
            <?php $net_wrt = $tot_ast-$tot_lbt?>
            <td><strong><i>NET WORTH</i></strong></td><td><?php echo number_format(strval($net_wrt),2); ?></td>
        </tr>
    </table>
    <!--    <br pagebreak="true" />-->
<?php } ?>


