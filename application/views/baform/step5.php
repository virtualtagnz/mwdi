<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <?php echo (isset($ba_uid) ? '<a href="' . base_url('baform/pdf/' . $this->encrypt->encode($ba_uid)) . '" class="btn btn-primary pull-right" target="_blank"><i class="fa fa-print"></i> &nbsp; Print Form</a>' : false) ?>
                Forms  <i class="lnr lnr-chevron-right"></i> Business Application <i class="lnr lnr-chevron-right"></i> Business Details
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <?php //echo response_messages(); ?>
                    <form id="baform" action="<?php echo base_url('baform/save/form/step5') ?>" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <small class="pull-right timer-counter-started"></small>
                                <h4 style="margin-top: 30px;">TELL US ABOUT YOUR BUSINESS</h4>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Is this business</label>
                                    <p style="padding: 6px 0;">
                                        <label class="radio radio-inline" style="margin: 0">
                                            <input name="biz" value="1" type="radio" <?php echo (isset($business_details) && $business_details && $business_details->biz == '1' ? 'checked="checked"' : false) ?>>
                                            <span>New</span>
                                        </label>
                                        <label class="radio radio-inline">
                                            <input name="biz" value="2" type="radio" <?php echo (isset($business_details) && $business_details && $business_details->biz == '2' ? 'checked="checked"' : false) ?>>
                                            <span>Existing</span>
                                        </label>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>If existing, how long has it be operating?</label>
                                    <input type="text" class="form-control" id="biz_period" name="biz_period" placeholder="If existing, how long has it be operating?" maxlength="100" value="<?php echo (isset($business_details) && $business_details ? $business_details->biz_period : false) ?>" data-validation="yes_pre_loan" data-validation="required" data-validation-error-msg="The field what was the outcome is mandatory, please check again!" data-validation-depends-on="biz" data-validation-depends-on-value="2">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Have you been in business before?</label>
                                    <p style="padding: 6px 0;">
                                        <label class="radio radio-inline" style="margin: 0">
                                            <input name="biz_bef" value="1" type="radio" <?php echo (isset($business_details) && $business_details && $business_details->biz_bef == '1' ? 'checked="checked"' : false) ?>>
                                            <span>Yes</span>
                                        </label>
                                        <label class="radio radio-inline">
                                            <input name="biz_bef" value="2" type="radio" <?php echo (isset($business_details) && $business_details && $business_details->biz_bef == '2' ? 'checked="checked"' : false) ?>>
                                            <span>No</span>
                                        </label>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Trading As</label>
                                    <select class="form-control" name="biz_trading" data-validation="required" data-validation-error-msg="The field trading as is mandatory, please check again!">
                                        <option value="">Trading As</option>
                                        <option value="1" <?php echo (isset($business_details) && $business_details && $business_details->biz_trading == '1' ? 'selected="selected"' : false) ?>>Limited</option>
                                        <option value="2" <?php echo (isset($business_details) && $business_details && $business_details->biz_trading == '2' ? 'selected="selected"' : false) ?>>Sole Trader</option>
                                        <option value="3" <?php echo (isset($business_details) && $business_details && $business_details->biz_trading == '3' ? 'selected="selected"' : false) ?>>Company</option>
                                        <option value="4" <?php echo (isset($business_details) && $business_details && $business_details->biz_trading == '4' ? 'selected="selected"' : false) ?>>Trust</option>
                                        <option value="5" <?php echo (isset($business_details) && $business_details && $business_details->biz_trading == '5' ? 'selected="selected"' : false) ?>>other</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>What is your product/service?</label>
                                    <i class="fa fa-info-circle pull-right" style="margin-top: 35px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Accept Formats: PDF, DOC, DOCX"></i>
                                    <label for="custom-file-upload" class="filupp form-control" style="width: 96%;">
                                        <span class="filupp-file-name biz_link">Upload</span>
                                        <input type="file" name="biz_doc" value="biz_doc" id="custom-file-upload" accept=".pdf, .doc, .docx" data-validation="size mime" data-validation-max-size="10M" data-validation-error-msg-size="Maximum upload size (10MB) exceeded, please try again!" data-validation-allowing="pdf, doc, docx" data-validation-error-msg-mime="Allowed File Types: PDF, DOC, DOCX">
                                    </label>
                                    <?php
                                    if (isset($business_details) && $business_details) {
                                        if ($business_details->biz_link) {
                                            if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/uploads/business_application/' . $business_details->biz_link)) {
                                                echo '<a href="' . base_url('uploads/business_application/' . $business_details->biz_link) . '" target="_blank" class="btn btn-fefault"><i class="fa fa-eye" style="margin-top: 10px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="View File"></i></a>';
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                                <p style="margin: -10px 0 20px 0;"><i class="help-block" style="color: #999; font-weight: normal;">Attach any relevant information with your business plan.</i></p>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>No of Employees</label>
                                    <input type="number" class="form-control" id="no_epm" name="no_epm" placeholder="No of Employees" maxlength="4" value="<?php echo (isset($business_details) && $business_details ? $business_details->no_positions : false) ?>">
                                    <i class="help-block" style="color: #999; font-weight: normal;">How many new positions have been or will be created.</i>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Employees Full Time</label>
                                    <input type="number" class="form-control" id="ft" name="ft" placeholder="Employees Full Time" maxlength="4" value="<?php echo (isset($business_details) && $business_details ? $business_details->full_time : false) ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Employees Part Time</label>
                                    <input type="number" class="form-control" id="pt" name="pt" placeholder="Employees Part Time" maxlength="4" value="<?php echo (isset($business_details) && $business_details ? $business_details->part_time : false) ?>">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>How did you hear about MWDI?</label>
                                    <textarea class="form-control" name="find" id="find" placeholder="How did you hear about MWDI?" rows="4" data-validation="required" data-validation-error-msg="The field how did you hear is mandatory, please check again!"><?php echo (isset($business_details) && $business_details ? $business_details->abt_mdwi : false) ?></textarea>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Have you previously applied for a loan with MWDI?</label>
                                    <p style="padding: 6px 0;">
                                        <label class="radio radio-inline" style="margin: 0">
                                            <input name="pre" value="1" type="radio" <?php echo (isset($business_details) && $business_details && $business_details->pre_apply == '1' ? 'checked="checked"' : false) ?>>
                                            <span>Yes</span>
                                        </label>
                                        <label class="radio radio-inline">
                                            <input name="pre" value="0" type="radio" <?php echo (isset($business_details) && $business_details && $business_details->pre_apply == '0' ? 'checked="checked"' : false) ?>>
                                            <span>No</span>
                                        </label>
                                    </p>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>If Yes, what was the outcome?</label>
                                    <textarea class="form-control" name="preyes" id="preyes" placeholder="If Yes, what was the outcome?" rows="4" data-validation="yes_pre_loan" data-validation="required" data-validation-error-msg="The field what was the outcome is mandatory, please check again!" data-validation-depends-on="pre" data-validation-depends-on-value="1"><?php echo (isset($business_details) && $business_details ? $business_details->pre_outcome : false) ?></textarea>
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <hr>
                                <h4 style="margin-top: 30px;">SECURITY</h4>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>List security being provided for the loan</label>
                                    <textarea class="form-control" name="seclist" id="seclist" placeholder="List security being provided for the loan" rows="4" data-validation="required" data-validation-error-msg="The field List security being provided for the loan is mandatory, please check again!"><?php echo (isset($business_details) && $business_details ? $business_details->sec_list : false) ?></textarea>
                                    <i class="help-block" style="color: #999; font-weight: normal;">Must be up to the value of the loan requested.</i>
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <h4 style="margin-top: 30px;" class="text-danger">Please complete Security schedule, identifying insurance policy and cover on all chattels.</h4>
                            </div>

                            <div class="col-md-12" style="padding-top: 20px; padding-bottom: 20px;">
                                <input type="hidden" name="timer_started" id="timer_started" value="<?php echo (isset($business_details) && $business_details && $business_details->start_date ? $business_details->start_date : false) ?>">
                                <input type="hidden" name="timer_end" id="timer_end" value="<?php echo (isset($business_details) && $business_details && $business_details->end_date ? $business_details->end_date : false) ?>">
                                <input type="hidden" name="ba_uid" id="ba_uid" name="ba_uid" value="<?php echo $ba_uid; ?>">
                                <a href="<?php echo base_url('baform/step4/' . $ba_uid_encrypt) ?>" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; BACK</a> &nbsp;
                                <button type="submit" class="btn btn-default" name="save_form" value="1">SAVE FORM &nbsp; <i class="fa fa-save"></i></button> &nbsp;
                                <button type="submit" class="btn btn-primary" name="next_form" value="1">NEXT &nbsp; <i class="fa fa-arrow-right"></i></button>
                                &nbsp; [5/6]
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>  
</div>
<?php if ($this->session->flashdata('success')) { ?>
    <script>swal({type: 'success', title: 'Success', text: 'The form has been successfully saved!', timer: 3000});</script>
<?php } else if ($this->session->flashdata('warning')) { ?>
    <script>swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000});</script>
<?php } ?>
<script type="text/javascript">
    $(function () {

        $.formUtils.addValidator({
            name: 'yes_pre_loan',
            validatorFunction: function (value, $el, config, language, $form) {
                var preLoan = $("input[name=pre]:checked").val();
                if (preLoan && preLoan == 1) {
                    if (!value) {
                        return false;
                    }
                }
            },
            errorMessage: 'This is a required field',
            errorMessageKey: 'badEvenNumber'
        });

        $.validate({
            form: '#baform',
            modules: 'security,file'
        });

        $("#baform :input").change(function () {
            if (!$("#timer_started").val()) {
//                var datetime = new Date().toLocaleString();
//                $(".timer-counter-started").html('Form started at ' + datetime);
//                $("#timer_started").val(datetime);
                  $('#timer_started').val(timer());
            }
        });

        $("input[name='biz']").click(function () {
            var value = $(this).val();
            if (value == '2')
                $('#biz_period').attr('data-validation', 'required');
            else
                $('#biz_period').removeAttr('data-validation');
        });
        
        $('input[type="file"]').change(function () {
            $('.biz_link').text(this.files[0].name);
        });
    })
</script>