<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <?php echo (isset($ba_uid) ? '<a href="' . base_url('baform/pdf/' . $this->encrypt->encode($ba_uid)) . '" class="btn btn-primary pull-right" target="_blank"><i class="fa fa-print"></i> &nbsp; Print Form</a>' : false) ?>
                Forms  <i class="lnr lnr-chevron-right"></i> Business Application <i class="lnr lnr-chevron-right"></i> Bank Information
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <?php //var_dump($bank_information); ?>
                    <form id="baform" action="<?php echo base_url('baform/save/form/step6') ?>" method="post">
                        <div class="row">
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <small class="pull-right timer-counter-started"></small>
                                <h4 style="margin-top: 30px;">WHERE DO YOU CURRENTLY BANK?</h4>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Do you have a bank account open/operating?</label>
                                    <p style="padding: 5px 0;">
                                        <label class="radio radio-inline" style="margin: 0">
                                            <input name="hv_bk" value="1" type="radio" <?php echo (isset($bank_information) && $bank_information && $bank_information->hv_bk == '1' ? 'checked="checked"' : false) ?>>
                                            <span>Yes</span>
                                        </label>
                                        <label class="radio radio-inline">
                                            <input name="hv_bk" value="0" type="radio" <?php echo (isset($bank_information) && $bank_information && $bank_information->hv_bk == '0' ? 'checked="checked"' : false) ?>>
                                            <span>No</span>
                                        </label>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>If Yes, the account number is</label>
                                    <input type="text" class="form-control" id="bkno" name="bkno" placeholder="If Yes, the account number is" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->bk_no : '') ?>" data-validation="required" data-validation-error-msg-required="The field account number is mandatory, please check again!" data-validation-depends-on="hv_bk" data-validation-depends-on-value="1">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name Of Bank</label>
                                    <input type="text" class="form-control" id="bk_name" name="bk_name"  placeholder="Name Of Bank" maxlength="50" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->bk_name : false) ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Branch</label>
                                    <input type="text" class="form-control" id="branch" name="branch"  placeholder="Branch" maxlength="50" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->bk_branch : false) ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Telephone</label>
                                    <input type="text" class="form-control phone" id="tel" name="tel" placeholder="Telephone" maxlength="12" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->bk_tel : false) ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name Of Accountant</label>
                                    <input type="text" class="form-control" id="acc_name" name="acc_name"  placeholder="Name Of Accountant" maxlength="50" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->acc_name : false) ?>" data-validation="required" data-validation-error-msg="The field name of accountant is mandatory, please check again!">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Address</label>
                                    <input type="text" class="form-control autocomplete" name="acc_add" id="acc_add" placeholder="Address" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->acc_add : false) ?>" autocomplete="off" data-validation="required" data-validation-error-msg="The field accountant address is mandatory, please check again!">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Telephone</label>
                                    <input type="text" class="form-control phone" id="acc_tel" name="acc_tel" placeholder="Telephone" maxlength="12" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->acc_tel : false) ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name Of Lawyer</label>
                                    <input type="text" class="form-control" id="low_name" name="low_name"  placeholder="Name Of Lawyer" maxlength="50" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->lyr_name : false) ?>" data-validation="required" data-validation-error-msg="The field name of lawyer is mandatory, please check again!">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Address</label>
                                    <input type="text" class="form-control autocomplete" name="low_add" id="low_add" placeholder="Address" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->lyr_add : false) ?>" autocomplete="off" data-validation="required" data-validation-error-msg="The field address lawyer is mandatory, please check again!">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Telephone</label>
                                    <input type="text" class="form-control phone" id="low_tel" name="low_tel" placeholder="Telephone" maxlength="12" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->lyr_tel : false) ?>">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Have you approached your bank for a loan?</label>
                                    <p style="padding: 5px 0;">
                                        <label class="radio radio-inline" style="margin: 0">
                                            <input name="hv_loan" value="1" type="radio" <?php echo (isset($bank_information) && $bank_information && $bank_information->try_bk_loan == '1' ? 'checked="checked"' : false) ?>>
                                            <span>Yes</span>
                                        </label>
                                        <label class="radio radio-inline">
                                            <input name="hv_loan" value="0" type="radio" <?php echo (isset($bank_information) && $bank_information && $bank_information->try_bk_loan == '0' ? 'checked="checked"' : false) ?>>
                                            <span>No</span>
                                        </label>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Have you approached any other institution or organisation for a loan?</label>
                                    <p style="padding: 5px 0;">
                                        <label class="radio radio-inline" style="margin: 0">
                                            <input name="ot_loan" value="1" type="radio" <?php echo (isset($bank_information) && $bank_information && $bank_information->try_other_ins == '1' ? 'checked="checked"' : false) ?>>
                                            <span>Yes</span>
                                        </label>
                                        <label class="radio radio-inline">
                                            <input name="ot_loan" value="0" type="radio" <?php echo (isset($bank_information) && $bank_information && $bank_information->try_other_ins == '0' ? 'checked="checked"' : false) ?>>
                                            <span>No</span>
                                        </label>
                                    </p>
                                </div>
                            </div>

                            <?php
                            $try_ops = array();
                            if (isset($bank_information) && $bank_information && $bank_information->try_ops) {
                                $try_ops = explode(',', $bank_information->try_ops);
                            }
                            ?>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tick what is the reason for declined:</label>
                                    <p style="padding: 5px 0;">
                                        <label class="checkbox checkbox-inline" style="margin: 0">
                                            <input value="1" type="checkbox" name="dec28[]" <?php echo (in_array("1", $try_ops) ? 'checked="checked"' : false) ?>>
                                            <span>Security</span>
                                        </label>
                                        <label class="checkbox checkbox-inline">
                                            <input value="2" type="checkbox" name="dec28[]" <?php echo (in_array("2", $try_ops) ? 'checked="checked"' : false) ?>>
                                            <span>Cashflow</span>
                                        </label>
                                        <label class="checkbox checkbox-inline">
                                            <input value="3" type="checkbox" name="dec28[]" <?php echo (in_array("3", $try_ops) ? 'checked="checked"' : false) ?>>
                                            <span>Credit rating</span>
                                        </label>
                                        <label class="checkbox checkbox-inline">
                                            <input value="4" type="checkbox" id="ot_loan_4" name="dec28[]" <?php echo (in_array("4", $try_ops) ? 'checked="checked"' : false) ?>>
                                            <span>Other</span>
                                        </label>
                                    </p>
                                </div>
                            </div>

                            <div class="col-md-12 <?php echo (!in_array("4", $try_ops) ? 'hidden' : false) ?>" id="collapse01">
                                <div class="form-group">
                                    <label>If other, please comment what the result was (if declined please state why)</label>
                                    <textarea class="form-control" name="otyes" id="otyes" placeholder="If other, please comment what the result was (if declined please state why)" rows="4" data-validation="required" data-validation-error-msg="The field comment is mandatory, please check again!"><?php echo (isset($bank_information) && $bank_information ? $bank_information->try_result : false) ?></textarea>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>If Yes, Name of the institution(s)</label>
                                    <textarea class="form-control" name="ot_name" id="ot_name" placeholder="If Yes, Name of the institution(s)" rows="4" data-validation="required" data-validation-error-msg="The field name of the institution(s) is mandatory, please check again!" data-validation-depends-on="ot_loan" data-validation-depends-on-value="1"><?php echo (isset($bank_information) && $bank_information ? $bank_information->ins_names : false) ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>If Yes, Telephone of the institution(s)</label>
                                    <textarea class="form-control" name="ot_tel" id="ot_tel" placeholder="If Yes, Telephone of the institution(s)" rows="4" data-validation="required" data-validation-error-msg="The field telephone of the institution(s) is mandatory, please check again!" data-validation-depends-on="ot_loan" data-validation-depends-on-value="1"><?php echo (isset($bank_information) && $bank_information ? $bank_information->ins_tels : false) ?></textarea>
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <hr>
                                <h4 style="margin-top: 30px;">DETAILS OF YOUR LOAN</h4>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Purpose For Which The Loan</label>
                                    <textarea class="form-control" name="purpose" id="purpose" placeholder="Purpose For Which The Loan Is required" rows="8" data-validation="required" data-validation-error-msg="The field purpose is mandatory, please check again!"><?php echo (isset($bank_information) && $bank_information ? $bank_information->loan_purps : false) ?></textarea>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Cost of purchase</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <input type="text" class="form-control maskMoney" name="cop" id="cop" placeholder="Cost of purchase" min="0" step="0.01" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->cop : false) ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Less own cash contribtuion</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <input type="text" class="form-control maskMoney" name="loc" id="loc" placeholder="Less own cash contribtuion" min="0" step="0.01" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->own_cont : false) ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Loan</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <input type="text" class="form-control maskMoney" name="lr" id="lr" placeholder="Loan" min="0" step="0.01" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->loan_amt_req : false) ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <hr>
                                <h4 style="margin-top: 30px;">BUSINESS MENTORING</h4>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Do you have a Business Mentor?</label>
                                    <p style="padding: 5px 0;">
                                        <label class="radio radio-inline" style="margin: 0">
                                            <input name="hvment" value="1" type="radio" <?php echo (isset($bank_information) && $bank_information && $bank_information->hv_mentor == '1' ? 'checked="checked"' : false) ?>>
                                            <span>Yes</span>
                                        </label>
                                        <label class="radio radio-inline">
                                            <input name="hvment" value="0" type="radio" <?php echo (isset($bank_information) && $bank_information && $bank_information->hv_mentor == '0' ? 'checked="checked"' : false) ?>>
                                            <span>No</span>
                                        </label>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" class="form-control" id="ment_name" name="ment_name"  placeholder="Name" maxlength="50" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->ment_name : false) ?>" data-validation="required" data-validation-error-msg="The field mentor name is mandatory, please check again!" data-validation-depends-on="hvment" data-validation-depends-on-value="1">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Telephone</label>
                                    <input type="text" class="form-control phone" id="ment_tel" name="ment_tel" placeholder="Telephone" maxlength="12" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->ment_tel : false) ?>" data-validation="required" data-validation-error-msg="The field mentor telephone is mandatory, please check again!" data-validation-depends-on="hvment" data-validation-depends-on-value="1">
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <hr>
                                <h4 style="margin-top: 30px;">YOUR FINANCIAL POSITION</h4>
                            </div>

                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12" style="margin-bottom: 15px;">
                                        <h4 class="text-primary">What you owe (show total amounts outstanding)</h4><br>

                                        <div class="form-group">
                                            <label>Amount</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">$</div>
                                                <input type="text" class="form-control maskMoney" name="z1" id="z1" placeholder="Amount" min="0" step="0.01" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->owe_amt : false) ?>">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Term</label>
                                            <input type="text" class="form-control maskMoney" name="z2" id="z2" placeholder="Term" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->owe_terms : false) ?>">
                                        </div>

                                        <div class="form-group">
                                            <label>Years</label>
                                            <input type="text" class="form-control maskMoney" name="z3" id="z3" placeholder="Years" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->owe_yrs : false) ?>">
                                        </div>

                                        <div class="form-group">
                                            <label>Interest rate</label>
                                            <input type="text" class="form-control maskMoney" name="z4" id="z4" placeholder="Interest rate" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->owe_int_rate : false) ?>">
                                        </div>

                                        <div class="form-group">
                                            <label>Lender</label>
                                            <input type="text" class="form-control" id="z5" name="z5"  placeholder="Lender" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->lender_name : false) ?>">
                                        </div>

                                        <div class="form-group">
                                            <label>Amount owing(total)</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">$</div>
                                                <input type="text" class="form-control maskMoney" name="z6" id="z6" placeholder="Amount owing(total)" min="0" step="0.01" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->other_owe : false) ?>">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>TOTAL LIABILITIES</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">$</div>
                                                <input type="text" class="form-control maskMoney" name="z7" id="z7" placeholder="TOTAL LIABILITIES" min="0" step="0.01" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" id="assets">
                                <div class="row">
                                    <div class="col-md-12" style="margin-bottom: 15px;">
                                        <h4 class="text-primary">Assets (what do you own)</h4><br>

                                        <div class="form-group">
                                            <label>Cheque account</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">$</div>
                                                <input type="text" class="form-control maskMoney" name="x1" id="x1" placeholder="Cheque account" min="0" step="0.01" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->ast_chq_amt : false) ?>">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Other bank accounts</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">$</div>
                                                <input type="text" class="form-control maskMoney" name="x2" id="x2" placeholder="Other bank accounts" min="0" step="0.01" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->other_bk_acnt : false) ?>">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Life insurance cover</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">$</div>
                                                <input type="text" class="form-control maskMoney" name="x3" id="x3" placeholder="Life insurance cover" min="0" step="0.01" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->life_ins : false) ?>">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Premiums paid to date (approx)</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">$</div>
                                                <input type="text" class="form-control maskMoney" name="x4" id="x4" placeholder="Premiums paid to date (approx)" min="0" step="0.01" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->prem_paid : false) ?>">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Government stock</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">$</div>
                                                <input type="text" class="form-control maskMoney" name="x5" id="x5" placeholder="Government stock" min="0" step="0.01" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->go_stck : false) ?>">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>House and land (owners value)</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">$</div>
                                                <input type="text" class="form-control maskMoney" name="x6" id="x6" placeholder="House and land (owners value)" min="0" step="0.01" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->house_land : false) ?>">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Other property</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">$</div>
                                                <input type="text" class="form-control maskMoney" name="x7" id="x7" placeholder="Other property" min="0" step="0.01" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->othr_propty : false) ?>">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Motor vehicle (value)</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">$</div>
                                                <input type="text" class="form-control maskMoney" name="x8" id="x8" placeholder="Motor vehicle (value)" min="0" step="0.01" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->mot_val : false) ?>">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Furniture and personal effects</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">$</div>
                                                <input type="text" class="form-control maskMoney" name="x9" id="x9" placeholder="Furniture and personal effects" min="0" step="0.01" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->furniture_val : false) ?>">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Other assets over $1,000 (total)</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">$</div>
                                                <input type="text" class="form-control maskMoney" name="x10" id="x10" placeholder="Other assets over $1,000 (total)" min="0" step="0.01" value="<?php echo (isset($bank_information) && $bank_information ? $bank_information->othr_asts : false) ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <hr style="margin: 0 0 30px 0;">
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>TOTAL ASSETS</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">$</div>
                                                <input type="text" class="form-control maskMoney" name="x11" id="x11" placeholder="TOTAL ASSETS" min="0" step="0.01" value="">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>LESS TOTAL LIABILITIES</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">$</div>
                                                <input type="text" class="form-control maskMoney" name="x12" id="x12" placeholder="LESS TOTAL LIABILITIES" min="0" step="0.01" value="">
                                            </div>
                                        </div>

                                        <p><button type="button" class="btn btn-primary" id="btn-calc">Calculate Net Worth &nbsp; <i class="fa fa-calculator"></i></button></p>

                                        <div class="form-group">
                                            <label>NET WORTH</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">$</div>
                                                <input type="text" class="form-control maskMoney" name="x13" id="x13" placeholder="NET WORTH" min="0" step="0.01" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12" style="padding-top: 20px; padding-bottom: 20px;">
                                <input type="hidden" name="ba_uid" id="ba_uid" value="<?php echo $ba_uid; ?>">
                                <input type="hidden" name="submit_val" id="submit_val" value="">

                                <input type="hidden" name="timer_started" id="timer_started" value="<?php echo (isset($bank_information) && $bank_information && $bank_information->start_date ? $bank_information->start_date : false) ?>">
                                <input type="hidden" name="timer_end" id="timer_end" value="<?php echo (isset($bank_information) && $bank_information && $bank_information->end_date ? $bank_information->end_date : false) ?>">
                                <a href="<?php echo base_url('baform/step5/' . $ba_uid_encrypt) ?>" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; BACK</a> &nbsp;
                                <button type="submit" class="btn btn-default" name="save_form" value="1">SAVE FORM &nbsp; <i class="fa fa-save"></i></button> &nbsp;
                                <button type="button" class="btn btn-primary submit-form" name="submit_form" value="1">SUBMIT FORM &nbsp; <i class="fa fa-arrow-right"></i></button>
                                &nbsp; [6/6]
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($this->session->flashdata('create_success')) { ?>
    <script>swal({type: 'success', title: 'success', text: 'The form has been successfully saved', timer: 3000});</script>
<?php } else if ($this->session->flashdata('submit_success')) { ?>
    <script>swal({type: 'success', title: 'success', text: 'The form has been successfully submitted'}).then(function () {
        location.href = '<?php echo $this->session->userdata('role') == '5'?base_url('client_forms'):base_url('baform/list') ?>';
        });</script>
<?php } ?>    

<script src="<?php echo base_url('assets/vendor/mask/jquery.priceformat.min.js') ?>"></script>
<script type="text/javascript">
        $(function () {

            $('.maskMoney').priceFormat({
                prefix: '',
                thousandsSeparator: '',
                clearOnEmpty: true
            });

            readyCalculate();
            function readyCalculate() {
                var val1 = $('#z1').val();
                var val2 = $('#z6').val();
                var tot = Number(val1) + Number(val2);
                var fix = tot.toFixed(2);
                $('#z7,#x12').val(fix);

                var val = 0;
                for (var i = 1; i < 11; i++) {
                    val = Number(val) + Number($('#x' + i).val());
                }
                var fix = val.toFixed(2);
                $('#x11').val(fix);

                var tot_ast = $('#x11').val();
                var tot_lib = $('#x12').val();
                var net = Number(tot_ast) - Number(tot_lib);
                var net_fix = net.toFixed(2);
                $('#x13').val(net_fix);
            }

            $('#assets :input').on('change', function () {
                var val = 0;
                for (var i = 1; i < 11; i++) {
                    val = Number(val) + Number($('#x' + i).val());
                }
                var fix = val.toFixed(2);
                $('#x11').val(fix);
            });

            $('#z1,#z6').on('change', function () {
                var val = ($(this).val());
                var tot = $('#z7').val();
                var out = Number(tot) + Number(val);
                var fix = out.toFixed(2);
                $('#z7,#x12').val(fix);
            });
            $('#btn-calc').on('click', function () {
                var dif = Number($('#x11').val()) - Number($('#x12').val());
                var fix = dif.toFixed(2);
                $('#x13').val(fix);
            });

            $.formUtils.addValidator({
                name: 'yes_pre_loan',
                validatorFunction: function (value, $el, config, language, $form) {
                    var preLoan = $("input[name=pre]:checked").val();
                    if (preLoan && preLoan == 1) {
                        if (!value) {
                            return false;
                        }
                    }
                },
                errorMessage: 'This is a required field',
                errorMessageKey: 'badEvenNumber'
            });

            $.validate({
                form: '#baform',
                modules: 'security'
            });

            $("#baform :input").change(function () {
                if (!$("#timer_started").val()) {
//                    var datetime = new Date().toLocaleString();
//                    $(".timer-counter-started").html('Form started at ' + datetime);
//                    $("#timer_started").val(datetime);
                      $('#timer_started').val(timer());
                }
            });

            $("#ot_loan_4").click(function () {
                if ($(this).is(':checked')) {
                    console.log('clicked checked');
                    $("#collapse01").removeClass('hidden');
                } else {
                    $("#collapse01").addClass('hidden');
                }
            })

            $(".submit-form").click(function () {
//            if (confirm('Do you want to submit this form?')) {
//                $("#submit_val").val('1');
//                $("#baform").submit();
//            }
                swal({
                    title: 'Do you want submit this form?',
                    confirmButtonText: 'Submit',
                    html: '<span style="color:red;">You cannot edit this form after submit</span>',
                    showLoaderOnConfirm: true,
                    showCancelButton: true
                }).then((result)=>{
                    console.log(result);
                    $("#submit_val").val('1');
                    $("#baform").submit();
                }).catch(swal.noop);
            });

            $('#tel,#acc_tel,#low_tel,#ment_tel').mask('000000000000', {placeholder: "Telephone"});
            $('#bkno').mask('00-0000-0000000-00');

            $.validate({
                form: '#baform',
                modules: 'security,date,logic'
            });


        })
</script>
<script>
    function initAutocomplete() {
        var acInputs = document.getElementsByClassName("autocomplete");
        for (var i = 0; i < acInputs.length; i++) {
            var autocomplete = new google.maps.places.Autocomplete(acInputs[i], {types: ['geocode'], componentRestrictions: {country: "nz"}});
            autocomplete.inputId = acInputs[i].id;
        }
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCU5pwsCrj8K5Sf7dvZ1fmq2VRfG4CMg64&libraries=places&callback=initAutocomplete"
async defer></script>