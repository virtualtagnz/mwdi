<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <?php echo (isset($ba_uid) ? '<a href="' . base_url('baform/pdf/' . $this->encrypt->encode($ba_uid)) . '" class="btn btn-primary pull-right"><i class="fa fa-print" target="_blank"></i> &nbsp; Print Form</a>' : false) ?>
                Forms  <i class="lnr lnr-chevron-right"></i> Business Application <i class="lnr lnr-chevron-right"></i> Next of Kin Details
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <?php //echo response_messages(); ?>
                    <form id="baform" action="<?php echo base_url('baform/save/form/step3') ?>" method="post">
                        <div class="row">
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <small class="pull-right timer-counter-started"></small>
                                <h4 style="margin-top: 30px;">NEXT OF KIN 1</h4>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" class="form-control" id="nk1_name" name="nk1_name" placeholder="Name" maxlength="100" value="<?php echo (isset($kin_details) && $kin_details ? $kin_details->nk1_name : false); ?>" data-validation="required" data-validation-error-msg="The field name is mandatory, please check again!">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Relationship to you</label>
                                    <input type="text" class="form-control" id="nk1_rel" name="nk1_rel" placeholder="Relationship to you" maxlength="100" value="<?php echo (isset($kin_details) && $kin_details ? $kin_details->nk1_rel : false); ?>" data-validation="required" data-validation-error-msg="The field relationship to you is mandatory, please check again!">
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <h4 style="margin-top: 30px;">Contact Details</h4>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Home Phone</label>
                                    <input type="text" class="form-control phone" id="nk1_hm_phn" name="nk1_hm_phn" placeholder="Home Phone" maxlength="12" value="<?php echo (isset($kin_details) && $kin_details ? $kin_details->nk1_hm_phn : false); ?>" data-validation="required number length" data-validation-length="min6" data-validation-allowing="float" data-validation-error-msg="The field home phone is mandatory, please check again!">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Work Phone</label>
                                    <input type="text" class="form-control phone" name="nk1_wk_phn" id="nk1_wk_phn" placeholder="Work Phone" maxlength="12" value="<?php echo (isset($kin_details) && $kin_details ? $kin_details->nk1_wk_phn : false); ?>" data-validation="required number length" data-validation-length="min6" data-validation-allowing="float" data-validation-error-msg="The field work phone is mandatory, please check again!">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Mobile</label>
                                    <input type="text" class="form-control phone" id="nk1_mob" name="nk1_mob" placeholder="Mobile" maxlength="12" value="<?php echo (isset($kin_details) && $kin_details ? $kin_details->nk1_mob : false); ?>" data-validation="required number length" data-validation-length="min6" data-validation-allowing="float" data-validation-error-msg="The field mobile is mandatory, please check again!">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" class="form-control" id="nk1_email" name="nk1_email" placeholder="Email" maxlength="50" value="<?php echo (isset($kin_details) && $kin_details ? $kin_details->nk1_email : false); ?>" data-validation="required email" data-validation-error-msg-required="The field email is mandatory, please check again!" data-validation-error-email="You have not given a correct e-mail address">
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <hr>
                                <h4 style="margin-top: 30px;">NEXT OF KIN 2</h4>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" class="form-control" id="nk2_name" name="nk2_name" placeholder="Name" maxlength="100" value="<?php echo (isset($kin_details) && $kin_details ? $kin_details->nk2_name : false); ?>" data-validation="required" data-validation-error-msg="The field name is mandatory, please check again!">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Relationship to you</label>
                                    <input type="text" class="form-control" id="nk2_rel" name="nk2_rel" placeholder="Relationship to you" maxlength="100" value="<?php echo (isset($kin_details) && $kin_details ? $kin_details->nk2_rel : false); ?>" data-validation="required" data-validation-error-msg="The field relationship to you is mandatory, please check again!">
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <h4 style="margin-top: 30px;">Contact Details</h4>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Home Phone</label>
                                    <input type="text" class="form-control phone" id="nk2_hm_phn" name="nk2_hm_phn" placeholder="Home Phone" maxlength="12" value="<?php echo (isset($kin_details) && $kin_details ? $kin_details->nk2_hm_phn : false); ?>" data-validation="required number length" data-validation-length="min6" data-validation-allowing="float" data-validation-error-msg="The field home phone is mandatory, please check again!">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Work Phone</label>
                                    <input type="text" class="form-control phone" id="nk2_wk_phn" name="nk2_wk_phn" placeholder="Work Phone" maxlength="12" value="<?php echo (isset($kin_details) && $kin_details ? $kin_details->nk2_wk_phn : false); ?>" data-validation="required number length" data-validation-length="min6" data-validation-allowing="float" data-validation-error-msg="The field work phone is mandatory, please check again!">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Mobile</label>
                                    <input type="text" class="form-control phone" id="nk2_mob" name="nk2_mob" placeholder="Mobile" maxlength="12" value="<?php echo (isset($kin_details) && $kin_details ? $kin_details->nk2_mob : false); ?>" data-validation="required number length" data-validation-length="min6" data-validation-allowing="float" data-validation-error-msg="The field mobile is mandatory, please check again!">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" class="form-control" id="nk2_email" name="nk2_email" placeholder="Email" maxlength="50" value="<?php echo (isset($kin_details) && $kin_details ? $kin_details->nk2_email : false); ?>" data-validation="required email" data-validation-error-msg-required="The field email is mandatory, please check again!" data-validation-error-email="You have not given a correct e-mail address">
                                </div>
                            </div>

                            <div class="col-md-12" style="padding-top: 20px; padding-bottom: 20px;">
                                <input type="hidden" name="ba_uid" id="ba_uid" value="<?php echo $ba_uid; ?>">
                                <input type="hidden" name="timer_started" id="timer_started" value="<?php echo (isset($kin_details) && $kin_details && $kin_details->start_date ? $kin_details->start_date : false) ?>">
                                <input type="hidden" name="timer_end" id="timer_end" value="<?php echo (isset($kin_details) && $kin_details && $kin_details->end_date ? $kin_details->end_date : false) ?>">
                                <a href="<?php echo base_url('baform/step2/' . $ba_uid_encrypt) ?>" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; BACK</a> &nbsp;
                                <button type="submit" class="btn btn-default" name="save_form" value="1">SAVE FORM &nbsp; <i class="fa fa-save"></i></button> &nbsp;
                                <button type="submit" class="btn btn-primary" name="next_form" value="1">NEXT &nbsp; <i class="fa fa-arrow-right"></i></button>
                                &nbsp; [3/6]
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($this->session->flashdata('success')) { ?>
    <script>swal({type: 'success', title: 'Success', text: 'The form has been successfully saved!', timer: 3000});</script>
<?php } else if ($this->session->flashdata('warning')) { ?>
    <script>swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000});</script>
<?php } ?>
<script type="text/javascript">
    $(function () {
        $.validate({
            form: '#baform',
            modules: 'security'
        });

        $("#baform :input").change(function () {
            if (!$("#timer_started").val()) {
//                var datetime = new Date().toLocaleString();
//                $(".timer-counter-started").html('Form started at ' + datetime);
//                $("#timer_started").val(datetime);
                  $('#timer_started').val(timer());
            }
        });

        $('#nk1_hm_phn,#nk2_hm_phn').mask('000000000000', {placeholder: "Home Phone 091234567"});
        $('#nk1_wk_phn,#nk2_wk_phn').mask('000000000000', {placeholder: "Work Phone 091234567"});
        $('#nk1_mob,#nk2_mob').mask('000000000000', {placeholder: "Mobile 0211234567"});
    })
</script>