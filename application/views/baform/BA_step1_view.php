<?php
$ba_form1_details = (json_decode($details));
$list_title = array('1' => 'Mr', '2' => 'Mrs', '3' => 'Miss', '4' => 'Ms');
$list_acoommodations = array('own' => 'Own your own home', 'rent' => 'Rent', 'share' => 'Share a flat with others', 'lease' => 'Lease', 'partner' => 'Live with partner');
?>
<style>
    td{
        border: 1px solid #9D8526;
        padding: 5px;
    }
    tr.bg{
        background-color: #252c35;
        color: #ffffff;
    }
    .title{
        text-align: center;
    }
</style>

<div class="title">
    <h2>KIA ORA , Tell Us About Yourself</h2>
</div>
<?php if ($ba_form1_details) { ?>
    <table cellspacing="0" cellpadding="5" border="1">
        <tr class="bg">
            <td colspan="2">
                <h4 class="">Details about yourself</h4>
            </td>
        </tr>
        <tr>
            <td>Name</td><td><?php echo $list_title[$ba_form1_details->bac_title].' '.$ba_form1_details->bac_first_name.' '.$ba_form1_details->bac_last_name;?></td>
        </tr>
        <tr>
            <td>Home Address</td><td><?php echo $ba_form1_details->bac_home_add;?></td>
        </tr>
        <tr>
            <td>Business Address</td><td><?php echo $ba_form1_details->bac_bus_add;?></td>
        </tr>
        <tr class="bg">
            <td colspan="2">
                <h4 class="">Contact Details Home</h4>
            </td>
        </tr>
        <tr>
            <td>Phone</td><td><?php echo $ba_form1_details->bac_home_phn;?></td>
        </tr>
        <tr>
            <td>Fax</td><td><?php echo $ba_form1_details->bac_home_fax;?></td>
        </tr>
        <tr>
            <td>Mobile</td><td><?php echo $ba_form1_details->bac_home_mob;?></td>
        </tr>
        <tr>
            <td>Email</td><td><?php echo $ba_form1_details->bac_home_email;?></td>
        </tr>
        <tr class="bg">
            <td colspan="2">
                <h4 class="">Contact Details Business</h4>
            </td>
        </tr>
        <tr>
            <td>Phone</td><td><?php echo $ba_form1_details->bac_bus_phn;?></td>
        </tr>
        <tr>
            <td>Fax</td><td><?php echo $ba_form1_details->bac_bus_fax;?></td>
        </tr>
        <tr>
            <td>Mobile</td><td><?php echo $ba_form1_details->bac_bus_mob;?></td>
        </tr>
        <tr>
            <td>Email</td><td><?php echo $ba_form1_details->bac_bus_email;?></td>
        </tr>
        <tr>
            <td>Birthday</td><td><?php echo $ba_form1_details->bac_dob;?></td>
        </tr>
        <tr>
            <td>Gender</td><td><?php echo ucfirst($ba_form1_details->bac_gender);?></td>
        </tr>
        <tr>
            <td>Marital Status</td><td><?php echo ucfirst($ba_form1_details->bac_mat_stat);?></td>
        </tr>
        <tr>
            <td>No of dependents</td><td><?php echo $ba_form1_details->bac_no_dep;?></td>
        </tr>
        <tr>
            <td>Dependents Age(s)</td><td><?php echo $ba_form1_details->bac_ages;?></td>
        </tr>
        <tr>
            <td>Accommodation</td><td><?php echo $list_acoommodations[$ba_form1_details->bac_accom];?></td>
        </tr>
        <tr>
            <td>Have you had a Business or Management Education?</td><td><?php echo $ba_form1_details->stud_bus=='Y'?'Yes':'No';?></td>
        </tr>
        <tr>
            <td>Whanau Name</td><td><?php echo $ba_form1_details->bac_whanau_name;?></td>
        </tr>
        <tr>
            <td>Tribal Affiliations</td><td><?php echo $ba_form1_details->bac_ta;?></td>
        </tr>
    </table>
<!--    <br pagebreak="true" />-->
<?php } ?>


