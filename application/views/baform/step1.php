<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">

                <?php echo (isset($baform) && $baform && $baform->ba_id ? '<a href="' . base_url('baform/pdf/' . $this->encrypt->encode($baform->ba_id)) . '" class="btn btn-primary pull-right" target="_blank"><i class="fa fa-print"></i> &nbsp; Print Form</a>' : false) ?>
                Forms  <i class="lnr lnr-chevron-right"></i> Business Application
            </h3>
            <div class="panel">
                <div class="panel-heading">
                    <small class="pull-right timer-counter-started"></small>
                    <h3 class="panel-title">KIA ORA , TELL US ABOUT YOURSELF</h3>
                </div>
                <div class="panel-body">
                    <?php //var_dump($baform); ?>
                    <form id="baform" action="<?php echo base_url('baform/save/form/step1') ?>" method="post">
                        <input type="hidden" name="client_id" id="client_id" value="<?php echo $baform->ba_client_ref?>">
                        <input type="hidden" name="sub_stat" id="sub_stat" value="<?php echo $baform->sub_status?>">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Title</label>
                                    <select name="tit" id="tit" class="form-control" data-validation="required" data-validation-error-msg="The field title is mandatory, please check again!">
                                        <option value="">Title</option>
                                        <option value="1" <?php echo (isset($baform) && $baform && $baform->bac_title == '1' ? 'selected="selected"' : false) ?>>Mr</option>
                                        <option value="2" <?php echo (isset($baform) && $baform && $baform->bac_title == '2' ? 'selected="selected"' : false) ?>>Mrs</option>
                                        <option value="3" <?php echo (isset($baform) && $baform && $baform->bac_title == '3' ? 'selected="selected"' : false) ?>>Miss</option>
                                        <option value="4" <?php echo (isset($baform) && $baform && $baform->bac_title == '4' ? 'selected="selected"' : false) ?>>Ms</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" maxlength="50" data-validation-error-msg="The field first name is mandatory, please check again!" value="<?php echo (isset($baform) && $baform ? $baform->bac_first_name : false) ?>" data-validation="required">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" maxlength="50" value="<?php echo (isset($baform) && $baform ? $baform->bac_last_name : false) ?>" data-validation="required" data-validation-error-msg="The field last name is mandatory, please check again!">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Home Address</label>
                                    <input type="text" class="form-control autocomplete" name="home_add" id="home_add" placeholder="Home Address" value="<?php echo (isset($baform) && $baform ? $baform->bac_home_add : false) ?>" autocomplete="off" data-validation="required" data-validation-error-msg="The field home address is mandatory, please check again!">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Business Address</label>
                                    <input type="text" class="form-control autocomplete" name="bus_add" id="bus_add" placeholder="Business Address" value="<?php echo (isset($baform) && $baform ? $baform->bac_bus_add : false) ?>" autocomplete="off" data-validation="required" data-validation-error-msg="The field business address is mandatory, please check again!">
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <hr>
                                <h4 style="margin-top: 30px;">CONTACT DETAILS HOME</h4>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Phone</label>
                                    <input type="num" class="form-control phone" id="hphone" name="hphone" placeholder="Phone" maxlength="12" value="<?php echo (isset($baform) && $baform ? $baform->bac_home_phn : false) ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Fax</label>
                                    <input type="num" class="form-control" id="hfax" name="hfax" placeholder="Fax" maxlength="20" value="<?php echo (isset($baform) && $baform ? $baform->bac_home_fax : false) ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Mobile</label>
                                    <input type="num" class="form-control phone" id="hmob" name="hmob" placeholder="Mobile" maxlength="12" value="<?php echo (isset($baform) && $baform ? $baform->bac_home_mob : false) ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" class="form-control" id="hemail" name="hemail" placeholder="Email" maxlength="50" value="<?php echo (isset($baform) && $baform ? $baform->bac_home_email : false) ?>" data-validation="email" data-validation-error-email="You have not given a correct e-mail address" data-validation-optional="true">
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-bottom: 15px;">
                                <hr>
                                <h4 style="margin-top: 30px;">CONTACT DETAILS BUSINESS</h4>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Phone</label>
                                    <input type="text" class="form-control phone" id="bphone" name="bphone" placeholder="Phone" maxlength="12" value="<?php echo (isset($baform) && $baform ? $baform->bac_bus_phn : false) ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Fax</label>
                                    <input type="text" class="form-control" id="bfax" name="bfax" placeholder="Fax" maxlength="20" value="<?php echo (isset($baform) && $baform ? $baform->bac_bus_fax : false) ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Mobile</label>
                                    <input type="text" class="form-control phone" id="bmob" name="bmob" placeholder="Mobile" maxlength="12" value="<?php echo (isset($baform) && $baform ? $baform->bac_bus_mob : false) ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" class="form-control" id="bemail" name="bemail" placeholder="Email" maxlength="50" value="<?php echo (isset($baform) && $baform ? $baform->bac_bus_email : false) ?>" data-validation="email" data-validation-error-email="You have not given a correct e-mail address" data-validation-optional="true">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Birthday</label>
                                    <input type="text" class="form-control date" id="dob" name="dob" placeholder="Birthday (mm/dd/yyyy)" value="<?php echo (isset($baform) && $baform ? $baform->bac_dob : false) ?>" maxlength="10" data-validation="required birthdate" data-validation-format="dd/mm/yyyy" data-validation-error-msg-required="The field birthday is mandatory, please check again!" data-validation-error-msg-birthdate="Invalid Date (dd/mm/yyyy)">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Gender</label>
                                    <p style="padding: 6px 0;">
                                        <label class="radio radio-inline" style="margin: 0">
                                            <input name="gender" value="male" type="radio" <?php echo (isset($baform) && $baform && $baform->bac_gender == 'male' ? 'checked="checked"' : false) ?>>
                                            <span>Male</span>
                                        </label>
                                        <label class="radio radio-inline">
                                            <input name="gender" value="female" type="radio" <?php echo (isset($baform) && $baform && $baform->bac_gender == 'female' ? 'checked="checked"' : false) ?>>
                                            <span>Female</span>
                                        </label>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Marital Status</label>
                                    <p style="padding: 6px 0;">
                                        <label class="radio radio-inline" style="margin: 0">
                                            <input name="marry" value="married" type="radio" <?php echo (isset($baform) && $baform && $baform->bac_mat_stat == 'married' ? 'checked="checked"' : false) ?>>
                                            <span>Married</span>
                                        </label>
                                        <label class="radio radio-inline">
                                            <input name="marry" value="single" type="radio" <?php echo (isset($baform) && $baform && $baform->bac_mat_stat == 'single' ? 'checked="checked"' : false) ?>>
                                            <span>Single</span>
                                        </label>
                                        <label class="radio radio-inline">
                                            <input name="marry" value="partner" type="radio" <?php echo (isset($baform) && $baform && $baform->bac_mat_stat == 'partner' ? 'checked="checked"' : false) ?>>
                                            <span>Partner</span>
                                        </label>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>No. of dependents</label>
                                    <input type="number" name="dep" id="dep" min="1" class="form-control" placeholder="No. of dependents" maxlength="2" value="<?php echo (isset($baform) && $baform ? $baform->bac_no_dep : false) ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Dependents Age(s)</label>
                                    <input type="text" class="form-control" id="age" name="age" placeholder="Dependents Age(s)" maxlength="20" value="<?php echo (isset($baform) && $baform ? $baform->bac_ages : false) ?>" data-validation="required" data-validation-error-msg="The field age is mandatory, please check again!" data-validation-depends-on="dep">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Accommodation</label>
                                    <p style="padding: 6px 0;">
                                        <label class="radio radio-inline" style="margin: 0">
                                            <input name="accommodation" value="own" type="radio" <?php echo (isset($baform) && $baform && $baform->bac_accom == 'own' ? 'checked="checked"' : false) ?> data-validation="required" data-validation-error-msg="The field accommodation is mandatory, please check again!">
                                            <span>Own your own home?</span>
                                        </label>
                                        <label class="radio radio-inline">
                                            <input name="accommodation" value="rent" type="radio" <?php echo (isset($baform) && $baform && $baform->bac_accom == 'rent' ? 'checked="checked"' : false) ?> data-validation="required" data-validation-error-msg="The field accommodation is mandatory, please check again!">
                                            <span>Rent?</span>
                                        </label>
                                        <label class="radio radio-inline">
                                            <input name="accommodation" value="share" type="radio" <?php echo (isset($baform) && $baform && $baform->bac_accom == 'share' ? 'checked="checked"' : false) ?> data-validation="required" data-validation-error-msg="The field accommodation is mandatory, please check again!">
                                            <span>Share a flat with others?</span>
                                        </label>
                                        <label class="radio radio-inline">
                                            <input name="accommodation" value="lease" type="radio" <?php echo (isset($baform) && $baform && $baform->bac_accom == 'lease' ? 'checked="checked"' : false) ?> data-validation="required" data-validation-error-msg="The field accommodation is mandatory, please check again!">
                                            <span>Lease?</span>
                                        </label>
                                        <label class="radio radio-inline">
                                            <input name="accommodation" value="partner" type="radio" <?php echo (isset($baform) && $baform && $baform->bac_accom == 'partner' ? 'checked="checked"' : false) ?> data-validation="required" data-validation-error-msg="The field accommodation is mandatory, please check again!">
                                            <span>Live with partner?</span>
                                        </label>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Have you had a Business or Management Education?</label>
                                    <p style="padding: 6px 0;">
                                        <label class="radio radio-inline" style="margin: 0">
                                            <input name="bus_ed" value="Y" type="radio" <?php echo (isset($baform) && $baform && $baform->stud_bus == 'Y' ? 'checked="checked"' : false) ?> data-validation="required" data-validation-error-msg="The field management education is mandatory, please check again!">
                                            <span>Yes</span>
                                        </label>
                                        <label class="radio radio-inline">
                                            <input name="bus_ed" value="N" type="radio" <?php echo (isset($baform) && $baform && $baform->stud_bus == 'N' ? 'checked="checked"' : false) ?> data-validation="required" data-validation-error-msg="The field management education is mandatory, please check again!">
                                            <span>No</span>
                                        </label>
                                    </p>
                                </div>
                            </div>                                
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Whanau Name</label>
                                    <input type="text" class="form-control" id="w_name" name="w_name" placeholder="Whanau Name" maxlength="150" value="<?php echo (isset($baform) && $baform ? $baform->bac_whanau_name : false) ?>" data-validation="required" data-validation-error-msg="The field whanau name is mandatory, please check again!">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Tribal Affiliations</label>
                                    <input type="text" class="form-control" id="tribal" name="tribal" placeholder="Tribal Affiliations" maxlength="150" value="<?php echo (isset($baform) && $baform ? $baform->bac_ta : false) ?>" data-validation="required" data-validation-error-msg="The field tribal affiliations is mandatory, please check again!">
                                </div>
                            </div>
                            <div class="col-md-12" style="padding-top: 20px; padding-bottom: 20px;">
                                <input type="hidden" name="ba_uid" id="ba_uid" value="<?php echo (isset($baform) && $baform && $baform->ba_id ? $baform->ba_id : false) ?>">
                                <input type="hidden" name="timer_started" id="timer_started" value="<?php echo (isset($baform) && $baform && $baform->bac_start_date!='0000-00-00 00:00:00' ? $baform->bac_start_date : false) ?>">
                                <input type="hidden" name="timer_end" id="timer_end" value="<?php echo (isset($baform) && $baform && $baform->bac_end_date!='0000-00-00 00:00:00' ? $baform->bac_end_date : false) ?>">
                                <!--<a href="javascript: history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; Back</a> &nbsp;-->
                                <button type="submit" class="btn btn-default" name="save_form" value="1">SAVE FORM &nbsp; <i class="fa fa-save"></i></button> &nbsp;
                                <button type="submit" class="btn btn-primary" name="next_form" value="1">NEXT &nbsp; <i class="fa fa-arrow-right"></i></button>
                                &nbsp; [1/6]
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>  
</div>
<?php if ($this->session->flashdata('pdf_error')) { ?>
    <script>swal({type: 'error', title: 'PDF Error', text: 'No information to print', timer: 3000});</script>
<?php } else if ($this->session->flashdata('success')) { ?>
    <script>swal({type: 'success', title: 'Success', text: 'The form has been successfully saved!', timer: 3000});</script>
<?php } else if ($this->session->flashdata('warning')) { ?>
    <script>swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000});</script>
<?php } ?>
<script type="text/javascript">
    $(function () {
        //$('#client_id').select2();

        $.validate({
            form: '#baform',
            modules: 'security,date,logic'
        });
        
        $("#baform :input").change(function () {
            if (!$("#timer_started").val()) {
//                var datetime = new Date().toLocaleString();
//                $(".timer-counter-started").html('Form started at ' + datetime);
//                $("#timer_started").val(datetime);
                  $('#timer_started').val(timer());
            }
        });

        $("#client_id").change(function () {
            var thisVALUE = $(this).val();
            if (thisVALUE) {

                $.ajax({
                    url: config.base_url + 'baform/ajax/client',
                    data: {id: thisVALUE},
                    method: 'POST',
                    dataType: 'json',
                    beforeSend: function () {
                        swal({
                            type: 'info',
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            title: 'Checking client, please wait....'
                        });
                    }
                }).done(function (data) {
                    if (data.status == true) {
                        window.location = data.location;
                    } else {
                        $("#ba_uid").val('');
                        swal.close();
                    }
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    swal.close();
                    console.log(errorThrown);
                });
            }
        });

        $("#hphone, #hfax, #hmob, #bphone, #bfax, #bmob").keydown(function (e) {
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                    (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                return;
            }
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        $('#dob').mask('00/00/0000', {placeholder: "Birthday (DD/MM/YYYY)"});
    });
</script>
<script>
    function initAutocomplete() {
        var acInputs = document.getElementsByClassName("autocomplete");
        for (var i = 0; i < acInputs.length; i++) {
            var autocomplete = new google.maps.places.Autocomplete(acInputs[i], {types: ['geocode'], componentRestrictions: {country: "nz"}});
            autocomplete.inputId = acInputs[i].id;
        }
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCU5pwsCrj8K5Sf7dvZ1fmq2VRfG4CMg64&libraries=places&callback=initAutocomplete"
async defer></script>