<?php
$ba_form4_details = (json_decode($details));
$testimonials = (json_decode($testimonials));
?>
<style>
    td{
        border: 1px solid #9D8526;
        padding: 5px;
    }
    tr.bg{
        background-color: #252c35;
        color: #ffffff;
    }
    .title{
        text-align: center;
    }
</style>
<div class="title">
    <h2>Employment Details</h2>
</div>
<?php if ($ba_form4_details) { ?>
    <table cellspacing="0" cellpadding="5" border="1">
        <tr class="bg">
            <td colspan="2">
                <h4 class="">Employment Details</h4>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="5" border="1">
        <thead>
            <tr>
                <td>Name of previous or present employer</td>
                <td>Role</td>
                <td>Period of employment</td>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($ba_form4_details as $emp) { ?>
            <tr>
                <td><?php echo $emp->emp_name?></td>
                <td><?php echo $emp->wrk?></td>
                <td><?php echo $emp->long_wrk?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <table cellspacing="0" cellpadding="5" border="1">
        <tr class="bg">
            <td colspan="2">
                <h4 class="">Copies Of Testimonials</h4>
            </td>
        </tr>
        <tr>
            <td>Name</td><td><?php echo $testimonials->tes_name1;?></td>
        </tr>
        <tr>
            <td>Phone</td><td><?php echo $testimonials->tes_phn1;?></td>
        </tr>
        <tr>
            <td>Address</td><td><?php echo $testimonials->tes_add1;?></td>
        </tr>
        <tr>
            <td>Name</td><td><?php echo $testimonials->tes_name2;?></td>
        </tr>
        <tr>
            <td>Phone</td><td><?php echo $testimonials->tes_phn2;?></td>
        </tr>
        <tr>
            <td>Address</td><td><?php echo $testimonials->tes_add2;?></td>
        </tr>
    </table>
<!--<br pagebreak="true" />-->
<?php } ?>
