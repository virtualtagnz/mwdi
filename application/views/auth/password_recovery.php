<div class="vertical-align-wrap">
    <div class="vertical-align-middle">
        <div class="auth-box ">
            <div class="left">
                <div class="content">
                    <div class="header">
                        <div class="logo text-center"><img src="assets/img/logo.png" alt=""></div>
                        <p class="lead">Recover my password</p>
                    </div>
                    <form class="form-auth-small" action="<?php echo base_url('recover'); ?>" method="post" autocomplete="off" name="pwd_recovery" id="pwd_recovery">
                        <p>Please enter your user name and email address below to receive instructions for resetting password.</p>
                        <div class="form-group">
                            <label for="" class="control-label sr-only">User Name</label>
                            <input type="text" class="form-control" id="rec_username" name="rec_username" placeholder="User Name" data-validation="required" data-validation-error-msg="Please enter user name" value="<?php echo set_value('rec_username'); ?>">
                            <div id="infoMessage"><?php echo form_error('rec_username'); ?></div>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label sr-only">Email</label>
                            <input type="text" class="form-control" id="rec_email" name="rec_email" placeholder="Email" data-validation="email" value="<?php echo set_value('rec_email'); ?>">
                            <div id="infoMessage"><?php echo form_error('rec_email'); ?></div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-lg btn-block">RESET PASSWORD</button>
                        <div class="bottom">
                            <i class="fa fa-sign-in"></i>&nbsp; <a href="<?php echo base_url('Auth')?>">Back to Login</a>
                        </div>
                    </form>
                    <div class="text-center" style="color: #a94442;">
                        <?php if ($this->session->flashdata('result')) { ?>
                            <div> <?= $this->session->flashdata('result') ?> </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="right">
                <div class="overlay"></div>
                <div class="content text">
                    <h1 class="heading">Welcome to Māori Womens Development Inc</h1>
                    <p>Hei Manaaki i Te Mana Wāhine Māori</p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $.validate({
            form: '#pwd_recovery'
        });
    });
</script>
