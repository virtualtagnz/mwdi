<?php //var_dump($this->session->userdata());?>
<div class="vertical-align-wrap">
    <div class="vertical-align-middle">
        <div class="auth-box ">
            <div class="left">
                <div class="content">
                    <div class="header">
                        <div class="logo text-center"><img src="assets/img/logo.png" alt=""></div>
                        <p class="lead">Login to your account</p>
                    </div>
                    <form class="form-auth-small" action="<?php echo base_url('login'); ?>" method="post" autocomplete="off" name="login" id="login">
                        <div class="form-group">
                            <label for="" class="control-label sr-only">User Name</label>
                            <input type="text" class="form-control" id="username" name="username" placeholder="User Name" data-validation="required" data-validation-error-msg="Please enter user name" value="<?php echo set_value('username'); ?>">
                            <div id="infoMessage"><?php echo form_error('username'); ?></div>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label sr-only">Password</label>
                            <input type="password" class="form-control" id="pwd" name="pwd" placeholder="Password" data-validation="required" data-validation-error-msg="Please enter password" value="<?php echo set_value('pwd'); ?>">
                            <div id="infoMessage"><?php echo form_error('pwd'); ?></div>
                        </div>
                        <div class="form-group clearfix">
                            <span class="pull-right"><a href="<?php echo base_url('password_recovery')?>">Forgot password?</a></span>
<!--                            <label class="fancy-checkbox element-left">
                                <input type="checkbox">
                                <span>Remember me</span>
                            </label>-->
                        </div>
                        <button type="submit" class="btn btn-primary btn-lg btn-block">LOGIN</button>
                        <div class="bottom">
                            <?php if ($this->session->flashdata('success')) { ?>
                            <div class="text-center" style="color: #629f5c;">
                                <div>You have been successfully registered as a Client!</div>
                            </div>
                            <?php } else { ?>                            
                            <i class="fa fa-user-plus"></i>&nbsp; <a href="<?php echo base_url('register')?>">Register as a Client</a>
                            <?php } ?>
                        </div>
                    </form>
                    <div class="text-center" style="color: #a94442;">
                        <?php if ($this->session->flashdata('result')) { ?>
                            <div> <?= $this->session->flashdata('result') ?> </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="right">
                <div class="overlay"></div>
                <div class="content text">
                    <h1 class="heading">Welcome to Māori Womens Development Inc</h1>
                    <p>Hei Manaaki i Te Mana Wāhine Māori</p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $.validate({
            form: '#login'
        });
    });
</script>
