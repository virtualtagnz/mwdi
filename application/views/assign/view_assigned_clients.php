<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                Assign <i class="lnr lnr-chevron-right"></i> Clients to Coaches  <i class="lnr lnr-chevron-right"></i> View Assigned
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable js-pointstable display" id="res_ass_tab">
                            <thead>
                                <tr>
                                    <th>Coach Name</th>
                                    <th>Client Name</th>
                                    <th>Date Assigned</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $all_users_list = json_decode($users_list) ?>
                                <?php foreach ($all_users_list as $users) { ?>
                                    <tr>
                                        <td><?php echo $users->coach_fname.' '.$users->coach_lname?></td>
                                        <td><?php echo $users->client_fname.' '.$users->client_lname?></td>
                                        <td><?php echo date('d/m/Y',strtotime($users->assign_date))?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>

                    <hr>

                    <p><a href="javascript: history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; Back</a> &nbsp; </p>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<?php if ($this->session->flashdata('successfully_assigned')) { ?>
    <script>swal({type: 'success', title: 'success', text: 'The client has been successfully assigned', timer: 3000});</script>
<?php } else if ($this->session->flashdata('error_msg')) { ?>
    <script>swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000});</script>
<?php } ?>
<script>
    $(document).ready(function () {
        $('#res_ass_tab').dataTable({
            "pageLength": 10,
            "searching": true,
            "info": true,
            "ordering": true
        });
    });
</script>

