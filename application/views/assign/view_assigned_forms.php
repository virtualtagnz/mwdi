<?php $user_type = $this->encrypt->decode(end($this->uri->segments)); ?>
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                Assign <i class="lnr lnr-chevron-right"></i> Application Forms to <?php echo $user_type == '4' ? 'Coach' : 'Client' ?> <i class="lnr lnr-chevron-right"></i> View Assigned
            </h3>
            <?php //var_dump($assigned_resource) ?>
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable js-pointstable display" id="app_ass_tab">
                            <thead>
                                <tr>
                                    <th><?php echo $user_type == '4' ? 'Coach' : 'Client' ?> Name</th>
                                    <th>Form Name</th>
                                    <th>Date Assigned</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $ass_resources = json_decode($assigned_resource) ?>
                                <?php foreach ($ass_resources as $res) { ?>
                                <?php //var_dump($res);?>
                                    <tr>
                                        <td><?php echo $res->first_name.' '.$res->last_name?></td>
                                        <td><?php echo $res->frm_name?></td>
                                        <td><?php echo date('d/m/Y',strtotime($res->assign_date))?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>

                    <hr>

                    <p><a href="javascript: history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; Back</a> &nbsp; </p>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<?php if ($this->session->flashdata('successfully_assigned')) { ?>
    <script>swal({type: 'success', title: 'success', text: 'The form has been successfully assigned', timer: 3000});</script>
<?php } else if ($this->session->flashdata('error_msg')) { ?>
    <script>swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000});</script>
<?php } ?>
<script>
    $(document).ready(function () {
        $('#app_ass_tab').dataTable({
            "pageLength": 10,
            "searching": true,
            "info": true,
            "ordering": true
        });
    });
</script>

