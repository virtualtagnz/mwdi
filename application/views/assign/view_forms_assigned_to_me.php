<?php $user_type = $this->encrypt->decode(end($this->uri->segments)); ?>
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                Forms <i class="lnr lnr-chevron-right"></i> <?php echo $this->session->userdata('role')=='4'?'My Forms':'Coach Forms'?>
            </h3>
            <?php if($this->session->userdata('role')=='4'){?>
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Create New Form for Client</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable js-pointstable display" id="app_ass_tab">
                            <thead>
                                <tr>
                                    <th>Form Name</th>
                                    <th>Date Assigned</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $assigned_forms = json_decode($my_forms) ?>
                                <?php foreach ($assigned_forms as $form) { ?>
                                    <?php //var_dump($form);?>
                                    <tr>
                                        <td>
                                            <a href="<?php echo base_url('view_engagement/' . $form->frm_id); ?>"><?php echo $form->frm_name ?></a>
                                        </td>
                                        <td><?php echo date('d/m/Y', strtotime($form->assign_date)) ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>

                    <hr>

                    <p><a href="javascript: history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; Back</a> &nbsp; </p>
                </div>
            </div>
            <?php }?>
            <div class="panel">
                <?php if($this->session->userdata('role')=='4'){?>
                <div class="panel-heading">
                    <h3 class="panel-title">Submitted / Draft Forms</h3>
                </div>
                <?php }?>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="archive_frm_tab" class="table table-striped table-bordered dataTable js-pointstable display">
                            <thead>
                                <tr>
                                    <th>Client Name</th>
                                    <th>Form Name</th>
                                    <th>Status</th>
                                    <th>Edit/ View</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $all_archives = json_decode($archives) ?>
                                <?php foreach ($all_archives as $archive) { ?>
                                    <?php //var_dump($archive); ?>
                                    <tr>
                                        <td><?php echo $archive->fname . ' ' . $archive->lname ?></td>
                                        <td><?php echo $archive->form_name ?></td>
                                        <td><?php echo $archive->stat == 'Y' ? 'Submited' : 'Draft' ?></td>
                                        <?php if ($archive->stat != 'Y') { ?>
                                            <td><a href="<?php echo base_url('view_engagement/' . $archive->form_type . '/' . $this->encrypt->encode($archive->form_id)); ?>" class="btn btn-fefault" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a> &nbsp;</td>
                                        <?php } else { ?>
                                            <td>
                                                <?php if ($this->session->userdata('role') == '1') { ?>
                                                    <a href="<?php echo base_url('view_engagement/' . $archive->form_type . '/' . $this->encrypt->encode($archive->form_id)); ?>" class="btn btn-fefault" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a> &nbsp;
                                                <?php } ?>
        <!--                                                <a rel="<?php echo $archive->form_type; ?>" id="<?php echo $this->encrypt->encode($archive->form_id); ?>" class="btn btn-fefault view_form" data-toggle="tooltip" data-placement="top" title="View"><i class="fa fa-eye"></i></a> &nbsp;-->
                                                <a href="<?php echo base_url('view_single_form/' . $archive->form_type . '/' . $this->encrypt->encode($archive->form_id)); ?>" target="blank" class="btn btn-fefault" data-toggle="tooltip" data-placement="top" title="Print"><i class="fa fa-print"></i></a> &nbsp;
                                            </td>        
                                        <?php } ?>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<?php if ($this->session->flashdata('submit_success')) { ?>
    <script>swal({type: 'success', title: 'success', text: 'The form has been successfully submitted', timer: 3000});</script>
<?php } ?>
<script>
    $(document).ready(function () {
        $('#archive_frm_tab').dataTable({
            "pageLength": 10,
            "searching": true,
            "info": true,
            "ordering": true
        });

        $('body').delegate('.view_form', 'click', (function () {
            var formType = $(this).get(0).rel;
            var id = $(this).get(0).id;
            console.log(id);
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('view_single_form/') ?>" + formType + '/' + id,
                success: function (results) {
                    //console.log(results);
                    switch (formType) {
                        case '5':
                            $('#modalView').html(results);
                            $('#coach_form_1_modal').modal('show');
                            break;
                    }
                }
            })
        }));
    });
</script>