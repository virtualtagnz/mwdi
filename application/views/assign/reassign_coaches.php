<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                Assign <i class="lnr lnr-chevron-right"></i> Re-Assign Clients to Coaches
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <form action="<?php echo base_url('reassign'); ?>" method="post" name="reassign_client" id="reassign_client" style="padding-top: 20px;">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Select Coach</label>
                                    <select class="form-control" name="curr_coach" id="curr_coach" data-validation="required" data-validation-error-msg="The field select coach is mandatory, please check again!">
                                        <option selected="true" disabled="true">-- Select --</option>
                                        <?php if ($coches) { ?>
                                            <?php $ext_coaches = json_decode($coches) ?>
                                            <?php foreach ($ext_coaches as $coach) { ?>
                                                <option value="<?php echo $this->encrypt->encode($coach->coach_id) ?>"><?php echo $coach->first_name . ' ' . $coach->last_name ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                    <div id="infoMessage"><?php echo form_error('curr_coach'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Clients Belongs This Coach</label>
                                    <select class="form-control" name="belongs_client[]" id="belongs_client" multiple="true" data-validation="required" data-validation-error-msg-required="The field client belongs to this coach is mandatory, please check again!">
                                        <option selected="true" disabled="true">-- Select --</option>
                                    </select>
                                    <div id="infoMessage"><?php echo form_error('belongs_client'); ?></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Select Coach For Reassign</label>
                                    <select class="form-control" name="reassign_coach" id="reassign_coach" data-validation="required" data-validation-error-msg-required="The field coach for reassign is mandatory, please check again!">
                                        <option selected="true" disabled="true">-- Select --</option>
                                        <?php if ($coches) { ?>
                                            <?php $ext_coaches = json_decode($coches) ?>
                                            <?php foreach ($ext_coaches as $coach) { ?>
                                                <option value="<?php echo $this->encrypt->encode($coach->coach_id) ?>"><?php echo $coach->first_name . ' ' . $coach->last_name ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                    <div id="infoMessage"><?php echo form_error('reassign_coach'); ?></div>
                                </div>
                            </div>
                        </div>
                        <p> 
                            <a href="javascript: history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; Back</a> &nbsp;
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-secondary" onclick="location.href = '<?php echo base_url('view_assigned_clients'); ?>'">View Assigned Clients</button>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<?php if ($this->session->flashdata('success_mag')) { ?>
    <script>swal({type: 'success', title: 'success', text: 'The client has been successfully re-assigned', timer: 3000});</script>
<?php } else if ($this->session->flashdata('error_msg')) { ?>
    <script>swal({type: 'error', title: 'Error!', text: 'An application error occurred on the server, please try again!!', timer: 3000});</script>
<?php } ?>
<script>
    $(document).ready(function () {
        $('#curr_coach,#belongs_client,#reassign_coach').select2();
        
        //----------------------------------------------------------------------
        
        $.validate({
            form: '#reassign_client',
            modules: 'security'
        });

        //----------------------------------------------------------------------
        
        $('#curr_coach').on('change', function () {
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('my_users/'); ?>" + $(this).val(),
                success: function (results) {
                    var my_users = $.parseJSON(results);
                    if (my_users) {
                        $('#belongs_client').empty();
                        $.each(my_users, function (key, value) {
                            $('#belongs_client').append($("<option/>", {
                                value: value.client_id,
                                text: value.first_name + ' ' + value.last_name
                            }));
                        });
                    }
                }
            });
        });
        
        //----------------------------------------------------------------------
        
        $('#reassign_coach').on('change', function () {
            $('#infoMessage p').empty();
        });

    });
</script>