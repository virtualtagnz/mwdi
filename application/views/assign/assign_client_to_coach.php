<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                <a href="<?php echo base_url('reassign_coaches')?>" class="btn btn-primary pull-right">Re-Assign Clients to Coaches</a>
                Assign <i class="lnr lnr-chevron-right"></i> Clients to Coaches
            </h3>
            <?php //var_dump($clients) ?>
            <div class="panel">
                <div class="panel-body">
                    <form action="<?php echo base_url('assign_users');?>" method="post" name="assign_client" id="assign_client" style="padding-top: 20px;">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <select class="form-control" name="selected_coach" id="selected_coach" data-validation="required" data-validation-error-msg="The field select coach is mandatory, please check again!">
                                        <option selected="true" disabled="true">-- Select --</option>
                                        <?php if ($coches) { ?>
                                            <?php $ext_coaches = json_decode($coches) ?>
                                            <?php foreach ($ext_coaches as $coach) { ?>
                                                <option value="<?php echo $this->encrypt->encode($coach->coach_id) ?>"><?php echo $coach->first_name . ' ' . $coach->last_name ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                    <div id="infoMessage"><?php echo form_error('selected_coach'); ?></div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <select class="form-control" name="selected_client[]" id="selected_client" multiple="true" data-validation="required" data-validation-error-msg="The field select client is mandatory, please check again!">
                                        <option selected="true" disabled="true">-- Select --</option>
                                        <?php if ($clients) { ?>
                                            <?php $ext_clients = json_decode($clients) ?>
                                            <?php foreach ($ext_clients as $client) { ?>
                                                <option value="<?php echo $this->encrypt->encode($client->client_id) ?>"><?php echo $client->first_name . ' ' . $client->last_name ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                     <div id="infoMessage"><?php echo form_error('selected_client'); ?></div>
                                </div>
                            </div>
                        </div>

                        <p> 
                            <button type="submit" class="btn btn-primary">Assign</button>
                            <button type="button" class="btn btn-secondary" onclick="location.href='<?php echo base_url('view_assigned_clients');?>'">View Assigned Clients</button>
                        </p>
                    </form>
                </div>
            </div>
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable js-pointstable display" id="my_users_tab">
                            <thead>
                                <tr>
                                    <th>Client Name</th>
                                    <th>Date Assigned</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script>
    $(document).ready(function () {
        $('#selected_coach,#selected_client').select2();

        $.validate({
            form: '#assign_client',
            modules: 'security'
        });
        
        $('#selected_coach').on('change', function () {
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('my_users/'); ?>"+$(this).val(),
                success: function (results) {
                    var my_users = $.parseJSON(results);
                    $('#my_users_tab').DataTable({
                        "destroy":true,
                        "aaData": $.parseJSON(results),
                        "aoColumns": [
                            {"mData": "","mRender": function (data, type, row) {
                                    return row.first_name+' '+row.last_name;
                            }},
                            {"mData": "assign_date"}
                        ]
                    });
                }
            });
        });

    });
</script>