<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                My Resources
            </h3>
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable js-pointstable display" id="res_ass_tab">
                            <thead>
                                <tr>
                                    <th>Resource Name</th>
                                    <th>Date Assigned</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $resources = json_decode($my_resources) ?>
                                <?php foreach ($resources as $single_resource) { ?>
                                    <tr>
                                        <td><a href="<?php echo base_url('uploads/resources/' . $single_resource->res_linq); ?>" target="blank"><?php echo $single_resource->res_name?></a></td>
                                        <td><?php echo date('d/m/Y',strtotime($single_resource->assigned_date))?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>

                    <hr>

                    <p><a href="javascript: history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp; Back</a> &nbsp; </p>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

