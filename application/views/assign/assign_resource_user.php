<?php $user_type = $this->encrypt->decode(end($this->uri->segments)); ?>
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">Assign <i class="lnr lnr-chevron-right"></i> Resource to <?php echo $user_type == '4' ? 'Coach' : 'Client' ?></h3>
            <div class="panel">
                <div class="panel-body">
                    <form action="<?php echo base_url('assign_forms/'.end($this->uri->segments));?>" method="post" name="assign_frm" id="assign_frm" style="padding-top: 20px;">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <select class="form-control js-example-basic-single" name="user_select" id="user_select" data-validation="required" data-validation-error-msg="The field user is mandatory, please check again!">
                                        <option value="" selected="selected">-- Select <?php echo $user_type == '4' ? 'Coach' : 'Client' ?> --</option>
                                        <?php if ($users) { ?>
                                            <?php $ext_users = json_decode($users) ?>
                                            <?php foreach ($ext_users as $user) { ?>
                                                <option value="<?php echo $this->encrypt->encode($user->user_id) ?>"><?php echo $user->first_name . ' ' . $user->last_name ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                    <div id="infoMessage"><?php echo form_error('user_select'); ?></div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <select class="form-control js-example-basic-multiple" name="res_select[]" id="res_select" multiple="true" data-validation="required" data-validation-error-msg="The field form is mandatory, please check again!">
                                    </select>
                                    <div id="infoMessage"><?php echo form_error('res_select'); ?></div>
                                </div>
                            </div>
                        </div>
                        <p>
                            <button type="submit" class="btn btn-primary">Assign Resource</button> &nbsp; 
                            <button type="button" class="btn btn-secondary" onclick="location.href='<?php echo base_url('view_assigned_forms/'.end($this->uri->segments));?>'">View Assigned Resource</button>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script>
    $(document).ready(function () {
        $('#user_select,#res_select').select2();
        
        $.validate({
            form: '#assign_frm',
            modules: 'security'
        });
        
        $('#user_select').on('change', function () {
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('Assignment/load_form_list/'); ?>" + $(this).val(),
                success: function (results) {
                    console.log(results);
                    var resources = $.parseJSON(results);
                    if (resources) {
                        $('#res_select').empty();
                        $.each(resources, function (key, value) {
                        $('#res_select').append($("<option/>", {
                            value: value.res_id,
                            text: value.res_name
                        }));
                    });
                    }
                }
            });
        });
    });
</script>