<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>MWDI</title>
        <style type="text/css">
            @import url(http://fonts.googleapis.com/css?family=Lato:400);
            a,img{text-decoration:none;outline:0}img{max-width:600px;-ms-interpolation-mode:bicubic}a{border:0;color:#21BEB4}a img{border:none}h1,h2,h3,td{font-family:Helvetica,Arial,sans-serif;font-weight:400}body{-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;width:100%;height:100%;color:#37302d;background:#fff}h1,h2,h3{padding:0;margin:0;color:#fff;font-weight:400}h3{color:#21c5ba;font-size:24px}
        </style>
        <style type="text/css" media="screen">
            @media screen{h1,h2,h3,td{font-family:Lato,'Helvetica Neue',Arial,sans-serif!important}}
        </style>

        <style type="text/css" media="only screen and (max-width: 480px)">
            @media only screen and (max-width:480px){table[class=w320],td[class=w320]{width:320px!important}table[class=w300]{width:300px!important}table[class=w290]{width:290px!important}td[class=mobile-center]{text-align:center!important}td[class=mobile-padding]{padding-left:20px!important;padding-right:20px!important;padding-bottom:20px!important}}
        </style>
    </head>
    <body class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none" bgcolor="#ffffff">
        <table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%" >
            <tr>
                <td align="center" valign="top" bgcolor="#ffffff"  width="100%">

                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td style="background: #8c7722; height: 20px;">
                                <center>
                                    <table cellspacing="0" cellpadding="0" width="500" class="w320">
                                        <tr><td>&nbsp;</td></tr>
                                    </table>
                                </center> 
                            </td>
                        </tr>
                        <tr>
                            <td style="border: 3px solid #8c7722; background-color: #FFFFFF" width="100%">
                                <center>
                                    <table cellspacing="0" cellpadding="0" width="500" class="w320">
                                        <tr>
                                            <td valign="top" style="padding:10px 0; text-align:left;" class="mobile-center">
                                                <a href="<?php echo base_url(); ?>" target="blank"><img src="<?php echo base_url('assets/img/logo-email.png'); ?>"/></a>
                                            </td>
                                        </tr>
                                    </table>
                                </center>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <center>
                                    <table cellspacing="0" cellpadding="0" width="500" class="w320">
                                        <tr>
                                            <td>

                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td class="mobile-padding" style="text-align:left;">
                                                            <br/>
                                                            <br/>
                                                            <p>Tēnā koe <strong><?php echo $name ?></strong></p>
                                                            <p>Thank you for submitting your online Loan Application form and supporting information, one of our Team members will be in touch with you soon.</p>

                                                            <p>Ngā mihi<br>Māori Women’s Development Inc</p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="mobile-padding">
                                                <br/>&nbsp;
                                                <br/>
                                            </td>
                                        </tr>
                                    </table>
                                </center>
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color:#000000;border: 3px solid #8c7722;height: 50px; padding: 15px;"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>