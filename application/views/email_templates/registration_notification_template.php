<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>MWDI</title>
        <style type="text/css">
            @import url(http://fonts.googleapis.com/css?family=Lato:400);
            a,img{text-decoration:none;outline:0}img{max-width:600px;-ms-interpolation-mode:bicubic}a{border:0;color:#21BEB4}a img{border:none}h1,h2,h3,td{font-family:Helvetica,Arial,sans-serif;font-weight:400}body{-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;width:100%;height:100%;color:#37302d;background:#fff}h1,h2,h3{padding:0;margin:0;color:#fff;font-weight:400}h3{color:#21c5ba;font-size:24px}
        </style>
        <style type="text/css" media="screen">
            @media screen{h1,h2,h3,td{font-family:Lato,'Helvetica Neue',Arial,sans-serif!important}}
        </style>

        <style type="text/css" media="only screen and (max-width: 480px)">
            @media only screen and (max-width:480px){table[class=w320],td[class=w320]{width:320px!important}table[class=w300]{width:300px!important}table[class=w290]{width:290px!important}td[class=mobile-center]{text-align:center!important}td[class=mobile-padding]{padding-left:20px!important;padding-right:20px!important;padding-bottom:20px!important}}
        </style>
    </head>
    <body class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none" bgcolor="#ffffff">
        <table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%" >
            <tr>
                <td align="center" valign="top" bgcolor="#ffffff"  width="100%">

                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td style="background: #8c7722; height: 20px;">
                                <center>
                                    <table cellspacing="0" cellpadding="0" width="500" class="w320">
                                        <tr><td style="color: #cccccc;"><a style="color: #cccccc;" href="<?php echo base_url('subscribe/auth') ?>">Login</a></td></tr>
                                    </table>
                                </center> 
                            </td>
                        </tr>
                        <tr>
                            <td style="border: 3px solid #8c7722; background-color: #FFFFFF" width="100%">
                                <center>
                                    <table cellspacing="0" cellpadding="0" width="500" class="w320">
                                        <tr>
                                            <td valign="top" style="padding:10px 0; text-align:left;" class="mobile-center">
                                                <a href="<?php echo base_url(); ?>" target="blank"><img src="<?php echo base_url('assets/img/logo-email.png'); ?>"/></a>
                                            </td>
                                        </tr>
                                    </table>
                                </center>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <center>
                                    <table cellspacing="0" cellpadding="0" width="500" class="w320">
                                        <tr>
                                            <td>

                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td class="mobile-padding" style="text-align:left;">
                                                            <br/>
                                                            <br/>
                                                            Tēnā koe <?php echo $name ?>,<br/><br/>
                                                            You have now been registered on The Māori Women’s Development Inc Client Database; please ensure you store your username and password securely and that all your profile details are correct.<br/><br/>
                                                            
                                                            User Name : <?php echo $user_name ?><br/>
                                                            Password : <?php echo $new_pwd ?><br/>
                                                            
                                                            <?php if($user_type=='5'){?>
                                                            <p><strong><i>Applying for a Māori Women’s Development Inc Loan?</i></strong></p>
                                                            <p>Using your new access you now have the ability to submit a full loan application and upload all necessary documents.  Please ensure you complete all areas and required fields throughout the application forms <strong>(you will not be able to move through your forms if fields are incomplete, this includes uploading required documents)</strong>.  You can also save your forms and continue working on them at a later time.  Once complete submit your application directly to MWDI and one of our team will be in contact with you.</p>
                                                            <p><strong><i>Are you working with an MWDI Coach?</i></strong></p>
                                                            <p>Using your new access you now have the ability to submit a full loan application <strong>(see above notes)</strong>.</p>
                                                            <p>You will also be able to share <strong>(upload/download)</strong> documents and resources with your assigned Hinepreneur Coach.</p>
                                                            <?php } elseif($user_type=='4'){?>
                                                            <p><strong><i>Working with your Coachees</i></strong></p>
                                                            <p>Using your new access you now have the ability to:</p>
                                                            <p>
                                                                <ol>
                                                                    <li>View all your assigned coachees and update any of their profile details if required</li>
                                                                    <ul>
                                                                        <li><strong>Go to left column and select Users > My Clients</strong></li>
                                                                    </ul></br>
                                                                    <li>Complete your coaching engagement forms and submit them directly to Māori Women’s Development Inc.  You can upload any additional coaching notes as document files within the coaching forms.</li>
                                                                    <ul>
                                                                        <li><strong>Go to left column and select Forms > My Forms and complete all required fields.</strong></li>
                                                                        <li><strong>To attach files select File Attachment > select Clients name from drop down menu > Name the file and add a file description then select File attachment and upload your document from your computer then select Submit.</strong></li>
                                                                    </ul><br/>
                                                                    <li>Receive resources/documents into your profile directly from MWDI to assist you in coaching or to share with your coachees.</li>
                                                                    <ul>
                                                                        <li><strong>Go to left column and select My Resources</strong></li>
                                                                    </ul></br>
                                                                </ol>
                                                            </p>
                                                            <?php }?>
                                                            <p><strong>If you have any concerns with the database you will need to contact <a href="https://virtualtag.atlassian.net/servicedesk/customer/portal/1/group/1">The Virtual Tag Help Center</a> - or call 0800 882 458.</strong></p>
                                                            <p>Ngā mihi<br>Māori Women’s Development Inc</p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="mobile-padding">
                                                <br/>&nbsp;
                                                <br/>
                                            </td>
                                        </tr>
                                    </table>
                                </center>
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color:#000000;border: 3px solid #8c7722;height: 50px; padding: 15px;"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>