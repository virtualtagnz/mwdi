<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                Submitted Forms <i class="lnr lnr-chevron-right"></i> Clients
            </h3>
            <?php //var_dump($my_forms); ?>
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="archive_frm_tab" class="table table-striped table-bordered dataTable js-pointstable display">
                            <thead>
                                <tr>
                                    <th>Form Name</th>
                                    <th>Status</th>
                                    <th>Start/ Edit/ View</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($ba_submitted[0])) { ?>
                                    <tr>
                                        <td><?php echo $ba_submitted[0]->frm_name ?></td>
                                        <td><?php echo $ba_submitted[0]->sub_status == 'Y' ? 'Submitted' : 'Draft' ?></td>
                                        <td>
                                            <?php if ($ba_submitted[0]->sub_status == 'Y') { ?>
                                                <a href="<?php echo base_url('baform/pdf/' . $this->encrypt->encode($ba_submitted[0]->ba_id)) ?>" target="blank">View</a>
                                            <?php } else { ?>
                                                <a href="<?php echo base_url('baform/check/edit/' . $this->session->userdata('user_level_id')); ?>">Edit Form</a>      
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } else { ?>
                                    <tr>
                                        <td><?php echo $my_forms[0]->frm_name ?></td> 
                                        <td>Not Started</td>
                                        <td><a href="<?php echo base_url('baform/check/edit/' . $this->session->userdata('user_level_id')); ?>">Start Form</a></td>
                                    </tr>
                                <?php } ?>

                                <?php if (isset($ppms_submitted[0])) { ?>
                                    <tr>
                                        <td><?php echo $ppms_submitted[0]->frm_name ?></td>
                                        <td><?php echo $ppms_submitted[0]->sub_stat == 'Y' ? 'Submitted' : 'Draft' ?></td>
                                        <td>
                                            <?php if ($ppms_submitted[0]->sub_stat == 'Y') { ?>
                                                <a href="<?php echo base_url('uploads/ppms_forms/' . $ppms_submitted[0]->file_link); ?>" target="blank">View</a>
                                            <?php } else { ?>
                                                <a href="<?php echo base_url('ppms_form') ?>">PPMS Form</a>      
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } else { ?>
                                    <tr>
                                        <td><?php echo $my_forms[1]->frm_name ?></td> 
                                        <td>Not Started</td>
                                        <td><a href="<?php echo base_url('ppms_form') ?>">Start Form</a></td>
                                    </tr>   
                                <?php } ?>
                                <?php if (isset($security_submitted[0])) { ?>
                                    <tr>
                                        <td><?php echo $security_submitted[0]->frm_name ?></td>
                                        <td><?php echo $security_submitted[0]->sub_stat == 'Y' ? 'Submitted' : 'Draft' ?></td>
                                        <td>
                                            <?php if ($security_submitted[0]->sub_stat == 'Y') { ?>
                                                <a href="<?php echo base_url('view_security_form/' . $this->encrypt->encode($this->session->userdata('user_level_id'))); ?>" target="blank">View</a>
                                            <?php } else { ?>
                                                <a href="<?php echo base_url('security_form/step1/' . $this->encrypt->encode($this->session->userdata('user_level_id'))) ?>">Edit Form</a>      
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } else { ?>
                                    <tr>
                                        <td><?php echo $my_forms[2]->frm_name ?></td> 
                                        <td>Not Started</td>
                                        <td><a href="<?php echo base_url('security_form/step1/' . $this->encrypt->encode($this->session->userdata('user_level_id'))) ?>">Start Form</a></td>
                                    </tr>    
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<div id="modalView"></div>
<script>
    $(document).ready(function () {
        $('#archive_frm_tab').dataTable({
            "pageLength": 10,
            "searching": true,
            "info": true,
            "ordering": true
        });

        $('body').delegate('.view_form', 'click', (function () {
            var formType = $(this).get(0).rel;
            var id = $(this).get(0).id;
            console.log(id);
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('view_single_form/') ?>" + formType + '/' + id,
                success: function (results) {
                    //console.log(results);
                    switch (formType) {
                        case '5':
                            $('#modalView').html(results);
                            $('#coach_form_1_modal').modal('show');
                            break;
                    }
                }
            })
        }));
    });
</script>
