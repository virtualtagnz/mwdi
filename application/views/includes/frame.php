<!DOCTYPE html>
<html lang="en" class="fullscreen-bg">
    <?php $this->load->view('includes/head'); ?>
    <body>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!-- Javascript -->
        <!--bootstrap-->
        <script> var config = {'base_url':'<?php echo site_url()?>'}</script>
        <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/scripts/script.js'); ?>"></script>
        <script src="<?php echo base_url('assets/scripts/mwdi.js'); ?>"></script>
        <!--JQuery form validator-->
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
        <!--Datatable JS-->
        <script src="//cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.js"></script>
        <!--Sweet alert-->
        <script src="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.4/sweetalert2.all.min.js"></script>
        <!--jQuery-Mask-Plugin-->
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.12/jquery.mask.js"></script>
        <!--Select 2-->
        <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
        <!--ck editor-->
        <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
        <!--Jquery highlighter-->
        <script src="<?php echo base_url('assets/scripts/jQuery-Highlighter.js'); ?>"></script>
        <!--NProgress-->
        <script src="https://unpkg.com/nprogress@0.2.0/nprogress.js"></script>
        <div id="wrapper">
            <?php $this->load->view('includes/nav'); ?>
            <?php $this->load->view($mainContent); ?>
            <?php $this->load->view('includes/footer'); ?>
        </div>
    </body>
</html>


