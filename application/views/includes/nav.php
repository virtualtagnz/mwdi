<!-- NAVBAR -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="brand">
        <a href="#"><img src="<?php echo base_url('assets/img/logo.png') ?>" alt="" class="img-responsive logo"></a>
    </div>
    <div class="container-fluid">
        <div class="navbar-btn">
            <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
        </div>
        <?php if ($this->session->userdata('role') == '1' || $this->session->userdata('role') == '2' || $this->session->userdata('role') == '3') { ?>
            <form class="form-inline navbar-form navbar-left" action="" method="post" name="search_frm" id="search_frm">
                <div class="form-group">
                    <input type="text" name="key_word" id="key_word" class="form-control" placeholder="Search..." required>

                    <select class="form-control" name="key_area" id="key_area">
                        <option value="1">Users</option>
                    </select>

                    <button type="submit" class="btn btn-default" name="btn_search" id="btn_search"><i class="fa fa-search"></i></button>
                </div>
            </form>
        <?php } ?>
        <div id="navbar-menu">
            <?php $notifications = json_decode(view_notification()); ?>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                        <i class="lnr lnr-alarm"></i>
                        <span class="badge bg-danger" id="nft_cnt"><?php echo count($notifications); ?></span>
                    </a>
                    <ul class="dropdown-menu notifications">
                        <li><a href="<?php echo base_url('Notification') ?>" class="more">See all notifications</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<?php echo $this->session->userdata('prof_image') != NULL ? base_url('uploads/profile_image/' . $this->session->userdata('prof_image')) : base_url('assets/img/user_default.jpg'); ?>" class="img-circle" alt=""> <span><?php echo $this->session->userdata('full_name'); ?></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url('edit_user/' . $this->encrypt->encode($this->session->userdata('uid')) . '/' . $this->encrypt->encode($this->session->userdata('role'))) ?>"><i class="lnr lnr-user"></i> <span>My Profile</span></a></li>
                        <li><a href="<?php echo base_url('logout') ?>"><i class="lnr lnr-exit"></i> <span>Logout</span></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- END NAVBAR -->
<!-- LEFT SIDEBAR -->
<div id="sidebar-nav" class="sidebar">
    <div class="sidebar-scroll">
        <nav>
            <ul class="nav">
                <li><a href="<?php echo base_url('Dashboard') ?>" class="<?php echo $currentPage == 'index' ? 'active' : ''; ?>"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
                <!-- User Section -->
                <?php if ($this->session->userdata('role') == '1') { ?>
                    <li>
                        <a href="#subUsers" data-toggle="collapse" class="collapsed <?php echo in_array($currentPage, array('users_admin', 'users_account', 'users_coach', 'users_client')) ? 'active' : '' ?>" ><i class="lnr lnr-users"></i> <span>Users</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="subUsers" class="collapse <?php echo in_array($currentPage, array('users_admin', 'users_account', 'users_coach', 'users_client')) ? 'in' : '' ?>">
                            <ul class="nav">
                                <li><a href="<?php echo base_url('view_user_lists/' . $this->encrypt->encode('2')) ?>" class="<?php echo $currentPage == 'users_admin' ? 'active' : ''; ?>">Admin</a></li>
                                <li><a href="<?php echo base_url('view_user_lists/' . $this->encrypt->encode('3')) ?>" class="<?php echo $currentPage == 'users_account' ? 'active' : ''; ?>">Accountant</a></li>
                                <li><a href="<?php echo base_url('view_user_lists/' . $this->encrypt->encode('4')) ?>" class="<?php echo $currentPage == 'users_coach' ? 'active' : ''; ?>">Coach</a></li>
                                <li><a href="<?php echo base_url('view_user_lists/' . $this->encrypt->encode('5')) ?>" class="<?php echo $currentPage == 'users_client' ? 'active' : ''; ?>">Client</a></li>     
                            </ul>
                        </div>
                    </li>
                <?php } elseif ($this->session->userdata('role') == '2' || $this->session->userdata('role') == '3') { ?>
                    <li>
                        <a href="#subUsers" data-toggle="collapse" class="collapsed <?php echo in_array($currentPage, array('users_admin', 'users_account', 'users_coach', 'users_client')) ? 'active' : '' ?>" ><i class="lnr lnr-users"></i> <span>Users</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="subUsers" class="collapse <?php echo in_array($currentPage, array('users_admin', 'users_account', 'users_coach', 'users_client')) ? 'in' : '' ?>">
                            <ul class="nav">
                                <li><a href="<?php echo base_url('view_user_lists/' . $this->encrypt->encode('4')) ?>" class="<?php echo $currentPage == 'users_coach' ? 'active' : ''; ?>">Coach</a></li>
                                <li><a href="<?php echo base_url('view_user_lists/' . $this->encrypt->encode('5')) ?>" class="<?php echo $currentPage == 'users_client' ? 'active' : ''; ?>">Client</a></li>                     
                            </ul>
                        </div>
                    </li>
                <?php } elseif ($this->session->userdata('role') == '4') { ?>
                    <li>
                        <a href="#subUsers" data-toggle="collapse" class="collapsed <?php echo in_array($currentPage, array('users_admin', 'users_account', 'users_coach', 'users_client')) ? 'active' : '' ?>" ><i class="lnr lnr-users"></i> <span>Users</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="subUsers" class="collapse <?php echo in_array($currentPage, array('users_admin', 'users_account', 'users_coach', 'users_client')) ? 'in' : '' ?>">
                            <ul class="nav">
                                <li><a href="<?php echo base_url('view_user_lists/' . $this->encrypt->encode('5')) ?>" class="<?php echo $currentPage == 'users_client' ? 'active' : ''; ?>">My Clients</a></li>     
                            </ul>
                        </div>
                    </li>
                <?php } ?>
                <!-- Application Form Section -->
                <li>
                    <a href="#subForms" data-toggle="collapse" class="collapsed <?php echo in_array($currentPage, array('form_business', 'form_security', 'form_ppms', 'form_coach')) ? 'active' : '' ?>"><i class="lnr lnr-text-align-justify"></i> <span>Forms</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                    <div id="subForms" class="collapse <?php echo in_array($currentPage, array('form_business', 'form_security', 'form_ppms', 'form_coach')) ? 'in' : '' ?>">
                        <ul class="nav">
                            <?php if ($this->session->userdata('role') == '1' || $this->session->userdata('role') == '2') { ?>
                                <li><a href="<?php echo base_url('baform/list') ?>" class="<?php echo $currentPage == 'form_business' ? 'active' : ''; ?>">Business Application</a></li>
                                <li><a href="<?php echo base_url('submitted_security_forms') ?>" class="<?php echo $currentPage == 'form_security' ? 'active' : ''; ?>">Security Form</a></li>
                                <li><a href="<?php echo base_url('submitted_ppms_forms') ?>" class="<?php echo $currentPage == 'form_ppms' ? 'active' : ''; ?>">PPMS</a></li>
                                <li><a href="<?php echo base_url('submited_coach_form') ?>" class="<?php echo $currentPage == 'form_coach' ? 'active' : ''; ?>">Coach Form</a></li>
                                <!--<li><a href="<?php echo base_url('coaching_form') ?>" class="<?php echo $currentPage == 'form_temporary' ? 'active' : ''; ?>">Coaching Enquire Form</a></li>-->
                            <?php } elseif ($this->session->userdata('role') == '3'){?> 
                                <li><a href="<?php echo base_url('baform/list') ?>" class="<?php echo $currentPage == 'form_business' ? 'active' : ''; ?>">Business Application</a></li>
                                <li><a href="<?php echo base_url('submitted_security_forms') ?>" class="<?php echo $currentPage == 'form_security' ? 'active' : ''; ?>">Security Form</a></li>
                                <li><a href="<?php echo base_url('submitted_ppms_forms') ?>" class="<?php echo $currentPage == 'form_ppms' ? 'active' : ''; ?>">PPMS</a></li>
                                <li><a href="<?php echo base_url('submited_coach_form') ?>" class="<?php echo $currentPage == 'form_coach' ? 'active' : ''; ?>">Coach Form</a></li>
                            <?php } elseif ($this->session->userdata('role') == '4') { ?>
                                <li><a href="<?php echo base_url('my_forms/' . $this->encrypt->encode($this->session->userdata('user_level_id')) . '/' . $this->encrypt->encode($this->session->userdata('role'))) ?>" class="<?php echo $currentPage == 'my_forms' ? 'active' : ''; ?>">My Forms</a></li>
<!--                                <li><a href="<?php echo base_url('submited_coach_form') ?>" class="<?php echo $currentPage == 'form_coach' ? 'active' : ''; ?>">Archived Forms</a></li>-->
                                <!--<li><a href="<?php echo base_url('coaching_form') ?>" class="<?php echo $currentPage == 'form_temporary' ? 'active' : ''; ?>">Coaching Enquire Form</a></li>-->
                            <?php } elseif ($this->session->userdata('role') == '5') { ?>
<!--                                <li><a href="<?php echo base_url('my_forms/' . $this->encrypt->encode($this->session->userdata('user_level_id')) . '/' . $this->encrypt->encode($this->session->userdata('role'))) ?>" class="<?php echo $currentPage == 'my_forms' ? 'active' : ''; ?>">My Forms</a></li>-->
                                <li><a href="<?php echo base_url('client_forms') ?>" class="<?php echo $currentPage == 'my_forms' ? 'active' : ''; ?>">My Forms</a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </li>
                <!-- Assignment Section -->
                <li>
                    <?php if ($this->session->userdata('role') == '1') { ?>
                        <a href="#subAssign" data-toggle="collapse" class="collapsed"><i class="lnr lnr-select"></i> <span>Assign</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="subAssign" class="collapse">
                            <ul class="nav">
                                <li><a href="<?php echo base_url('assign_clients_to_coaches') ?>" class="<?php echo $currentPage == 'assign_client_to_coach' ? 'active' : '' ?>">Clients to Coaches</a></li>
                                <li><a href="<?php echo base_url('view_assigned_application_forms/' . $this->encrypt->encode('5')); ?>" class="">Forms to Client</a></li>
                                <li><a href="<?php echo base_url('assign_application_forms/' . $this->encrypt->encode('4')) ?>" class="<?php echo $currentPage == 'assign_application_forms_coach' ? 'active' : '' ?>">Forms to Coach</a></li>
                                <li><a href="<?php echo base_url('assign_resource/' . $this->encrypt->encode('5')) ?>" class="<?php echo $currentPage == 'assign_resource_client' ? 'active' : '' ?>">Resources to Client</a></li>
                                <li><a href="<?php echo base_url('assign_resource/' . $this->encrypt->encode('4')) ?>" class="<?php echo $currentPage == 'assign_resource_coach' ? 'active' : '' ?>">Resources to Coach</a></li>
                            </ul>
                        </div>
                    <?php } ?>
                </li>
                <!-- Resources Section -->
                <?php if ($this->session->userdata('role') == '1') { ?>
                    <li><a href="<?php echo base_url('resource_list'); ?>" class="<?php $currentPage == 'resources' ? 'active' : '' ?>"><i class="lnr lnr-cog"></i> <span>Create Resources</span></a></li>
                <?php } elseif ($this->session->userdata('role') == '4' || $this->session->userdata('role') == '5') { ?>
                    <li><a href="<?php echo base_url('my_resource_list'); ?>" class="<?php $currentPage == 'resources' ? 'active' : '' ?>"><i class="lnr lnr-cog"></i> <span>My Resources</span></a></li>    
                <?php } ?>
                <!-- File Attachment Section -->
                <li><a href="<?php echo base_url('file_attachments') ?>" class="<?php $currentPage == 'file_attachment' ? 'active' : '' ?>"><i class="lnr lnr-paperclip"></i> <span>File Attachment</span></a></li>
                <!-- Reports Section -->
                <?php if ($this->session->userdata('role') == '1' || $this->session->userdata('role') == '3') { ?>
                    <li>
                        <a href="#subReports" data-toggle="collapse" class="collapsed <?php echo in_array($currentPage, array('report_enquires', 'report_temp', 'report_loan', 'report_approved_loans', 'report_jobs', 'report_bank', 'report_client_regions', 'report_industry')) ? 'active' : '' ?>"><i class="lnr lnr-pie-chart"></i> <span>Reports</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="subReports" class="collapse <?php echo in_array($currentPage, array('report_enquires', 'report_temp', 'report_loan', 'report_approved_loans', 'report_jobs', 'report_bank', 'report_client_regions', 'report_industry')) ? 'in' : '' ?>">
                            <ul class="nav">
                                <li><a href="<?php echo base_url('reports/bank_declined_summary') ?>" class="<?php echo $currentPage == 'report_bank' ? 'active' : '' ?>">Bank Declined Summary</a></li>
                                <li><a href="<?php echo base_url('reports/client_regions') ?>" class="<?php echo $currentPage == 'report_client_regions' ? 'active' : '' ?>">Client Regions</a></li>
<!--                                <li><a href="<?php echo base_url('reports/enquires') ?>" class="<?php echo $currentPage == 'report_enquires' ? 'active' : '' ?>">Enquiries</a></li>-->
                                <li><a href="<?php echo base_url('reports/industry') ?>" class="<?php echo $currentPage == 'report_industry' ? 'active' : '' ?>">Industry</a></li>
                                <li><a href="<?php echo base_url('reports/jobs_created') ?>" class="<?php echo $currentPage == 'report_jobs' ? 'active' : '' ?>">Jobs Created</a></li>
                                <li><a href="<?php echo base_url('reports/loan_applications') ?>" class="<?php echo $currentPage == 'report_loan' ? 'active' : '' ?>">Loan Applications</a></li>
                                <li><a href="<?php echo base_url('reports/loan_approved_rejected') ?>" class="<?php echo $currentPage == 'report_approved_loans' ? 'active' : '' ?>">Loans Approved/Rejected</a></li>
                                <li><a href="<?php echo base_url('reports/loan_enquires') ?>" class="<?php echo $currentPage == 'report_temp' ? 'active' : '' ?>">Loan enquiries</a></li>
                                <li><a href="<?php echo base_url('reports/show_clients_mark_as_start') ?>" class="<?php echo $currentPage == 'report_client_mark_as_start' ? 'active' : '' ?>">Client - Coach engagement time</a></li>
                            </ul>
                        </div>
                    </li>
                <?php } ?>
                <!-- Email Section -->
                <?php if ($this->session->userdata('role') == '1') { ?>
                    <li><a href="<?php echo base_url('Email') ?>" class="<?php echo $currentPage == 'emails_management' ? 'active' : '' ?>"><i class="lnr lnr-envelope"></i> <span>Emails Management</span></a></li>
                <?php } ?>
                <!-- Loan Section --> 
                <?php if ($this->session->userdata('role') == '1' || $this->session->userdata('role') == '3') { ?>
                    <li><a href="<?php echo base_url('Loans_management') ?>" class="<?php echo $currentPage == 'loans_management' ? 'active' : '' ?>"><i class="fa fa-money"></i> <span>Loans Approve/Reject</span></a></li>
                <?php } ?>
            </ul>
        </nav>
    </div>
</div>
<div class="modal fade bs-example-modal-lg col-lg-12" id="search-results" role="dialog" style="">
    <div class="col-lg-12">&nbsp;</div>
    <div class="col-lg-3">&nbsp;</div>
    <div class="container-fluid col-lg-6" id="add-user" style="background-color: #ffffff;">
        <img src="<?php echo base_url('assets/img/Cancel.png') ?>" class="pull-right cancel-fm" style="margin-top: 10px;cursor:pointer;"/>
        <div class="page-title text-center" id="title"><h3>Search Results</h3><br/></div>
        <div class="col-md-12" id="search-result-div">
            <table id="search-results-tb" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Result</th>
                        <th>View</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="col-lg-3">&nbsp;</div>
</div>
<!-- END LEFT SIDEBAR -->
<!--<script src="//unpkg.com/sweetalert2"></script>
<script src="//unpkg.com/promise-polyfill"></script>-->
<script>
    $(document).ready(function () {
        var table;
        var view_profile = '<?php echo base_url('view_profile')?>';
        $('#search_frm').on('submit', function (e) {
            e.preventDefault();
            var key_word = $('#key_word').val();
            var key_area = $('#key_area').val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('global_search'); ?>",
                data: {'key_word': key_word, 'key_area': key_area},
                success: function (results) {
                    //console.log(results);
                    if (results != null) {
                        table = $('#search-results-tb').DataTable({
                            "destroy": true,
                            "paging": true,
                            "searching": false,
                            "info": false,
                            "ordering": false,
                            "aaData": $.parseJSON(results),
                            "aoColumns": [
                                {"mData": "", "mRender": function (data, type, row) {
                                        return '<ul class="list-unstyled">\n\
                                            <li><strong>Name -</strong> ' + row.first_name + ' ' + row.last_name + '</li>\n\
                                            <li><strong>Description -</strong> ' + row.description + '</li>\n\
                                            <li><strong>Phone & Mobile -</strong> ' + row.phone + '-' + row.mobile + '</li>\n\
                                            <li><strong>Address -</strong> ' + row.address + '</li>\n\
                                            <li><strong>Industry -</strong> ' + row.industry + '</li>\n\
                                            </ul>';
                                    }},
                                {"mData": "", "mRender": function (data, type, row) {
                                        return '<a href="'+view_profile+'/'+row.id+'/'+row.area+'">Visit</a>';
                                    }}
                            ]
                        });
                        $('#search-results').modal('show');
                        var tableContent = $(table.table().body());
                        $(tableContent).highlight(key_word);
                        table.on('draw', function () {
                            var tableContent = $(table.table().body());
                            $(tableContent).highlight(key_word);
                        });
                    }

                }
            });
        });
        
        $('.cancel-fm').on('click',function(){
            $('#search-results').modal('hide');
        });
        
        //----------------------------------------------------------------------
        
        my_current_notifications = '<?php echo $this->session->userdata('my_notification')?>';

        function check_new_notification(){
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('notification_check'); ?>",
                success: function (results) {
                    //console.log(results);
                    if(my_current_notifications < JSON.parse(results)){
                        swal({
                            position: 'top-right',
                            html: '<p><i class="fa fa-bell custom-bg-gold" style="-webkit-border-radius: 50%;-moz-border-radius: 50%;border-radius: 50%;width: 35px;height: 35px;line-height: 35px;background: #a0aeba;color: #fff;font-size: 18px;text-align: center;"></i> You have new notification</p>',
                            showConfirmButton: false
                    }).catch(swal.noop);
                    change_current_notification("<?php echo base_url('increase_current_notification_count'); ?>");
                    }
                }
            });    
        }
        
        var role = '<?php echo $this->session->userdata('role')?>';
        if(role == '1' || role == '2' || role == '3'){
            setInterval(check_new_notification, 3000);
        }
        
        $("#jsd-widget").css('left', '0');
    });
</script>
<script data-jsd-embedded data-key="d558a107-1cd7-4c74-923b-bc308086738f" data-base-url="https://jsd-widget.atlassian.com" src="https://jsd-widget.atlassian.com/assets/embed.js"></script>