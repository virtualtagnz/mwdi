<div class="clearfix"></div>
<footer>
    <div class="container-fluid">
        <p class="copyright">&copy; <?php echo date("Y"); ?> <strong>Maori Women's Development Inc</strong>. All Rights Reserved.<br><strong>Version </strong>2.00.001</p>
    </div>
</footer>
