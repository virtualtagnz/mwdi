<!DOCTYPE html>
<html lang="en" class="fullscreen-bg">
    <?php $this->load->view('includes/head'); ?>
    <body>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
        <div id="wrapper">
            <?php $this->load->view($mainContent); ?>
        </div>
    </body>
</html>

