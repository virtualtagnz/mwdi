<head>
    <title>MWDI - CRM</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url('assets/img/favicon.png');?>">
    <!-- Vendor CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/vendor/font-awesome/css/font-awesome.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/vendor/linearicons/style.css');?>">
    <!-- Main CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/main.css');?>">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700" rel="stylesheet">
    <!-- CSS Percentage Circle -->
    <link rel="stylesheet" href="<?php echo base_url('assets/vendor/circle/circle.min.css');?>">
    <!--Datatables CSS-->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.css"/>
    <!--Select-2-->
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <!--NProgress-->
    <link href="https://unpkg.com/nprogress@0.2.0/nprogress.css" rel="stylesheet" />
</head>