<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $data['mainContent'] = "auth/login";
        $this->load->view('includes/login_frame', $data);
    }

    public function do_login() {
        $this->form_validation->set_rules('username', 'User Name', 'trim|required');
        $this->form_validation->set_rules('pwd', 'Password', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $availability = $this->Login_model->is_user_available($this->input->post('username'), $this->input->post('pwd'), '1');
            if ($availability) {
                $data = $this->Login_model->user_authentication($this->input->post('username'), $this->input->post('pwd'));
                $details = $this->Login_model->get_user_details($data->user_id, $data->role_id);
                if ($data->status != '0') {
                    $session_array = array();
                    $session_array['uid'] = $data->user_id;
                    $session_array['role'] = $data->role_id;
                    $session_array['user_name'] = $data->user_name;
                    $session_array['email'] = $data->user_email;
                    $session_array['status'] = $data->status;
                    $session_array['user_level_id'] = $details->user_level_id;
                    $session_array['full_name'] = $details->first_name . ' ' . $details->last_name;
                    $session_array['prof_image'] = $details->image_link;
                    $session_array['my_notification'] = $this->Notification_model->get_notification_cnt($data->role_id);
                    $this->session->set_userdata($session_array);
                    if ($data->role_id == '4' || $data->role_id == '5') {
                        add_activities($data->role_id, $details->user_level_id, 'Log In MWDI');
                    }
                    redirect('Dashboard', 'refresh');
                } else {
                    $this->session->set_flashdata('result', 'Your account has been disabled. Please contact site admin');
                    $this->index();
                }
            } else {
                $this->session->set_flashdata('result', 'Sorry. Given user name or password can\'t be found. Please contact site admin');
                $this->index();
            }
        }
    }

    public function signout() {
        if (!$this->session->userdata('uid')) {
            redirect(site_url("Auth"));
        } else {
            if ($this->session->userdata('role') == '4' || $this->session->userdata('role') == '5') {
                add_activities($this->session->userdata('role'), $this->session->userdata('user_level_id'), 'Log out MWDI');
            }
            $this->session->unset_userdata('uid');
            $this->session->unset_userdata('role');
            $this->session->unset_userdata('user_name');
            $this->session->unset_userdata('email');
            $this->session->unset_userdata('status');
            $this->session->unset_userdata('full_name');
            $this->session->unset_userdata('prof_image');
            $this->session->unset_userdata('my_notification');
            redirect(base_url("Auth"));
        }
    }

    public function password_recover() {
        $data['mainContent'] = "auth/password_recovery";
        $this->load->view('includes/login_frame', $data);
    }

    public function do_password_recover() {
        $this->form_validation->set_rules('rec_username', 'User Name', 'trim|required');
        $this->form_validation->set_rules('rec_email', 'Email', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->password_recover();
        } else {
            $availability = $this->Login_model->is_user_available($this->input->post('rec_username'), $this->input->post('rec_email'), '2');
            if ($availability) {
                $new_password = uniqid();
                $user_data = $this->Login_model->get_user_id($this->input->post('rec_username'), $this->input->post('rec_email'));
                $rslt = $this->Login_model->update_password($user_data->user_id, array('pwd' => $new_password));
                if ($rslt) {
                    $email_data['to'] = $this->input->post('rec_email');
                    $email_data['subject'] = 'Password Recovery';
                    $user_name_arr = $this->Login_model->get_user_details($user_data->user_id, $user_data->role_id);
                    $template_data['name'] = $user_name_arr->first_name . ' ' . $user_name_arr->last_name;
                    $template_data['new_pwd'] = $new_password;
                    $email_data['msg'] = $this->load->view('email_templates/password_recovery_template', $template_data, TRUE);
                    $email_send_rslt = send_emails($email_data);
                    if ($email_send_rslt) {
                        $this->session->set_flashdata('result', 'Password has been reset. Please check your email');
                        $this->password_recover();
                    }
                } else {
                    $this->session->set_flashdata('result', 'An application error occurred on the server, please try again!');
                    $this->password_recover();
                }
            } else {
                $this->session->set_flashdata('result', 'Sorry. Given user name or password can\'t be found. Please contact site admin');
                $this->password_recover();
            }
        }
    }

}
