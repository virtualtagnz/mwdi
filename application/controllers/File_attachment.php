<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class File_attachment extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('uid')) {
            redirect(base_url('Auth'));
        }
    }

    //--------------------------------------------------------------------------

    public function index() {
        $data['files_info'] = json_encode($this->File_attachment_model->get_all_file_attachments());
        $data['currentPage'] = "file_attachment";
        $data['mainContent'] = "file_attachments/file_log";
        $this->load->view('includes/frame', $data);
    }

    //--------------------------------------------------------------------------

    public function new_file_attachment() {
        $data['clients'] = $this->System_user_model->gel_all_users('5');
        $data['currentPage'] = "file_attachment";
        $data['mainContent'] = "file_attachments/add_files";
        $this->load->view('includes/frame', $data);
    }

    //--------------------------------------------------------------------------

    public function show_files_belongs_to_client($client_id) {
        $my_files = json_encode($this->File_attachment_model->get_file_attachment_by_client_id($this->encrypt->decode($client_id)));
        echo $my_files;
    }

    //--------------------------------------------------------------------------

    public function upload_attach_files() {
        if ($this->session->userdata('role') != '5') {
            $this->form_validation->set_rules('fa_client', 'Client Name', 'trim|required');
        }
        $this->form_validation->set_rules('fa_name', 'File Name', 'trim|required');
        //$this->form_validation->set_rules('fa_des', 'Description', 'trim|required');
        //$this->form_validation->set_rules('fa_file', 'File', 'trim|required');
        if ($this->form_validation->run()) {
            if ($this->session->userdata('role') != '5') {
                $client_id = $this->encrypt->decode($this->input->post('fa_client'));
            } else {
                $client_id = $this->session->userdata('user_level_id');
            }

            $data = array();
            $temp_path = 'uploads/file_attachments/';
            if (!is_dir($temp_path . $client_id)) {
                if (mkdir($temp_path . $client_id, 0777, TRUE)) {
                    $full_path = realpath((APPPATH . '../public_html/uploads/file_attachments/' . $client_id));
                } else {
                    $full_path = FALSE;
                }
            } else {
                $full_path = realpath((APPPATH . '../public_html/uploads/file_attachments/' . $client_id));
            }
            $image_upload_rslt = upload_files($full_path, 'fa_file', 'common');
            if ($image_upload_rslt['stat']) {
                $data['client_id'] = $client_id;
                $data['file_name'] = $this->input->post('fa_name');
                $data['file_description'] = $this->input->post('fa_des');
                $data['file_id'] = $image_upload_rslt['fileArr']['file_name'];
                $data['created_by'] = $this->session->userdata('full_name');
                $data['create_role'] = $this->session->userdata('role');
                $data['create_date'] = date('Y-m-d H:i:s');
                $data['	is_deleted'] = 'N';
                $rtn = $this->File_attachment_model->insert_file_attachments($data);
                if ($rtn) {
                    if($this->session->userdata('role')=='5'){
                        add_activities($this->session->userdata('role'), $this->session->userdata('user_level_id'), 'Attach file to profile');
                    }elseif ($this->session->userdata('role')=='4') {
                        $clent_details = $this->System_user_model->get_single_user_by_user_level_id('5',$client_id);
                        add_activities($this->session->userdata('role'), $this->session->userdata('user_level_id'), 'Attach file to '.$clent_details->first_name.' '.$clent_details->last_name);
                    }
                    $this->session->set_flashdata('success_msg', 'success');
                    $this->index();
                } else {
                    $this->session->set_flashdata('error_msg', 'error');
                    $this->index();
                }
            } else {
                $this->session->set_flashdata('img_error', $image_upload_rslt['fileError']);
                $this->new_file_attachment();
            }
        } else {
            $this->new_file_attachment();
        }
    }

    //--------------------------------------------------------------------------

    public function delete_attached_files() {
        $file_id = $this->input->post('file_id');
        $file_link = $this->input->post('file_link');
        $client_id = $this->input->post('cid');
        $path = realpath(APPPATH . '../public_html/uploads/file_attachments/' . $client_id);
        if (unlink($path . '/' . $file_link)) {
            $data = array();
            $data['is_deleted'] = 'Y';
            $data['delete_by'] = $this->session->userdata('full_name');
            $data['delete_role'] = $this->session->userdata('role');
            $data['delete_date'] = date('Y-m-d H:i:s');
            echo ($this->File_attachment_model->mark_as_deleted($data, $file_id)) ? TRUE : FALSE;
        } else {
            echo FALSE;
        }
    }

}
