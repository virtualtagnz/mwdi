<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Coaching_form extends CI_Controller {

    public $single_user;

    public function __construct() {
        parent::__construct();

        //if (!$this->session->userdata('uid')) redirect(base_url('Auth'));


        $this->is_admin = (in_array($this->session->userdata('role'), array(1, 2)) ? true : false);
//        $this->load->model('Coaching_form_model');
//        $this->load->library('Pdf');
        $this->single_user = $this->System_user_model->get_single_user('1', '1');
    }

    /* ------------------------------------------------------------------------------------------------- */

    public function index() {
        if (!$this->session->userdata('uid')) {
            redirect(base_url('Auth'));
        } else {
            $data['templist'] = $this->Coaching_form_model->get_coaching_list();
            $data['currentPage'] = "form_coaching";
            $data['mainContent'] = "coaching_form/list";
            $this->load->view('includes/frame', $data);
        }
    }

    /* ------------------------------------------------------------------------------------------------- */

    public function create_coaching() {
        if (!$this->session->userdata('uid')) {
            redirect(base_url('Auth'));
        }

        $this->form_validation->set_rules('first_name', 'First Name', 'required|trim');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required|trim');
        $this->form_validation->set_rules('city_region', 'City', 'required|trim');
        $this->form_validation->set_rules('phone', 'Phone', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim');
        $this->form_validation->set_rules('comments', 'Comments', 'required|trim');

        if ($this->form_validation->run() == FALSE) {

            $data['currentPage'] = "form_coaching";
            $data['mainContent'] = "coaching_form/create";
            $this->load->view('includes/frame', $data);
        } else {

            $string_data['first_name'] = $this->input->post('first_name');
            $string_data['last_name'] = $this->input->post('last_name');
            $string_data['city_region'] = $this->input->post('city_region');
            $string_data['phone'] = $this->input->post('phone');
            $string_data['email'] = $this->input->post('email');
            $string_data['business_description'] = $this->input->post('comments');
            $string_data['created_date'] = date('Y-m-d H:i:s');
            $string_data['is_temp'] = '1';

            $result = $this->Coaching_form_model->add_coaching_form($string_data);

            if ($result) {
                $template_data = array();
                $template_data['client_name'] = $string_data['first_name'] . ' ' . $string_data['last_name'];
                $template_data['create_date'] = $string_data['created_date'];
                $template_data['super_admin'] = $this->single_user->first_name . ' ' . $this->single_user->last_name;
                $this->send_email_to_super_admin($template_data, '1');
                $this->session->set_flashdata('submit', 'success');
                redirect('coaching_form');
            } else {
                $this->session->set_flashdata('error', 'error');
                redirect('coaching_form');
            }
        }
    }

    /* ------------------------------------------------------------------------------------------------- */

    public function create_coaching_outside_crm() {
        if (!$this->session->userdata('uid')) {
            redirect(base_url('Auth'));
        }

        $this->form_validation->set_rules('first_name', 'First Name', 'required|trim');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required|trim');
        $this->form_validation->set_rules('city_region', 'City', 'required|trim');
        $this->form_validation->set_rules('phone', 'Phone', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim');
        $this->form_validation->set_rules('comments', 'Comments', 'required|trim');

        if ($this->form_validation->run() == FALSE) {

            $data['currentPage'] = "form_coaching";
            $data['mainContent'] = "coaching_form/create_out";
            $this->load->view('includes/create_out_frame', $data);
        } else {

            $string_data['first_name'] = $this->input->post('first_name');
            $string_data['last_name'] = $this->input->post('last_name');
            $string_data['city_region'] = $this->input->post('city_region');
            $string_data['phone'] = $this->input->post('phone');
            $string_data['email'] = $this->input->post('email');
            $string_data['business_description'] = $this->input->post('comments');
            $string_data['created_date'] = date('Y-m-d H:i:s');
            $string_data['is_temp'] = '1';

            $result = $this->Coaching_form_model->add_coaching_form($string_data);

            if ($result) {
                $template_data = array();
                $template_data['client_name'] = $string_data['first_name'] . ' ' . $string_data['last_name'];
                $template_data['create_date'] = $string_data['created_date'];
                $template_data['super_admin'] = $this->single_user->first_name . ' ' . $this->single_user->last_name;
                $this->send_email_to_super_admin($template_data, '1');
                $this->session->set_flashdata('success', 'success');
                redirect('register');
            } else {
                $this->session->set_flashdata('result', 'error');
                redirect('register');
            }
        }
    }

    /* ------------------------------------------------------------------------------------------------- */

    public function edit_coaching($uid) {
        if (!$this->session->userdata('uid')) {
            redirect(base_url('Auth'));
        }

        $this->form_validation->set_rules('first_name', 'First Name', 'required|trim');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required|trim');
        $this->form_validation->set_rules('city_region', 'City', 'required|trim');
        $this->form_validation->set_rules('phone', 'Phone', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim');
        $this->form_validation->set_rules('comments', 'Comments', 'required|trim');

        if ($this->form_validation->run() == FALSE) {

            $data['form_info'] = $this->Coaching_form_model->get_coaching_form($uid);
            $data['currentPage'] = "form_coaching";
            $data['mainContent'] = "coaching_form/edit";
            $this->load->view('includes/frame', $data);
        } else {

            $string_data['first_name'] = $this->input->post('first_name');
            $string_data['last_name'] = $this->input->post('last_name');
            $string_data['city_region'] = $this->input->post('city_region');
            $string_data['phone'] = $this->input->post('phone');
            $string_data['email'] = $this->input->post('email');
            $string_data['business_description'] = $this->input->post('comments');
            $string_data['created_date'] = date('Y-m-d H:i:s');

            $result = $this->Coaching_form_model->update_coaching_form($string_data, $uid);

            if ($result) {
                $this->session->set_flashdata('submit', 'success');
                redirect('coaching_form');
            } else {
                $this->session->set_flashdata('error', 'error');
                redirect('coaching_form');
            }
        }
    }

    /* ------------------------------------------------------------------------------------------------- */

    public function print_coaching($uid) {
        if (!$this->session->userdata('uid')) {
            redirect(base_url('Auth'));
        }

        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('MWDI');
        $pdf->SetFont('helvetica', '', 10);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->setJPEGQuality(100);
        $pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.1, 'depth_h' => 0, 'color' => array(255, 255, 255), 'opacity' => 1, 'blend_mode' => 'Normal'));
        $pdf->AddPage();

        $data['form_details'] = $this->Coaching_form_model->get_coaching_form($uid);
        $html = $this->load->view('coaching_form/print', $data, TRUE);
        $pdf->SetTitle('Coaching Enquire Form');

        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->lastPage();
        $pdf->Output('example_007.pdf', 'I');
    }

    /* ------------------------------------------------------------------------------------------------- */

    public function print_coaching_full_form() {
        if (!$this->session->userdata('uid')) {
            redirect(base_url('Auth'));
        }

        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);

        $pdf->footer_align = 190;
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('MWDI');
        $pdf->SetFont('helvetica', '', 10);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->setJPEGQuality(100);
        $pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.1, 'depth_h' => 0, 'color' => array(255, 255, 255), 'opacity' => 1, 'blend_mode' => 'Normal'));
        //$pdf->AddPage('L', 'A4');
        $pdf->AddPage('L', 'A4');


        $data['templist'] = $this->Coaching_form_model->get_coaching_list();
        $html = $this->load->view('coaching_form/print_list', $data, TRUE);
        $pdf->SetTitle('Coaching Enquire Formsss');

        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->lastPage();
        $pdf->Output('Coaching_Enquire_Form_'.date("ymdhis").'.pdf', 'I');
    }

    /* ------------------------------------------------------------------------------------------------- */

    public function mark_as_answered($uid) {
        if (!$this->session->userdata('uid')) {
            redirect(base_url('Auth'));
        }

        if ($this->Coaching_form_model->mark_as_answered($uid, $this->session->userdata('uid')))
            $response = 'success';
        else
            $response = 'error';
        echo json_encode($response);
    }

    /* ------------------------------------------------------------------------------------------------- */

    public function become_coach($uid) {
        if (!$this->session->userdata('uid')) {
            redirect(base_url('Auth'));
        }

        if ($this->Coaching_form_model->become_coach($uid, $this->session->userdata('uid'))) { 
            $arr = $this->Coaching_form_model->get_data_send_email($uid);
            $template_data = array();
            $template_data['client_name'] = $arr->client_first_name . ' ' . $arr->client_last_name;
            $template_data['coach_name'] = $arr->coach_first_name . ' ' . $arr->coach_last_name;
            $template_data['super_admin'] = $this->single_user->first_name . ' ' . $this->single_user->last_name;
            $rtn = $this->send_email_to_super_admin($template_data, '2');
            $response = 'success';
        } else {
            $response = 'error';
        }
        echo json_encode($response);
    }

    /* ------------------------------------------------------------------------------------------------- */

    public function send_email_to_super_admin($template_arr, $case) {
        if (!$this->session->userdata('uid')) {
            redirect(base_url('Auth'));
        }
        
        $single_user = $this->System_user_model->get_single_user('1', '1');
        $email_data['to'] = $single_user->user_email;
        switch ($case) {
            case '1':$email_data['subject'] = 'Coaching enquire form has been Submitted';
                $email_data['msg'] = $this->load->view('email_templates/temporaray_client_notification', $template_arr, TRUE);
                break;
            case '2':$email_data['subject'] = 'Coach has been started';
                $email_data['msg'] = $this->load->view('email_templates/become_coach_notification', $template_arr, TRUE);
                break;
        }

//        $template_data['client_name'] = $client_name;
//        $template_data['create_date'] = $create_date;
//        $template_data['super_admin'] = $single_user->first_name . ' ' . $single_user->last_name;
//        $email_data['msg'] = $this->load->view('email_templates/temporaray_client_notification', $template_arr, TRUE);
        $email_send_rslt = send_emails($email_data);
        return $email_send_rslt;
    }

}
