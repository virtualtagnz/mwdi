<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class BAForm extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if (!$this->session->userdata('user_level_id')) {
            redirect(base_url('Auth'));
        }

        $this->is_admin = (in_array($this->session->userdata('role'), array(1, 2)) ? true : false);
//        $this->load->model('Clients_model');
//        $this->load->model('BAForm_model');
//        $this->load->library('Pdf');
    }

    public function index() {
        redirect('baform/submitted');
    }

    public function submittedForms() {
        redirect('baform/list');
    }

    public function pdf($ba_uid = false) {
        if ($ba_uid) {
            $ba_uid = $this->encrypt->decode($ba_uid);

            $main_form = $this->BAForm_model->fetch($ba_uid);
            $education_details = $this->BAForm_model->fetch_education_details($ba_uid);
            $kin_details = $this->BAForm_model->fetch_kin_details($ba_uid);
            $employment_details = $this->BAForm_model->fetch_employment_details($ba_uid);
            $testimonials = $this->BAForm_model->fetch_testimonials($ba_uid);
            $business_details = $this->BAForm_model->fetch_business_details($ba_uid);
            $bank_information = $this->BAForm_model->fetch_bank_information($ba_uid);

            if ($main_form || $education_details || $kin_details || $employment_details || $testimonials || $business_details || $bank_information) {
                //create pdf
                $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
                $pdf->SetCreator(PDF_CREATOR);
                $pdf->SetAuthor('MWDI');
                $pdf->SetFont('helvetica', '', 10);

                // set margins
                $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
                $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
                $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

                // set auto page breaks
                $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
                $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
                $pdf->setJPEGQuality(100);
                $pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.1, 'depth_h' => 0, 'color' => array(255, 255, 255), 'opacity' => 1, 'blend_mode' => 'Normal'));
                if ($main_form) {
                    $pdf->AddPage();
                    $data['details'] = json_encode($main_form);
                    $html = $this->load->view('baform/BA_step1_view', $data, TRUE);
                    $pdf->SetTitle('KIA ORA , TELL US ABOUT YOURSELF');
                    $pdf->writeHTML($html, true, false, true, false, '');
                }
                if ($education_details) {
                    $pdf->AddPage();
                    $data['details'] = json_encode($education_details);
                    $html = $this->load->view('baform/BA_step2_view', $data, TRUE);
                    $pdf->SetTitle('Education Details');
                    $pdf->writeHTML($html, true, false, true, false, '');
                }
                if ($kin_details) {
                    $pdf->AddPage();
                    $data['details'] = json_encode($kin_details);
                    $html = $this->load->view('baform/BA_step3_view', $data, TRUE);
                    $pdf->SetTitle('Next of Kin Details');
                    $pdf->writeHTML($html, true, false, true, false, '');
                }
                if ($employment_details && $testimonials) {
                    $pdf->AddPage();
                    $data['details'] = json_encode($employment_details);
                    $data['testimonials'] = json_encode($testimonials);
                    $html = $this->load->view('baform/BA_step4_view', $data, TRUE);
                    $pdf->SetTitle('Employment Details');
                    $pdf->writeHTML($html, true, false, true, false, '');
                }
                if ($business_details) {
                    $pdf->AddPage();
                    $data['details'] = json_encode($business_details);
                    $html = $this->load->view('baform/BA_step5_view', $data, TRUE);
                    $pdf->SetTitle('Business Details');
                    $pdf->writeHTML($html, true, false, true, false, '');
                }
                if ($bank_information) {
                    $pdf->AddPage();
                    $data['details'] = json_encode($bank_information);
                    $html = $this->load->view('baform/BA_step6_view', $data, TRUE);
                    $pdf->SetTitle('Bank Information');
                    $pdf->writeHTML($html, true, false, true, false, '');
                }
                $pdf->lastPage();
                $pdf->Output('business_application.pdf', 'I');
            } else {
                $this->session->set_flashdata('pdf_error', 'error');
                redirect('baform/step1/' . $this->encrypt->encode($ba_uid), 'refresh');
            }
        }
    }

    public function check($action = false, $user_id = false) {
        if ($action && $user_id) {
            $baform = $this->BAForm_model->fetch(false, $user_id);

            if ($baform) {
                $ba_uid = $baform->ba_id;
            } else {
                $increment_id = $this->BAForm_model->get_max_main_form();
                $string_data['ba_id'] = $increment_id;
                $string_data['ba_client_ref'] = $user_id;
                $baform = $this->BAForm_model->save_main_form($string_data, false);
                $baform = $increment_id;

                if (!$baform || $baform == "0") {
                    $this->check($action, $user_id);
                } else {
                    $ba_uid = $baform;
                }
            }

            $ba_uid_encrypt = $this->encrypt->encode($ba_uid);
            switch ($action) {
                case 'edit':
                    redirect('baform/step1/' . $ba_uid_encrypt);
                    break;

                case 'pdf':
                    redirect('baform/pdf/' . $ba_uid_encrypt);
                    break;
            }
        } else {
            redirect('Dashboard');
        }
    }

    public function listBAForms() {
//        $clients = $this->Clients_model->fetch_clients();
//        $data['is_admin'] = $this->is_admin;
//        $data['clients'] = $clients;
        $data['ba_submitted'] = json_encode($this->BAForm_model->get_all_submitted_BA_form());
        $data['currentPage'] = 'baform';
        $data['mainContent'] = "baform/list";
        $this->load->view('includes/frame', $data);
    }

    public function step1() {
        $clients = false;
        if ($this->is_admin) {
            $clients = $this->Clients_model->fetch_clients();
            $client_id = false;
            $ba_uid = $this->encrypt->decode(end($this->uri->segments));
        } else {
            $client_id = $this->session->userdata('user_level_id');
            $ba_uid = false;
        }

        $baform = $this->BAForm_model->fetch($ba_uid, $client_id);

        if (!$this->is_admin) {
            if ($baform) {
                if ($baform->ba_client_ref == $this->session->userdata('user_level_id') && $baform->sub_status == 'Y') {
                    //redirect('baform/view/' . $this->encrypt->encode($baform->ba_id));
                    redirect('baform/pdf/' . $this->encrypt->encode($baform->ba_id));
                }
            }
        }

        $data['baform'] = $baform;
//        $data['clients'] = $clients;
        $data['is_admin'] = $this->is_admin;
        $data['currentPage'] = 'baform';
        $data['mainContent'] = "baform/step1";
        $this->load->view('includes/frame', $data);
    }

    public function step2() {

        $ba_uid_encrypt = end($this->uri->segments);
        $ba_uid = $this->encrypt->decode($ba_uid_encrypt);
        $baform = $this->BAForm_model->fetch($ba_uid);
        $education_details = $this->BAForm_model->fetch_education_details($ba_uid);

        if (!$baform) {
            redirect('baform/step1/');
        }

        $data['education_details'] = $education_details;
        $data['is_admin'] = $this->is_admin;
        $data['ba_uid_encrypt'] = $ba_uid_encrypt;
        $data['ba_uid'] = $ba_uid;
        $data['currentPage'] = 'baform';
        $data['mainContent'] = "baform/step2";
        $this->load->view('includes/frame', $data);
    }

    public function step3() {
        $ba_uid_encrypt = end($this->uri->segments);
        $ba_uid = $this->encrypt->decode($ba_uid_encrypt);
        $baform = $this->BAForm_model->fetch($ba_uid);
        $kin_details = $this->BAForm_model->fetch_kin_details($ba_uid);

        if (!$baform) {
            redirect('baform/step1/');
        }

        $data['is_admin'] = $this->is_admin;
        $data['kin_details'] = $kin_details;
        $data['ba_uid_encrypt'] = $ba_uid_encrypt;
        $data['ba_uid'] = $ba_uid;
        $data['currentPage'] = 'baform';
        $data['mainContent'] = "baform/step3";
        $this->load->view('includes/frame', $data);
    }

    public function step4() {
        $ba_uid_encrypt = end($this->uri->segments);
        $ba_uid = $this->encrypt->decode($ba_uid_encrypt);
        $baform = $this->BAForm_model->fetch($ba_uid);
        $employment_details = $this->BAForm_model->fetch_employment_details($ba_uid);
        $testimonials = $this->BAForm_model->fetch_testimonials($ba_uid);

        if (!$baform) {
            redirect('baform/step1/');
        }

        $data['is_admin'] = $this->is_admin;
        $data['employment_details'] = $employment_details;
        $data['testimonials'] = $testimonials;
        $data['ba_uid_encrypt'] = $ba_uid_encrypt;
        $data['ba_uid'] = $ba_uid;
        $data['currentPage'] = 'baform';
        $data['mainContent'] = "baform/step4";
        $this->load->view('includes/frame', $data);
    }

    public function step5() {
        $ba_uid_encrypt = end($this->uri->segments);
        $ba_uid = $this->encrypt->decode($ba_uid_encrypt);
        $baform = $this->BAForm_model->fetch($ba_uid);
        $business_details = $this->BAForm_model->fetch_business_details($ba_uid);

        if (!$baform) {
            redirect('baform/step1/');
        }

        $data['is_admin'] = $this->is_admin;
        $data['business_details'] = $business_details;
        $data['ba_uid_encrypt'] = $ba_uid_encrypt;
        $data['ba_uid'] = $ba_uid;
        $data['currentPage'] = 'baform';
        $data['mainContent'] = "baform/step5";
        $this->load->view('includes/frame', $data);
    }

    public function step6() {
        $ba_uid_encrypt = end($this->uri->segments);
        $ba_uid = $this->encrypt->decode($ba_uid_encrypt);
        $baform = $this->BAForm_model->fetch($ba_uid);
        $bank_information = $this->BAForm_model->fetch_bank_information($ba_uid);

        if (!$baform) {
            redirect('baform/step1/');
        }

        $data['is_admin'] = $this->is_admin;
        $data['ba_uid_encrypt'] = $ba_uid_encrypt;
        $data['ba_uid'] = $ba_uid;
        $data['bank_information'] = $bank_information;
        $data['currentPage'] = 'baform';
        $data['mainContent'] = "baform/step6";
        $this->load->view('includes/frame', $data);
    }

    public function ajaxRequest($action) {
        $json = array('status' => false);
        switch ($action) {
            case 'client':
                $id = $this->input->post('id');
                $baform = $this->BAForm_model->fetch(false, $id);

                if ($baform) {
                    $json = array('status' => true, 'location' => base_url('baform/step1/' . $this->encrypt->encode($baform->ba_id)));
                } else {
                    $json = array('status' => false);
                }
                break;

            case 'education_details':
                switch ($this->input->post('action')) {
                    case 'add':
                        $string['ba_ref_id'] = $this->input->post('ba_uid');
                        $string['edu_place'] = $this->input->post('educate_at');
                        $string['edu_qua'] = $this->input->post('qualifications');
                        $string['edu_yr'] = $this->input->post('year_obtained');
                        $string['start_date'] = $this->input->post('timer_started');
                        $string['end_date'] = date("Y-m-d H:i:s");
                        $string['filled_by'] = $this->session->userdata('user_name');

                        $education_details = $this->BAForm_model->add_education_details($string);
                        if ($education_details) {
                            $json = array('status' => true, 'uid' => $education_details);
                        } else {
                            $json = array('status' => false);
                        }
                        break;
                    case 'delete':
                        $uid = $this->input->post('ba_uid');
                        $json = array('status' => false);

                        if ($uid) {
                            if ($this->BAForm_model->delete_education_details($uid)) {
                                $json = array('status' => true);
                            }
                        }

                        break;
                }

                break;

            case 'employment_details':
                switch ($this->input->post('action')) {
                    case 'add':
                        $string['emp_main_ref'] = $this->input->post('ba_uid');
                        $string['emp_name'] = $this->input->post('emp_name');
                        $string['wrk'] = $this->input->post('wrk');
                        $string['long_wrk'] = $this->input->post('long_wrk');
                        $string['start_date'] = $this->input->post('timer_started');
                        $string['end_date'] = date("Y-m-d H:i:s");
                        $string['filled_by'] = $this->session->userdata('user_name');

                        $education_details = $this->BAForm_model->add_employment_details($string);
                        if ($education_details) {
                            $json = array('status' => true, 'uid' => $education_details);
                        } else {
                            $json = array('status' => false);
                        }
                        break;
                    case 'delete':
                        $uid = $this->input->post('ba_uid');
                        $json = array('status' => false);

                        if ($uid) {
                            if ($this->BAForm_model->delete_employment_details($uid)) {
                                $json = array('status' => true);
                            }
                        }

                        break;
                }

                break;
        }

        echo json_encode($json);
    }

    public function saveForm($step = false) {
        switch ($step) {
            case 'step1':
                $string['ba_client_ref'] = ($this->is_admin ? $this->input->post('client_id') : $this->session->userdata('user_level_id'));
                $string['bac_title'] = $this->input->post('tit');
                $string['bac_first_name'] = $this->input->post('first_name');
                $string['bac_last_name'] = $this->input->post('last_name');
                $string['bac_home_add'] = $this->input->post('home_add');
                $string['bac_bus_add'] = $this->input->post('bus_add');
                $string['bac_home_phn'] = $this->input->post('hphone');
                $string['bac_home_fax'] = $this->input->post('hfax');
                $string['bac_home_mob'] = $this->input->post('hmob');
                $string['bac_home_email'] = $this->input->post('hemail');
                $string['bac_bus_phn'] = $this->input->post('bphone');
                $string['bac_bus_fax'] = $this->input->post('bfax');
                $string['bac_bus_mob'] = $this->input->post('bmob');
                $string['bac_bus_email'] = $this->input->post('bemail');
                $string['bac_gender'] = $this->input->post('gender');
                $string['bac_dob'] = $this->input->post('dob');
                $string['bac_mat_stat'] = $this->input->post('marry');
                $string['bac_no_dep'] = $this->input->post('dep');
                $string['bac_ages'] = $this->input->post('age');
                $string['bac_accom'] = $this->input->post('accommodation');
                $string['stud_bus'] = $this->input->post('bus_ed');
                $string['bac_whanau_name'] = $this->input->post('w_name');
                $string['bac_ta'] = $this->input->post('tribal');
                $string['bac_start_date'] = $this->input->post('timer_started');
                $string['bac_end_date'] = $this->input->post('timer_end') ? $this->input->post('timer_end') : date("Y-m-d H:i:s");
                $string['bac_filled_by'] = $this->session->userdata('user_name');
                $string['sub_status'] = $this->input->post('sub_stat') ? $this->input->post('sub_stat') : 'N';
                $string['form_type'] = '1';

                $ba_uid = ($this->input->post('ba_uid') ? $this->input->post('ba_uid') : false);
                $baform_save = $this->BAForm_model->save_main_form($string, $ba_uid);

                if ($baform_save) {

                    $ba_uid_encrypt = $this->encrypt->encode($baform_save);

                    if ($this->input->post('save_form') == '1') {
                        $this->session->set_flashdata('success', 'The form has been successfully saved!');
                        redirect('baform/step1/' . $ba_uid_encrypt);
                    }

                    if ($this->input->post('next_form') == '1') {
                        redirect('baform/step2/' . $ba_uid_encrypt);
                    }
                } else {
                    $this->session->set_flashdata('warning', 'An application error occurred on the server, please try again!');
                    redirect('baform/submitted');
                }

                return;

                break;

            case 'step2':
                $baform = $this->BAForm_model->fetch($this->input->post('ba_uid'));
                if ($baform) {
                    $ba_uid_encrypt = $this->encrypt->encode($this->input->post('ba_uid'));
                    if ($this->input->post('save_form') == '1') {
                        $this->session->set_flashdata('success', 'The form has been successfully saved!');
                        redirect('baform/step2/' . $ba_uid_encrypt);
                    }

                    if ($this->input->post('next_form') == '1') {
                        redirect('baform/step3/' . $ba_uid_encrypt);
                    }
                } else {
                    $this->session->set_flashdata('warning', 'An application error occurred on the server, please try again!');
                    redirect('baform/submitted');
                }

                return;
                break;

            case 'step3':
                $kin_details = $this->BAForm_model->fetch_kin_details($this->input->post('ba_uid'));

                $string['nk1_name'] = $this->input->post('nk1_name');
                $string['nk1_rel'] = $this->input->post('nk1_rel');
                $string['nk1_hm_phn'] = $this->input->post('nk1_hm_phn');
                $string['nk1_wk_phn'] = $this->input->post('nk1_wk_phn');
                $string['nk1_mob'] = $this->input->post('nk1_mob');
                $string['nk1_email'] = $this->input->post('nk1_email');

                $string['nk2_name'] = $this->input->post('nk2_name');
                $string['nk2_rel'] = $this->input->post('nk2_rel');
                $string['nk2_hm_phn'] = $this->input->post('nk2_hm_phn');
                $string['nk2_wk_phn'] = $this->input->post('nk2_wk_phn');
                $string['nk2_mob'] = $this->input->post('nk2_mob');
                $string['nk2_email'] = $this->input->post('nk2_email');
                $string['ba_main_ref'] = $this->input->post('ba_uid');

                $string['start_date'] = $this->input->post('timer_started');
                $string['end_date'] = $this->input->post('timer_end') ? $this->input->post('timer_end') : date("Y-m-d H:i:s");
                $string['filled_by'] = $this->session->userdata('user_name');

                $save_kin_details = $this->BAForm_model->save_kin_details($string, ($kin_details ? $kin_details->nk_id : false), $this->input->post('ba_uid'));
                if ($save_kin_details) {
                    $ba_uid_encrypt = $this->encrypt->encode($this->input->post('ba_uid'));
                    if ($this->input->post('save_form') == '1') {
                        $this->session->set_flashdata('success', 'The form has been successfully saved!');
                        redirect('baform/step3/' . $ba_uid_encrypt);
                    }

                    if ($this->input->post('next_form') == '1') {
                        redirect('baform/step4/' . $ba_uid_encrypt);
                    }
                } else {
                    $this->session->set_flashdata('warning', 'An application error occurred on the server, please try again!');
                    redirect('baform/submitted');
                }

                return;
                break;

            case 'step4':

                $testimonials = $this->BAForm_model->fetch_testimonials($this->input->post('ba_uid'));
                $string['tes_name1'] = $this->input->post('tes_name1');
                $string['tes_phn1'] = $this->input->post('tes_phn1');
                $string['tes_add1'] = $this->input->post('tes_add1');
                $string['tes_name2'] = $this->input->post('tes_name2');
                $string['tes_phn2'] = $this->input->post('tes_phn2');
                $string['tes_add2'] = $this->input->post('tes_add2');
                $string['res_main_ref'] = $this->input->post('ba_uid');

                $testimonials = $this->BAForm_model->save_testimonials($string, ($testimonials ? $testimonials->tes_no : false), $this->input->post('ba_uid'));

                if ($testimonials) {
                    $ba_uid_encrypt = $this->encrypt->encode($this->input->post('ba_uid'));
                    if ($this->input->post('save_form') == '1') {
                        $this->session->set_flashdata('success', 'The form has been successfully saved!');
                        redirect('baform/step4/' . $ba_uid_encrypt);
                    }

                    if ($this->input->post('next_form') == '1') {
                        redirect('baform/step5/' . $ba_uid_encrypt);
                    }
                } else {
                    $this->session->set_flashdata('warning', 'An application error occurred on the server, please try again!');
                    redirect('baform/submitted');
                }

                return;
                break;

            case 'step5':
                $business_details = $this->BAForm_model->fetch_business_details($this->input->post('ba_uid'));
                $string['biz'] = $this->input->post('biz');
                $string['biz_period'] = $this->input->post('biz_period');
                $string['biz_bef'] = $this->input->post('biz_bef');
                $string['biz_trading'] = $this->input->post('biz_trading');
                $string['no_positions'] = $this->input->post('no_epm');
                $string['full_time'] = $this->input->post('ft');
                $string['part_time'] = $this->input->post('pt');
                $string['abt_mdwi'] = $this->input->post('find');
                $string['pre_apply'] = $this->input->post('pre');
                $string['pre_outcome'] = $this->input->post('preyes');
                $string['sec_list'] = $this->input->post('seclist');
                $string['biz_main_ref'] = $this->input->post('ba_uid');


                $file = upload_files($_SERVER['DOCUMENT_ROOT'] . '/uploads/business_application/', 'biz_doc', 'files');
                if ($file['stat']) {
                    $string['biz_link'] = $file['fileArr']['file_name'];
                }

                $string['start_date'] = $this->input->post('timer_started');
                $string['end_date'] = $this->input->post('timer_end') ? $this->input->post('timer_end') : date("Y-m-d H:i:s");
                $string['filled_by'] = $this->session->userdata('user_name');

                $business_details = $this->BAForm_model->save_business_details($string, ($business_details ? $business_details->biz_id : false), $this->input->post('ba_uid'));
                if ($business_details) {
                    $ba_uid_encrypt = $this->encrypt->encode($this->input->post('ba_uid'));
                    if ($this->input->post('save_form') == '1') {
                        $this->session->set_flashdata('success', 'The form has been successfully saved!');
                        redirect('baform/step5/' . $ba_uid_encrypt);
                    }

                    if ($this->input->post('next_form') == '1') {
                        redirect('baform/step6/' . $ba_uid_encrypt);
                    }
                } else {
                    $this->session->set_flashdata('warning', 'An application error occurred on the server, please try again!');
                    redirect('baform/submitted');
                }

                return;
                break;

            case 'step6':
                $bank_information = $this->BAForm_model->fetch_bank_information($this->input->post('ba_uid'));

                $string['hv_bk'] = $this->input->post('hv_bk');
                $string['bk_no'] = $this->input->post('hv_bk') == '1' ? $this->input->post('bkno') : NULL;
                $string['bk_name'] = $this->input->post('bk_name');
                $string['bk_branch'] = $this->input->post('branch');
                $string['bk_tel'] = $this->input->post('tel');

                $string['acc_name'] = $this->input->post('acc_name');
                $string['acc_add'] = $this->input->post('acc_add');
                $string['acc_tel'] = $this->input->post('acc_tel');

                $string['lyr_name'] = $this->input->post('low_name');
                $string['lyr_add'] = $this->input->post('low_add');
                $string['lyr_tel'] = $this->input->post('low_tel');


                $string['try_bk_loan'] = $this->input->post('hv_loan');
                $string['try_other_ins'] = $this->input->post('ot_loan');
                if ($this->input->post('dec28'))
                    $string['try_ops'] = implode(",", $this->input->post('dec28'));
                $string['try_result'] = $this->input->post('otyes');
                $string['ins_names'] = $this->input->post('ot_name');
                $string['ins_tels'] = $this->input->post('ot_tel');

                $string['loan_purps'] = $this->input->post('purpose');
                $string['cop'] = $this->input->post('cop');
                $string['own_cont'] = $this->input->post('loc');
                $string['loan_amt_req'] = $this->input->post('lr');
                $string['hv_mentor'] = $this->input->post('hvment');
                $string['ment_name'] = $this->input->post('ment_name');
                $string['ment_tel'] = $this->input->post('ment_tel');

                $string['owe_amt'] = $this->input->post('z1');
                $string['other_owe'] = $this->input->post('z6');
                $string['owe_terms'] = $this->input->post('z2');
                $string['owe_yrs'] = $this->input->post('z3');
                $string['owe_int_rate'] = $this->input->post('z4');
                $string['lender_name'] = $this->input->post('z5');

                $string['ast_chq_amt'] = $this->input->post('x1');
                $string['other_bk_acnt'] = $this->input->post('x2');
                $string['life_ins'] = $this->input->post('x3');
                $string['prem_paid'] = $this->input->post('x4');
                $string['go_stck'] = $this->input->post('x5');
                $string['house_land'] = $this->input->post('x6');
                $string['othr_propty'] = $this->input->post('x7');
                $string['mot_val'] = $this->input->post('x8');
                $string['furniture_val'] = $this->input->post('x9');
                $string['othr_asts'] = $this->input->post('x10');

                $string['start_date'] = $this->input->post('timer_started');
                $string['end_date'] = $this->input->post('timer_end') ? $this->input->post('timer_end') : date("Y-m-d H:i:s");
                $string['bk_main_ref'] = $this->input->post('ba_uid');
                $string['filled_by'] = $this->session->userdata('user_name');

//                var_dump($string);
//                die();

                $save_bank_information = $this->BAForm_model->save_bank_information($string, ($bank_information ? $bank_information->bk_id : false), $this->input->post('ba_uid'), ($this->input->post('next_form') ? true : false));
//                                var_dump($this->input->post('submit_val'));
//                                die();
                if ($save_bank_information) {
                    $ba_uid_encrypt = $this->encrypt->encode($this->input->post('ba_uid'));
                    if ($this->input->post('save_form') == '1') {
                        $this->session->set_flashdata('create_success', 'The form has been successfully saved!');
                        redirect('baform/step6/' . $ba_uid_encrypt);
                    }

                    if ($this->input->post('submit_val') == '1') {

                        $admins = $coach = false;
                        //$admins = $this->BAForm_model->get_all_admins_notifications();
                        //$coach = $this->BAForm_model->get_coach_baform($this->input->post('ba_uid'));
                        $super_admins = $this->BAForm_model->get_all_super_admins_notifications();
                        $accounts = $this->BAForm_model->get_all_accounts_notifications();
                        $client = $this->BAForm_model->get_client_baform($this->input->post('ba_uid'));

                        send_email_confirmation_baform($super_admins, $admins, $accounts, $coach, $client);

                        $string_data['sub_status'] = 'Y';
                        $string_data['date_submit'] = date("Y-m-d H:i:s");

                        $this->BAForm_model->save_main_form($string_data, $this->input->post('ba_uid'));
                        create_notification('6', '1', 'Submit BA Form by ' . $this->session->userdata('user_name'));
                        add_activities($this->session->userdata('role'), $this->session->userdata('user_level_id'), 'Submit BA Form');

                        $this->session->set_flashdata('submit_success', 'The form has been successfully submitted!');
                        //redirect('baform/view/'.$ba_uid_encrypt);
                        redirect('baform/step6/' . $ba_uid_encrypt);
                    }
                } else {
                    $this->session->set_flashdata('warning', 'An application error occurred on the server, please try again!');
                    redirect('baform/submitted');
                }

                break;
        }
    }

}

/* End of file bAForm.php */
/* Location: ./application/controllers/bAForm.php */