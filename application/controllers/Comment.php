<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Comment extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('uid')) {
            redirect(base_url('Auth'));
        }
    }
    
    //--------------------------------------------------------------------------
    
    public function index() {
        
    }
    
    //--------------------------------------------------------------------------
    
    public function add_comment($user_type,$user_id) {
        $data = array();
        $data['comment'] = $this->input->post('cmnt');
        $data['client_ref'] = $this->encrypt->decode($this->input->post('client_ref'));
        $data['user_ref'] = $this->session->userdata('user_level_id');
        $data['date_create'] = date('Y-m-d H:i:s');
        $data['role_create'] = $this->session->userdata('user_name');
        $data['code_create'] = $this->session->userdata('role');
        $data['is_visible'] = '1';
        $result = $this->Comment_model->insert_comments($data);
        if($result){
            $this->session->set_flashdata('success_msg', 'success');
            redirect(base_url('view_profile/'.$user_type.'/'.$user_id));
        }else{
           $this->session->set_flashdata('error_msg', 'error');
           redirect(base_url('view_profile/'.$user_type.'/'.$user_id));
        }   
    }
}

