<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Resources extends CI_Controller {

    public $currentPage;
    public $file_name;

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('uid')) {
            redirect(base_url('Auth'));
        }
        $this->currentPage = 'resources';
    }

    public function index() {
        $data['all_resources'] = $this->Resource_model->get_all_resources();
        $data['currentPage'] = $this->currentPage;
        $data['mainContent'] = "resources/view_resource_list";
        $this->load->view('includes/frame', $data);
    }

    public function create_resource_view() {
        $data['currentPage'] = $this->currentPage;
        $data['mainContent'] = "resources/create_resources";
        $this->load->view('includes/frame', $data);
    }

    public function edit_resource_view($res_id) {
        $data['single_resource'] = $this->Resource_model->get_single_resource($this->encrypt->decode($res_id));
        $data['currentPage'] = $this->currentPage;
        $data['mainContent'] = "resources/edit_resources";
        $this->load->view('includes/frame', $data);
    }

    public function create() {
        $is_valid = $this->form_validation(FALSE);
        if ($is_valid) {
            $valid_data = $this->get_form_data(FALSE);
            $rtn = $this->Resource_model->insert($valid_data);
            echo($rtn) ? $this->session->set_flashdata('create_success', 'success') : $this->session->set_flashdata('error_msg', 'error');
            redirect(base_url('go_edit_resource/' . $this->encrypt->encode($rtn)));
        } else {
            $this->create_resource_view();
        }
    }
    
    public function edit($res_id) {
        $is_valid = $this->form_validation(TRUE);
        if ($is_valid) {
            $valid_data = $this->get_form_data(TRUE);
            $rtn = $this->Resource_model->update($this->encrypt->decode($res_id),$valid_data);
            echo($rtn) ? $this->session->set_flashdata('update_success', 'success') : $this->session->set_flashdata('error_msg', 'error');
            redirect(base_url('go_edit_resource/' . $res_id));
        } else {
            $this->create_resource_view();
        }
    }

    public function form_validation($is_update) {
        $this->form_validation->set_rules('res_name', 'Resource Name', 'trim|required');
        $this->form_validation->set_rules('des', 'Resource Description', 'trim|required');
        if ($is_update) {
            if (!empty($_FILES['res_file']['name'])) {
                $this->form_validation->set_rules('res_file', 'File', 'trim|callback_file_check');
            } else {
                $this->file_name = $this->input->post('current_file');
            }
        } else {
            $this->form_validation->set_rules('res_file', 'File', 'trim|required');
            if (!empty($_FILES['res_file']['name'])) {
                $this->form_validation->set_rules('res_file', 'File', 'trim|callback_file_check');
            }
        }
        return $this->form_validation->run();
    }

    public function get_form_data($is_update) {
        $data = array();
        $data['res_name'] = $this->input->post('res_name');
        $data['res_des'] = $this->input->post('des');
        $data['res_linq'] = $this->file_name;
        if ($is_update) {
            $data['date_mod'] = date('Y-m-d H:i:s');
            $data['code_mod'] = $this->session->userdata('uid');
            $data['role_mod'] = $this->session->userdata('role');
        } else {
            $data['date_create'] = date('Y-m-d H:i:s');
            $data['create_by'] = $this->session->userdata('uid');
            $data['create_role'] = $this->session->userdata('role');
        }

        return $data;
    }

    public function file_check() {
        $path = realpath((APPPATH . '../public_html/uploads/resources'));
        $image_upload_rslt = upload_files($path, 'res_file', 'common');
        if ($image_upload_rslt['stat']) {
            $this->file_name = $image_upload_rslt['fileArr']['file_name'];
            return TRUE;
        } else {
            $this->form_validation->set_message('file_check', $image_upload_rslt['fileError']);
            return FALSE;
        }
    }
    
}
