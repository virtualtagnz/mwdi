<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Loans_management extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('uid')) {
            redirect(base_url('Auth'));
        }
    }

    //--------------------------------------------------------------------------

    public function index() {
        $data['clients'] = json_encode($this->Loans_management_model->get_approval_list());
        $data['currentPage'] = "loans_management";
        $data['mainContent'] = "loan/loan_approval_list";
        $this->load->view('includes/frame', $data);
    }

    //--------------------------------------------------------------------------

    public function loan_approval_view($client_id) {
        $data['client_info'] = $this->System_user_model->get_single_user_by_user_level_id('5', $this->encrypt->decode($client_id));
        $data['currentPage'] = "loans_management";
        $data['mainContent'] = "loan/loan_approval";
        $this->load->view('includes/frame', $data);
    }

    //--------------------------------------------------------------------------

    public function loan_approve($client_id) {
        $this->form_validation->set_rules('app_stat', 'approve or reject', 'trim|required');
        if ($this->input->post('app_stat') == 1) {
            $this->form_validation->set_rules('loan_amt', 'loan amount', 'trim|required');
            $this->form_validation->set_rules('end_date', 'proposed end date for final payment', 'trim|required');
        }
        if ($this->form_validation->run()) {
            $data = array();
            $data['ln_client_ref'] = $this->encrypt->decode($client_id);
            $data['ln_amount'] = str_replace(",", "", $this->input->post('loan_amt'));
            $data['date_last_pmt'] = date('Y-m_d', (strtotime(str_replace('/', '-', $this->input->post('end_date')))));
            $data['approved_stat'] = $this->input->post('app_stat') == '1' ? 'Y' : 'N';
            $data['ln_description'] = $this->input->post('app_des');
            $data['approved_date'] = date('Y-m-d H:i:s');
            $data['approved_role'] = $this->session->userdata('role');
            $data['approved_by'] = $this->session->userdata('user_name');
            $result = $this->Coach_form_model->insert_loan_approve_data($data);
            if ($result) {
                $this->session->set_flashdata('success_msg', 'success');
                $this->index();
            } else {
                $this->session->set_flashdata('error_msg', 'error');
                $this->index();
            }
        } else {
            $this->loan_approval_view($client_id);
        }
    }

}
