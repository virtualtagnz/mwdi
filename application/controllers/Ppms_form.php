<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ppms_form extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('uid')) {
            redirect(base_url('Auth'));
        }
    }

    public function index() {
        $data['currentPage'] = "my_forms";
        $data['mainContent'] = "ppms_form/ppms_form";
        $this->load->view('includes/frame', $data);
    }

    public function create_download() {
        $result = $this->Ppms_form_model->get_download_count();
        $data = array();
        $data['ppms_client_ref'] = $this->session->userdata('user_level_id');
        $data['ppms_down_time'] = date('Y-m-d H:i:s');
        $data['sub_stat'] = 'N';
        $data['form_type'] = '2';
        if ($result) {
            $rtn = $this->Ppms_form_model->update_ppms($result, $data);
        } else {
            $rtn = $this->Ppms_form_model->insert_ppms($data);
        }
        echo $rtn;
    }

    public function upload_ppms_form() {
        $this->form_validation->set_rules('ppms_up', 'PPMS File', 'trim|required');
        if ($this->form_validation->run()) {
            $this->index();
        } else {
            $result = $this->Ppms_form_model->get_download_count();
            if ($result) {
                $path = realpath((APPPATH . '../public_html/uploads/ppms_forms'));
                $image_upload_rslt = upload_files($path, 'custom-file-upload', 'files');
                if ($image_upload_rslt['stat']) {
                    $data['ppms_up_time'] = date('Y-m-d H:i:s');
                    $data['sub_stat'] = 'Y';
                    $data['file_link'] = $image_upload_rslt['fileArr']['file_name'];
                    $rtn = $this->Ppms_form_model->update_ppms($result, $data);
                    if($rtn){
                        create_notification('6','1', 'Submit PPMS Form by '.$this->session->userdata('user_name'));
                        add_activities($this->session->userdata('role'), $this->session->userdata('user_level_id'), 'Submit PPMS Form');
                        $this->session->set_flashdata('upload_success', 'success');
                    }  else {
                        $this->session->set_flashdata('error_msg', 'error');
                    }
                    //echo($rtn) ? $this->session->set_flashdata('upload_success', 'success') : $this->session->set_flashdata('error_msg', 'error');
                    redirect(base_url('client_forms'));
                } else {
                    echo $this->session->set_flashdata('file_upload_error',$image_upload_rslt['fileError']);
                    $this->index();
                }
            } else {
                echo $this->session->set_flashdata('no_result_found', 'error');
                $this->index();
            }
        }
    }
    
    public function check_ppms_has_submitted() {
        $is_submit = $this->Ppms_form_model->get_submitted_ppms();
        echo $is_submit;
    }
    
    public function view_submitted_ppms_forms() {
        $data['ppms_submitted'] = json_encode($this->Ppms_form_model->get_all_submitted_ppms_forms());
        $data['currentPage'] = "form_ppms";
        $data['mainContent'] = "ppms_form/submitted_ppms_forms";
        $this->load->view('includes/frame', $data);
    }

}
