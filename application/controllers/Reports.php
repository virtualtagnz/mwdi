<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

	public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('uid')) {
            redirect(base_url('Auth'));
        }

        $this->is_admin = (in_array($this->session->userdata('role'), array(1,2))?true:false);
        $this->load->model('Reports_model');
        $this->load->library('Pdf');
    }

    /* ------------------------------------------------------------------------------------------------- */

	public function show_enquires() {

        $all_stats = $this->Reports_model->get_enquires_stats();

        $srate = 0;
        if (intval($all_stats['completed_forms']) > 0 && intval($all_stats['forms_assigned'])) $srate = (intval($all_stats['completed_forms']) / intval($all_stats['forms_assigned'])) * 100;
        $all_stats['success_rate'] = $srate;

        $data['all_stats']   = $all_stats;
        $data['enquires']    = $this->Reports_model->get_enquires_list();
        $data['currentPage'] = "report_enquires";
        $data['mainContent'] = "reports/enquires";
        $this->load->view('includes/frame', $data);	

	}

    /* ------------------------------------------------------------------------------------------------- */

    public function show_temporary_enquires() {
        $data['stat']        = $this->Reports_model->get_temp_enquires_stat();
        $data['enquires']    = $this->Reports_model->get_temp_enquires_list();
        $data['currentPage'] = "report_temp";
        $data['mainContent'] = "reports/temporary_enquires";
        $this->load->view('includes/frame', $data);     
    }

    /* ------------------------------------------------------------------------------------------------- */

    public function show_loan_applications() {
        $data['all_stats']    = $this->Reports_model->get_loan_stats();
        $data['applications'] = $this->Reports_model->get_loan_list();
        $data['currentPage']  = "report_loan";
        $data['mainContent']  = "reports/loan_applications";
        $this->load->view('includes/frame', $data);     
    }  

    /* ------------------------------------------------------------------------------------------------- */

    public function show_loan_approved_rejected() {
        $data['all_stats']   = $this->Reports_model->get_loan_ar_stats();
        $data['ar_list']     = $this->Reports_model->get_loan_ar_list();
        $data['currentPage'] = "report_approved_loans";
        $data['mainContent'] = "reports/loan_approved_rejected";
        $this->load->view('includes/frame', $data);     
    }  

    /* ------------------------------------------------------------------------------------------------- */ 
    
    public function show_jobs_created() {
        $data['all_stats']   = $this->Reports_model->get_jobs_created_stats();
        $data['jobs_list']   = $this->Reports_model->get_jobs_created_list();
        $data['currentPage'] = "report_jobs";
        $data['mainContent'] = "reports/jobs_created";
        $this->load->view('includes/frame', $data); 
    }   

    /* ------------------------------------------------------------------------------------------------- */  

    public function show_bank_declined_summary() {
        $data['all_stats']   = $this->Reports_model->get_bank_stats();
        $data['bank_list']   = $this->Reports_model->get_bank_list();
        $data['currentPage'] = "report_bank";
        $data['mainContent'] = "reports/bank_declined_summary";
        $this->load->view('includes/frame', $data);     
    }   

    /* ------------------------------------------------------------------------------------------------- */

    public function show_client_regions() {
        $data['client_region_summary'] = $this->Reports_model->get_client_region_summary();
        $data['client_region_list']    = $this->Reports_model->get_client_region_list();
        $data['currentPage'] = "report_client_regions";
        $data['mainContent'] = "reports/client_regions";
        $this->load->view('includes/frame', $data);
    } 

    /* ------------------------------------------------------------------------------------------------- */ 
     
    public function show_industry() {
        $data['industry_list'] = $this->Reports_model->get_industry_list();
        $data['currentPage']   = "report_industry";
        $data['mainContent']   = "reports/industry";
        $this->load->view('includes/frame', $data);     
    }      

    /* ------------------------------------------------------------------------------------------------- */ 
    
    public function show_clients_mark_as_start() {
        $data['coach_client_list'] = json_encode($this->Reports_model->get_coaches_start_work_on_client());
        $data['currentPage']   = "report_client_mark_as_start";
        $data['mainContent']   = "reports/coach_start_work_on_client";
        $this->load->view('includes/frame', $data);
    }
    /* ------------------------------------------------------------------------------------------------- */

    public function view_report_pdf($report) {
        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('MWDI');
        $pdf->SetFont('helvetica', '', 10);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->setJPEGQuality(100);
        $pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.1, 'depth_h' => 0, 'color' => array(255, 255, 255), 'opacity' => 1, 'blend_mode' => 'Normal'));
        $pdf->AddPage();
        $out_put_name = "report";
        
        switch ($report) {
            case 'enquires':
                $data['report_stats'] = json_encode($this->Reports_model->get_enquires_stats());
                $data['report_list']  = $this->Reports_model->get_enquires_list(); 
                $html = $this->load->view('reports/print/enquires', $data,TRUE); 
                $pdf->SetTitle('Enquires Report'); 
                $out_put_name = "enquires_report";
                break;
            case 'temporary_enquires':
                $data['report_stats'] = $this->Reports_model->get_temp_enquires_stat();
                $data['report_list']  = $this->Reports_model->get_temp_enquires_list(); 
                $html = $this->load->view('reports/print/temporary_enquires', $data,TRUE); 
                $pdf->SetTitle('Temporary Enquires Report');
                $out_put_name = "temporary_enquires_report";
                break;
            case 'loan_applications':
                $data['report_stats'] = $this->Reports_model->get_loan_stats();
                $data['report_list']  = $this->Reports_model->get_loan_list(); 
                $html = $this->load->view('reports/print/loan_applications', $data,TRUE); 
                $pdf->SetTitle('Loan Applications Report'); 
                $out_put_name = "loan_applications_report";
                break;
            case 'loan_ar':
                $data['report_stats'] = $this->Reports_model->get_loan_ar_stats();
                $data['report_list']  = $this->Reports_model->get_loan_ar_list(); 
                $html = $this->load->view('reports/print/loan_approved_rejected', $data,TRUE); 
                $pdf->SetTitle('Loan Approved / Rejected Report'); 
                $out_put_name = "loan_report";
                break;
            case 'jobs_created':
                $data['report_stats'] = $this->Reports_model->get_jobs_created_stats();
                $data['report_list']  = $this->Reports_model->get_jobs_created_list(); 
                $html = $this->load->view('reports/print/jobs_created', $data,TRUE); 
                $pdf->SetTitle('Jobs Created Report'); 
                $out_put_name = "jobs_created_report";
                break;        
            case 'bank_declined':
                $data['report_stats'] = $this->Reports_model->get_bank_stats();
                $data['report_list']  = $this->Reports_model->get_bank_list(); 
                $html = $this->load->view('reports/print/bank_declined', $data,TRUE); 
                $pdf->SetTitle('Bank Declined Report'); 
                $out_put_name = "bank_declined_report";
                break;
            case 'client_region':
                $data['report_list']  = $this->Reports_model->get_client_region_list($this->input->post('selected_category'),$this->input->post('selected_region')); 
                $html = $this->load->view('reports/print/client_regions', $data,TRUE); 
                $pdf->SetTitle('Client Regions Report'); 
                $out_put_name = "client_regions_report";
                break;
            case 'industry':
                $data['report_list']  = $this->Reports_model->get_industry_list(); 
                $html = $this->load->view('reports/print/industry', $data,TRUE); 
                $pdf->SetTitle('Industry Report');  
                $out_put_name = "industry_report";
                break;
            case 'client_mark_as_start':
                $data['coach_client_list'] = json_encode($this->Reports_model->get_coaches_start_work_on_client());
                $html = $this->load->view('reports/print/coach_start_work_on_client', $data,TRUE); 
                $pdf->SetTitle('Client - Coach engagement time');
                $out_put_name = "client_Coach_engagement_time";
                break;
        }
        
        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->lastPage();
        $pdf->Output($out_put_name.'.pdf', 'I');
    }

    /* ------------------------------------------------------------------------------------------------- */    

}