<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Security_form extends CI_Controller {

    public $client_id;
    public $main_form_id;

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('uid')) {
            redirect(base_url('Auth'));
        }
        $this->client_id = $this->encrypt->decode(end($this->uri->segments));
        $this->main_form_id = $this->Security_form_model->get_main_form_id($this->client_id);
    }

    public function index($step) {
        $data['currentPage'] = "my_forms";
        switch ($step) {
            case 'step1':
                $data['data'] = $this->Security_form_model->read_security_form_step1($this->client_id);
                if ($data['data']) {
                    $data['mainContent'] = "security_form/edit_security_form_step_1";
                } else {
                    $data['mainContent'] = "security_form/security_form_step_1";
                }
                break;
            case 'step2':
                $return_arr = $this->Security_form_model->read_security_form_step2($this->client_id);
                if ($return_arr) {
                    $data['data'] = json_encode($return_arr);
                    $data['mainContent'] = "security_form/edit_security_form_step_2";
                } else {
                    $data['mainContent'] = "security_form/security_form_step_2";
                }
                break;
            case 'step3':
                $return_arr = $this->Security_form_model->read_security_form_step3($this->client_id);
                if ($return_arr) {
                    $data['data'] = json_encode($return_arr);
                    $data['mainContent'] = "security_form/edit_security_form_step_3";
                } else {
                    $data['mainContent'] = "security_form/security_form_step_3";
                }
                break;
            case 'step4':
                $return_arr = $this->Security_form_model->read_security_form_step4($this->client_id);
                if ($return_arr) {
                    $data['data'] = json_encode($return_arr);
                    $data['mainContent'] = "security_form/edit_security_form_step_4";
                } else {
                    $data['mainContent'] = "security_form/security_form_step_4";
                }

                break;
        }
        $this->load->view('includes/frame', $data);
    }

    public function check_main_form_availablity() {
        $form_id = $this->Security_form_model->get_main_form_id($this->client_id);
        echo $form_id;
    }

    private function security_form_step1_validation() {
        $this->form_validation->set_rules('sec1_cline_name', 'Client Name', 'trim|required');
        $this->form_validation->set_rules('sec1_dob', 'Birthday', 'trim|required');
        $this->form_validation->set_rules('sec1_business_name', 'Business Name', 'trim|required');
        $this->form_validation->set_rules('sec1_address', 'Address', 'trim|required');
        $this->form_validation->set_rules('sec1_business_address', 'Business Address', 'trim|required');
        if ($this->input->post('sec1_home_tel')) {
            $this->form_validation->set_rules('sec1_home_tel', 'Home Telephone', 'numeric');
        }
        if ($this->input->post('sec1_business_tel')) {
            $this->form_validation->set_rules('sec1_business_tel', 'Business Telephone', 'numeric');
        }
        if ($this->input->post('sec1_email')) {
            $this->form_validation->set_rules('sec1_email', 'Email', 'valid_email');
        }
        return $this->form_validation->run();
    }

    private function security_form_step1_form_data($is_update) {
        $data = array();
        $path = realpath((APPPATH . '../public_html/uploads/security_forms'));
        for ($i = 1; $i < 8; $i++) {
            if (!empty($_FILES['sec1_file_' . $i]['name'])) {
                $image_upload_rslt = upload_files($path, 'sec1_file_' . $i, 'files');
                if ($image_upload_rslt['stat']) {
                    switch ($i) {
                        case '1':
                            $data['sbp_link'] = $image_upload_rslt['fileArr']['file_name'];
                            break;
                        case '2':
                            $data['spp_link'] = $image_upload_rslt['fileArr']['file_name'];
                            break;
                        case '3':
                            $data['cis_link'] = $image_upload_rslt['fileArr']['file_name'];
                            break;
                        case '4':
                            $data['cot_link'] = $image_upload_rslt['fileArr']['file_name'];
                            break;
                        case '5':
                            $data['eoe_link'] = $image_upload_rslt['fileArr']['file_name'];
                            break;
                        case '6':
                            $data['cof_link'] = $image_upload_rslt['fileArr']['file_name'];
                            break;
                        case '7':
                            $data['sec_fac'] = $image_upload_rslt['fileArr']['file_name'];
                            break;
                    }
                } else {
                    
                }
            } else {
                if ($is_update) {
                    switch ($i) {
                        case '1':
                            $data['sbp_link'] = $this->input->post('sbp_file');
                            break;
                        case '2':
                            $data['spp_link'] = $this->input->post('spp_file');
                            break;
                        case '3':
                            $data['cis_link'] = $this->input->post('cis_file');
                            break;
                        case '4':
                            $data['cot_link'] = $this->input->post('cot_file');
                            break;
                        case '5':
                            $data['eoe_link'] = $this->input->post('eoe_file');
                            break;
                        case '6':
                            $data['cof_link'] = $this->input->post('cof_file');
                            break;
                        case '7':
                            $data['sec_fac'] = $this->input->post('fac_file');
                            break;
                    }
                }
            }
        }

        if ($is_update) {
            
        } else {
            $data['start_date'] = $this->input->post('start_time');
            $data['end_date'] = date('Y-m-d H;i:s');
        }
        $data['sec_client_ref'] = $this->client_id;
        $data['sec_name'] = $this->input->post('sec1_cline_name');
        $data['sec_dob'] = $this->input->post('sec1_dob');
        $data['sec_cli_no'] = $this->input->post('sec1_client_no');
        $data['sec_biz_name'] = $this->input->post('sec1_business_name');
        $data['sec_add'] = $this->input->post('sec1_address');
        $data['sec_biz_add'] = $this->input->post('sec1_business_address');
        $data['sec_hm_tel'] = $this->input->post('sec1_home_tel');
        $data['sec_biz_tel'] = $this->input->post('sec1_business_tel');
        $data['sec_mob'] = $this->input->post('sec1_mobile');
        $data['sec_email'] = $this->input->post('sec1_email');
        $data['filled_by'] = $this->session->userdata('user_name');
        if($this->session->userdata('role') == '5'){
            $data['sub_stat'] = 'N';
        }
        $data['form_type'] = '4';
        return $data;
    }

    public function save_security_form_step1() {
        $is_valid = $this->security_form_step1_validation(FALSE);
        if ($is_valid) {
            $btnAction = $this->input->post('btnaction');
            $valid_data = $this->security_form_step1_form_data(FALSE);
            $rslt = $this->Security_form_model->insert_security_form_step1($valid_data);
            if ($btnAction == 'next') {
                redirect(base_url('security_form/step2/' . $this->encrypt->encode($this->client_id)));
            } else {
                echo($rslt) ? $this->session->set_flashdata('create_success', 'success') : $this->session->set_flashdata('error_msg', 'error');
                $this->index('step1');
            }
        } else {
            $this->index('step1');
        }
    }

    public function update_security_form_step1() {
        $is_valid = $this->security_form_step1_validation(TRUE);
        if ($is_valid) {
            $btnAction = $this->input->post('btnaction');
            $valid_data = $this->security_form_step1_form_data(TRUE);
            $rslt = $this->Security_form_model->update_security_form_step1($this->encrypt->decode($this->input->post('form_id')), $valid_data);
            if ($btnAction == 'next') {
                redirect(base_url('security_form/step2/' . $this->encrypt->encode($this->client_id)));
            } else {
                echo($rslt) ? $this->session->set_flashdata('update_success', 'success') : $this->session->set_flashdata('error_msg', 'error');
                $this->index('step1');
            }
        } else {
            $this->index('step1');
        }
    }

    public function check_submit_status() {
        $rslt = $this->Security_form_model->get_main_form_submit_stat($this->client_id);
        echo $rslt;
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    private function security_form_step2_validation() {
        $this->form_validation->set_rules('tab_validator', 'tab_validator', 'callback_table_validator');
        return $this->form_validation->run();
    }

    private function security_form_step2_form_data($is_update) {
        $vehical_list = json_decode($this->input->post('json_str'));
        $data = array();
        $i = 0;
        if ($vehical_list != NULL) {
            foreach ($vehical_list as $vehical) {
                $data[$i]['vcl_sec_main_ref'] = $this->main_form_id;
                $data[$i]['vcl_item_des'] = $vehical->col1;
                $data[$i]['vcl_mod'] = $vehical->col2;
                $data[$i]['vcl_year'] = $vehical->col3;
                $data[$i]['vcl_amt'] = str_replace(",", "", $vehical->col4);
                $data[$i]['vcl_start_date'] = $this->input->post('start_time');
                if ($is_update) {
                    $data[$i]['vcl_end_date'] = $this->input->post('end_time');
                } else {
                    $data[$i]['vcl_end_date'] = date('Y-m_d H:i:s');
                }
                $data[$i]['filled_by'] = $this->session->userdata('user_name');
                $i++;
            }
            return $data;
        }
    }

    public function table_validator() {
        if (count(json_decode($this->input->post('json_str'))) == 0) {
            $this->form_validation->set_message('table_validator', 'Please enter vehicle details');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function save_security_form_step2() {
        $btnAction = $this->input->post('btnaction');
        if ($btnAction == 'save') {
            $is_valid = $this->security_form_step2_validation();
            if ($is_valid) {
                $valid_data = $this->security_form_step2_form_data(FALSE);
                $rslt = $this->Security_form_model->insert_security_form_step2($valid_data);
                echo($rslt) ? $this->session->set_flashdata('create_success', 'success') : $this->session->set_flashdata('error_msg', 'error');
                $this->index('step2');
            } else {
                $this->index('step2');
            }
        } else {
            $valid_data = $this->security_form_step2_form_data(FALSE);
            if ($valid_data) {
                $rslt = $this->Security_form_model->insert_security_form_step2($valid_data);
                if ($rslt) {
                    redirect(base_url('security_form/step3/' . $this->encrypt->encode($this->client_id)));
                } else {
                    $this->index('step2');
                }
            } else {
                redirect(base_url('security_form/step3/' . $this->encrypt->encode($this->client_id)));
            }
        }
    }

    public function update_security_form_step2() {
        $btnAction = $this->input->post('btnaction');
        if ($btnAction == 'save') {
            $is_valid = $this->security_form_step2_validation();
            if ($is_valid) {
                $valid_data = $this->security_form_step2_form_data(TRUE);
                $rslt = $this->Security_form_model->update_security_form_step2($this->main_form_id, $valid_data);
                echo($rslt) ? $this->session->set_flashdata('update_success', 'success') : $this->session->set_flashdata('error_msg', 'error');
                $this->index('step2');
            } else {
                $this->index('step2');
            }
        } else {
            $valid_data = $this->security_form_step2_form_data(TRUE);
            if ($valid_data) {
                $rslt = $this->Security_form_model->update_security_form_step2($this->main_form_id, $valid_data);
                if ($rslt) {
                    redirect(base_url('security_form/step3/' . $this->encrypt->encode($this->client_id)));
                } else {
                    $this->index('step2');
                }
            } else {
                redirect(base_url('security_form/step3/' . $this->encrypt->encode($this->client_id)));
            }
        }
    }

    public function delete_single_vehicle() {
        $result = $this->Security_form_model->delete_single_vehicle_item($this->input->post('vahicle_id'));
        echo $result;
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    private function security_form_step3_validation() {
        $this->form_validation->set_rules('tab_validator', 'tab_validator', 'callback_table_validator_sec3');
        return $this->form_validation->run();
    }

    public function table_validator_sec3() {
        if (count(json_decode($this->input->post('json_str'))) == 0) {
            $this->form_validation->set_message('table_validator_sec3', 'Please enter property details');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    private function security_form_step3_form_data($is_update) {
        $property_list = json_decode($this->input->post('json_str'));
        $data = array();
        $i = 0;
        if ($property_list != NULL) {
            foreach ($property_list as $property) {
                $data[$i]['prop_sec_main_ref'] = $this->main_form_id;
                $data[$i]['prop_des'] = $property->col1;
                $data[$i]['prop_amt'] = str_replace(",", "", $property->col2);
                $data[$i]['prop_start_date'] = $this->input->post('start_time');
                if ($is_update) {
                    $data[$i]['prop_end_date'] = $this->input->post('end_time');
                } else {
                    $data[$i]['prop_end_date'] = date('Y-m_d H:i:s');
                }
                $data[$i]['filled_by'] = $this->session->userdata('user_name');
                $i++;
            }
            return $data;
        }
    }

    public function save_security_form_step3() {
        $btnAction = $this->input->post('btnaction');
        if ($btnAction == 'save') {
            $is_valid = $this->security_form_step3_validation();
            if ($is_valid) {
                $valid_data = $this->security_form_step3_form_data(FALSE);
                $rslt = $this->Security_form_model->insert_security_form_step3($valid_data);
                echo($rslt) ? $this->session->set_flashdata('create_success', 'success') : $this->session->set_flashdata('error_msg', 'error');
                $this->index('step3');
            } else {
                $this->index('step3');
            }
        } else {
            $valid_data = $this->security_form_step3_form_data(FALSE);
            if ($valid_data) {
                $rslt = $this->Security_form_model->insert_security_form_step3($valid_data);
                if ($rslt) {
                    redirect(base_url('security_form/step4/' . $this->encrypt->encode($this->client_id)));
                } else {
                    $this->index('step3');
                }
            } else {
                redirect(base_url('security_form/step4/' . $this->encrypt->encode($this->client_id)));
            }
        }
    }

    public function update_security_form_step3() {
        $btnAction = $this->input->post('btnaction');
        if ($btnAction == 'save') {
            $is_valid = $this->security_form_step3_validation();
            if ($is_valid) {
                $valid_data = $this->security_form_step3_form_data(TRUE);
                $rslt = $this->Security_form_model->update_security_form_step3($this->main_form_id, $valid_data);
                echo($rslt) ? $this->session->set_flashdata('create_success', 'success') : $this->session->set_flashdata('error_msg', 'error');
                $this->index('step3');
            } else {
                $this->index('step3');
            }
        } else {
            $valid_data = $this->security_form_step3_form_data(TRUE);
            if ($valid_data) {
                $rslt = $this->Security_form_model->update_security_form_step3($this->main_form_id, $valid_data);
                if ($rslt) {
                    redirect(base_url('security_form/step4/' . $this->encrypt->encode($this->client_id)));
                } else {
                    $this->index('step3');
                }
            } else {
                redirect(base_url('security_form/step4/' . $this->encrypt->encode($this->client_id)));
            }
        }
    }

    public function delete_single_property() {
        $result = $this->Security_form_model->delete_single_property_item($this->input->post('property_id'));
        echo $result;
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    private function security_form_step4_validation() {
        $this->form_validation->set_rules('tab_validator', 'tab_validator', 'callback_table_validator_sec4');
        return $this->form_validation->run();
    }

    public function table_validator_sec4() {
        if (count(json_decode($this->input->post('json_str'))) == 0) {
            $this->form_validation->set_message('table_validator_sec4', 'Please enter chattels details');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    private function security_form_step4_form_data($is_update, $start_time = FALSE, $end_time = FALSE) {
        $chattles_list = json_decode($this->input->post('json_str'));
        $data = array();
        $i = 0;
        if ($chattles_list != NULL) {
            foreach ($chattles_list as $chattle) {
                $data[$i]['cat_sec_main_ref'] = $this->main_form_id;
                $data[$i]['cat_des'] = $chattle->col1;
                $data[$i]['cat_amt'] = str_replace(",", "", $chattle->col2);
                $data[$i]['cat_start_date'] = $start_time ? $start_time : $this->input->post('start_time');
                if ($is_update) {
                    $data[$i]['cat_end_date'] = $end_time ? $end_time : $this->input->post('end_time');
                } else {
                    $data[$i]['cat_end_date'] = $end_time ? $end_time : date('Y-m_d H:i:s');
                }
                $data[$i]['ccat_filled'] = $this->session->userdata('user_name');
                $i++;
            }
            return $data;
        }
    }

    public function save_security_form_step4() {
        $is_valid = $this->security_form_step4_validation();
        if ($is_valid) {
            $btnAction = $this->input->post('btnaction');
            $valid_data = $this->security_form_step4_form_data(FALSE);
            $rslt = $this->Security_form_model->insert_security_form_step4($valid_data);
            if ($btnAction == 'submit') {
                //redirect(base_url('security_form/step4/' . $this->encrypt->encode($this->client_id)));
            } else {
                echo($rslt) ? $this->session->set_flashdata('create_success', 'success') : $this->session->set_flashdata('error_msg', 'error');
                $this->index('step4');
            }
        } else {
            $this->index('step4');
        }
    }

    public function update_security_form_step4() {
        $is_valid = $this->security_form_step4_validation();
        if ($is_valid) {
            $btnAction = $this->input->post('btnaction');
            $valid_data = $this->security_form_step4_form_data(TRUE);
            $rslt = $this->Security_form_model->update_security_form_step4($this->main_form_id, $valid_data);
            if ($btnAction == 'next') {
                //redirect(base_url('security_form/step4/' . $this->encrypt->encode($this->client_id)));
            } else {
                echo($rslt) ? $this->session->set_flashdata('create_success', 'success') : $this->session->set_flashdata('error_msg', 'error');
                $this->index('step4');
            }
        } else {
            $this->index('step4');
        }
    }

    public function delete_single_chattel() {
        $result = $this->Security_form_model->delete_single_chattel_item($this->input->post('chattel_id'));
        echo $result;
    }

    public function sumbit_security_form() {
        $arr = $this->Security_form_model->read_security_form_step4($this->client_id);
        if (count($arr) > 0) {
            if (count(json_decode($this->input->post('json_str'))) != 0) {
                $valid_data = $this->security_form_step4_form_data($this->input->post('stat')=='1'?FALSE:TRUE,$this->input->post('start_time'),$this->input->post('end_time'));
                $res1 = $this->Security_form_model->update_security_form_step4($this->main_form_id, $valid_data);
            }
            $res2 = $this->Security_form_model->update_submit_status($this->main_form_id);
            if ($res2) {
                create_notification('6', '1', 'Submit Security Form by ' . $this->session->userdata('user_name'));
                add_activities($this->session->userdata('role'), $this->session->userdata('user_level_id'), 'Submit Security Form');
                echo 'success';
            } else {
                echo 'data_saving_error';
            }
        } else {
            if (count(json_decode($this->input->post('json_str'))) != 0) {
                $valid_data = $this->security_form_step4_form_data($this->input->post('stat')=='1'?FALSE:TRUE,$this->input->post('start_time'),$this->input->post('end_time'));
                $res1 = $this->Security_form_model->insert_security_form_step4($valid_data);
            }
            $res2 = $this->Security_form_model->update_submit_status($this->main_form_id);
            if ($res2) {
                create_notification('6', '1', 'Submit Security Form by ' . $this->session->userdata('user_name'));
                add_activities($this->session->userdata('role'), $this->session->userdata('user_level_id'), 'Submit Security Form');
                echo 'success';
            } else {
                echo 'data_saving_error';
            }
        }
    }

    public function view_security_form_pdf() {
        //pull out all step data
        $sec_form_1_data = $this->Security_form_model->read_security_form_step1($this->client_id);
        $sec_form_2_data = $this->Security_form_model->read_security_form_step2($this->client_id);
        $sec_form_3_data = $this->Security_form_model->read_security_form_step3($this->client_id);
        $sec_form_4_data = $this->Security_form_model->read_security_form_step4($this->client_id);

        if ($sec_form_1_data || $sec_form_2_data || $sec_form_3_data || $sec_form_3_data) {
            //create pdf
            $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('MWDI');
            $pdf->SetFont('helvetica', '', 10);

            // set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

            // set auto page breaks
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            $pdf->setJPEGQuality(100);
            $pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.1, 'depth_h' => 0, 'color' => array(255, 255, 255), 'opacity' => 1, 'blend_mode' => 'Normal'));
            if ($sec_form_1_data) {
                $pdf->AddPage();
                $data['details'] = json_encode($sec_form_1_data);
                $html = $this->load->view('security_form/security_form_step_1_view', $data, TRUE);
                $pdf->SetTitle('Instrument By Way Of Security');
                $pdf->writeHTML($html, true, false, true, false, '');
            }
            if ($sec_form_2_data) {
                $pdf->AddPage();
                $data['details'] = json_encode($sec_form_2_data);
                $html = $this->load->view('security_form/security_form_step_2_view', $data, TRUE);
                $pdf->SetTitle('Instrument By Way Of Security');
                $pdf->writeHTML($html, true, false, true, false, '');
            }
            if ($sec_form_3_data) {
                $pdf->AddPage();
                $data['details'] = json_encode($sec_form_3_data);
                $html = $this->load->view('security_form/security_form_step_3_view', $data, TRUE);
                $pdf->SetTitle('Instrument By Way Of Security');
                $pdf->writeHTML($html, true, false, true, false, '');
            }
            if ($sec_form_4_data) {
                $pdf->AddPage();
                $data['details'] = json_encode($sec_form_4_data);
                $html = $this->load->view('security_form/security_form_step_4_view', $data, TRUE);
                $pdf->SetTitle('Instrument By Way Of Security');
                $pdf->writeHTML($html, true, false, true, false, '');
            }

            $pdf->lastPage();
            $pdf->Output('security_form.pdf', 'I');
        }
        $this->session->set_flashdata('pdf_error', 'error');
        redirect(base_url('security_form/step1/' . $this->encrypt->encode($this->client_id)));
    }

    public function view_submitted_security_forms() {
        $data['sec_forms_submitted'] = json_encode($this->Security_form_model->get_all_submitted_security_form());
        $data['currentPage'] = "form_security";
        $data['mainContent'] = "security_form/submitted_security_forms";
        $this->load->view('includes/frame', $data);
    }

}
