<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('uid')) {
            redirect(base_url('Auth'));
        }
    }

    public function index() {
        $main_form_id = $this->Dashboard_model->get_main_ba_form_id($this->session->userdata('user_level_id'));
        $main_security_form_id = $this->Dashboard_model->get_main_security_form_id($this->session->userdata('user_level_id'));
        $ppms_form_id = $this->Dashboard_model->get_ppms_form_id($this->session->userdata('user_level_id'));

        //BA FORM PROGRESS
        $data['main_ba_progress'] = Calculate_form_progress($this->Dashboard_model->get_ba_form_se1($main_form_id));
        $data['edu_progress'] = Calculate_form_progress($this->Dashboard_model->get_ba_form_sec2($main_form_id));
        $data['nxt_kin_progress'] = Calculate_form_progress($this->Dashboard_model->get_ba_form_sec3($main_form_id));
        $data['emp_progress'] = Calculate_form_progress($this->Dashboard_model->get_ba_form_sec4($main_form_id));
        $data['biz_progress'] = Calculate_form_progress($this->Dashboard_model->get_ba_form_sec5($main_form_id));
        $data['bk_progress'] = Calculate_form_progress($this->Dashboard_model->get_ba_form_sec6($main_form_id));

        //SECURITY FORM PROGRESS
        $data['main_sec_progress'] = Calculate_form_progress($this->Dashboard_model->get_security_form_sec1($main_security_form_id));
        $data['sec_vehicle_progress'] = Calculate_form_progress($this->Dashboard_model->get_security_form_sec2($main_security_form_id));
        $data['sec_property_progress'] = Calculate_form_progress($this->Dashboard_model->get_security_form_sec3($main_security_form_id));
        $data['sec_chattles_progress'] = Calculate_form_progress($this->Dashboard_model->get_security_form_sec4($main_security_form_id));

        //Coah and clients activities
        if ($this->session->userdata('role') == '1' || $this->session->userdata('role') == '2' || $this->session->userdata('role') == '3') {
            $data['all_coach_activities'] = json_encode($this->Activity_model->get_all_user_activities('4'));
            $data['all_cilent_activities'] = json_encode($this->Activity_model->get_all_user_activities('5'));
        } elseif ($this->session->userdata('role') == '4') {
            $data['all_coach_activities'] = json_encode($this->Activity_model->get_all_user_activities($this->session->userdata('role'), $this->session->userdata('user_level_id')));
            $data['all_cilent_activities'] = json_encode($this->Activity_model->get_my_clients_activities($this->session->userdata('user_level_id')));
        } elseif ($this->session->userdata('role') == '5') {
            $data['all_coach_activities'] = json_encode($this->Activity_model->get_all_user_activities($this->session->userdata('role'), $this->session->userdata('user_level_id')));
        }

        //BA form completion time
        if ($this->session->userdata('role') == '1' || $this->session->userdata('role') == '2' || $this->session->userdata('role') == '3') {
            $ba_sec1 = $this->calculate_ba_form_complete_time($this->Dashboard_model->get_main_ba_form_time());
            $ba_sec2 = $this->calculate_ba_form_complete_time($this->Dashboard_model->get_ba_form_sec2_time());
            $ba_sec3 = $this->calculate_ba_form_complete_time($this->Dashboard_model->get_ba_form_sec3_time());
            $ba_sec4 = $this->calculate_ba_form_complete_time($this->Dashboard_model->get_ba_form_sec4_time());
            $ba_sec5 = $this->calculate_ba_form_complete_time($this->Dashboard_model->get_ba_form_sec5_time());
            $ba_sec6 = $this->calculate_ba_form_complete_time($this->Dashboard_model->get_ba_form_sec6_time());
            $number_of_submitted_forms = $this->Dashboard_model->get_number_of_summitted_ba_forms();
            if (intval($number_of_submitted_forms) > 0) {
                $data['ba_form_complete_time'] = ((intval($ba_sec1) + intval($ba_sec2) + intval($ba_sec3) + intval($ba_sec4) + intval($ba_sec5) + intval($ba_sec6)) / intval($number_of_submitted_forms));
            } else {
                $data['ba_form_complete_time'] = 0;
            }
        } elseif ($this->session->userdata('role') == '5') {
            $data['is_ba_form_sumbit'] = $this->Dashboard_model->ba_form_submit($main_form_id);
            if ($data['is_ba_form_sumbit'] == 'Y') {
                $ba_sec1 = $this->Dashboard_model->get_main_ba_form_time($main_form_id);
                $ba_sec2 = $this->Dashboard_model->get_ba_form_sec2_time($main_form_id);
                $ba_sec3 = $this->Dashboard_model->get_ba_form_sec3_time($main_form_id);
                $ba_sec4 = $this->Dashboard_model->get_ba_form_sec4_time($main_form_id);
                $ba_sec5 = $this->Dashboard_model->get_ba_form_sec5_time($main_form_id);
                $ba_sec6 = $this->Dashboard_model->get_ba_form_sec6_time($main_form_id);
                $data['ba_form_complete_time'] = ($ba_sec1?intval($ba_sec1[0]->ba_main_time):0) + ($ba_sec2?intval($ba_sec2[0]->ba_edu_time):0) + ($ba_sec3?intval($ba_sec3[0]->ba_nk_time):0) + ($ba_sec4?intval($ba_sec4[0]->ba_emp_time):0) + ($ba_sec5?intval($ba_sec5[0]->ba_biz_time):0) + ($ba_sec6?intval($ba_sec6[0]->ba_bnk_time):0);
            }
        }

        //Security form completion time

        if ($this->session->userdata('role') == '1' || $this->session->userdata('role') == '2' || $this->session->userdata('role') == '3') {
            $sec_p1 = $this->calculate_ba_form_complete_time($this->Dashboard_model->get_security_form_sec1_time());
            $sec_p2 = $this->calculate_ba_form_complete_time($this->Dashboard_model->get_security_form_sec2_time());
            $sec_p3 = $this->calculate_ba_form_complete_time($this->Dashboard_model->get_security_form_sec3_time());
            $sec_p4 = $this->calculate_ba_form_complete_time($this->Dashboard_model->get_security_form_sec4_time());
            $number_of_submitted_forms = $this->Dashboard_model->get_number_of_summitted_security_forms();
            if (intval($number_of_submitted_forms) > 0) {
                $data['sec_form_complete_time'] = ((intval($sec_p1) + intval($sec_p2) + intval($sec_p3) + intval($sec_p4)) / intval($number_of_submitted_forms));
            } else {
                $data['sec_form_complete_time'] = 0;
            }
        } elseif ($this->session->userdata('role') == '5') {
            $data['is_sec_form_sumbit'] = $this->Dashboard_model->sec_form_submit($main_security_form_id);
            if ($data['is_sec_form_sumbit'] == 'Y') {
                $sec_p1 = $this->Dashboard_model->get_security_form_sec1_time($main_security_form_id);
                $sec_p2 = $this->Dashboard_model->get_security_form_sec2_time($main_security_form_id);
                $sec_p3 = $this->Dashboard_model->get_security_form_sec3_time($main_security_form_id);
                $sec_p4 = $this->Dashboard_model->get_security_form_sec4_time($main_security_form_id);
                $data['sec_form_complete_time'] = ($sec_p1?intval($sec_p1[0]->main_sec_time):0) + ($sec_p2?intval($sec_p2[0]->vcl_sec_time):0) + ($sec_p3?intval($sec_p3[0]->prop_sec_time):0) + ($sec_p4?intval($sec_p4[0]->chtl_sec_time):0);
            }
        }

        //PPMS form completion time

        if ($this->session->userdata('role') == '1' || $this->session->userdata('role') == '2' || $this->session->userdata('role') == '3') {
            $ppms_time = $this->calculate_ba_form_complete_time($this->Dashboard_model->get_ppms_form_complete_time());
            $number_of_submitted_forms = $this->Dashboard_model->get_number_of_summitted_ppms_forms();
            if (intval($number_of_submitted_forms)) {
                $data['ppms_form_complete_time'] = intval($ppms_time) / intval($number_of_submitted_forms);
            } else {
                $data['ppms_form_complete_time'] = 0;
            }
        } elseif ($this->session->userdata('role') == '5') {
            $data['is_ppms_form_sumbit'] = $this->Dashboard_model->ppms_form_submit($ppms_form_id);
            if ($data['is_ppms_form_sumbit'] == 'Y') {
                $ppms_time = $this->Dashboard_model->get_ppms_form_complete_time($ppms_form_id);
                $data['ppms_form_complete_time'] = $ppms_time?$ppms_time[0]->ppms_time:0;
            }
        }

        //Coach Dashboard
        if ($this->session->userdata('role') == '4') {
            $my_clients = json_decode($this->System_user_model->gel_all_users('5'));
            $data['tot_number_of_client'] = count($my_clients);
            $temp['submitted_ch1'] = $this->Dashboard_model->get_submitted_coach_form_sec1($this->session->userdata('user_level_id'));
            $temp['submitted_ch2'] = $this->Dashboard_model->get_submitted_coach_form_sec2($this->session->userdata('user_level_id'));
            $temp['submitted_ch3'] = $this->Dashboard_model->get_submitted_coach_form_sec3($this->session->userdata('user_level_id'));
            $temp['submitted_ch4'] = $this->Dashboard_model->get_submitted_coach_form_sec4($this->session->userdata('user_level_id'));
            if (intval($data['tot_number_of_client'] > 0)) {
                $data['coach']['ENGAGEMENT 1']['success_rate'] = round((intval($temp['submitted_ch1']) / intval($data['tot_number_of_client'])) * 100, 1);
                $data['coach']['ENGAGEMENT 1']['submit_cnt'] = $temp['submitted_ch1'];
                $data['coach']['ENGAGEMENT 2']['success_rate'] = round((intval($temp['submitted_ch2']) / intval($data['tot_number_of_client'])) * 100, 1);
                $data['coach']['ENGAGEMENT 2']['submit_cnt'] = $temp['submitted_ch2'];
                //$data['success_rate_ch3'] = round((intval($data['submitted_ch3']) / intval($data['tot_number_of_client'])) * 100, 1);
                foreach ($temp['submitted_ch3'] as $frq => $count) {
                    $data['coach']['ENGAGEMENT '.$count->frq]['success_rate']=round((intval($count->ch3_cnt) / intval($data['tot_number_of_client'])) * 100, 1);
                    $data['coach']['ENGAGEMENT '.$count->frq]['submit_cnt'] = $count->ch3_cnt;
                }
                $data['coach']['ENGAGEMENT FINAL']['success_rate'] = round((intval($temp['submitted_ch4']) / intval($data['tot_number_of_client'])) * 100, 1);
                $data['coach']['ENGAGEMENT FINAL']['submit_cnt'] = $temp['submitted_ch4'];
            } else {
                $data['success_rate_ch1'] = 0;
                $data['success_rate_ch2'] = 0;
                $data['success_rate_ch3'] = 0;
                $data['success_rate_ch4'] = 0;
            }
        }

        //Super admin dashboard top stat

        if ($this->session->userdata('role') == '1' || $this->session->userdata('role') == '2' || $this->session->userdata('role') == '3') {
            $data['tot_forms_assigned_client'] = $this->Dashboard_model->get_forms_assign_to_clients(array('1', '2', '4'));
            $submitted_ba_forms = count($this->BAForm_model->get_all_submitted_BA_form('Y'));
            $submitted_ppms_forms = count($this->Ppms_form_model->get_all_submitted_ppms_forms('Y'));
            $submitted_sec_forms = count($this->Security_form_model->get_all_submitted_security_form('Y'));

            $data['tot_submitted_forms'] = intval($submitted_ba_forms) + intval($submitted_ppms_forms) + intval($submitted_sec_forms);

            if ($data['tot_forms_assigned_client'] > 0) {
                $data['client_success_rate'] = round((intval($data['tot_submitted_forms']) / intval($data['tot_forms_assigned_client'])) * 100, 1);
            } else {
                $data['client_success_rate'] = 0;
            }
        }


        $data['currentPage'] = "index";
        $data['mainContent'] = "dashboard/dashboard";
        $this->load->view('includes/frame', $data);
    }

    //--------------------------------------------------------------------------

    private function calculate_ba_form_complete_time($arr) {
        $tot_each_form = 0;
        foreach ($arr as $key => $val) {
            foreach ($val as $val2) {
                $tot_each_form = $tot_each_form + intval($val2);
            }
        }
        return $tot_each_form;
    }

    //--------------------------------------------------------------------------

    public function get_client_stat() {
        $form_type = $this->input->post('form_type');
        $stat = array();
        if ($form_type) {
            $stat['tot_forms_assigned_client'] = $this->Dashboard_model->get_forms_assign_to_clients(array($form_type));
        }
        switch ($form_type) {
            case '1':$stat['submitted_forms'] = count($this->BAForm_model->get_all_submitted_BA_form('Y'));
                break;
            case '2':$stat['submitted_forms'] = count($this->Ppms_form_model->get_all_submitted_ppms_forms('Y'));
                break;
            case '4':$stat['submitted_forms'] = count($this->Security_form_model->get_all_submitted_security_form('Y'));
                break;
        }
        if ($stat['tot_forms_assigned_client'] > 0) {
            $stat['success_rate'] = round((intval($stat['submitted_forms']) / intval($stat['tot_forms_assigned_client'])) * 100, 1);
        } else {
            $stat['success_rate'] = 0;
        }
        echo json_encode($stat);
    }

    //--------------------------------------------------------------------------

    public function search() {
        $key_word = $this->input->post('key_word');
        $key_area = $this->input->post('key_area');
        $serach_result = $this->Dashboard_model->serach($key_word, $key_area);
        echo json_encode($serach_result);
    }


    public function fixFormClients($fix=false) {
        $formNeeds = array();
        $query = $this->db->query("SELECT * FROM clients_tb JOIN user_tb ON user_tb.user_id = clients_tb.u_id WHERE user_tb.role_id = 5");
        $query = $query->result();

        echo "<p>Total clients: ".count($query)."</p>";

        foreach($query as $client) {
            $query_forms = $this->db->where("cf_client_ref", $client->client_id)->get("client_forms_tb");
            $query_forms = $query_forms->result();
            if($query_forms) {
                $haveForms = array();
                foreach ($query_forms as $form) {
                    $haveForms[] = $form->cf_frm_id;
                }

                if(!in_array("1", $haveForms)) {
                    $formNeeds[$client->client_id][] = "1";
                }

                if(!in_array("2", $haveForms)) {
                    $formNeeds[$client->client_id][] = "2";
                }

                if(!in_array("4", $haveForms)) {
                    $formNeeds[$client->client_id][] = "4";
                }

            } else {
                $formNeeds[$client->client_id] = array("1","2","4");
            }
        }

        echo "<p>Total clients needs form: ".count($formNeeds)."</p>";
        if(count($formNeeds)>0) {
            if($fix) {
                $today = date("Y-m-d H:i:s");
                foreach ($formNeeds as $client_id => $forms) {
                    foreach($forms as $form) {
                       $this->db->insert("client_forms_tb", array("cf_client_ref" => $client_id, "  cf_frm_id" => $form, "assign_date" => $today, "assign_by" => "1", "role_assign" => "1", "date_mod" => "0000-00-00 00:00:00", "mod_by" => "", "role_mod" => "0"));
                    }
                }
            }
        }
    }
}
