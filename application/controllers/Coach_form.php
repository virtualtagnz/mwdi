<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Coach_form extends CI_Controller {

    public $path;

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('uid')) {
            redirect(base_url('Auth'));
        }
        $this->load->library('Pdf');
        $this->path = realpath(APPPATH . '../public_html/uploads/non_disclosure_agreements');
    }

    public function index() {
//        $data['archives'] = json_encode($this->Coach_form_model->get_all_coach_form());
//        $data['currentPage'] = "form_coach";
//        $data['mainContent'] = "coach_form/view_submited_coach_forms";
//        $this->load->view('includes/frame', $data);
        $data['my_forms'] = json_encode($this->Assignment_model->assigned_application_forms($this->session->userdata('role'), $this->session->userdata('user_level_id')));
        $data['archives'] = json_encode($this->Coach_form_model->get_all_coach_form());
        $data['currentPage'] = "my_forms";
        $data['mainContent'] = "assign/view_forms_assigned_to_me";
        $this->load->view('includes/frame', $data);
    }

    public function view_engagement($part, $form_id = FALSE) {
        $data['currentPage'] = "my_forms";
        switch ($part) {
            case '5':
                if ($form_id) {
                    $data['details'] = $this->Coach_form_model->get_single_coach_form_1($this->encrypt->decode($form_id));
                    $data['mainContent'] = "coach_form/coach_form_1_edit";
                } else {
                    $data['mainContent'] = "coach_form/coach_form_1";
                }

                if ($this->session->userdata('role') == '4') {
                    $temp_arr = $this->System_user_model->get_coach_form_submitted_clients($this->session->userdata('user_level_id'), '5');
                    $data['my_clients'] = json_encode($this->System_user_model->get_all_users_not_submit_coach_forms($this->session->userdata('user_level_id'), $temp_arr));
                    $data['my_all_clients'] = json_encode($this->System_user_model->get_my_users($this->session->userdata('user_level_id')));
                } else {
                    $temp_arr = $this->System_user_model->get_coach_form_submitted_clients($data['details']->chf_ch_ref, '5');
                    $data['my_clients'] = json_encode($this->System_user_model->get_all_users_not_submit_coach_forms($data['details']->chf_ch_ref, $temp_arr));
                    $data['my_all_clients'] = json_encode($this->System_user_model->get_my_users($data['details']->chf_ch_ref));
                }
                break;
            case '8':
                $temp_arr = $this->System_user_model->get_coach_form_submitted_clients($this->session->userdata('user_level_id'), '8');
                $data['my_clients'] = json_encode($this->System_user_model->get_all_users_not_submit_coach_forms($this->session->userdata('user_level_id'), $temp_arr));
                if ($form_id) {
                    $data['details'] = $this->Coach_form_model->get_single_coach_form_3($this->encrypt->decode($form_id));
                    $data['mainContent'] = "coach_form/coach_form_3_edit";
                } else {
                    $data['mainContent'] = "coach_form/coach_form_3";
                }
                break;
            case '9':
                $temp_arr = $this->System_user_model->get_coach_form_submitted_clients($this->session->userdata('user_level_id'), '10');
                $data['my_clients'] = json_encode($this->System_user_model->get_all_users_not_submit_coach_forms($this->session->userdata('user_level_id'), $temp_arr));
                if ($form_id) {
                    $data['details'] = $this->Coach_form_model->get_single_coach_form_4($this->encrypt->decode($form_id));
                    $data['mainContent'] = "coach_form/coach_form_4_edit";
                } else {
                    $data['mainContent'] = "coach_form/coach_form_4";
                }
                break;
            case '10':
                $temp_arr = $this->System_user_model->get_coach_form_submitted_clients($this->session->userdata('user_level_id'), '10');
                $data['my_clients'] = json_encode($this->System_user_model->get_all_users_not_submit_coach_forms($this->session->userdata('user_level_id'), $temp_arr));
                if ($form_id) {
                    $data['details'] = $this->Coach_form_model->get_single_coach_form_5($this->encrypt->decode($form_id));
                    $data['mainContent'] = "coach_form/coach_form_5_edit";
                } else {
                    $data['mainContent'] = "coach_form/coach_form_5";
                }
                break;
        }
        $this->load->view('includes/frame', $data);
    }

    public function load_single_user() {
        $user_type = $this->input->post('usr_type');
        $uid = $this->input->post('uid');
        $user_details = json_encode($this->System_user_model->get_single_user_by_user_level_id($this->encrypt->decode($user_type), $uid));
        echo $user_details;
    }

    //--------------------------------------------------------------------------

    public function coach_form_1_create() {
        $is_valid = $this->coach_form_1_validation(FALSE);
        if ($is_valid) {
            $btnAction = $this->input->post('button_type');
            $valid_data = $this->coach_form_1_form_data(FALSE, $btnAction);
            $rslt = $this->Coach_form_model->insert_coach_form_main($valid_data);
            if ($rslt) {
                if ($btnAction == 'frm_save') {
                    $this->session->set_flashdata('create_success', 'success');
                    redirect(base_url('view_engagement/' . '5' . '/' . $this->encrypt->encode($rslt)));
                } else {
                    $this->session->set_flashdata('submit_success', 'success');
                    create_notification('6', '1', 'Submit Hinepreneur Coach Form Engagement 1 by ' . $this->session->userdata('user_name'));
                    add_activities($this->session->userdata('role'), $this->session->userdata('user_level_id'), 'Submit Hinepreneur Coach Form Engagement 1');
                    $this->index();
                }
            } else {
                $this->session->set_flashdata('error_msg', 'error');
                redirect(base_url('view_engagement/' . '5' . '/' . $this->encrypt->encode($rslt)));
            }
        } else {
            $this->view_engagement('5');
        }
    }

    //--------------------------------------------------------------------------

    public function coach_form_1_edit($form_id) {
        $is_valid = $this->coach_form_1_validation(TRUE);
        if ($is_valid) {
            $btnAction = $this->input->post('button_type');
            $valid_data = $this->coach_form_1_form_data(TRUE, $btnAction);
            $rslt = $this->Coach_form_model->update_coach_form_1($this->encrypt->decode($form_id), $valid_data);
            if ($rslt) {
                if ($btnAction == 'frm_edit') {
                    $this->session->set_flashdata('create_success', 'success');
                    redirect(base_url('view_engagement/' . '5' . '/' . $form_id));
                } else {
                    $this->session->set_flashdata('submit_success', 'success');
                    create_notification('6', '1', 'Submit Hinepreneur Coach Form Engagement 1 by ' . $this->session->userdata('user_name'));
                    add_activities($this->session->userdata('role'), $this->session->userdata('user_level_id'), 'Submit Hinepreneur Coach Form Engagement 1');
                    $this->index();
                }
            } else {
                $this->session->set_flashdata('error_msg', 'error');
                redirect(base_url('view_engagement/' . '5' . '/' . $form_id));
            }
        } else {
            $this->view_engagement('5');
        }
    }

    //--------------------------------------------------------------------------

    public function coach_form_1_validation($is_update) {
        if ($is_update) {
            
        } else {
            $this->form_validation->set_rules('sel_client', 'client name', 'trim|required');
        }
        $this->form_validation->set_rules('sel_type', 'new/existing Client', 'trim|required');
        $this->form_validation->set_rules('region', 'region of delivery', 'trim|required');
        $this->form_validation->set_rules('referred', 'Referred by', 'trim|required');
//        $this->form_validation->set_rules('intro_client', 'Name of Client Referred', 'trim|required');
        $this->form_validation->set_rules('address', 'address', 'trim|required');
        $this->form_validation->set_rules('purpose', 'Purpose of Visit', 'trim|required');
        //$this->form_validation->set_rules('nxt_kin', 'Emergency Contact Person', 'trim|required');
        $this->form_validation->set_rules('phone', 'Phone Number', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('ch2_place', 'where did you meet', 'trim|required');
        $this->form_validation->set_rules('ch2_st_time', 'Start Time', 'trim|required');
        $this->form_validation->set_rules('ch2_ed_time', 'End Time', 'trim|required');
        $this->form_validation->set_rules('ch2_oview', 'general overview', 'trim|required');
        $this->form_validation->set_rules('ch2_goals', 'key actions', 'trim|required');
        return $this->form_validation->run();
    }

    //--------------------------------------------------------------------------

    public function coach_form_1_form_data($is_update, $btn_action) {
        $data = array();
        if ($is_update) {
            
        } else {
            $data['start_date'] = $this->input->post('ch1_start_time');
            $data['end_date'] = date('Y-m-d H:i:s');
            $data['chf_ch_ref'] = $this->session->userdata('user_level_id');
            $data['chf_cl_ref'] = $this->input->post('sel_client');
        }
        $data['chf_cl_type'] = $this->input->post('sel_type');
        $data['chf_reg'] = $this->input->post('region');
        $data['chf_ref'] = $this->input->post('referred');
        $data['chf_ref_client'] = $this->input->post('intro_client');
        $data['chf_add'] = $this->input->post('address');
        $data['chf_phn'] = $this->input->post('phone');
        $data['chf_nxt_contact'] = $this->input->post('nxt_kin');
        $data['chf_nxt_phn'] = $this->input->post('mobile');
        $data['chf_email'] = $this->input->post('email');
        $data['chf_purpose'] = $this->input->post('purpose');
        $data['chp2_f1'] = $this->input->post('ch2_place');
        $data['chp2_f2'] = $this->input->post('ch2_st_time');
        $data['chp2_f3'] = $this->input->post('ch2_ed_time');
        $data['chp2_f4'] = $this->input->post('ch2_oview');
        $data['chp2_f5'] = $this->input->post('ch2_goals');
        $data['chp2_f6'] = $this->input->post('info_list') ? implode(",", $this->input->post('info_list')) : '';
        $data['chp2_f7'] = $this->input->post('nxt_app');
        $data['chp2_f8'] = $this->input->post('next_date');
        $data['chp2_f9'] = $this->input->post('other_ch_flow');
        $data['chp2_f10'] = $this->input->post('any_consider');
        $data['chp2_f11'] = $this->input->post('invo_sent');
        $data['filled_by'] = $this->session->userdata('user_name');
        if ($this->input->post('sub_stat')!='Y'){
            $data['is_submit'] = $btn_action == 'frm_submit' ? 'Y' : 'N';
            $data['submit_date'] = $btn_action == 'frm_submit' ? date('Y-m-d H:i:s') : '';
        }
//        if ($this->session->userdata('role') == '4') {
//            $data['is_submit'] = $btn_action == 'frm_submit' ? 'Y' : 'N';
//            $data['submit_date'] = $btn_action == 'frm_submit' ? date('Y-m-d H:i:s') : '';
//        } else {
//            if ($this->input->post('sub_stat')) {
//                $data['is_submit'] = $this->input->post('sub_stat');
//                $data['submit_date'] = $this->input->post('sub_stat') == 'Y' ? date('Y-m-d H:i:s') : '';
//            } else {
//                $data['is_submit'] = $btn_action == 'frm_submit' ? 'Y' : 'N';
//                $data['submit_date'] = $btn_action == 'frm_submit' ? date('Y-m-d H:i:s') : '';
//            }
//        }

        $data['form_type'] = '5';
        //$up_arr = $this->upload_nd_agreement();
        if (in_array('11', $this->input->post('info_list'))) {
            if ($is_update) {
                if (!empty($_FILES['nd_upload']['name'])) {
                    $rtn_arr = $this->upload_nd_agreement();
                    $data['nd_link'] = $rtn_arr['fileArr']['file_name'];
                    $data['nd_name'] = $rtn_arr['fileArr']['orig_name'];
                } else {
                    $data['nd_link'] = $this->input->post('uploaded_link');
                    $data['nd_name'] = $this->input->post('uploaded_name');
                }
            } else {
                $rtn_arr = $this->upload_nd_agreement();
                $data['nd_link'] = $rtn_arr['fileArr']['file_name'];
                $data['nd_name'] = $rtn_arr['fileArr']['orig_name'];
            }
        } else {
            $unlink_res = unlink($this->path.'/'.$this->input->post('uploaded_link'));
            if ($unlink_res) {
                $data['nd_link'] = "";
                $data['nd_name'] = "";
            }
        }
//        var_dump($data);
//        die();
        return $data;
    }

    //-----------------------------file upload----------------------------------

    private function upload_nd_agreement() {
        $image_upload_rslt = upload_files($this->path, 'nd_upload', 'files');
        return $image_upload_rslt;
    }

    //-----------------------------end of form 1--------------------------------

    public function coach_form_3_create() {
        $is_valid = $this->coach_form_3_validation(FALSE);
        if ($is_valid) {
            $btn_action = $this->input->post('button_type');
            $valid_data = $this->coach_form_3_form_data(FALSE, $btn_action);
            $rslt = $this->Coach_form_model->insert_coach_form_3($valid_data);
            if ($rslt) {
                if ($btn_action == 'frm_save') {
                    $this->session->set_flashdata('create_success', 'success');
                    redirect(base_url('view_engagement/' . '8' . '/' . $this->encrypt->encode($rslt)));
                } else {
                    $this->session->set_flashdata('submit_success', 'success');
                    create_notification('6', '1', 'Submit Hinepreneur Coach Form Engagement 2 by ' . $this->session->userdata('user_name'));
                    add_activities($this->session->userdata('role'), $this->session->userdata('user_level_id'), 'Submit Hinepreneur Coach Form Engagement 2');
                    $this->index();
                }
            } else {
                $this->session->set_flashdata('error_msg', 'error');
                redirect(base_url('view_engagement/' . '8' . '/' . $this->encrypt->encode($rslt)));
            }
        } else {
            $this->view_engagement('8');
        }
    }

    //--------------------------------------------------------------------------

    public function coach_form_3_edit($form_id) {
        $is_valid = $this->coach_form_3_validation(TRUE);
        if ($is_valid) {
            $btn_action = $this->input->post('button_type');
            $valid_data = $this->coach_form_3_form_data(TRUE, $btn_action);
            $rslt = $this->Coach_form_model->update_coach_form_3($this->encrypt->decode($form_id), $valid_data);
            if ($rslt) {
                if ($btn_action == 'frm_edit') {
                    $this->session->set_flashdata('create_success', 'success');
                    redirect(base_url('view_engagement/' . '8' . '/' . $form_id));
                } else {
                    $this->session->set_flashdata('submit_success', 'success');
                    create_notification('6', '1', 'Submit Hinepreneur Coach Form Engagement 2 by ' . $this->session->userdata('user_name'));
                    add_activities($this->session->userdata('role'), $this->session->userdata('user_level_id'), 'Submit Hinepreneur Coach Form Engagement 2');
                    $this->index();
                }
            } else {
                $this->session->set_flashdata('error_msg', 'error');
                redirect(base_url('view_engagement/' . '8' . '/' . $this->encrypt->encode($rslt)));
            }
        } else {
            $this->view_engagement('8');
        }
    }

    //--------------------------------------------------------------------------

    public function coach_form_3_validation($is_update) {
        if ($is_update) {
            
        } else {
            $this->form_validation->set_rules('ch3_client', 'client Name', 'trim|required');
        }

        $this->form_validation->set_rules('ch3_place', 'where did you meet', 'trim|required');
        $this->form_validation->set_rules('ch3_st_time', 'Start Time', 'trim|required');
        $this->form_validation->set_rules('ch3_ed_time', 'End Time', 'trim|required');
        $this->form_validation->set_rules('ch3_oview', 'general overview', 'trim|required');
        return $this->form_validation->run();
    }

    //--------------------------------------------------------------------------

    public function coach_form_3_form_data($is_update, $btn_action) {
        $data = array();
        if ($is_update) {
            
        } else {
            $data['chp3_main_ref'] = $this->encrypt->decode($this->input->post('main_form_id'));
            $data['chp3_f23'] = $this->input->post('ch3_client');
            $data['start_date'] = $this->input->post('ch3_start_time');
            $data['end_date'] = date('Y-m-d H:i:s');
        }
        $data['chp3_f24'] = $this->input->post('ch3_place');
        $data['chp3_f25'] = $this->input->post('ch3_st_time');
        $data['chp3_f26'] = $this->input->post('ch3_ed_time');
        $data['chp3_f27'] = $this->input->post('ch3_oview');
        $data['chp3_f28'] = $this->input->post('chk_list') ? implode(",", $this->input->post('chk_list')) : '';
        $data['chp3_f29'] = $this->input->post('rel_points');
        $data['chp3_f30'] = $this->input->post('note');
        $data['chp3_f31'] = $this->input->post('lq_1');
        $data['chp3_f32'] = $this->input->post('lq_2');
        $data['chp3_f33'] = $this->input->post('lq_3');
        $data['chp3_f34'] = $this->input->post('ft_1');
        $data['chp3_f35'] = $this->input->post('ft_2');
        $data['chp3_f36'] = $this->input->post('ft_3');
        $data['chp3_f37'] = $this->input->post('tr_1');
        $data['chp3_f38'] = $this->input->post('tr_2');
        $data['chp3_f39'] = $this->input->post('info_1');
        $data['chp3_f40'] = $this->input->post('info_2');
        $data['chp3_f41'] = $this->input->post('info_3');
        $data['chp3_f42'] = $this->input->post('bd_1');
        $data['chp3_f43'] = $this->input->post('bd_2');
        $data['chp3_f44'] = $this->input->post('bd_3');
        $data['chp3_f45'] = $this->input->post('bd_4');
        $data['chp3_f46'] = $this->input->post('bd_5');
        $data['chp3_f47'] = $this->input->post('bd_6');
        $data['chp3_f48'] = $this->input->post('ch3_nxt_app');
        $data['chp3_f49'] = $this->input->post('ch3_next_date');
        $data['chp3_f50'] = $this->input->post('ch3_invo_sent');
        $data['filled_by'] = $this->session->userdata('user_name');
        if ($this->input->post('sub_stat')!='Y'){
            $data['is_submit'] = $btn_action == 'frm_submit' ? 'Y' : 'N';
            $data['submit_date'] = $btn_action == 'frm_submit' ? date('Y-m-d H:i:s') : '';
        }
//        if ($this->session->userdata('role') == '4') {
//            $data['is_submit'] = $btn_action == 'frm_submit' ? 'Y' : 'N';
//            $data['submit_date'] = $btn_action == 'frm_submit' ? date('Y-m-d H:i:s') : '';
//        } else {
//            if ($this->input->post('sub_stat')) {
//                $data['is_submit'] = $this->input->post('sub_stat');
//                $data['submit_date'] = $this->input->post('sub_stat') == 'Y' ? date('Y-m-d H:i:s') : '';
//            } else {
//                $data['is_submit'] = $btn_action == 'frm_submit' ? 'Y' : 'N';
//                $data['submit_date'] = $btn_action == 'frm_submit' ? date('Y-m-d H:i:s') : '';
//            }
//        }
        $data['form_type'] = '8';
        return $data;
    }

    //-----------------------------end of form 3--------------------------------

    public function coach_form_4_create() {
        $is_valid = $this->coach_form_4_validation(FALSE);
        if ($is_valid) {
            $btn_action = $this->input->post('button_type');
            $valid_data = $this->coach_form_4_form_data(FALSE, $btn_action);
            $rslt = $this->Coach_form_model->insert_coach_form_4($valid_data);
            if ($rslt) {
                if ($btn_action == 'frm_save') {
                    $this->session->set_flashdata('create_success', 'success');
                    redirect(base_url('view_engagement/' . '9' . '/' . $this->encrypt->encode($rslt)));
                } else {
                    $this->session->set_flashdata('submit_success', 'success');
                    create_notification('6', '1', 'Submit Hinepreneur Coach Form Engagement 3 by ' . $this->session->userdata('user_name'));
                    add_activities($this->session->userdata('role'), $this->session->userdata('user_level_id'), 'Submit Hinepreneur Coach Form Engagement 3');
                    $this->index();
                }
            } else {
                $this->session->set_flashdata('error_msg', 'error');
                redirect(base_url('view_engagement/' . '9' . '/' . $this->encrypt->encode($rslt)));
            }
        } else {
            $this->view_engagement('9');
        }
    }

    //--------------------------------------------------------------------------

    public function coach_form_4_edit($form_id) {
        $is_valid = $this->coach_form_4_validation(TRUE);
        if ($is_valid) {
            $btn_action = $this->input->post('button_type');
            $valid_data = $this->coach_form_4_form_data(TRUE, $btn_action);
            $rslt = $this->Coach_form_model->update_coach_form_4($this->encrypt->decode($form_id), $valid_data);
            if ($rslt) {
                if ($btn_action == 'frm_edit') {
                    $this->session->set_flashdata('create_success', 'success');
                    redirect(base_url('view_engagement/' . '9' . '/' . $form_id));
                } else {
                    $this->session->set_flashdata('submit_success', 'success');
                    create_notification('6', '1', 'Submit Hinepreneur Coach Form Engagement 3 by ' . $this->session->userdata('user_name'));
                    add_activities($this->session->userdata('role'), $this->session->userdata('user_level_id'), 'Submit Hinepreneur Coach Form Engagement 3');
                    $this->index();
                }
            } else {
                $this->session->set_flashdata('error_msg', 'error');
                redirect(base_url('view_engagement/' . '9' . '/' . $form_id));
            }
        } else {
            $this->view_engagement('9');
        }
    }

    //--------------------------------------------------------------------------

    public function coach_form_4_validation($is_update) {
        if ($is_update) {
            
        } else {
            $this->form_validation->set_rules('ch4_client', 'client Name', 'trim|required');
        }

        $this->form_validation->set_rules('ch4_place', 'where did you meet', 'trim|required');
        $this->form_validation->set_rules('ch4_st_time', 'Start Time', 'trim|required');
        $this->form_validation->set_rules('ch4_ed_time', 'End Time', 'trim|required');
        $this->form_validation->set_rules('ch4_oview', 'general overview', 'trim|required');
        return $this->form_validation->run();
    }

    //--------------------------------------------------------------------------

    public function coach_form_4_form_data($is_update, $btn_action) {
        $data = array();
        if ($is_update) {
            
        } else {
            $data['chp4_main_ref'] = $this->encrypt->decode($this->input->post('main_form_id'));
            $data['chp4_f51'] = $this->input->post('ch4_client');
            $data['start_date'] = $this->input->post('ch4_start_time');
            $data['end_date'] = date('Y-m-d H:i:s');
        }
        $data['chp4_f52'] = $this->input->post('ch4_place');
        $data['chp4_f53'] = $this->input->post('ch4_st_time');
        $data['chp4_f54'] = $this->input->post('ch4_ed_time');
        $data['chp4_f55'] = $this->input->post('ch4_oview');
        $data['chp4_f56'] = $this->input->post('ch4_actions');
        $data['chp4_f57'] = $this->input->post('lst_eng');
        $data['chp4_f58'] = $this->input->post('ch4_lq_1');
        $data['chp4_f59'] = $this->input->post('ch4_lq_2');
        $data['chp4_f60'] = $this->input->post('ch4_lq_3');
        $data['chp4_f61'] = $this->input->post('ch4_lq_4');
        $data['chp4_f62'] = $this->input->post('ch4_lq_5');
        $data['chp4_f63'] = $this->input->post('ch4_lq_6');
        $data['chp4_f64'] = $this->input->post('ch4_lq_7');
        $data['chp4_f65'] = $this->input->post('ch4_lq_8');
        $data['chp4_f66'] = $this->input->post('ch4_ft_1');
        $data['chp4_f67'] = $this->input->post('ch4_ft_2');
        $data['chp4_f68'] = $this->input->post('ch4_ft_3');
        $data['chp4_f69'] = $this->input->post('ch4_nxt_app');
        $data['chp4_f70'] = $this->input->post('ch4_next_date');
        $data['chp4_f71'] = $this->input->post('ch4_flw_other');
        $data['chp4_f72'] = $this->input->post('ch4_app_chk');
        $data['chp4_f73'] = $this->input->post('ch3_invo_sent');
        $data['filled_by'] = $this->session->userdata('user_name');
        $data['frequency'] = $this->input->post('eng_freq');
        if ($this->input->post('sub_stat')!='Y'){
            $data['is_submit'] = $btn_action == 'frm_submit' ? 'Y' : 'N';
            $data['submit_date'] = $btn_action == 'frm_submit' ? date('Y-m-d H:i:s') : '';
        }
//        if ($this->session->userdata('role') == '4') {
//            $data['is_submit'] = $btn_action == 'frm_submit' ? 'Y' : 'N';
//            $data['submit_date'] = $btn_action == 'frm_submit' ? date('Y-m-d H:i:s') : '';
//        } else {
//            if ($this->input->post('sub_stat')) {
//                $data['is_submit'] = $this->input->post('sub_stat');
//                $data['submit_date'] = $this->input->post('sub_stat') == 'Y' ? date('Y-m-d H:i:s') : '';
//            } else {
//                $data['is_submit'] = $btn_action == 'frm_submit' ? 'Y' : 'N';
//                $data['submit_date'] = $btn_action == 'frm_submit' ? date('Y-m-d H:i:s') : '';
//            }
//        }
        $data['form_type'] = '9';
        return $data;
    }
    
    public function get_engagement_frequency(){
        $freq = json_encode($this->Coach_form_model->get_engagement_frequency($this->encrypt->decode($this->input->post('main_frm_id'))));
        echo $freq;
    }

    //-----------------------------end of form 4--------------------------------

    public function coach_form_5_create() {
        $is_valid = $this->coach_form_5_validation(FALSE);
        if ($is_valid) {
            $btn_action = $this->input->post('button_type');
            $valid_data = $this->coach_form_5_form_data(FALSE, $btn_action);
            $rslt = $this->Coach_form_model->insert_coach_form_5($valid_data);
            if ($rslt) {
                if ($btn_action == 'frm_save') {
                    $this->session->set_flashdata('create_success', 'success');
                    redirect(base_url('view_engagement/' . '10' . '/' . $this->encrypt->encode($rslt)));
                } else {
                    $this->session->set_flashdata('submit_success', 'success');
                    create_notification('6', '1', 'Submit Hinepreneur Coach Form Engagement 4 by ' . $this->session->userdata('user_name'));
                    add_activities($this->session->userdata('role'), $this->session->userdata('user_level_id'), 'Submit Hinepreneur Coach Form Engagement 4');
                    $this->index();
                }
            } else {
                $this->session->set_flashdata('error_msg', 'error');
                redirect(base_url('view_engagement/' . '10' . '/' . $this->encrypt->encode($rslt)));
            }
        } else {
            $this->view_engagement('10');
        }
    }

    //--------------------------------------------------------------------------

    public function coach_form_5_edit($form_id) {
        $is_valid = $this->coach_form_5_validation(TRUE);
        if ($is_valid) {
            $btn_action = $this->input->post('button_type');
            $valid_data = $this->coach_form_5_form_data(TRUE, $btn_action);
            $rslt = $this->Coach_form_model->update_coach_form_5($this->encrypt->decode($form_id), $valid_data);
            if ($rslt) {
                if ($btn_action == 'frm_edit') {
                    $this->session->set_flashdata('create_success', 'success');
                    redirect(base_url('view_engagement/' . '10' . '/' . $form_id));
                } else {
                    $this->session->set_flashdata('submit_success', 'success');
                    create_notification('6', '1', 'Submit Hinepreneur Coach Form Engagement 4 by ' . $this->session->userdata('user_name'));
                    add_activities($this->session->userdata('role'), $this->session->userdata('user_level_id'), 'Submit Hinepreneur Coach Form Engagement 4');
                    $this->index();
                }
            } else {
                $this->session->set_flashdata('error_msg', 'error');
                redirect(base_url('view_engagement/' . '10' . '/' . $form_id));
            }
        } else {
            $this->view_engagement('10');
        }
    }

    //--------------------------------------------------------------------------

    public function coach_form_5_validation($is_update) {
        if ($is_update) {
            
        } else {
            $this->form_validation->set_rules('ch5_client', 'client Name', 'trim|required');
        }

        $this->form_validation->set_rules('ch5_place', 'where did you meet', 'trim|required');
        $this->form_validation->set_rules('ch5_st_time', 'Start Time', 'trim|required');
        $this->form_validation->set_rules('ch5_ed_time', 'End Time', 'trim|required');
        $this->form_validation->set_rules('ch5_oview', 'general overview', 'trim|required');
        return $this->form_validation->run();
    }

    //--------------------------------------------------------------------------

    public function coach_form_5_form_data($is_update, $btn_action) {
        $data = array();
        if ($is_update) {
            
        } else {
            $data['chp5_main_ref'] = $this->encrypt->decode($this->input->post('main_form_id'));
            $data['chp5_f74'] = $this->input->post('ch5_client');
            $data['start_date'] = $this->input->post('ch5_start_time');
            $data['end_date'] = date('Y-m-d H:i:s');
        }
        $data['chp5_f75'] = $this->input->post('ch5_place');
        $data['chp5_f77'] = $this->input->post('ch5_st_time');
        $data['chp5_f78'] = $this->input->post('ch5_ed_time');
        $data['chp5_f79'] = $this->input->post('ch5_oview');
        $data['chp5_f80'] = $this->input->post('completed_list') ? implode(",", $this->input->post('completed_list')) : '';
        $data['chp5_f81'] = $this->input->post('tr_dev');
        $data['chp5_f82'] = $this->input->post('follow_up');
        $data['chp5_f83'] = $this->input->post('sel_list');
        $data['chp5_f84'] = $this->input->post('eng_nature');
        $data['chp5_f85'] = $this->input->post('other_pnts');
        $data['chp5_f86'] = $this->input->post('sel_list2');
        $data['chp5_f87'] = $this->input->post('ch5_invo_sent');
        $data['filled_by'] = $this->session->userdata('user_name');
        if ($this->input->post('sub_stat')!='Y'){
            $data['is_submit'] = $btn_action == 'frm_submit' ? 'Y' : 'N';
            $data['submit_date'] = $btn_action == 'frm_submit' ? date('Y-m-d H:i:s') : '';
        }
//        if ($this->session->userdata('role') == '4') {
//            $data['is_submit'] = $btn_action == 'frm_submit' ? 'Y' : 'N';
//            $data['submit_date'] = $btn_action == 'frm_submit' ? date('Y-m-d H:i:s') : '';
//        } else {
//            if ($this->input->post('sub_stat')) {
//                $data['is_submit'] = $this->input->post('sub_stat');
//                $data['submit_date'] = $this->input->post('sub_stat') == 'Y' ? date('Y-m-d H:i:s') : '';
//            } else {
//                $data['is_submit'] = $btn_action == 'frm_submit' ? 'Y' : 'N';
//                $data['submit_date'] = $btn_action == 'frm_submit' ? date('Y-m-d H:i:s') : '';
//            }
//        }
        $data['form_type'] = '10';
        return $data;
    }

    //-----------------------------end of form 5--------------------------------

    public function check_main_form_available_p1() {
        $main_form_id = json_encode($this->Coach_form_model->get_main_form_id($this->input->post('uid')));
        echo $main_form_id;
    }

    public function check_main_form_available_p2() {
        $main_form_id = json_encode($this->Coach_form_model->get_main_form_reference($this->input->post('form_id'), $this->input->post('uid')));
        echo $main_form_id;
    }
    
    public function check_main_form_available_p3() {
        $main_form_id = json_encode($this->Coach_form_model->get_main_form_ref_for_engagement3($this->input->post('uid')));
        echo $main_form_id;
    }

    //--------------------------------------------------------------------------

    public function submit_form($form_type, $form_id) {
        $result = json_encode($this->Coach_form_model->update_form_as_submit($form_type, $this->encrypt->decode($form_id)));
        if ($result) {
            switch ($form_type) {
                case '5':create_notification('6', '1', 'Submit Hinepreneur Coach Form Engagement 1 by ' . $this->session->userdata('user_name'));
                    add_activities($this->session->userdata('role'), $this->session->userdata('user_level_id'), 'Submit Hinepreneur Coach Form Engagement 1');
                    break;
                case '8':create_notification('6', '1', 'Submit Hinepreneur Coach Form Engagement 2 by ' . $this->session->userdata('user_name'));
                    add_activities($this->session->userdata('role'), $this->session->userdata('user_level_id'), 'Submit Hinepreneur Coach Form Engagement 2');
                    break;
                case '9':create_notification('6', '1', 'Submit Hinepreneur Coach Form Engagement 3 by ' . $this->session->userdata('user_name'));
                    add_activities($this->session->userdata('role'), $this->session->userdata('user_level_id'), 'Submit Hinepreneur Coach Form Engagement 3');
                    break;
                case '10':create_notification('6', '1', 'Submit Hinepreneur Coach Form Engagement 4 by ' . $this->session->userdata('user_name'));
                    add_activities($this->session->userdata('role'), $this->session->userdata('user_level_id'), 'Submit Hinepreneur Coach Form Engagement 4');
                    break;
            }
        }
        echo $result;
    }

    //--------------------------------------------------------------------------

    public function view_single_form_pdf($form_type, $form_id) {
        if ($form_id != 'FALSE') {
            $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('MWDI');
            $pdf->SetFont('helvetica', '', 10);

            // set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

            // set auto page breaks
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            $pdf->setJPEGQuality(100);
            $pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.1, 'depth_h' => 0, 'color' => array(255, 255, 255), 'opacity' => 1, 'blend_mode' => 'Normal'));
            $pdf->AddPage();
            $out_put_name = "Client_Form";
            switch ($form_type) {
                case '5':
                    $data['details'] = json_encode($this->Coach_form_model->get_single_coach_form_1($this->encrypt->decode($form_id)));
                    $html = $this->load->view('coach_form/coach_form_1_view_modal', $data, TRUE);
                    $pdf->SetTitle('Client Form Engagement 1');
                    $out_put_name = 'client_form_engagement_1';
                    break;
                case '8':
                    $data['details'] = json_encode($this->Coach_form_model->get_single_coach_form_3($this->encrypt->decode($form_id)));
                    $html = $this->load->view('coach_form/coach_form_3_view_modal', $data, TRUE);
                    $pdf->SetTitle('Client Form Engagement 2');
                    $out_put_name = 'client_form_engagement_2';
                    break;
                case '9':
                    $rtn_temp_arr = $this->Coach_form_model->get_single_coach_form_4($this->encrypt->decode($form_id));
                    $data['details'] = json_encode($rtn_temp_arr);
                    $html = $this->load->view('coach_form/coach_form_4_view_modal', $data, TRUE);
                    $pdf->SetTitle('Client Form Engagement'.' '.$rtn_temp_arr->frequency);
                    $out_put_name = 'client_form_engagement_'.' '.$rtn_temp_arr->frequency;
                    break;
                case '10':
                    $data['details'] = json_encode($this->Coach_form_model->get_single_coach_form_5($this->encrypt->decode($form_id)));
                    $html = $this->load->view('coach_form/coach_form_5_view_modal', $data, TRUE);
                    $pdf->SetTitle('Client Form Engagement 4');
                    $out_put_name = 'client_form_engagement_4';
                    break;
            }
            $pdf->writeHTML($html, true, false, true, false, '');
            $pdf->lastPage();
            $pdf->Output($out_put_name.'.pdf', 'I');
        } else {
            $this->session->set_flashdata('pdf_error', 'error');
            redirect('view_engagement/' . $form_type, 'refresh');
        }
    }

}
