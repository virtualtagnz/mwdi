<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Assignment extends CI_Controller {

    public $user_type;

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('uid')) {
            redirect(base_url('Auth'));
        }
        $this->user_type = $this->encrypt->decode(end($this->uri->segments));
    }

    //--------------------------------------------------------------------------

    public function index() {
        if ($this->user_type == '4') {
            $data['currentPage'] = "assign_resource_coach";
            $data['users'] = $this->System_user_model->gel_all_users($this->user_type);
        } else {
            $data['currentPage'] = "assign_resource_client";
            $data['users'] = $this->System_user_model->gel_all_users($this->user_type);
        }
        $data['mainContent'] = "assign/assign_resource_user";
        $this->load->view('includes/frame', $data);
    }

    //--------------------------------------------------------------------------

    public function assign_application_forms() {
        if ($this->user_type == '4') {
            $data['currentPage'] = "assign_application_forms_coach";
            $data['users'] = $this->System_user_model->gel_all_users($this->user_type);
        } else {
            $data['currentPage'] = "assign_application_forms_client";
            $data['users'] = $this->System_user_model->gel_all_users($this->user_type);
        }
        $data['mainContent'] = "assign/assign_forms_user";
        $this->load->view('includes/frame', $data);
    }

    //--------------------------------------------------------------------------

    public function assign_forms() {
        $this->form_validation->set_rules('user_select', 'User', 'trim|required');
        $this->form_validation->set_rules('res_select', 'Forms', 'trim|required');
        if ($this->form_validation->run()) {
            $this->index();
        } else {
            $data = array();
            $selected_forms = array();
            $selected_forms = $this->input->post('res_select');
            $i = 0;
            foreach ($selected_forms as $selected_form) {
                $data[$i]['ares_user'] = $this->encrypt->decode($this->input->post('user_select'));
                $data[$i]['ares_user_role'] = $this->user_type;
                $data[$i]['ares_res_id'] = $selected_form;
                $data[$i]['assigned_by'] = $this->session->userdata('uid');
                $data[$i]['assigned_role'] = $this->session->userdata('role');
                $data[$i]['assigned_date'] = date('Y-m-d H:i:s');
                $i++;
            }
            $rtn = $this->Assignment_model->insert($data);
            echo($rtn) ? $this->session->set_flashdata('successfully_assigned', 'success') : $this->session->set_flashdata('error_msg', 'error');
            redirect(base_url('view_assigned_forms/' . $this->encrypt->encode($this->user_type)));
        }
    }

    //--------------------------------------------------------------------------

    public function load_form_list($uid) {
        $data = $this->Assignment_model->load_forms($this->encrypt->decode($uid));
        echo json_encode($data);
    }

    public function assigned_resources() {
        if ($this->user_type == '4') {
            $data['currentPage'] = "assign_resource_coach";
            $data['users'] = $this->System_user_model->gel_all_users($this->user_type);
        } else {
            $data['currentPage'] = "assign_resource_client";
            $data['users'] = $this->System_user_model->gel_all_users($this->user_type);
        }
        $data['assigned_resource'] = json_encode($this->Assignment_model->assigned_forms($this->user_type));
        $data['mainContent'] = "assign/veiw_assigned_resources";
        $this->load->view('includes/frame', $data);
    }

    //--------------------------------------------------------------------------

    public function view_resouces_assigned_to_me($my_id) {
        $data['currentPage'] = "assign_resource_coach";
        $data['mainContent'] = "assign/veiw_assigned_resources";
        $this->load->view('includes/frame', $data);
    }

    //--------------------------------------------------------------------------

    public function view_assigned_forms() {
        if ($this->user_type == '4') {
            $data['currentPage'] = "assign_application_forms_coach";
            $data['users'] = $this->System_user_model->gel_all_users($this->user_type);
        } else {
            $data['currentPage'] = "assign_application_forms_client";
            $data['users'] = $this->System_user_model->gel_all_users($this->user_type);
        }
        $data['assigned_resource'] = json_encode($this->Assignment_model->assigned_application_forms($this->user_type));
        $data['mainContent'] = "assign/view_assigned_forms";
        $this->load->view('includes/frame', $data);
    }

    //--------------------------------------------------------------------------

    public function load_application_form_list($uid) {
        $data = $this->Assignment_model->load_unselected_forms($this->encrypt->decode($uid));
        echo json_encode($data);
    }

    //--------------------------------------------------------------------------

    public function assign_application_forms_to_users() {
        $this->form_validation->set_rules('user_select', 'User', 'trim|required');
        $this->form_validation->set_rules('res_select', 'Forms', 'trim|required');
        if ($this->form_validation->run()) {
            $this->index();
        } else {
            $data = array();
            $selected_forms = $this->input->post('res_select');
            $i = 0;
            foreach ($selected_forms as $selected_form) {
                $data[$i]['ch_coach_id'] = $this->encrypt->decode($this->input->post('user_select'));
                $data[$i]['ch_frm_id'] = $selected_form;
                $data[$i]['assign_date'] = date('Y-m-d H:i:s');
                $data[$i]['assign_by'] = $this->session->userdata('uid');
                $data[$i]['role_assign'] = $this->session->userdata('role');
                $i++;
            }
            $rtn = $this->Assignment_model->insert_to_coach_forms($data);
            if ($rtn) {
                create_notification($this->user_type, $this->encrypt->decode($this->input->post('user_select')), 'You have ' . count($selected_forms) . ' Forms Assigned By ' . $this->session->userdata('user_name'));
                $this->session->set_flashdata('successfully_assigned', 'success');
            } else {
                $this->session->set_flashdata('error_msg', 'error');
            }
            //echo($rtn) ? $this->session->set_flashdata('successfully_assigned', 'success') : $this->session->set_flashdata('error_msg', 'error');
            redirect(base_url('view_assigned_application_forms/' . $this->encrypt->encode($this->user_type)));
        }
    }

    //--------------------------------------------------------------------------

    public function get_application_forms_assigned_to_user($user_id) {
        $data['my_forms'] = json_encode($this->Assignment_model->assigned_application_forms($this->user_type, $this->encrypt->decode($user_id)));
        $data['archives'] = json_encode($this->Coach_form_model->get_all_coach_form());
        $data['currentPage'] = "my_forms";
        $data['mainContent'] = "assign/view_forms_assigned_to_me";
        $this->load->view('includes/frame', $data);
    }

    //--------------------------------------------------------------------------

    public function client_to_coach() {
        $data['coches'] = $this->System_user_model->gel_all_users('4');
        $data['clients'] = json_encode($this->System_user_model->load_unselected_clients());
        $data['currentPage'] = "assign_client_to_coach";
        $data['mainContent'] = "assign/assign_client_to_coach";
        $this->load->view('includes/frame', $data);
    }

    //--------------------------------------------------------------------------

    public function assign_clients_to_coach() {
        $this->form_validation->set_rules('selected_coach', 'select coach', 'trim|required');
        $this->form_validation->set_rules('selected_client', 'select client', 'trim|required');
        if ($this->form_validation->run()) {
            $this->client_to_coach();
        } else {
            $data = array();
            $selected_clients = $this->input->post('selected_client');
            $i = 0;
            foreach ($selected_clients as $selected_client) {
                $data[$i]['coach_ref'] = $this->encrypt->decode($this->input->post('selected_coach'));
                $data[$i]['client_ref'] = $this->encrypt->decode($selected_client);
                $data[$i]['assign_by'] = $this->session->userdata('user_name');
                $data[$i]['assign_date'] = date('Y-m_d H:i:s');
                $data[$i]['assign_role'] = $this->session->userdata('role');
                $i++;
            }
            $rtn = $this->Assignment_model->insert_to_coach_client_rel($data);
            echo($rtn) ? $this->session->set_flashdata('successfully_assigned', 'success') : $this->session->set_flashdata('error_msg', 'error');
            redirect(base_url('view_assigned_clients'));
        }
    }

    //--------------------------------------------------------------------------

    public function view_my_users($my_id) {
        $list_of_my_users = json_encode($this->System_user_model->get_my_users($this->encrypt->decode($my_id)));
        echo $list_of_my_users;
    }

    //--------------------------------------------------------------------------

    public function get_assigned_client_list() {
        $data['users_list'] = json_encode($this->Assignment_model->get_all_client_coach_rel());
        $data['currentPage'] = "assign_client_to_coach";
        $data['mainContent'] = "assign/view_assigned_clients";
        $this->load->view('includes/frame', $data);
    }

    //--------------------------------------------------------------------------

    public function view_my_resource() {
        $data['my_resources'] = json_encode($this->Assignment_model->get_resources_by_user_id());
        $data['currentPage'] = "resources";
        $data['mainContent'] = "assign/view_resouces_assigned_to_me";
        $this->load->view('includes/frame', $data);
    }

    //--------------------------------------------------------------------------

    public function link_reassign_coaches() {
        $data['coches'] = $this->System_user_model->gel_all_users('4');
        $data['currentPage'] = "assign_client_to_coach";
        $data['mainContent'] = "assign/reassign_coaches";
        $this->load->view('includes/frame', $data);
    }

    //--------------------------------------------------------------------------

    public function reassign_coaches() {
        $this->form_validation->set_rules('curr_coach', 'coach', 'trim|required');
        $this->form_validation->set_rules('belongs_client', 'clients belongs this coach', 'trim');
        $this->form_validation->set_rules('reassign_coach', 'coach for reassign', 'trim|required|callback_same_coach');
        if ($this->form_validation->run() == FALSE) {
            $this->link_reassign_coaches();
        } else {
            ;
            $data = array();
            $data2 = array();
            $current_coach = $this->encrypt->decode($this->input->post('curr_coach'));
            $reass_coach = $this->encrypt->decode($this->input->post('reassign_coach'));
            $selected_clients = $this->input->post('belongs_client');
            $i = 0;
            foreach ($selected_clients as $selected_client) {
                $data[$i]['tab_id'] = $this->Assignment_model->get_tab_id_from_coach_rel($current_coach, $selected_client);
                $data[$i]['coach_ref'] = $reass_coach;
                $data[$i]['date_mod'] = $this->session->userdata('user_name');
                $data[$i]['mod_by'] = date('Y-m_d H:i:s');
                $data[$i]['mod_role'] = $this->session->userdata('role');
                $data[$i]['previous_coah'] = $current_coach;
                $i++;
            }
            $x = 0;
            foreach ($selected_clients as $selected_client) {
                $rtn_id = $this->Assignment_model->get_coach_form_id($current_coach, $selected_client);
                if ($rtn_id) {
                    $data2[$x]['chf_id'] = $this->Assignment_model->get_coach_form_id($current_coach, $selected_client);
                    $data2[$x]['chf_ch_ref'] = $reass_coach;
                    $x++;
                }
            }
            $rtn_1 = $this->Assignment_model->update_client_coach_rel($data);
            if ($data2) {
                $rtn_2 = $this->Assignment_model->update_main_coach_form_coach_id($data2);
            }
            echo($rtn_1) ? $this->session->set_flashdata('success_mag', 'success') : $this->session->set_flashdata('error_msg', 'error');
            redirect(base_url('reassign_coaches/'));
        }
    }

    //--------------------------------------------------------------------------

    public function view_reassign_coaches() {
        
    }

    //--------------------------------------------------------------------------

    public function same_coach() {
        $coach_1 = $this->encrypt->decode($this->input->post('curr_coach'));
        $coach_2 = $this->encrypt->decode($this->input->post('reassign_coach'));
        if ($coach_1 == $coach_2) {
            $this->form_validation->set_message('same_coach', 'Same coach selected, Please select another coach to reassign!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

}
