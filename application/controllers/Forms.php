<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Forms extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('uid')) {
            redirect(base_url('Auth'));
        }
    }
    
    public function index() {
        $data['my_forms'] = $this->Assignment_model->assigned_application_forms($this->session->userdata('role'),$this->session->userdata('user_level_id'));
        $data['ppms_submitted'] = $this->Ppms_form_model->get_all_submitted_ppms_forms();
        $data['security_submitted'] = $this->Security_form_model->get_all_submitted_security_form();
        $data['ba_submitted'] = $this->BAForm_model->get_all_submitted_BA_form();
        $data['currentPage'] = "clients_forms";
        $data['mainContent'] = "clients_forms/view_submitted_clients_forms";
        $this->load->view('includes/frame', $data);
    }
}

