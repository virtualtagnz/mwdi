<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class System_users extends CI_Controller {

    public $user_type;
    public $img;

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('uid')) {
            redirect(base_url('Auth'));
        }
        $this->user_type = $this->encrypt->decode(end($this->uri->segments));
    }

    public function index() {
        switch ($this->user_type) {
            case '2':$data['currentPage'] = "users_admin";
                break;
            case '3':$data['currentPage'] = "users_account";
                break;
            case '4':$data['currentPage'] = "users_coach";
                break;
            case '5':$data['currentPage'] = "users_client";
                break;
        }
        $data['users'] = $this->System_user_model->gel_all_users($this->user_type);
        $data['mainContent'] = "users/view_user";
        $this->load->view('includes/frame', $data);
    }

    public function create_user($temp_client_id = FALSE) {
        switch ($this->user_type) {
            case '2':$data['currentPage'] = "users_admin";
                break;
            case '3':$data['currentPage'] = "users_account";
                break;
            case '4':$data['currentPage'] = "users_coach";
                break;
            case '5':$data['currentPage'] = "users_client";
                break;
        }

        if($temp_client_id){
            $this->load->model('Loan_enquire_model');
            $data['temp_client_info'] = $this->Loan_enquire_model->get_loan_enquire_form($this->encrypt->decode($temp_client_id));
        }

        $data['mainContent'] = "users/create_user";
        $this->load->view('includes/frame', $data);
    }

    public function edit_user($user_id) {
        switch ($this->user_type) {
            case '1':$data['currentPage'] = "users_admin";
                break;
            case '2':$data['currentPage'] = "users_admin";
                break;
            case '3':$data['currentPage'] = "users_account";
                break;
            case '4':$data['currentPage'] = "users_coach";
                break;
            case '5':$data['currentPage'] = "users_client";
                break;
        }
        $data['single_user'] = $this->System_user_model->get_single_user($this->encrypt->decode($user_id), $this->user_type);
        $data['mainContent'] = "users/edit_user";
        $this->load->view('includes/frame', $data);
    }

    public function static_user_view($user_id) {
        switch ($this->user_type) {
            case '2':$data['currentPage'] = "users_admin";
                break;
            case '3':$data['currentPage'] = "users_account";
                break;
            case '4':$data['currentPage'] = "users_coach";
                break;
            case '5':$data['currentPage'] = "users_client";
                break;
        }
        $data['single_user'] = $this->System_user_model->get_single_user($this->encrypt->decode($user_id), $this->user_type);
        if ($this->user_type == '5') {
            $data['user_files'] = json_encode($this->File_attachment_model->get_file_attachment_by_client_id($data['single_user']->client_id));
            $data['user_comments'] = json_encode($this->Comment_model->get_comment_by_id($data['single_user']->client_id));
            $data['check_list'] = $this->Check_list_model->get_checklist_items($data['single_user']->client_id);

            $main_form_id = $this->Dashboard_model->get_main_ba_form_id($data['single_user']->client_id);
            $main_security_form_id = $this->Dashboard_model->get_main_security_form_id($data['single_user']->client_id);

            //BA FORM PROGRESS
            $data['main_ba_progress'] = Calculate_form_progress($this->Dashboard_model->get_ba_form_se1($main_form_id));
            $data['edu_progress'] = Calculate_form_progress($this->Dashboard_model->get_ba_form_sec2($main_form_id));
            $data['nxt_kin_progress'] = Calculate_form_progress($this->Dashboard_model->get_ba_form_sec3($main_form_id));
            $data['emp_progress'] = Calculate_form_progress($this->Dashboard_model->get_ba_form_sec4($main_form_id));
            $data['biz_progress'] = Calculate_form_progress($this->Dashboard_model->get_ba_form_sec5($main_form_id));
            $data['bk_progress'] = Calculate_form_progress($this->Dashboard_model->get_ba_form_sec6($main_form_id));

            //SECURITY FORM PROGRESS
            $data['main_sec_progress'] = Calculate_form_progress($this->Dashboard_model->get_security_form_sec1($main_security_form_id));
            $data['sec_vehicle_progress'] = Calculate_form_progress($this->Dashboard_model->get_security_form_sec2($main_security_form_id));
            $data['sec_property_progress'] = Calculate_form_progress($this->Dashboard_model->get_security_form_sec3($main_security_form_id));
            $data['sec_chattles_progress'] = Calculate_form_progress($this->Dashboard_model->get_security_form_sec4($main_security_form_id));
        }
        $data['mainContent'] = "users/static_user_view";
        $this->load->view('includes/frame', $data);
    }

    public function create() {
        $is_valid = $this->form_validation(FALSE);
        if ($is_valid) {
            $valid_data = $this->get_form_data(FALSE);
            $user_id = $this->System_user_model->insert($valid_data[0], $valid_data[1], $this->user_type);
            if ($user_id) {

                if($this->input->post('create_type')=='convert_client'){
                    $this->load->model('Loan_enquire_model');
                    $this->Loan_enquire_model->delete_loan_form($this->encrypt->decode($this->input->post('temp_client_id')));
                }
                
                $return_val = $this->System_user_model->get_single_user($user_id, $this->user_type);
                $email_data['to'] = $return_val->user_email;
                $email_data['subject'] = 'Registration Notification';
                $template_data['name'] = $return_val->first_name . ' ' . $return_val->last_name;
                $template_data['user_name'] = $return_val->user_name;
                $template_data['new_pwd'] = $return_val->pwd;
                $template_data['user_type'] = $this->user_type;
                $email_data['msg'] = $this->load->view('email_templates/registration_notification_template', $template_data, TRUE);
                $email_send_rslt = send_emails($email_data);
                echo($email_send_rslt) ? $this->session->set_flashdata('create_success', 'success') : $this->session->set_flashdata('error_msg', 'error');
                redirect(base_url('edit_user/' . $this->encrypt->encode($return_val->user_id) . '/' . $this->encrypt->encode($return_val->role_id)));
            }
        } else {
            $this->create_user();
        }
    }

    public function edit($user_id) {
        $is_valid = $this->form_validation(TRUE);
        if ($is_valid) {
            $valid_data = $this->get_form_data(TRUE);
            $reslt = $this->System_user_model->update($valid_data[0], $valid_data[1], $this->user_type, $this->encrypt->decode($user_id));
            echo($reslt) ? $this->session->set_flashdata('update_success', 'success') : $this->session->set_flashdata('error_msg', 'error');
            redirect(base_url('edit_user/' . $user_id . '/' . $this->encrypt->encode($this->user_type)));
        } else {
            $this->edit_user($user_id);
        }
    }

    public function form_validation($is_update) {
        if ($is_update) {
            if ($this->input->post('current_prof_img')) {
                $this->img = $this->input->post('current_prof_img');
            }
            if ($this->input->post('pwd')) {
                $this->form_validation->set_rules('confirm_pwd', 'Confirm Password', 'trim|required');
            }
        } else {
            $this->form_validation->set_rules('username', 'User Name', 'trim|required|is_unique[user_tb.user_name]');
            $this->form_validation->set_message('is_unique', 'This user name is already registered, please check again!');
            $this->form_validation->set_rules('pwd', 'Password', 'trim|required');
            if ($this->input->post('pwd')) {
                $this->form_validation->set_rules('confirm_pwd', 'Confirm Password', 'trim|required');
            }
        }
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('status', 'Status', 'trim|required');
        $this->form_validation->set_rules('fname', 'First Name', 'trim|required');
        $this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
        //$this->form_validation->set_rules('dob', 'Date of Birth', 'trim|required');
        $this->form_validation->set_rules('gender', 'Gender', 'trim|required');
        //$this->form_validation->set_rules('phone', 'Land Phone', 'trim|required');
        //$this->form_validation->set_rules('mobile', 'Mobile', 'trim|required');
        if ($this->user_type == '5') {
            $this->form_validation->set_rules('address', 'Address', 'trim|required');
            $this->form_validation->set_rules('industry', 'Industry', 'trim|required');
        }
        if (!empty($_FILES['prof_image']['name'])) {
            $this->form_validation->set_rules('prof_image', 'Profile Image', 'trim|callback_img_check');
        }
        return $this->form_validation->run();
    }

    public function get_form_data($is_update) {
        $data = array();
        $info = array();

        $data['role_id'] = $this->user_type;
        $data['user_email'] = $this->input->post('email');
        $data['status'] = $this->input->post('status');
        $info['first_name'] = $this->input->post('fname');
        $info['last_name'] = $this->input->post('lname');
        $fixDate = explode('/', trim($this->input->post('dob')));
        $info['dob'] = $fixDate[2] . '-' . $fixDate[1] . '-' . $fixDate[0];
        //$info['dob'] = date('Y-d-m', strtotime(trim($this->input->post('dob'))));
        $info['gender'] = $this->input->post('gender');
        $info['phone'] = $this->input->post('phone');
        $info['mobile'] = $this->input->post('mobile');
        $info['address'] = $this->input->post('address');
        $info['add_street_no'] = $this->input->post('street_number');
        $info['add_street_name'] = $this->input->post('route');
        $info['add_city'] = $this->input->post('locality');
        $info['add_post_code'] = $this->input->post('postal_code');
        $info['add_region'] = $this->input->post('administrative_area_level_1');
        $info['image_link'] = $this->img;
        $info['description'] = $this->input->post('description');
        if ($this->user_type == '5') {
            $info['industry'] = $this->input->post('industry');
            $info['client_category'] = $this->input->post('client_category');
        }
        if ($is_update) {
            if ($this->input->post('pwd')) {
                $data['pwd'] = $this->input->post('pwd');
            }
            $data['date_mod'] = date("Y-m-d H:i:s");
            $data['role_mod'] = $this->session->userdata('role');
            $data['code_mod'] = $this->session->userdata('uid');
            $info['date_mod'] = date("Y-m-d H:i:s");
            $info['role_mod'] = $this->session->userdata('role');
            $info['code_mod'] = $this->session->userdata('uid');
        } else {
            $data['user_name'] = $this->input->post('username');
            $data['pwd'] = $this->input->post('pwd');
            $data['date_create'] = date("Y-m-d H:i:s");
            $data['role_carete'] = $this->session->userdata('role');
            $data['code_create'] = $this->session->userdata('uid');
            $info['date_create'] = date("Y-m-d H:i:s");
            $info['role_create'] = $this->session->userdata('role');
            $info['code_create'] = $this->session->userdata('uid');
        }
        return array($data, $info);
    }

    public function img_check() {
        $path = realpath((APPPATH . '../public_html/uploads/profile_image'));
        $image_upload_rslt = upload_files($path, 'prof_image', 'image', 120);
        if ($image_upload_rslt['stat']) {
            $this->img = $image_upload_rslt['fileArr']['file_name'];
            return TRUE;
        } else {
            $this->form_validation->set_message('img_check', $image_upload_rslt['fileError']);
            return FALSE;
        }
    }

    public function deactivate_users($uid) {
        if ($this->System_user_model->update_status($this->encrypt->decode($uid))) {
            echo json_encode(array('status' => '1'));
        } else {
            echo json_encode(array('status' => '0'));
        }
    }

    public function activate_users($uid) {
        if ($this->System_user_model->update_status($this->encrypt->decode($uid), true)) {
            echo json_encode(array('status' => '1'));
        } else {
            echo json_encode(array('status' => '0'));
        }
    }
    
    public function mark_as_start_work_with_client() {
        $data['strat_work'] = TRUE;
        $data['start_date'] = date('Y-m-d H:i:s');
        if($this->System_user_model->update_as_start_work($this->input->post('uid'), $data)){
            echo json_encode('success');
        }  else {
            echo json_encode('error');
        }
    }

}
