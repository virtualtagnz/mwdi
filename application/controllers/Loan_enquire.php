<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_enquire extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('Reports_model');
        $this->load->model('Loan_enquire_model');
    }

    public function loan_enquire_report() {
        if (!$this->session->userdata('uid')) {
            redirect(base_url('Auth'));
        }

        $data['enquires'] = $this->Loan_enquire_model->get_loan_enquire_list();

        $data['currentPage'] = "report_temp";
        $data['mainContent'] = "reports/loan_enquire";
        $this->load->view('includes/frame', $data);
    }

    public function loan_enquire_form() {
        $regions = $this->Reports_model->get_client_region_summary();
        $this->form_validation->set_rules('name', 'Name', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim');
        $this->form_validation->set_rules('region', 'Region', 'required|trim');
        $this->form_validation->set_rules('industry', 'Industry', 'required|trim');

        if ($this->form_validation->run() == FALSE) {
            $data['regions'] = $regions;
            $data['currentPage'] = "loan_enquiry";
            $data['mainContent'] = "loan_enquiry/create_out";
            $this->load->view('includes/create_out_frame', $data);
        } else {
            $string_data['name'] = $this->input->post('name');
            $string_data['email'] = strtolower($this->input->post('email'));
            $string_data['region'] = $this->input->post('region');
            $string_data['industry'] = $this->input->post('industry');
            $string_data['created_date'] = date('Y-m-d H:i:s');

            $result = $this->Loan_enquire_model->add_loan_form($string_data);

            if ($result) {
                $email_data = array();
                $email_data['to'] = 'mwdi@mwdi.co.nz';//'mwdi@mwdi.co.nz';
                $email_data['subject'] = 'Loan Enquiry Submitted';
                $email_data['msg'] = $this->load->view('email_templates/loan_equiry_admin', array(), TRUE);
                send_emails($email_data);

                $email_data = array();
                $email_data['to'] = $string_data['email'];
                $email_data['subject'] = 'MWDI | Loan Enquiry Submitted';
                $email_data['msg'] = $this->load->view('email_templates/loan_equiry_client', array('name' => $string_data['name']), TRUE);
                if($this->input->post('attach_forms')){
                    $file_arr = array();
                    array_push($file_arr, $_SERVER['DOCUMENT_ROOT'] . 'downloads/Application.pdf');
                    array_push($file_arr, $_SERVER['DOCUMENT_ROOT'] . 'downloads/Business Criteria.pdf');
                    $email_data['files_attachments'] = $file_arr;
                }
                send_emails($email_data);
                $this->session->set_flashdata('success','success');
                redirect('loan/enquire/form');
            } else {
                $this->session->set_flashdata('result', 'error');
                redirect('loan/enquire/form');
            }
        }
    }

    public function loan_enquire_ajax() {
        if (!$this->session->userdata('uid')) {
            redirect(base_url('Auth'));
        }

        $uid = $this->input->post('uid');
        if ($uid) {
            $this->Loan_enquire_model->delete_loan_form($uid);
            echo json_encode(array('status' => true));
        }
    }

    public function loan_enquire_import() {
        /* if (!$this->session->userdata('uid')) {
          redirect(base_url('Auth'));
          }

          $client_enq_form_temp_tb = $this->Reports_model->get_temp_enquires_list();
          if($client_enq_form_temp_tb) {
          foreach($client_enq_form_temp_tb as $client) {
          $string_data = array();
          $string_data['name'] = $client->u_name;
          $string_data['email'] = strtolower($client->enq_mail);
          $string_data['region'] = $client->enq_biz_add_reg;
          $string_data['industry'] = $client->enq_insd;
          $string_data['created_date'] = $client->start_date;
          $string_data['old'] = "1";
          $this->Loan_enquire_model->add_loan_form($string_data);
          }
          }

          echo 'OK'; */
        //debug($client_enq_form_temp_tb);
    }

    public function mark_as_answered($uid) {
        if (!$this->session->userdata('uid')) {
            redirect(base_url('Auth'));
        }

        if ($this->Loan_enquire_model->mark_as_answered($uid, $this->session->userdata('uid')))
            $response = 'success';
        else
            $response = 'error';
        echo json_encode($response);
    }

    public function print_full_form() {
        if (!$this->session->userdata('uid')) {
            redirect(base_url('Auth'));
        }
        ini_set('memory_limit', '2048M');
        set_time_limit(0);

        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);

        $pdf->footer_align = 190;
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('MWDI');
        $pdf->SetFont('helvetica', '', 9);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->setJPEGQuality(100);
        $pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.1, 'depth_h' => 0, 'color' => array(255, 255, 255), 'opacity' => 1, 'blend_mode' => 'Normal'));
        //$pdf->AddPage('L', 'A4');
        $pdf->AddPage('L', 'A4');


        $data['templist'] = $this->Loan_enquire_model->get_loan_enquire_list();
        $html = $this->load->view('loan_enquiry/print_list', $data, TRUE);

        $pdf->SetTitle('Loan Enquires Form');

        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->lastPage();
        $pdf->Output('Loan_Enquires_Form_' . date("ymdhis") . '.pdf', 'I');
    }

}

/* End of file loan_enquiry.php */
/* Location: ./application/controllers/loan_enquire.php */