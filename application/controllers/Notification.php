<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('uid')) {
            redirect(base_url('Auth'));
        }
    }

    //--------------------------------------------------------------------------

    public function index() {
        $data['notifications'] = json_encode($this->Notification_model->get_all_notifications());
        //$data['notifications'] = json_encode($this->re_arrange_notifications());
        $data['currentPage'] = "notification";
        $data['mainContent'] = "notification/notifications";
        $this->load->view('includes/frame', $data);
    }

    public function mark_selected_as_read() {
        $recive_arr = json_decode($this->input->post('json_str'));
        $notification_data = array();
        if ($recive_arr) {
            $i = 0;
            foreach ($recive_arr as $value) {
                $notification_data[$i]['nf_id'] = $value;
                switch ($this->session->userdata('role')) {
                    case '1':$notification_data[$i]['super_mark'] = 'Y';
                        break;
                    case '2':$notification_data[$i]['admin_mark'] = 'Y';
                        break;
                    case '3':$notification_data[$i]['account_mark'] = 'Y';
                        break;
                    case '4':$notification_data[$i]['coach_mark'] = 'Y';
                        break;
                    case '5':$notification_data[$i]['client_mark'] = 'Y';
                        break;
                }
                $notification_data[$i]['date_mark'] = date('Y-m-d H;i:s');
                $i++;
            }
        }
        $rtn = $this->Notification_model->update_selected_notifications($notification_data);
        echo $rtn;
    }

    public function mark_all_as_read() {
        $notification_arr = array();
        switch ($this->session->userdata('role')) {
            case '1':$notification_arr['super_mark'] = 'Y';
                break;
            case '2':$notification_arr['admin_mark'] = 'Y';
                break;
            case '3':$notification_arr['account_mark'] = 'Y';
                break;
            case '4':$notification_arr['coach_mark'] = 'Y';
                break;
            case '5':$notification_arr['client_mark'] = 'Y';
                break;
        }
        $rtn = $this->Notification_model->update_all_notifications($notification_arr);
        echo $rtn;
    }

    public function notification_check() {
        $recent_cnt = $this->Notification_model->get_notification_cnt($this->session->userdata('role'));
        echo json_encode($recent_cnt);
    }

    public function increase_current_notification_count() {
        if ($this->session->userdata('my_notification')) {
            $this->session->unset_userdata('my_notification');
            $data_array['my_notification'] = $this->Notification_model->get_notification_cnt($this->session->userdata('role'));
            $this->session->set_userdata($data_array);
            echo json_encode($data_array['my_notification']);
        }  else {
            $data_array['my_notification'] = $this->Notification_model->get_notification_cnt($this->session->userdata('role'));
            $this->session->set_userdata($data_array);
            echo json_encode($data_array['my_notification']);
        }
    }

//    private function re_arrange_notifications() {
//        $notifications_list = $this->Notification_model->get_all_notifications();
//        $return_arr = array();
//        if ($notifications_list) {
//            $i = 0;
//            foreach ($notifications_list as $notification) {
//                $return_arr[$i]['txt'] = preg_replace('/\W\w+\s*(\W*)$/', '$1', $notification['notification_txt']);
//                $return_arr[$i]['name'] = $notification['user_details']->first_name . ' ' . $notification['user_details']->last_name;
//                $return_arr[$i]['date'] = date('M d, Y', strtotime($notification['notify_date']));
//                $i++;
//                if ($i == '3') {
//                    break;
//                }
//            }
//        } else {
//            
//        }
//        return $return_arr;
//    }

}
