<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends CI_Controller {

    public $temp_clients = array();
    public $clients = array();
    public $all_clients = array();
    public $total_per_dispatch;

    public function __construct() {
        parent::__construct();
//        if (!$this->session->userdata('uid')) {
//            redirect(base_url('Auth'));
//        }
        $this->temp_clients = $this->Email_model->get_client(array('loan-client'));//Previously fetch all temp users, now loan clients assign to 'temp client' variable
        $this->clients = $this->Email_model->get_client(array('cocheee'));//Previously fetch all permanent clients, now cochee clients assign to 'client' variable
        $this->all_clients = $this->Email_model->get_client(array('loan-client','cocheee','both'));
        $this->total_per_dispatch = 100;
    }

    public function index() {
        $data['all_created_emails'] = json_encode($this->Email_model->get_created_emails());
        $data['currentPage'] = "emails_management";
        $data['mainContent'] = "bulk_email/email_log";
        $this->load->view('includes/frame', $data);
    }

    public function create_new_email() {
        $data['temp_clients'] = $this->temp_clients;
        $data['clients'] = $this->clients;
        $data['all_clients'] = json_encode($this->all_clients);
        $data['currentPage'] = "emails_management";
        $data['mainContent'] = "bulk_email/send_email";
        $this->load->view('includes/frame', $data);
    }

    public function validation() {
        if ($this->input->post('to') == FALSE) {
            if ($this->input->post('client_email') == FALSE) {
                $this->form_validation->set_rules('client_email', 'client_email', 'trim|required');
            }
        }
        $this->form_validation->set_rules('email_sub', 'Subject', 'trim|required');
        $this->form_validation->set_rules('des', 'Description', 'trim|required');
        return $this->form_validation->run();
    }

    public function get_form_data() {
        $data = array();
        if ($this->input->post('client_email')) {
            $custom_arr = $this->input->post('client_email');
            $data['email_list'] = json_encode($custom_arr);
            $data['sending_type'] = 'custom';
            $data['pagination'] = $this->calc_pagination($custom_arr);
        } else {
            $user_type = $this->input->post('to');
            switch ($user_type) {
                case '1':
                    $data['sending_type'] = '1';
                    $data['pagination'] = $this->calc_pagination($this->all_clients);
                    break;
                case '2':
                    $data['sending_type'] = '2';
                    $data['pagination'] = $this->calc_pagination($this->temp_clients);
                    break;
                case '3':
                    $data['sending_type'] = '3';
                    $data['pagination'] = $this->calc_pagination($this->clients);
                    break;
            }
        }
        $data['subject'] = $this->input->post('email_sub');
        $data['content'] = $this->input->post('des');
        $data['status'] = 'pending';
        $data['dispatch'] = '{}';
        $data['create_date'] = date('Y-m-d H:i:s');
        $data['create_by'] = $this->session->userdata('uid');
        return $data;
    }

    public function composer() {
        $is_valid = $this->validation();
        if ($is_valid) {
            $valid_data = $this->get_form_data();
            $rtn = $this->Email_model->insert_bulk($valid_data);
            echo($rtn) ? $this->session->set_flashdata('create_success', 'success') : $this->session->set_flashdata('error_msg', 'error');
            redirect(base_url('Email'));
        } else {
            $this->create_new_email();
        }
    }

    public function dispatch($code_safe = false) {
        if ($code_safe && $code_safe == "7e4aE09655b75Raf9eV9e3e") {
            $update_data = array();
            $email_log = array();
            $dispatch_list = $this->Email_model->get_dispatch_pending_list();
            if ($dispatch_list->pagination != $dispatch_list->dispatch) {
                $start_point = 0;
                if ($dispatch_list->dispatch == '0') {
                    $start_point = 0;
                } else {
                    $start_point = (intval($dispatch_list->dispatch) * 100);
                }
                if ($dispatch_list->sending_type != 'custom') {
                    $output_arr = $this->get_email_list($dispatch_list->sending_type, $start_point, $this->total_per_dispatch);
                } else {
                    $output_arr = $this->re_arrange_array($dispatch_list->email_list, $start_point, $this->total_per_dispatch);
                }
                if ($output_arr) {
                    $update_data['status'] = 'processing';
                    $update_data['dispatch'] = intval($dispatch_list->dispatch) + 1;
                    $update_data['update_date'] = date('Y-m-d H:i:s');
                    $update_data['update_by'] = 'system';
                    $this->Email_model->update_dispatched_stat($update_data, $dispatch_list->uid);
                    foreach ($output_arr as $output) {
                        $email_data['to'] = $output->email;
                        $email_data['subject'] = $dispatch_list->subject;
                        $template_data['name'] = $output->first_name . ' ' . $output->last_name;
                        $template_data['msg'] = $dispatch_list->content;
                        $email_data['msg'] = $this->load->view('email_templates/common_template', $template_data, TRUE);
                        $email_send_rslt = send_emails($email_data);
                        if ($email_send_rslt) {
                            $email_log['email_sent_stat'] = 'sent';
                            $email_log['error'] = '';
                        } else {
                            $email_log['email_sent_stat'] = 'error';
                            $email_log['error'] = $email_send_rslt;
                        }
                        $email_log['bulk_email_ref'] = $dispatch_list->uid;
                        $email_log['user_name'] = $output->first_name . ' ' . $output->last_name;
                        $email_log['user_email'] = $output->email;
                        $email_log['user_id'] = $output->uid;
                        $email_log['date_sent'] = date('Y-m-d H:i:s');
                        $this->Email_model->insert_email_log($email_log);
                    }
                }
            } else {
                $update_data['status'] = 'closed';
                $update_data['update_date'] = date('Y-m-d H:i:s');
                $update_data['update_by'] = 'system';
                $this->Email_model->update_dispatched_stat($update_data, $dispatch_list->uid);
            }
        }
    }
    
    public function bulk_email_detail_view($email_uid) {
        $data['single_email'] = $this->Email_model->get_single_email($this->encrypt->decode($email_uid));
        $data['user_list'] = json_encode($this->Email_model->get_single_email_client_list($this->encrypt->decode($email_uid)));
        $data['currentPage'] = "emails_management";
        $data['mainContent'] = "bulk_email/single_email";
        $this->load->view('includes/frame', $data);
    }

    private function calc_pagination($arr) {
        $count = count($arr);
        $pagination = ceil(intval($count) / $this->total_per_dispatch);
        return $pagination;
    }

    private function get_email_list($type, $start_point, $end_pont) {
        switch ($type) {
            case '1':
                $output_arr = array_slice($this->all_clients, $start_point, $end_pont, true);
                break;
            case '2':
                $output_arr = array_slice($this->temp_clients, $start_point, $end_pont, true);
                break;
            case '3':
                $output_arr = array_slice($this->all_clients, $start_point, $end_pont, true);
                break;
        }
        return $output_arr;
    }

    private function re_arrange_array($json_str, $start_point, $end_pont) {
        $json_arr = json_decode($json_str);
        $output = array();
        $re_arr = array();
        $x = 0;
        foreach ($json_arr as $value) {
            $arr1[$x] = explode(':', $value);
            $arr2[$x] = explode(' ', trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $arr1[$x][1]))));
            $re_arr[$x]['uid'] = $arr1[$x][0];
            $re_arr[$x]['first_name'] = $arr2[$x][0];
            $re_arr[$x]['last_name'] = $arr2[$x][1];
            $re_arr[$x]['email'] = $arr1[$x][2];
            $x++;
        }
        foreach ($re_arr as $to_obj) {
            $output[] = (object) $to_obj;
        }
        $output_arr = array_slice($output, $start_point, $end_pont, true);
        return $output_arr;
    }

}
