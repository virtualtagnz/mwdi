<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Check_list extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('uid')) {
            redirect(base_url('Auth'));
        }
    }
    
    //--------------------------------------------------------------------------
    
    public function index() {
        
    }
    
    //--------------------------------------------------------------------------
    
    public function toggle_checklist() {
        $chk_uid = $this->input->post('uid');
        $data['is_checked'] = $this->input->post('chk')=='true'?'1':'0';
        $rtn = $this->Check_list_model->update_is_checked($chk_uid,$data);
        echo $rtn;
    }
}
