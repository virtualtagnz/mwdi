<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Notification_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function insert_notifications($data) {
        if ($this->db->insert('notification_tb', $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

//    public function get_notification_by_id() {
//        $this->db->select('nf_id,notification_txt,notify_date');
//        $this->db->from('notification_tb');
//        if ($this->session->userdata('role') == '4' || $this->session->userdata('role') == '5') {
//            $this->db->where(array('nf_role_id' => $this->session->userdata('role'), 'nf_user_id' => $this->session->userdata('uid')));
//        } else {
//            $this->db->where(array('nf_role_id' => '6', 'nf_user_id' => $this->session->userdata('uid')));
//        }
//        $this->db->order_by('notify_date');
//        $this->db->limit(3);
//        return $this->db->get()->result();
//    }

    public function get_all_notifications() {
        $this->db->select('nf_id,notification_txt,notify_date,nf_user_id');
        $this->db->select('SUBSTRING_INDEX(notification_txt," ", -1) as uname', FALSE);
        $this->db->from('notification_tb');
        if ($this->session->userdata('role') == '4' || $this->session->userdata('role') == '5') {
            $this->db->where(array('nf_role_id' => $this->session->userdata('role'), 'nf_user_id' => $this->session->userdata('user_level_id')));
        } else {
            $this->db->where(array('nf_role_id' => '6', 'nf_user_id' => '1'));
        }
        switch ($this->session->userdata('role')) {
            case '1':$this->db->where('super_mark', 'N');
                break;
            case '2':$this->db->where('admin_mark', 'N');
                break;
            case '3':$this->db->where('account_mark', 'N');
                break;
            case '4':$this->db->where('coach_mark', 'N');
                break;
            case '5':$this->db->where('client_mark', 'N');
                break;
        }
        $this->db->order_by('notify_date', 'DESC');
        $str = $this->db->get_compiled_select();
        $query = $this->db->query($str);



        if ($query->num_rows() > 0) {
            $data_arr = array();
            $i = 0;
            foreach ($query->result_array() as $row) {

                $userName = $this->get_user_details($row['uname'], false);
                if(!$userName) {
                    $userName = $this->get_user_details(false, $row['nf_user_id']);
                }

                $data_arr[$i]['nf_id'] = $row['nf_id'];
                $data_arr[$i]['notification_txt'] = $row['notification_txt'];
                $data_arr[$i]['user_details'] = $userName;
                $data_arr[$i]['notify_date'] = $row['notify_date'];
                $i++;
            }
            return $data_arr;
        }
    }

    public function get_user_details($uname,$user_id) {
        $this->db->select('user_id,role_id');
        $this->db->from('user_tb');
        if($uname) {
            $this->db->like('user_name', $uname);
        } else {
            $this->db->where('user_id', $user_id);
        }
        $arr = $this->db->get()->row();
        if ($arr) {
            $u_arr = $this->System_user_model->get_single_user($arr->user_id, $arr->role_id);
        } else {
            $u_arr = '';
        }
        return $u_arr;
    }

    public function update_selected_notifications($selected_notifications) {
        $this->db->trans_start();
        $this->db->update_batch('notification_tb', $selected_notifications, 'nf_id');
        $this->db->trans_complete();
        if ($this->db->trans_status()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function update_all_notifications($notification_arr) {
        $this->db->trans_start();
        if ($this->session->userdata('role') == '4' || $this->session->userdata('role') == '5') {
            $this->db->where(array('nf_role_id' => $this->session->userdata('role'), 'nf_user_id' => $this->session->userdata('user_level_id')));
        } else {
            $this->db->where(array('nf_role_id' => '6', 'nf_user_id' => '1'));
        }
        $this->db->update('notification_tb', $notification_arr);
        $this->db->trans_complete();
        if ($this->db->trans_status()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function get_notification_cnt($role) {
        $this->db->select('count(nf_id) AS notification_cnt');
        $this->db->from('notification_tb');
        $this->db->where(array('nf_role_id' => '6', 'nf_user_id' => '1'));
        switch ($role) {
            case '1':$this->db->where('super_mark', 'N');
                break;
            case '2':$this->db->where('admin_mark', 'N');
                break;
            case '3':$this->db->where('account_mark', 'N');
                break;
            case '4':$this->db->where('coach_mark', 'N');
                break;
            case '5':$this->db->where('client_mark', 'N');
                break;
        }
        return $this->db->get()->row('notification_cnt');
    }

}
