<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Coaching_form_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /* ------------------------------------------------------------------------------------------------- */

    public function get_coaching_list() {

        $this->db->select("uid, CONCAT(first_name,' ',last_name) AS cname, created_date, answered_by, answered_date, coach, u1.user_name as answered_by_name, u2.user_name AS coach_name");
        $this->db->join('user_tb u1', 'u1.user_id = client_temp_form.answered_by', 'left');
        $this->db->join('user_tb u2', 'u2.user_id = client_temp_form.coach', 'left');
        $this->db->where('is_temp', '1');
        $this->db->order_by('client_temp_form.uid', 'DESC');
        $query = $this->db->get('client_temp_form');
        $query = $query->result();
        if ($query)
            return $query;
        else
            return false;
    }

    /* ------------------------------------------------------------------------------------------------- */

    public function get_coaching_form($uid) {
        $query = $this->db->select("uid, first_name, last_name, city_region, phone, email, business_description,coach")->where('uid', $uid)->get('client_temp_form');
        $query = $query->result();
        if ($query)
            return reset($query);
        else
            return false;
    }

    /* ------------------------------------------------------------------------------------------------- */

    public function add_coaching_form($string_data) {
        if ($string_data && is_array($string_data)) {
            $this->db->insert('client_temp_form', $string_data);
            return $this->db->insert_id();
        } else
            return false;
    }

    /* ------------------------------------------------------------------------------------------------- */

    public function update_coaching_form($string_data, $uid) {
        if ($string_data && is_array($string_data)) {
            $this->db->where('uid', $uid);
            $this->db->update('client_temp_form', $string_data);
            return $this->db->insert_id();
        } else
            return false;
    }

    /* ------------------------------------------------------------------------------------------------- */

    public function mark_as_answered($uid, $user_uid) {
        $string_data['answered'] = '1';
        $string_data['answered_by'] = $user_uid;
        $string_data['answered_date'] = date('Y-m-d H:i:s');
        $this->db->where('uid', $uid);
        if ($this->db->update('client_temp_form', $string_data))
            return true;
        else
            return false;
    }

    /* ------------------------------------------------------------------------------------------------- */

    public function become_coach($uid, $user_uid) {
        $string_data['coach'] = $user_uid;
        $this->db->where('uid', $uid);
        if ($this->db->update('client_temp_form', $string_data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /* ------------------------------------------------------------------------------------------------- */
    
    public function get_data_send_email($uid) {
        $this->db->select('t1.first_name AS client_first_name,t1.last_name AS client_last_name,t2.first_name AS coach_first_name,t2.last_name AS coach_last_name');
        $this->db->from('client_temp_form t1');
        $this->db->join('coach_tb t2','t1.coach = t2.coach_id','left'); 
        $this->db->where('t1.uid',$uid);
        return $this->db->get()->row();
    }
}
