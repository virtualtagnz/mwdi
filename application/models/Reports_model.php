<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reports_model extends CI_Model {
    /* ------------------------------------------------------------------------------------------------- */
    /* - ENQUIRES - */

    public function get_enquires_stats() {
        $query1 = $this->db->select('count(enq_file_no) as forms_assigned')->where('sub_stat', 'Y')->where('temp_enq_file_no', '0')->get('client_enq_form_tb');
        $query1 = $query1->result();
        $forms_assigned = $query1[0]->forms_assigned;
        $query2 = $this->db->select('count(cf_id) as completed_forms')->where('cf_frm_id', '6')->get('client_forms_tb');
        $query2 = $query2->result();
        $completed_forms = $query2[0]->completed_forms;
        $query3 = $this->db->select('count(enq_file_no) as temporary_enquiry')->where('sub_stat', 'Y')->where('temp_enq_file_no!=', '0')->get('client_enq_form_tb');
        $query3 = $query3->result();
        $temporary_enquiry = $query3[0]->temporary_enquiry;
        $enquires_stats = array('forms_assigned' => $forms_assigned, 'completed_forms' => $completed_forms, 'temporary_enquiry' => $temporary_enquiry);
        return $enquires_stats;
    }

    public function get_enquires_list() {

        $query = $this->db->select("concat((first_name),(' '),(last_name)) AS cname")
                ->select("CASE WHEN(client_enq_form_tb.sub_stat = 'Y') THEN 'Submited' ELSE 'Not Submited' END AS sub_stat", FALSE)
                ->select("CASE WHEN(client_enq_form_tb.sub_stat = 'Y') THEN DATE_FORMAT(client_enq_form_tb.sub_date,'%m-%d-%Y') ELSE '-' END AS submit_date", FALSE)
                ->select("CASE WHEN(client_enq_form_tb.temp_enq_file_no = 0) THEN 'Normal' ELSE 'Temporary' END AS user_stat", FALSE)
                ->where('clients_tb.client_id = client_enq_form_tb.enq_client_ref', NULL, FALSE)
                ->get('client_enq_form_tb, clients_tb');
        $query = $query->result();
        if ($query)
            return $query;
        else
            return false;
    }

    /* ------------------------------------------------------------------------------------------------- */
    /* - TEMPORARY ENQUIRES - */

    public function get_temp_enquires_stat() {
        $query = $this->db->select('COUNT(enq_file_no) AS cnt')->where('isTemp', '1')->get('client_enq_form_temp_tb');
        $query = $query->result();
        if ($query)
            return $query;
        else
            return false;
    }

    public function get_temp_enquires_list() {
        $query = $this->db->select("concat((enq_fst_name),(' '),(enq_lst_name)) AS u_name,enq_mail,enq_phn,enq_mob,enq_hadd,enq_biz_add_reg,enq_insd,start_date")->where('isTemp', '1')->get('client_enq_form_temp_tb');
        $query = $query->result();
        if ($query)
            return $query;
        else
            return false;
    }

    /* ------------------------------------------------------------------------------------------------- */
    /* - LOAN APPLICATIONS - */

    public function get_loan_stats() {
        $query = $this->db->select('count(ba_id) AS cnt')->where('sub_status', 'Y')->get('ba_main');
        $query = $query->result();
        if ($query)
            $forms_assigned = $query[0]->cnt;
        else
            return $forms_assigned = 0;
        $query = $this->db->select('count(cf_id) AS cnt')->where('cf_frm_id', '1')->get('client_forms_tb');
        $query = $query->result();
        if ($query)
            $completed_forms = $query[0]->cnt;
        else
            return $completed_forms = 0;
        if (intval($completed_forms) > 0 && intval($forms_assigned)) {
            $srate = (intval($forms_assigned) / intval($completed_forms)) * 100;
        } else
            $srate = 0;
        $loan_stats = array('completed_forms' => $completed_forms, 'forms_assigned' => $forms_assigned, 'success_rate' => $srate);
        return $loan_stats;
    }

    public function get_loan_list() {

        $query = $this->db->select("concat(c1.first_name,' ',c1.last_name) AS cname")
                ->select("CASE WHEN(b.sub_status = 'Y') THEN 'Submited' ELSE 'Not Submited' END AS sub_stat", FALSE)
                ->select("CASE WHEN(b.sub_status = 'Y') THEN DATE_FORMAT(b.date_submit,'%d-%m-%Y') ELSE '-' END AS submit_date", FALSE)
                ->where("c1.client_id = b.ba_client_ref", NULL, FALSE)
                ->get("ba_main b,clients_tb c1");
        $query = $query->result();
        if ($query)
            return $query;
        else
            return false;
    }

    /* ------------------------------------------------------------------------------------------------- */
    /* - LOAN APPROVED / REJECTED - */

    public function get_loan_ar_stats() {
        $query = $this->db->select('count(loan_id) AS cnt')->where('approved_stat', 'Y')->get('approved_loan_tb');
        $query = $query->result();
        if ($query)
            $loan_approved = $query[0]->cnt;
        else
            return $loan_approved = 0;
        $query = $this->db->select('count(loan_id) AS cnt')->where('approved_stat', 'N')->get('approved_loan_tb');
        $query = $query->result();
        if ($query)
            $loan_rejected = $query[0]->cnt;
        else
            return $loan_rejected = 0;
        $loan_stats = array('loan_approved' => $loan_approved, 'loan_rejected' => $loan_rejected);
        return $loan_stats;
    }

    public function get_loan_ar_list() {
        $query = $this->db->select("concat(c.first_name,' ',c.last_name) AS cname")
                ->select("FORMAT(ln_amount,2) AS ln_amount")
                ->select("CASE WHEN(a.approved_stat = 'Y') THEN 'APPROVED' ELSE 'REJECTED' END AS app_stat", FALSE)
                ->select("DATE_FORMAT(a.approved_date,'%d-%m-%Y') AS app_date")
                ->select("DATE_FORMAT(a.date_last_pmt,'%d-%m-%Y') AS last_date")
                ->where("c.client_id = a.ln_client_ref")
                ->get("approved_loan_tb a,clients_tb c");
        $query = $query->result();
        if ($query)
            return $query;
        else
            return false;
    }

    /* ------------------------------------------------------------------------------------------------- */
    /* - JOBS CREATED - */

    public function get_jobs_created_stats() {
        $query = $this->db->select("sum(b.no_positions) AS tot_positions,sum(b.full_time) AS full_time,sum(b.part_time) AS part_time")
                ->where("b1.ba_id = b.biz_main_ref AND b1.sub_status='Y'", NULL, FALSE)
                ->get("ba_business_details b,ba_main b1");
        $query = $query->result();
        if ($query)
            return $query;
        else
            return false;
    }

    public function get_jobs_created_list() {

        $query = $this->db->select("concat(c.first_name,' ',c.last_name) AS cname, b.no_positions, b.full_time AS full_time, b.part_time AS part_time")
                ->where("b1.ba_id = b.biz_main_ref  AND c.client_id = b1.ba_client_ref AND b1.sub_status='Y'", NULL, FALSE)
                ->get("ba_business_details b,ba_main b1,clients_tb c");
        $query = $query->result();
        if ($query)
            return $query;
        else
            return false;
    }

    /* ------------------------------------------------------------------------------------------------- */
    /* - BANK DECLINED SUMMARY - */

    public function get_bank_stats() {
        $query = $this->db->select('b.try_ops')->where('b1.ba_id = b.bk_main_ref AND c.client_id = b1.ba_client_ref AND b1.sub_status="Y" AND b.try_ops != ""')->get('ba_bank_details b,ba_main b1,clients_tb c');
        $query = $query->result();
        if ($query) {
            $c1 = $c2 = $c3 = $c4 = 0;
            $pieces = array();
            foreach ($query as $bank) {
                $pieces = explode(',', $bank->try_ops);
                if (in_array('1', $pieces))
                    $c1++;
                if (in_array('2', $pieces))
                    $c2++;
                if (in_array('3', $pieces))
                    $c3++;
                if (in_array('4', $pieces))
                    $c4++;
            }
            $stats = array('security' => $c1, 'cashflow' => $c2, 'credit_rating' => $c3, 'other' => $c4);
            return $stats;
        } else
            return false;
    }

    public function get_bank_list() {
        $query = $this->db->select("concat(c.first_name, ' ', c.last_name) AS cname,b.try_ops,b.try_result")
                ->where("b1.ba_id = b.bk_main_ref AND c.client_id = b1.ba_client_ref AND b1.sub_status='Y' AND b.try_ops != ''", NULL, FALSE)
                ->order_by("cname", 'ASC')
                ->get("ba_bank_details b,ba_main b1,clients_tb c");

        $query = $query->result();
        if ($query)
            return $query;
        else
            return false;
    }

    /* ------------------------------------------------------------------------------------------------- */
    /* - CLIENT REGIONS - */

    public function get_client_region_list($selected_category=FALSE,$selected_region=FALSE) {
//        $query = $this->db->select("concat(c.first_name,' ',c.last_name) AS cname,c.client_category, c.add_region")
//                        ->from('clients_tb c')
//                        ->join('user_tb u', 'u.user_id = c.u_id')
//                        ->where('u.status', '1')
//                        ->order_by("c.add_region")->get();
//        $query = $query->result();
        $this->db->select("concat(c.first_name,' ',c.last_name) AS cname,c.client_category, c.add_region");
        $this->db->from('clients_tb c');
        $this->db->join('user_tb u', 'u.user_id = c.u_id');
        $this->db->where('u.status', '1');
        if($selected_category){
            $this->db->where('c.client_category', $selected_category);
        }
        if($selected_region){
            $this->db->where('c.add_region', $selected_region);
        }
        $this->db->order_by("c.add_region");
        //$str = $this->db->get_compiled_select();

        $query = $this->db->get()->result();
        if ($query)
            return $query;
        else
            return false;
    }

    public function get_client_region_summary() {
        $query = $this->db->select('count(c.add_region) AS cnt, c.add_region')
                        ->from('clients_tb c')
                        ->join('user_tb u', 'u.user_id = c.u_id')
                        ->where('u.status', '1')
                        ->group_by('add_region')->get();
        $query = $query->result();
        if ($query) {
            $region_list = array();
            $city_leter = '';
            foreach ($query as $region) {
                if ($region->add_region == 'Northland' || $region->add_region == 'NTL' || $region->add_region == 'ntl')
                    $city_leter = 'A';
                elseif ($region->add_region == 'Auckland' || $region->add_region == 'AUK' || $region->add_region == 'auk')
                    $city_leter = 'B';
                elseif ($region->add_region == 'Waikato' || $region->add_region == 'WKO' || $region->add_region == 'wko')
                    $city_leter = 'C';
                elseif (trim($region->add_region) == 'Bay Of Plenty' || $region->add_region == 'BOP' || $region->add_region == 'bop')
                    $city_leter = 'D';
                elseif ($region->add_region == 'Gisborne' || $region->add_region == 'GIS' || $region->add_region == 'gis')
                    $city_leter = 'E';
                elseif ($region->add_region == 'Hawke\'s Bay' || $region->add_region == 'HKB' || $region->add_region == 'hkb')
                    $city_leter = 'F';
                elseif ($region->add_region == 'Taranaki' || $region->add_region == 'TKI' || $region->add_region == 'tki')
                    $city_leter = 'G';
                elseif ($region->add_region == 'Manawatu-Wanganui' || $region->add_region == 'MWT' || $region->add_region == 'mwt')
                    $city_leter = 'H';
                elseif ($region->add_region == 'Wellington' || $region->add_region == 'WGN' || $region->add_region == 'wgn')
                    $city_leter = 'I';
                elseif ($region->add_region == 'Tasman' || $region->add_region == 'TAS' || $region->add_region == 'tas')
                    $city_leter = 'J';
                elseif ($region->add_region == 'Nelson' || $region->add_region == 'NSN' || $region->add_region == 'nsn')
                    $city_leter = 'K';
                elseif ($region->add_region == 'Marlborough' || $region->add_region == 'MBH' || $region->add_region == 'mbh')
                    $city_leter = 'L';
                elseif ($region->add_region == 'West Coast' || $region->add_region == 'WTC' || $region->add_region == 'wtc')
                    $city_leter = 'M';
                elseif ($region->add_region == 'Canterbury' || $region->add_region == 'CAN' || $region->add_region == 'can')
                    $city_leter = 'N';
                elseif ($region->add_region == 'Otago' || $region->add_region == 'OTA' || $region->add_region == 'ota')
                    $city_leter = 'O';
                elseif ($region->add_region == 'Southland' || $region->add_region == 'STL' || $region->add_region == 'stl')
                    $city_leter = 'P';
                elseif ($region->add_region == 'Chatham Islands' || $region->add_region == 'CIT' || $region->add_region == 'cit')
                    $city_leter = 'Q';
                else {
                    $city_leter = 'ND';
                }
                $region_list[] = array('add_region' => $region->add_region, 'cnt' => $region->cnt, 'city_leter' => $city_leter);
            }
            return $region_list;
        } else
            return false;
    }

    /* ------------------------------------------------------------------------------------------------- */
    /* - INDUSTRY - */

    public function get_industry_list() {
        $query = $this->db->select('COUNT(c.client_id) AS cnt, c.industry')
                        ->from('clients_tb c')
                        ->join('user_tb u', 'u.user_id = c.u_id')
                        ->where('u.status', '1')
                        ->group_by('industry')->get();
        $query = $query->result();
        if ($query)
            return $query;
        else
            return false;
    }

    /* ------------------------------------------------------------------------------------------------- */
    
    public function get_coaches_start_work_on_client() {
        $this->db->select('c1.first_name AS coach_fname,c1.last_name AS coach_lname,c.first_name AS client_fname,c.last_name AS client_lname,r.assign_date,r.strat_work,r.start_date');
        $this->db->from('client_coach_rel r');
        $this->db->join('clients_tb c', 'r.client_ref = c.client_id');
        $this->db->join('coach_tb c1', 'r.coach_ref = c1.coach_id');
        $this->db->join('user_tb t2', 't2.user_id = c1.u_id');
        $this->db->where(array('r.strat_work' => '1'));
        return $this->db->get()->result();
    }
}
