<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Resource_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function insert($data) {
        if ($this->db->insert('mwdi_resource', $data)) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }
    
    public function get_single_resource($res_id) {
        $this->db->select('*');
        $this->db->from('mwdi_resource');
        $this->db->where("res_id", $res_id);
        return $this->db->get()->row();
    }
    
    public function update($res_id,$data) {
        $this->db->where('res_id', $res_id);
        $this->db->update('mwdi_resource', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function get_all_resources() {
        $this->db->select('*');
        $this->db->from('mwdi_resource');
        $this->db->order_by('date_create', 'DESC');
        return json_encode($this->db->get()->result());
    }

}
