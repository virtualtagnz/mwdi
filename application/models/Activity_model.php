<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Activity_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    //--------------------------------------------------------------------------

    public function insert_activities($data) {
        if ($this->db->insert('activity_tb', $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //--------------------------------------------------------------------------

    public function get_all_user_activities($user_type,$uid=NULL) {
        $this->db->select('a.*,t2.first_name,t2.last_name,t2.image_link');
        $this->db->from('activity_tb a');
        switch ($user_type) {
            case '4':$this->db->join('coach_tb t2', 'a.act_user = t2.coach_id');
                break;
            case '5':$this->db->join('clients_tb t2', 'a.act_user = t2.client_id');
                break;
        }
        if($uid!=NULL){
          $this->db->where(array('a.act_role' => $user_type,'a.act_user'=>$uid));
        }  else {
          $this->db->where(array('a.act_role' => $user_type));  
        }
        $this->db->order_by('a.act_date', 'DESC');
        return $this->db->get()->result();
    }
    
    //--------------------------------------------------------------------------
    
    public function get_my_clients_activities($uid) {
        $this->db->select('a.*,t2.first_name,t2.last_name,t2.image_link');
        $this->db->from('activity_tb a');
        $this->db->join('clients_tb t2', 'a.act_user = t2.client_id');
        $this->db->join('client_coach_rel r', 'r.client_ref = t2.client_id');
        $this->db->where('r.coach_ref', $uid);
        return $this->db->get()->result();
    }
}
