<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class System_user_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function insert($data, $info, $user_type) {
        $this->db->trans_start();
        $this->db->insert('user_tb', $data);
        $user_id = $this->db->insert_id();
        $info['u_id'] = $user_id;
        switch ($user_type) {
            case '2':$this->db->insert('admin_tb', $info);
                break;
            case '3':$this->db->insert('accounts_tb', $info);
                break;
            case '4':$this->db->insert('coach_tb', $info);
                break;
            case '5':$this->db->insert('clients_tb', $info);
                $client_id = $this->db->insert_id();
                break;
        }
        if ($user_type == '5') {
            $application_form_data = array();
            $temp_forms = $this->Assignment_model->load_application_forms('L');
            $x = 0;
            foreach ($temp_forms as $form) {
                $application_form_data[$x]['cf_client_ref'] = $client_id;
                $application_form_data[$x]['cf_frm_id'] = $form->frm_id;
                $application_form_data[$x]['assign_date'] = date('Y-m-d H:i;s');
                $application_form_data[$x]['assign_by'] = $this->session->userdata('uid');
                $application_form_data[$x]['role_assign'] = $this->session->userdata('role');
                $x++;
            }
            $this->db->insert_batch('client_forms_tb', $application_form_data);
            create_notification($user_type, $client_id, 'You have 3 Forms Assigned By '.$this->session->userdata('user_name'));
            set_checklist_to_client($client_id);
        }
        $this->db->trans_complete();
        if ($this->db->trans_status()) {
            return $user_id;
        } else {
            return FALSE;
        }
    }

    public function get_single_user($uid, $user_type) {
        $this->db->select('u.*,t2.*');
        $this->db->from('user_tb u');
        switch ($user_type) {
            case '1':$this->db->join('super_admin_tb t2', 'u.user_id = t2.u_id');
                break;
            case '2':$this->db->join('admin_tb t2', 'u.user_id = t2.u_id');
                break;
            case '3':$this->db->join('accounts_tb t2', 'u.user_id = t2.u_id');
                break;
            case '4':$this->db->join('coach_tb t2', 'u.user_id = t2.u_id');
                break;
            case '5':$this->db->join('clients_tb t2', 'u.user_id = t2.u_id');
                break;
        }
        $this->db->where(array('u.user_id' => $uid));
        return $this->db->get()->row();
    }

    public function get_single_user_by_user_level_id($user_type, $uid) {
        $this->db->select('u.*,t2.*');
        $this->db->from('user_tb u');
        switch ($user_type) {
            case '2':
                $this->db->join('admin_tb t2', 'u.user_id = t2.u_id');
                $this->db->where(array('t2.admin_id' => $uid));
                break;
            case '3':
                $this->db->join('accounts_tb t2', 'u.user_id = t2.u_id');
                $this->db->where(array('t2.accounts_id' => $uid));
                break;
            case '4':
                $this->db->join('coach_tb t2', 'u.user_id = t2.u_id');
                $this->db->where(array('t2.coach_id' => $uid));
                break;
            case '5':
                $this->db->join('clients_tb t2', 'u.user_id = t2.u_id');
                $this->db->where(array('t2.client_id' => $uid));
                break;
        }
        return $this->db->get()->row();
    }

    public function gel_all_users($user_type) {
        $this->db->select('u.*,t2.*');
        $this->db->from('user_tb u');

        switch ($user_type) {
            case '2': 
                $this->db->join('admin_tb t2', 'u.user_id = t2.u_id');
                $this->db->where(array('u.status' => '1'));
                break;
            case '3':
                $this->db->join('accounts_tb t2', 'u.user_id = t2.u_id');
                $this->db->where(array('u.status' => '1'));
                break;
            case '4':
                $this->db->join('coach_tb t2', 'u.user_id = t2.u_id');
                $this->db->where(array('u.status' => '1'));
                break;
            case '5':
                $this->db->join('clients_tb t2', 'u.user_id = t2.u_id');
                break;
        }
        if ($this->session->userdata('role') == '4') {
            $this->db->select('r.tab_id,r.strat_work,r.start_date');
            $this->db->join('client_coach_rel r', 'r.client_ref = t2.client_id');
            $this->db->where('r.coach_ref', $this->session->userdata('user_level_id'));
        }
        $this->db->where(array('u.role_id' => $user_type));
        $this->db->order_by('t2.first_name', 'ASC');
        $query = $this->db->get();
        
        if($query) {
            return json_encode($query->result());
        } else {
            return false;
        }
    }

    public function update($data, $info, $user_type, $uid) {
        $this->db->trans_start();
        $this->db->where('user_id', $uid);
        $this->db->update('user_tb', $data);
        switch ($user_type) {
            case '1':
                $this->db->where('u_id', $uid);
                $this->db->update('super_admin_tb', $info);
                break;
            case '2':
                $this->db->where('u_id', $uid);
                $this->db->update('admin_tb', $info);
                break;
            case '3':
                $this->db->where('u_id', $uid);
                $this->db->update('accounts_tb', $info);
                break;
            case '4':
                $this->db->where('u_id', $uid);
                $this->db->update('coach_tb', $info);
                break;
            case '5':
                $this->db->where('u_id', $uid);
                $this->db->update('clients_tb', $info);
                break;
        }
        $this->db->trans_complete();
        if ($this->db->trans_status()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function update_status($uid, $status = false) {
        $this->db->where('user_id', $uid);
        $this->db->update('user_tb', array('status' => ($status?1:0)));
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function get_my_users($uid) {
        $this->db->select('c.*,r.*');
        $this->db->from('client_coach_rel r');
        $this->db->join('clients_tb c', 'r.client_ref = c.client_id');
        $this->db->where('r.coach_ref', $uid);
        return $this->db->get()->result();
    }
    
    public function get_all_users_not_submit_coach_forms($uid,$arr) {
        $this->db->select('c.*,r.*');
        $this->db->from('client_coach_rel r');
        $this->db->join('clients_tb c', 'r.client_ref = c.client_id');
        $this->db->where('coach_ref', $uid);
        $this->db->where_not_in('c.client_id',$arr);
        return $this->db->get()->result();
    }
    
    public function get_coach_form_submitted_clients($uid,$form_id) {
        $this->db->select('c.chf_cl_ref AS client_id');
        $this->db->from('coach_form_main c');
        switch ($form_id){
//            case '5':$this->db->from('coach_form_main c');
//                break;
            case '8':$this->db->join('coach_form_p3 c1', 'c1.chp3_main_ref = c.chf_id');
                break;
            case '9':$this->db->join('coach_form_p4 c1', 'c1.chp4_main_ref = c.chf_id');
                break;
            case '10':$this->db->join('coach_form_p5 c1', 'c1.chp5_main_ref = c.chf_id');
                break;
        }
        $this->db->where(array('c.is_submit'=>'Y','c.chf_ch_ref'=>$uid));
        $str = $this->db->get_compiled_select();
        $query = $this->db->query($str);
        if ($query->num_rows() > 0) {
            $arr = array();
            foreach ($query->result_array() as $row) {
                array_push($arr,$row['client_id']);
            }
            return $arr;
        }
        
    }

    public function load_unselected_clients() {
        $this->db->select('c.client_id,c.first_name,c.last_name');
        $this->db->from('clients_tb c');
        $this->db->join('client_coach_rel r', 'c.client_id = r.client_ref', 'left');
        $this->db->where('r.client_ref IS NULL', null, false);
        return $this->db->get()->result();
    }
    
    public function update_as_start_work($uid,$arr) {
        $this->db->where('tab_id', $uid);
        if ($this->db->update('client_coach_rel', $arr)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
