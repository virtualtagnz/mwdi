<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_enquire_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function add_loan_form($data) {
        if ($this->db->insert('loan_enquire_form', $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function get_loan_enquire_list() {
        $this->db->select('*');
        $this->db->from('loan_enquire_form');
        $this->db->where('deleted', '0');
        $this->db->order_by('uid', 'DESC');

        $query = $this->db->get();
        if($query) {
            $query = $query->result();

            if($query)
                return $query;
            else
                return false;
        } else {
            return false;
        }
    }

     public function delete_loan_form($uid) {
        $this->db->where('uid', $uid);
        if ($this->db->update('loan_enquire_form', array('deleted' => '1'))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function get_loan_enquire_form($uid) {
        $this->db->select('*');
        $this->db->from('loan_enquire_form');
        $this->db->where(array('uid' => $uid));

        $query = $this->db->get();

        if($query) {
            $query = $query->row();

            if($query)
                return $query;
            else
                return false;
        } else {
            return false;
        }
    }

    public function mark_as_answered($uid, $user_uid) {
        $string_data['answered_by'] = $user_uid;
        $string_data['date_answered'] = date('Y-m-d H:i:s');
        $this->db->where('uid', $uid);
        if ($this->db->update('loan_enquire_form', $string_data))
            return true;
        else
            return false;
    }

}

/* End of file loan_enquire_model.php */
/* Location: ./application/models/loan_enquire_model.php */