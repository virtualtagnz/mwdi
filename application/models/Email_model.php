<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Email_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_temporary_client() {
        $this->db->select('enq_file_no AS uid,enq_fst_name AS first_name,enq_lst_name AS last_name,enq_mail AS email');
        $this->db->from('client_enq_form_tb');
        return $this->db->get()->result();
    }

    public function get_client($client_category=FALSE) {
        $this->db->select('u.user_id AS uid,c.first_name AS first_name,c.last_name AS last_name,u.user_email AS email');
        $this->db->from('user_tb u');
        $this->db->join('clients_tb c', 'u.user_id = c.u_id');
        $this->db->where('u.status',1); 
        $this->db->where_in('c.client_category',$client_category);
        return $this->db->get()->result();
    }
    
    public function get_client_by_id($id_list) {
        
    }

    public function insert_bulk($data) {
        if ($this->db->insert('email_bulk_creation', $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function get_dispatch_pending_list() {
        $this->db->select('*');
        $this->db->from('email_bulk_creation');
        $this->db->where('status', 'pending');
        $this->db->or_where('status', 'processing');
        $this->db->order_by('create_date', 'ASC');
        $this->db->limit('1');
        return $this->db->get()->row();
    }

    public function update_dispatched_stat($data, $uid) {
        $this->db->where('uid', $uid);
        $this->db->update('email_bulk_creation', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function insert_email_log($data) {
        if ($this->db->insert('email_log', $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function get_created_emails() {
        $this->db->select('*');
        $this->db->from('email_bulk_creation');
        $this->db->order_by('create_date', 'DESC');
        return $this->db->get()->result();
    }
    
    public function get_single_email($email_id) {
        $this->db->select('*');
        $this->db->from('email_bulk_creation'); 
        $this->db->where('uid', $email_id);
        return $this->db->get()->row();
    }
    
    public function get_single_email_client_list($email_id) {
        $this->db->select('*');
        $this->db->from('email_log'); 
        $this->db->where('bulk_email_ref', $email_id);
        return $this->db->get()->result();
    }

}
