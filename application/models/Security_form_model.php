<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Security_form_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    //====================== Security form step 1 ==========================
    
    public function insert_security_form_step1($data) {
        if ($this->db->insert('sec_main', $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function read_security_form_step1($client_id) {
        $this->db->select('*');
        $this->db->from('sec_main s1');
        $this->db->where('s1.sec_client_ref', $client_id);
        return $this->db->get()->row();
    }

    public function update_security_form_step1($form_id, $data) {
        $this->db->where('sec_id', $form_id);
        $this->db->update('sec_main', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function get_main_form_id($client_id) {
        $this->db->select('sec_id');
        $this->db->from('sec_main');
        $this->db->where('sec_client_ref', $client_id);
        return $this->db->get()->row('sec_id');
    }
    
    public function get_main_form_submit_stat($client_id) {
        $this->db->select('sub_stat');
        $this->db->from('sec_main');
        $this->db->where('sec_client_ref', $client_id);
        return $this->db->get()->row('sub_stat');
    }
    
    //====================== Security form step 2 ==========================
    
    public function insert_security_form_step2($data) {
        if ($this->db->insert_batch('sec_item_vehicle', $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function read_security_form_step2($client_id) {
        $this->db->select('s1.*');
        $this->db->from('sec_item_vehicle s1');
        $this->db->join('sec_main s2','s1.vcl_sec_main_ref = s2.sec_id');
        $this->db->where('s2.sec_client_ref', $client_id);
        return $this->db->get()->result();
    }
    
    public function update_security_form_step2($form_id, $data) {
        $this->db->trans_start();
        $this->db->where('vcl_sec_main_ref', $form_id);
        $this->db->delete('sec_item_vehicle');
        $this->db->insert_batch('sec_item_vehicle', $data);
        $this->db->trans_complete();
        if ($this->db->trans_status()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function delete_single_vehicle_item($vid) {
        $this->db->where('vcl_id', $vid);
        $this->db->delete('sec_item_vehicle');
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    //====================== Security form step 3 ==========================
    
    public function insert_security_form_step3($data) {
        if ($this->db->insert_batch('sec_item_property', $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function read_security_form_step3($client_id) {
        $this->db->select('s1.*');
        $this->db->from('sec_item_property s1');
        $this->db->join('sec_main s2','s1.prop_sec_main_ref = s2.sec_id');
        $this->db->where('s2.sec_client_ref', $client_id);
        return $this->db->get()->result();
    }
    
    public function update_security_form_step3($form_id, $data) {
        $this->db->trans_start();
        $this->db->where('prop_sec_main_ref', $form_id);
        $this->db->delete('sec_item_property');
        $this->db->insert_batch('sec_item_property', $data);
        $this->db->trans_complete();
        if ($this->db->trans_status()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function delete_single_property_item($pid) {
        $this->db->where('prop_id', $pid);
        $this->db->delete('sec_item_property');
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    //====================== Security form step 4 ==========================

    public function insert_security_form_step4($data) {
        if ($this->db->insert_batch('sec_items_chattles', $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function read_security_form_step4($client_id) {
        $this->db->select('s1.*');
        $this->db->from('sec_items_chattles s1');
        $this->db->join('sec_main s2','s1.cat_sec_main_ref = s2.sec_id');
        $this->db->where('s2.sec_client_ref', $client_id);
        return $this->db->get()->result();
    }
    
    public function update_security_form_step4($form_id, $data) {
        $this->db->trans_start();
        $this->db->where('cat_sec_main_ref', $form_id);
        $this->db->delete('sec_items_chattles');
        $this->db->insert_batch('sec_items_chattles', $data);
        $this->db->trans_complete();
        if ($this->db->trans_status()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function update_submit_status($main_form_id) {
        $this->db->where('sec_id', $main_form_id);
        $this->db->update('sec_main', array('sub_stat'=>'Y','sub_date'=>date('Y-m-d H:i:s')));
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function delete_single_chattel_item($pid) {
        $this->db->where('cat_id', $pid);
        $this->db->delete('sec_items_chattles');
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    //==========================================================================
    
    public function get_all_submitted_security_form($is_submit = FALSE) {
        $this->db->select('s.*,f.frm_name,c.first_name,c.last_name');
        $this->db->from('sec_main s');
        $this->db->join('forms_tb f','s.form_type = f.frm_id');
        $this->db->join('clients_tb c','s.sec_client_ref = c.client_id');
        if($this->session->userdata('role')=='5'){
             $this->db->where(array('s.sec_client_ref'=>$this->session->userdata('user_level_id')));
        }
        if($is_submit){
            $this->db->where('sub_stat',$is_submit);
        }
        $this->db->where('f.frm_id<>','7');
        return $this->db->get()->result();
    }
}
