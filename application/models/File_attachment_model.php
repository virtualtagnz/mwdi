<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class File_attachment_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    //--------------------------------------------------------------------------
    
    public function get_all_file_attachments() {
        $this->db->select('c.first_name,c.last_name,f.*');
        $this->db->from('file_attachments f');
        $this->db->join('clients_tb c', 'f.client_id = c.client_id');
        if ($this->session->userdata('role') == '4') {
            $this->db->join('client_coach_rel r', 'r.client_ref = c.client_id');
            $this->db->where('r.coach_ref', $this->session->userdata('user_level_id'));
        }elseif ($this->session->userdata('role') == '5') {
            $this->db->where('f.client_id', $this->session->userdata('user_level_id'));
        }
        $this->db->order_by('c.first_name');
        return $this->db->get()->result();
    }
    
    //--------------------------------------------------------------------------
    
    public function get_file_attachment_by_client_id($client_id) {
        $this->db->select('f.*');
        $this->db->from('file_attachments f');
        $this->db->where(array('f.client_id'=>$client_id,'f.is_deleted'=>'N'));
        return $this->db->get()->result();
    }
    
    //--------------------------------------------------------------------------
    
    public function mark_as_deleted($data,$uid) {
        $this->db->where('uid', $uid);
        $this->db->update('file_attachments', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    //--------------------------------------------------------------------------
    
    public function insert_file_attachments($data) {
        if ($this->db->insert('file_attachments', $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}