<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Comment_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    //--------------------------------------------------------------------------

    public function insert_comments($data) {
        if ($this->db->insert('client_comments', $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //--------------------------------------------------------------------------

    public function get_comment_by_id($client_id) {
        $this->db->select('comment,role_create,date_create');
        $this->db->from('client_comments');
        $this->db->where(array('client_ref' => $client_id, 'is_visible' => '1'));
        $this->db->order_by('date_create', 'DESC');
        $str = $this->db->get_compiled_select();
        $query = $this->db->query($str);
        $data_arr = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result_array() as $row) {
                $data_arr[$i]['comment'] = $row['comment'];
                $data_arr[$i]['user_details'] = $this->Notification_model->get_user_details($row['role_create']);
                $data_arr[$i]['date_create'] = $row['date_create'];
                $i++;
            }
        }
        return $data_arr;
    }
}
