<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Coach_form_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function insert_coach_form_main($data) {
        if ($this->db->insert('coach_form_main', $data)) {
            $form_id = $this->db->insert_id();
            return $form_id;
        } else {
            return FALSE;
        }
    }

    public function get_single_coach_form_1($frm_id) {
        $this->db->select('c.*,t2.first_name,t2.last_name,t3.first_name AS ref_first_name,t3.last_name AS ref_last_name');
        $this->db->from('coach_form_main c');
        $this->db->join('clients_tb t2', 'c.chf_cl_ref = t2.client_id');
        $this->db->join('clients_tb t3', 'c.chf_ref_client = t3.client_id', 'left');
        $this->db->where('chf_id', $frm_id);
        return $this->db->get()->row();
    }

    public function update_coach_form_1($form_id, $data) {
        $this->db->where('chf_id', $form_id);
        $this->db->update('coach_form_main', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function insert_coach_form_2($data) {
        if ($this->db->insert('coach_form_p2', $data)) {
            $form_id = $this->db->insert_id();
            return $form_id;
        } else {
            return FALSE;
        }
    }

    public function get_single_coach_form_2($frm_id) {
        $this->db->select('*');
        $this->db->from('coach_form_p2');
        $this->db->where('chf_p2_id', $frm_id);
        return $this->db->get()->row();
    }

    public function get_all_coach_form_p1() {
        $this->db->select('c2.first_name AS fname,c2.last_name AS lname,c1.chf_id AS form_id,f.frm_name AS form_name,c1.form_type AS form_type,c1.is_submit AS stat,c2.client_id AS cid');
        $this->db->from('coach_form_main c1');
        $this->db->join('clients_tb c2', 'c1.chf_cl_ref = c2.client_id');
        $this->db->join('forms_tb f', 'f.frm_id = c1.form_type');
        if ($this->session->userdata('role') == '4') {
            $this->db->where('c1.chf_ch_ref', $this->session->userdata('user_level_id'));
        }
        $this->db->where('f.frm_id<>', '7');
        return $this->db->get_compiled_select();
    }

    public function get_all_coach_form_p2() {
        $this->db->select('c3.first_name AS fname,c3.last_name AS lname,c1.chp3_id AS form_id,f.frm_name AS form_name,c1.form_type AS form_type,c1.is_submit AS stat,c3.client_id AS cid');
        $this->db->from('coach_form_p3 c1');
        $this->db->join('coach_form_main c2', 'c1.chp3_main_ref = c2.chf_id');
        $this->db->join('clients_tb c3', 'c2.chf_cl_ref = c3.client_id');
        $this->db->join('forms_tb f', 'f.frm_id = c1.form_type');
        if ($this->session->userdata('role') == '4') {
            $this->db->where('c2.chf_ch_ref', $this->session->userdata('user_level_id'));
        }
        $this->db->where('f.frm_id<>', '7');
        return $this->db->get_compiled_select();
    }

    public function get_all_coach_form_p3() {
        $this->db->select('c3.first_name AS fname,c3.last_name AS lname,c1.chp4_id AS form_id,CONCAT(SUBSTRING_INDEX(f.frm_name," ",1)," ",c1.frequency," ",SUBSTRING_INDEX(f.frm_name," ",-4)) AS form_name,c1.form_type AS form_type,c1.is_submit AS stat,c3.client_id AS cid');
        $this->db->from('coach_form_p4 c1');
        $this->db->join('coach_form_main c2', 'c1.chp4_main_ref = c2.chf_id');
        $this->db->join('clients_tb c3', 'c2.chf_cl_ref = c3.client_id');
        $this->db->join('forms_tb f', 'f.frm_id = c1.form_type');
        if ($this->session->userdata('role') == '4') {
            $this->db->where('c2.chf_ch_ref', $this->session->userdata('user_level_id'));
        }
        $this->db->where('f.frm_id<>', '7');
        return $this->db->get_compiled_select();
    }

    public function get_all_coach_form_p4() {
        $this->db->select('c3.first_name AS fname,c3.last_name AS lname,c1.chp5_id AS form_id,f.frm_name AS form_name,c1.form_type AS form_type,c1.is_submit AS stat,c3.client_id AS cid');
        $this->db->from('coach_form_p5 c1');
        $this->db->join('coach_form_main c2', 'c1.chp5_main_ref = c2.chf_id');
        $this->db->join('clients_tb c3', 'c2.chf_cl_ref = c3.client_id');
        $this->db->join('forms_tb f', 'f.frm_id = c1.form_type');
        if ($this->session->userdata('role') == '4') {
            $this->db->where('c2.chf_ch_ref', $this->session->userdata('user_level_id'));
        }
        $this->db->where('f.frm_id<>', '7');
        return $this->db->get_compiled_select();
    }

    public function get_all_coach_form() {
        $sub_query_1 = $this->get_all_coach_form_p1();
        $sub_query_2 = $this->get_all_coach_form_p2();
        $sub_query_3 = $this->get_all_coach_form_p3();
        $sub_query_4 = $this->get_all_coach_form_p4();
        $query = $this->db->query($sub_query_1 . " UNION " . $sub_query_2 . " UNION " . $sub_query_3 . " UNION " . $sub_query_4);
        return $query->result();
    }

    public function get_main_form_id($client_id) {
        $this->db->select('c1.chf_id AS main_id');
        $this->db->from('coach_form_main c1');
        $this->db->where('c1.chf_cl_ref', $client_id);
        $str = $this->db->get_compiled_select();
        $query = $this->db->query($str);
        if ($query->num_rows() > 0) {
            $frm = array();
            foreach ($query->result_array() as $row) {
                $frm['main_id'] = $row['main_id'] != NULL ? $this->encrypt->encode($row['main_id']) : $row['main_id'];
            }
            return $frm;
        }
    }

    public function get_main_form_reference($form_id, $client_id) {
        switch ($form_id) {
            case '8':
                $this->db->select('c1.chf_id AS main_id,c2.chp3_id AS form_2_id');
                $this->db->from('coach_form_main c1');
                $this->db->join('coach_form_p3 c2', 'c1.chf_id = c2.chp3_main_ref', 'left');
                break;
            case '9':
                $this->db->select('c1.chf_id AS main_id,c2.chp4_id AS form_2_id');
                $this->db->from('coach_form_main c1');
                $this->db->join('coach_form_p4 c2', 'c1.chf_id = c2.chp4_main_ref', 'left');
                break;
            case '10':
                $this->db->select('c1.chf_id AS main_id,c2.chp5_id AS form_2_id');
                $this->db->from('coach_form_main c1');
                $this->db->join('coach_form_p5 c2', 'c1.chf_id = c2.chp5_main_ref', 'left');
                break;
        }
        $this->db->where('c1.chf_cl_ref', $client_id);
        $str = $this->db->get_compiled_select();
        $query = $this->db->query($str);
        if ($query->num_rows() > 0) {
            $frm = array();
            foreach ($query->result_array() as $row) {
                $frm['main_id'] = $row['main_id'] != NULL ? $this->encrypt->encode($row['main_id']) : $row['main_id'];
                $frm['form_2_id'] = $row['form_2_id'] != NULL ? $this->encrypt->encode($row['form_2_id']) : $row['form_2_id'];
            }
            return $frm;
        }
    }

    public function get_engagement3_count($client_id) {
        $this->db->select('count(chp4_id) AS cnt');
        $this->db->from('coach_form_p4');
        $this->db->where('chp4_f51', $client_id);
        $cnt_str = $this->db->get()->row('cnt');
        return $cnt_str;
    }

    public function get_submit_status_of_max_id($client_id) {
        $this->db->select('c1.is_submit AS sub');
        $this->db->from('coach_form_p4 c1');
        $this->db->where('c1.chp4_f51', $client_id);
        $this->db->order_by('c1.chp4_id', 'DESC');
        $this->db->limit(1);
        $str = $this->db->get_compiled_select();
        $query = $this->db->query($str);
        $rset = $query->row('sub');
        return $rset;
    }

    public function get_main_form_ref_for_engagement3($client_id) {
        $item_cnt = $this->get_engagement3_count($client_id);
        $submit_stat = $this->get_submit_status_of_max_id($client_id);
        if ($item_cnt == '0' || $submit_stat=='Y') {
            $this->db->select('c1.chf_id AS main_id,c2.chp4_id AS form_3_id, c2.is_submit AS submit_stat, count(c2.chp4_id) AS cnt');
            $this->db->from('coach_form_main c1');
            $this->db->join('coach_form_p4 c2', 'c1.chf_id = c2.chp4_main_ref', 'left');
            $this->db->where(array('c1.chf_cl_ref' => $client_id));
        } else {
            if ($item_cnt >= 1 && $submit_stat == 'N') {
                $this->db->select('chp4_main_ref AS main_id, chp4_id AS form_3_id, is_submit AS submit_stat, count(chp4_id) AS cnt');
                $this->db->from('coach_form_p4');
                $this->db->where(array('chp4_f51' => $client_id, 'is_submit' => 'N'));
            } 
        }
        $str = $this->db->get_compiled_select();
        $query = $this->db->query($str);
        if ($query->num_rows() > 0) {
            $frm = array();
            foreach ($query->result_array() as $row) {
                $frm['main_id'] = $row['main_id'] != NULL ? $this->encrypt->encode($row['main_id']) : $row['main_id'];
                $frm['form_3_id'] = $row['form_3_id'] != NULL ? $this->encrypt->encode($row['form_3_id']) : $row['form_3_id'];
                $frm['submit_stat'] = $row['submit_stat'];
                $frm['cnt'] = $row['cnt'];
            }
            return $frm;
        }
    }

    public function insert_coach_form_3($data) {
        if ($this->db->insert('coach_form_p3', $data)) {
            $form_id = $this->db->insert_id();
            return $form_id;
        } else {
            return FALSE;
        }
    }

    public function get_single_coach_form_3($frm_id) {
        $this->db->select('c1.*,c3.first_name,c3.last_name');
        $this->db->from('coach_form_p3 c1');
        $this->db->join('coach_form_main c2', 'c1.chp3_main_ref = c2.chf_id');
        $this->db->join('clients_tb c3', 'c2.chf_cl_ref = c3.client_id');
        $this->db->where('c1.chp3_id', $frm_id);
        return $this->db->get()->row();
    }

    public function update_coach_form_3($form_id, $data) {
        $this->db->where('chp3_id', $form_id);
        $this->db->update('coach_form_p3', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function update_form_as_submit($form_type, $form_id) {
        switch ($form_type) {
            case '5':
                $this->db->where('chf_id', $form_id);
                $this->db->update('coach_form_main', array('is_submit' => 'Y', 'submit_date' => date('Y-m_d H:i:s')));
                break;
            case '8':
                $this->db->where('chp3_id', $form_id);
                $this->db->update('coach_form_p3', array('is_submit' => 'Y', 'submit_date' => date('Y-m_d H:i:s')));
                break;
            case '9':
                $this->db->where('chp4_id', $form_id);
                $this->db->update('coach_form_p4', array('is_submit' => 'Y', 'submit_date' => date('Y-m_d H:i:s')));
                break;
            case '10':
                $this->db->where('chp5_id', $form_id);
                $this->db->update('coach_form_p5', array('is_submit' => 'Y', 'submit_date' => date('Y-m_d H:i:s')));
                break;
        }
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function insert_coach_form_4($data) {
        if ($this->db->insert('coach_form_p4', $data)) {
            $form_id = $this->db->insert_id();
            return $form_id;
        } else {
            return FALSE;
        }
    }

    public function get_single_coach_form_4($frm_id) {
        $this->db->select('c1.*,c3.first_name,c3.last_name');
        $this->db->from('coach_form_p4 c1');
        $this->db->join('coach_form_main c2', 'c1.chp4_main_ref = c2.chf_id');
        $this->db->join('clients_tb c3', 'c2.chf_cl_ref = c3.client_id');
        $this->db->where('c1.chp4_id', $frm_id);
        return $this->db->get()->row();
    }

    public function update_coach_form_4($form_id, $data) {
        $this->db->where('chp4_id', $form_id);
        $this->db->update('coach_form_p4', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function get_engagement_frequency($main_frm_id) {
        $this->db->select_max('frequency');
        $this->db->from('coach_form_p4');
        $this->db->where('chp4_main_ref	', $main_frm_id);
        return $this->db->get()->row();
    }

    public function insert_coach_form_5($data) {
        if ($this->db->insert('coach_form_p5', $data)) {
            $form_id = $this->db->insert_id();
            return $form_id;
        } else {
            return FALSE;
        }
    }

    public function get_single_coach_form_5($frm_id) {
        $this->db->select('c1.*,c3.first_name,c3.last_name');
        $this->db->from('coach_form_p5 c1');
        $this->db->join('coach_form_main c2', 'c1.chp5_main_ref = c2.chf_id');
        $this->db->join('clients_tb c3', 'c2.chf_cl_ref = c3.client_id');
        $this->db->where('c1.chp5_id', $frm_id);
        return $this->db->get()->row();
    }

    public function update_coach_form_5($form_id, $data) {
        $this->db->where('chp5_id', $form_id);
        $this->db->update('coach_form_p5', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function insert_loan_approve_data($data) {
        if ($this->db->insert('approved_loan_tb', $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
