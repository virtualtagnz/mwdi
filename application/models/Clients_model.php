<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clients_model extends CI_Model {

	public function fetch_clients() {
		$query = $this->db->order_by('first_name', 'ASC')->get('clients_tb');
		$query = $query->result();
		if($query) {
			return $query;
		} else {
			return false;
		}
	}
}

/* End of file clients_model.php */
/* Location: ./application/models/clients_model.php */