<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Check_list_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    //--------------------------------------------------------------------------
    
    public function get_checklist() {
        $this->db->select('checklist_id');
        $this->db->from('check_list');
        return $this->db->get()->result();
    }
    
    //--------------------------------------------------------------------------
    
    public function get_checklist_items($client_id) {
        $this->db->select('c1.checklistclient_id,description,c.checklist_id,c1.is_checked');
        $this->db->from('check_list c');
        $this->db->join('check_list_client c1', 'c.checklist_id = c1.checklist_ref');
        $this->db->where('c1.client_ref', $client_id);
        $this->db->order_by('c.checklist_id', 'ASC');
        return $this->db->get()->result();
    }
    
    //--------------------------------------------------------------------------
    
    public function insert_checklist($data) {
        if ($this->db->insert_batch('check_list_client', $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    //--------------------------------------------------------------------------
    
    public function update_is_checked($id,$visible) {
        $this->db->where('checklistclient_id', $id);
        $this->db->update('check_list_client', $visible);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}
