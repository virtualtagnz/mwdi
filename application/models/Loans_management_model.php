<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Loans_management_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_approval_list() {
        $this->db->select('c.client_id,c.first_name,c.last_name,c.image_link');
        $this->db->from('approved_loan_tb a');
        $this->db->join('clients_tb c', 'a.ln_client_ref = c.client_id','right');
        $this->db->join('user_tb u','c.u_id = u.user_id');
        $this->db->where('a.loan_id IS NULL',NULL,FALSE);
        $this->db->where('u.status','1');
        $str = $this->db->get_compiled_select();
        $query = $this->db->query($str);
        if ($query->num_rows() > 0) {
            $data_arr = array();
            $i = 0;
            foreach ($query->result_array() as $row) {
                $data_arr[$i]['client_id'] = $row['client_id'];
                $data_arr[$i]['first_name'] = $row['first_name'];
                $data_arr[$i]['last_name'] = $row['last_name'];
                $data_arr[$i]['image_link'] = $row['image_link'];
                $data_arr[$i]['BA'] = $this->check_client_has_submitted_BA_form($row['client_id']);
                $data_arr[$i]['PPMS'] = $this->check_client_has_submitted_PPMS_form($row['client_id']);
                $data_arr[$i]['SEC'] = $this->check_client_has_submitted_security_form($row['client_id']);
                $i++;
            }
            return $data_arr;
        }
        return $this->db->get()->result();
    }
    
    //--------------------------------------------------------------------------
    
    public function check_client_has_submitted_BA_form($client_id) {
        $this->db->select('ba_id');
        $this->db->from('ba_main');
        $this->db->where(array('ba_client_ref'=>$client_id,'sub_status'=>'Y'));
        return $this->db->get()->row('ba_id');
    }
    
    //--------------------------------------------------------------------------
    
    public function check_client_has_submitted_PPMS_form($client_id) {
        $this->db->select('file_link');
        $this->db->from('ppms_form_tb');
        $this->db->where(array('ppms_client_ref'=>$client_id,'sub_stat'=>'Y'));
        return $this->db->get()->row('file_link');
    }
    
    //--------------------------------------------------------------------------
    
    public function check_client_has_submitted_security_form($client_id) {
        $this->db->select('sec_id');
        $this->db->from('sec_main');
        $this->db->where(array('sec_client_ref'=>$client_id,'sub_stat'=>'Y'));
        return $this->db->get()->row('sec_id');
    }
    
}

