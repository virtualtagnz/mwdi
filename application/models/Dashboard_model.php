<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    //--------------------------------------------------------------------------

    public function get_main_ba_form_id($client_id) {
        $this->db->select('ba_id');
        $this->db->from('ba_main');
        $this->db->where(array('ba_client_ref' => $client_id));
        return $this->db->get()->row('ba_id');
    }

    //--------------------------------------------------------------------------

    public function get_ba_form_se1($form_id) {
        $this->db->select('bac_title, bac_first_name, bac_last_name, bac_home_add, bac_bus_add, bac_dob, bac_accom, stud_bus, bac_whanau_name, bac_ta');
        $this->db->from('ba_main');
        $this->db->where(array('ba_id' => $form_id));
        return $this->db->get()->row_array();
    }

    //--------------------------------------------------------------------------

    public function get_ba_form_sec2($form_id) {
        $this->db->select('edu_id');
        $this->db->from('ba_education');
        $this->db->where(array('ba_ref_id' => $form_id));
        return $this->db->get()->row_array();
    }

    //--------------------------------------------------------------------------

    public function get_ba_form_sec3($form_id) {
        $this->db->select('nk1_name, nk1_rel, nk1_hm_phn, nk1_wk_phn, nk1_mob, nk1_email, nk2_name, nk2_rel, nk2_hm_phn, nk2_wk_phn, nk2_mob, nk2_email');
        $this->db->from('ba_next_kin');
        $this->db->where(array('ba_main_ref' => $form_id));
        return $this->db->get()->row_array();
    }

    //--------------------------------------------------------------------------

    public function get_ba_form_sec4($form_id) {
        $this->db->select('b1.emp_id');
        $this->db->from('ba_emp_details b1');
        $this->db->where(array('b1.emp_main_ref' => $form_id));
        $str1 = $this->db->get_compiled_select();

        $this->db->select('b2.tes_no');
        $this->db->from('ba_emp_testimonials b2');
        $this->db->where(array('b2.res_main_ref' => $form_id));
        $str2 = $this->db->get_compiled_select();

        $query = $this->db->query($str1 . " UNION " . $str2);
        return $query->row();
    }

    //--------------------------------------------------------------------------

    public function get_ba_form_sec5($form_id) {
        $this->db->select('biz_trading,abt_mdwi,sec_list');
        $this->db->from('ba_business_details');
        $this->db->where(array('biz_main_ref' => $form_id));
        return $this->db->get()->row_array();
    }

    //--------------------------------------------------------------------------

    public function get_ba_form_sec6($form_id) {
        $this->db->select('acc_name,acc_add,lyr_name,lyr_add,loan_purps');
        $this->db->from('ba_bank_details');
        $this->db->where(array('bk_main_ref' => $form_id));
        return $this->db->get()->row_array();
    }

    //--------------------------------------------------------------------------

    public function get_main_security_form_id($client_id) {
        $this->db->select('sec_id');
        $this->db->from('sec_main');
        $this->db->where(array('sec_client_ref' => $client_id));
        return $this->db->get()->row('sec_id');
    }

    //--------------------------------------------------------------------------

    public function get_security_form_sec1($form_id) {
        $this->db->select('sec_name, sec_dob, sec_biz_name, sec_add, sec_biz_add');
        $this->db->from('sec_main');
        $this->db->where(array('sec_id' => $form_id));
        return $this->db->get()->row_array();
    }

    //--------------------------------------------------------------------------

    public function get_security_form_sec2($form_id) {
        $this->db->select('vcl_id');
        $this->db->from('sec_item_vehicle');
        $this->db->where(array('vcl_sec_main_ref' => $form_id));
        return $this->db->get()->row_array();
    }

    //--------------------------------------------------------------------------

    public function get_security_form_sec3($form_id) {
        $this->db->select('prop_id');
        $this->db->from('sec_item_property');
        $this->db->where(array('prop_sec_main_ref' => $form_id));
        return $this->db->get()->row_array();
    }

    //--------------------------------------------------------------------------

    public function get_security_form_sec4($form_id) {
        $this->db->select('cat_id');
        $this->db->from('sec_items_chattles');
        $this->db->where(array('cat_sec_main_ref' => $form_id));
        return $this->db->get()->row_array();
    }

    //--------------------------------------------------------------------------

    /*
     * Get BA form completion time
     */

    public function ba_form_submit($form_id) {
        $this->db->select('sub_status');
        $this->db->from('ba_main');
        $this->db->where(array('ba_id' => $form_id));
        return $this->db->get()->row('sub_status');
    }

    //--------------------------------------------------------------------------

    public function get_number_of_summitted_ba_forms() {
        $this->db->select('count(ba_id) AS ba_cnt');
        $this->db->from('ba_main');
        $this->db->where(array('sub_status' => 'Y'));
        return $this->db->get()->row('ba_cnt');
    }

    //--------------------------------------------------------------------------

    public function get_main_ba_form_time($form_id = FALSE) {
        $this->db->select('TIMESTAMPDIFF(SECOND,bac_start_date,bac_end_date) AS ba_main_time');
        $this->db->from('ba_main');
        if ($form_id) {
            $this->db->where(array('ba_id' => $form_id));
        }
        $this->db->where(array('sub_status' => 'Y'));
        return $this->db->get()->result();
    }

    //--------------------------------------------------------------------------

    public function get_ba_form_sec2_time($form_id = FALSE) {
        $this->db->select('DISTINCT(TIMESTAMPDIFF(SECOND,b1.start_date,b1.end_date)) AS ba_edu_time');
        $this->db->from('ba_education b1');
        $this->db->join('ba_main b2', 'b1.ba_ref_id=b2.ba_id');
        if ($form_id) {
            $this->db->where(array('b1.ba_ref_id' => $form_id));
        }
        $this->db->where(array('b2.sub_status' => 'Y'));
        return $this->db->get()->result();
    }

    //--------------------------------------------------------------------------

    public function get_ba_form_sec3_time($form_id = FALSE) {
        $this->db->select('TIMESTAMPDIFF(SECOND,b1.start_date,b1.end_date) AS ba_nk_time');
        $this->db->from('ba_next_kin b1');
        $this->db->join('ba_main b2', 'b1.ba_main_ref=b2.ba_id');
        if ($form_id) {
            $this->db->where(array('b1.ba_main_ref' => $form_id));
        }
        $this->db->where(array('b2.sub_status' => 'Y'));
        return $this->db->get()->result();
    }

    //--------------------------------------------------------------------------

    public function get_ba_form_sec4_time($form_id = FALSE) {
        $this->db->select('DISTINCT(TIMESTAMPDIFF(SECOND,b1.start_date,b1.end_date)) AS ba_emp_time');
        $this->db->from('ba_emp_details b1');
        $this->db->join('ba_main b2', 'b1.emp_main_ref=b2.ba_id');
        if ($form_id) {
            $this->db->where(array('b1.emp_main_ref' => $form_id));
        }
        $this->db->where(array('b2.sub_status' => 'Y'));
        return $this->db->get()->result();
    }

    //--------------------------------------------------------------------------

    public function get_ba_form_sec5_time($form_id = FALSE) {
        $this->db->select('TIMESTAMPDIFF(SECOND,b1.start_date,b1.end_date) AS ba_biz_time');
        $this->db->from('ba_business_details b1');
        $this->db->join('ba_main b2', 'b1.biz_main_ref=b2.ba_id');
        if ($form_id) {
            $this->db->where(array('b1.biz_main_ref' => $form_id));
        }
        $this->db->where(array('b2.sub_status' => 'Y'));
        return $this->db->get()->result();
    }

    //--------------------------------------------------------------------------

    public function get_ba_form_sec6_time($form_id = FALSE) {
        $this->db->select('TIMESTAMPDIFF(SECOND,b1.start_date,b1.end_date) AS ba_bnk_time');
        $this->db->from('ba_bank_details b1');
        $this->db->join('ba_main b2', 'b1.bk_main_ref=b2.ba_id');
        if ($form_id) {
            $this->db->where(array('b1.bk_main_ref' => $form_id));
        }
        $this->db->where(array('b2.sub_status' => 'Y'));
        return $this->db->get()->result();
    }

    //--------------------------------------------------------------------------

    /*
     * Get Security form completion time
     */

    public function sec_form_submit($form_id) {
        $this->db->select('sub_stat');
        $this->db->from('sec_main');
        $this->db->where(array('sec_id' => $form_id));
        return $this->db->get()->row('sub_stat');
    }

    //--------------------------------------------------------------------------

    public function get_number_of_summitted_security_forms() {
        $this->db->select('count(sec_id) AS sec_cnt');
        $this->db->from('sec_main');
        $this->db->where(array('sub_stat' => 'Y'));
        return $this->db->get()->row('sec_cnt');
    }

    //--------------------------------------------------------------------------

    public function get_security_form_sec1_time($form_id = FALSE) {
        $this->db->select('TIMESTAMPDIFF(SECOND,s1.start_date,s1.end_date) AS main_sec_time');
        $this->db->from('sec_main s1');
        if ($form_id) {
            $this->db->where(array('s1.sec_id' => $form_id));
        }
        $this->db->where(array('s1.sub_stat' => 'Y'));
        return $this->db->get()->result();
    }

    //--------------------------------------------------------------------------

    public function get_security_form_sec2_time($form_id = FALSE) {
        $this->db->select('DISTINCT(TIMESTAMPDIFF(SECOND,s1.vcl_start_date,s1.vcl_end_date)) AS vcl_sec_time');
        $this->db->from('sec_item_vehicle s1');
        $this->db->join('sec_main s2', 's1.vcl_sec_main_ref = s2.sec_id');
        if ($form_id) {
            $this->db->where(array('s1.vcl_sec_main_ref' => $form_id));
        }
        $this->db->where(array('s2.sub_stat' => 'Y'));
        return $this->db->get()->result();
    }

    //--------------------------------------------------------------------------

    public function get_security_form_sec3_time($form_id = FALSE) {
        $this->db->select('DISTINCT(TIMESTAMPDIFF(SECOND,s1.prop_start_date,s1.prop_end_date)) AS prop_sec_time');
        $this->db->from('sec_item_property s1');
        $this->db->join('sec_main s2', 's1.prop_sec_main_ref = s2.sec_id');
        if ($form_id) {
            $this->db->where(array('s1.prop_sec_main_ref' => $form_id));
        }
        $this->db->where(array('s2.sub_stat' => 'Y'));
        return $this->db->get()->result();
    }

    //--------------------------------------------------------------------------

    public function get_security_form_sec4_time($form_id = FALSE) {
        $this->db->select('DISTINCT(TIMESTAMPDIFF(SECOND,s1.cat_start_date,s1.cat_end_date)) AS chtl_sec_time');
        $this->db->from('sec_items_chattles s1');
        $this->db->join('sec_main s2', 's1.cat_sec_main_ref = s2.sec_id');
        if ($form_id) {
            $this->db->where(array('s1.cat_sec_main_ref' => $form_id));
        }
        $this->db->where(array('s2.sub_stat' => 'Y'));
        return $this->db->get()->result();
    }

    //--------------------------------------------------------------------------

    public function get_ppms_form_id($client_id) {
        $this->db->select('ppms_id');
        $this->db->from('ppms_form_tb');
        $this->db->where(array('ppms_client_ref' => $client_id));
        return $this->db->get()->row('ppms_id');
    }

    //--------------------------------------------------------------------------

    public function ppms_form_submit($form_id) {
        $this->db->select('sub_stat');
        $this->db->from('ppms_form_tb');
        $this->db->where(array('ppms_id' => $form_id));
        return $this->db->get()->row('sub_stat');
    }

    //--------------------------------------------------------------------------

    public function get_number_of_summitted_ppms_forms() {
        $this->db->select('count(ppms_id) AS sec_cnt');
        $this->db->from('ppms_form_tb');
        $this->db->where(array('sub_stat' => 'Y'));
        return $this->db->get()->row('sec_cnt');
    }

    //--------------------------------------------------------------------------

    public function get_ppms_form_complete_time($form_id = FALSE) {
        $this->db->select('TIMESTAMPDIFF(SECOND,p.ppms_down_time,p.ppms_up_time) AS ppms_time');
        $this->db->from('ppms_form_tb p');
        if ($form_id) {
            $this->db->where(array('p.ppms_id' => $form_id));
        }
        $this->db->where(array('p.sub_stat' => 'Y'));
        return $this->db->get()->result();
    }

    //--------------------------------------------------------------------------

    /*
     * Coach dashboard
     */

    public function get_submitted_coach_form_sec1($coach_id) {
        $this->db->select('count(chf_id) AS ch1_cnt');
        $this->db->from('coach_form_main');
        $this->db->where(array('chf_ch_ref' => $coach_id, 'is_submit' => 'Y'));
        return $this->db->get()->row('ch1_cnt');
    }

    //--------------------------------------------------------------------------

    public function get_submitted_coach_form_sec2($coach_id) {
        $this->db->select('count(c.chp3_id) AS ch2_cnt');
        $this->db->from('coach_form_p3 c');
        $this->db->join('coach_form_main c1', 'c.chp3_main_ref = c1.chf_id');
        $this->db->where(array('c1.chf_ch_ref' => $coach_id, 'c.is_submit' => 'Y'));
        return $this->db->get()->row('ch2_cnt');
    }

    //--------------------------------------------------------------------------

    public function get_submitted_coach_form_sec3($coach_id) {
        $this->db->select('count(c.chp4_id) AS ch3_cnt,frequency AS frq');
        $this->db->from('coach_form_p4 c');
        $this->db->join('coach_form_main c1', 'c.chp4_main_ref = c1.chf_id');
        $this->db->where(array('c1.chf_ch_ref' => $coach_id, 'c.is_submit' => 'Y'));
        $this->db->group_by('frequency');
        return $this->db->get()->result();
    }

    //--------------------------------------------------------------------------

    public function get_submitted_coach_form_sec4($coach_id) {
        $this->db->select('count(c.chp5_id) AS ch4_cnt');
        $this->db->from('coach_form_p5 c');
        $this->db->join('coach_form_main c1', 'c.chp5_main_ref = c1.chf_id');
        $this->db->where(array('c1.chf_ch_ref' => $coach_id, 'c.is_submit' => 'Y'));
        return $this->db->get()->row('ch4_cnt');
    }

    //--------------------------------------------------------------------------

    /*
     * Super admin dashboard client performence
     */

    public function get_forms_assign_to_clients($form_type) {
        $this->db->select('count(c.cf_id) AS form_count');
        $this->db->from('client_forms_tb c');
        $this->db->where_in('cf_frm_id', $form_type);
        //$str =$this->db->get_compiled_select();
        return $this->db->get()->row('form_count');
    }

    //--------------------------------------------------------------------------

    public function serach($key_word, $key_area) {
        if ($key_area == '1') {

            $searchQuery = explode(' ', $key_word);

            //$key_word = str_replace(' ', '%', $key_word);

            $this->db->select('client_id, u_id AS id, first_name, last_name , description, phone, mobile, address, industry, 5 AS area');
            $this->db->from('clients_tb');
            
            foreach($searchQuery as $key_word) {
                $this->db->or_like('first_name', $key_word, 'both');
                $this->db->or_like('last_name', $key_word, 'both');
                $this->db->or_like('dob', $key_word, 'both');
                $this->db->or_like('phone', $key_word, 'both');
                $this->db->or_like('mobile', $key_word, 'both');
                $this->db->or_like('address', $key_word, 'both');
                $this->db->or_like('add_city', $key_word, 'both');
                $this->db->or_like('add_region', $key_word, 'both');
                $this->db->or_like('description', $key_word, 'both');
                $this->db->or_like('industry', $key_word, 'both'); 
            }

            $sub_query_1 = $this->db->get_compiled_select();
            //$this->db->_reset_select();

            $this->db->select('coach_id,u_id AS id, first_name, last_name, description, phone, mobile, address, "" AS industry, 4 AS area');
            $this->db->from('coach_tb');

            foreach($searchQuery as $key_word) {
                $this->db->or_like('first_name', $key_word, 'both');
                $this->db->or_like('last_name', $key_word, 'both');
                $this->db->or_like('dob', $key_word, 'both');
                $this->db->or_like('phone', $key_word, 'both');
                $this->db->or_like('mobile', $key_word, 'both');
                $this->db->or_like('address', $key_word, 'both');
                $this->db->or_like('add_city', $key_word, 'both');
                $this->db->or_like('add_region', $key_word, 'both');
                $this->db->or_like('description', $key_word, 'both');
            }


            $sub_query_2 = $this->db->get_compiled_select();
            //$this->db->_reset_select();

            $query = $this->db->query($sub_query_1 . " UNION " . $sub_query_2);

            if ($query->num_rows() > 0) {
                $data_arr = array();
                $i = 0;
                foreach ($query->result_array() as $row) {
                    $data_arr[$i]['id'] = $this->encrypt->encode($row['id']);
                    $data_arr[$i]['first_name'] = $row['first_name'];
                    $data_arr[$i]['last_name'] = $row['last_name'];
                    $data_arr[$i]['description'] = $row['description'];
                    $data_arr[$i]['phone'] = $row['phone'];
                    $data_arr[$i]['mobile'] = $row['mobile'];
                    $data_arr[$i]['address'] = $row['address'];
                    $data_arr[$i]['industry'] = $row['industry'];
                    $data_arr[$i]['area'] = $this->encrypt->encode($row['area']);
                    $i++;
                }
                return $data_arr;
            }
        }
    }

}
