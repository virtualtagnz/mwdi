<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function is_user_available($username, $para2, $by_para) {
        $this->db->select('count(user_id) AS cnt');
        $this->db->from('user_tb');
        switch ($by_para) {
            case '1':$this->db->where(array('user_name' => $username, 'pwd' => $para2));
                break;
            case '2':$this->db->where(array('user_name' => $username, 'user_email' => $para2));
        }
        $is_available = $this->db->get()->row('cnt');
        if (intval($is_available) > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function user_authentication($username, $pwd) {
        $this->db->select('user_id, role_id, user_name,user_email,status');
        $this->db->from('user_tb');
        $this->db->where(array('user_name' => $username, 'pwd' => $pwd));
        return $this->db->get()->row();
    }

    public function get_user_details($uid, $user_role) {
        
        switch ($user_role) {
            case '1':
                $this->db->select('super_id AS user_level_id,first_name,last_name,image_link');
                $this->db->from('super_admin_tb');
                break;
            case '2':
                $this->db->select('admin_id AS user_level_id,first_name,last_name,image_link');
                $this->db->from('admin_tb');
                break;
            case '3':
                $this->db->select('accounts_id AS user_level_id,first_name,last_name,image_link');
                $this->db->from('accounts_tb');
                break;
            case '4':
                $this->db->select('coach_id AS user_level_id,first_name,last_name,image_link');
                $this->db->from('coach_tb');
                break;
            case '5':
                $this->db->select('client_id AS user_level_id,first_name,last_name,image_link');
                $this->db->from('clients_tb');
                break;
        }
        $this->db->where(array('u_id' => $uid));
        return $this->db->get()->row();
    }

    public function get_user_id($username, $email) {
        $this->db->select('user_id,role_id');
        $this->db->from('user_tb');
        $this->db->where(array('user_name' => $username, 'user_email' => $email));
        return $this->db->get()->row();
    }

    public function update_password($uid, $data) {
        $this->db->where('user_id', $uid);
        if ($this->db->update('user_tb',$data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
