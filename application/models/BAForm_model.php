<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class BAForm_model extends CI_Model {

    public function fetch($baform_uid = false, $client_uid = false) {
        if ($baform_uid || $client_uid) {
            if ($baform_uid) {
                $this->db->where('ba_id', $baform_uid);
            }

            if ($client_uid) {
                $this->db->where('ba_client_ref', $client_uid);
            }

            $query = $this->db->get('ba_main');
            $query = $query->result();

            if ($query) {
                return reset($query);
            }
        }

        return false;
    }

    public function get_max_main_form() {
        $query = $this->db->select('MAX(ba_id) count')->get('ba_main');
        $query = $query->result();
        $query = reset($query);
        return ($query->count + 1);
    }

    public function save_main_form($string_data = false, $ba_uid = false) {
        if ($string_data && is_array($string_data)) {
            if ($ba_uid) {
                $this->db->where('ba_id', $ba_uid)->update('ba_main', $string_data);
            } else {
                $this->db->insert('ba_main', $string_data);
                $ba_uid = $this->db->insert_id();
            }

            return $ba_uid;
        }

        return false;
    }

    /* STEP 2 */

    public function fetch_education_details($baform_uid = false) {
        if ($baform_uid) {
            $this->db->where('ba_ref_id', $baform_uid);
            $query = $this->db->get('ba_education');
            $query = $query->result();

            if ($query) {
                return $query;
            }
        }

        return false;
    }

    public function add_education_details($string_data = false) {
        if ($string_data && is_array($string_data)) {
            $this->db->insert('ba_education', $string_data);
            $insert_id = $this->db->insert_id();

            return $insert_id;
        }

        return false;
    }

    public function delete_education_details($uid) {
        return $this->db->where('edu_id', $uid)->delete('ba_education');
    }

    /* STEP 3 */

    public function fetch_kin_details($baform_uid = false) {
        if ($baform_uid) {
            $this->db->where('ba_main_ref', $baform_uid);
            $query = $this->db->get('ba_next_kin');
            $query = $query->result();

            if ($query) {
                return reset($query);
            }
        }

        return false;
    }

    public function save_kin_details($string_data = false, $nk_id = false, $ba_uid = false) {
        if ($string_data && is_array($string_data)) {
            if ($nk_id) {
                $this->db->where('nk_id', $nk_id)->update('ba_next_kin', $string_data);
            } else {
                $this->db->insert('ba_next_kin', $string_data);
                $nk_id = $this->db->insert_id();
            }

            return $ba_uid;
        }

        return false;
    }

    /* STEP 4 */

    public function fetch_employment_details($baform_uid = false) {
        if ($baform_uid) {
            $this->db->where('emp_main_ref', $baform_uid);
            $query = $this->db->get('ba_emp_details');
            $query = $query->result();

            if ($query) {
                return $query;
            }
        }

        return false;
    }

    public function fetch_testimonials($baform_uid = false) {
        if ($baform_uid) {
            $this->db->where('res_main_ref', $baform_uid);
            $query = $this->db->get('ba_emp_testimonials');
            $query = $query->result();

            if ($query) {
                return reset($query);
            } else {
                return false;
            }
        }

        return false;
    }

    public function add_employment_details($string_data = false) {
        if ($string_data && is_array($string_data)) {
            $this->db->insert('ba_emp_details', $string_data);
            $insert_id = $this->db->insert_id();

            return $insert_id;
        }

        return false;
    }

    public function delete_employment_details($uid) {
        return $this->db->where('emp_id', $uid)->delete('ba_emp_details');
    }

    public function save_testimonials($string_data = false, $tes_no = false, $ba_uid = false) {
        if ($string_data && is_array($string_data)) {
            if ($tes_no) {
                $this->db->where('tes_no', $tes_no)->update('ba_emp_testimonials', $string_data);
            } else {
                $this->db->insert('ba_emp_testimonials', $string_data);
                $tes_no = $this->db->insert_id();
                $this->update_emp_details_end_time($ba_uid);
            }

            return $ba_uid;
        }

        return false;
    }
    
    public function update_emp_details_end_time($ba_uid) {
        $this->db->where('emp_main_ref', $ba_uid);
        $this->db->update('ba_emp_details', array('end_date'=>date('Y-m-d H:i:s')));
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /* STEP 5 */

    public function fetch_business_details($baform_uid = false) {
        if ($baform_uid) {
            $this->db->where('biz_main_ref', $baform_uid);
            $query = $this->db->get('ba_business_details');
            $query = $query->result();

            if ($query) {
                return reset($query);
            }
        }

        return false;
    }

    public function save_business_details($string_data = false, $biz_id = false, $ba_uid = false) {
        if ($string_data && is_array($string_data)) {
            if ($biz_id) {
                $this->db->where('biz_id', $biz_id)->update('ba_business_details', $string_data);
            } else {
                $this->db->insert('ba_business_details', $string_data);
                $biz_id = $this->db->insert_id();
            }

            return $ba_uid;
        }

        return false;
    }

    /* STEP 6 */

    public function fetch_bank_information($baform_uid = false) {
        if ($baform_uid) {
            $this->db->where('bk_main_ref', $baform_uid);
            $query = $this->db->get('ba_bank_details');
            $query = $query->result();

            if ($query) {
                return reset($query);
            }
        }

        return false;
    }

    public function save_bank_information($string_data = false, $bk_id = false, $ba_uid = false) {
        if ($string_data && is_array($string_data)) {
            if ($bk_id) {
                $this->db->where('bk_id', $bk_id)->update('ba_bank_details', $string_data);
            } else {
                $this->db->insert('ba_bank_details', $string_data);
                $bk_id = $this->db->insert_id();
            }

            return $ba_uid;
        }

        return false;
    }
    
    public function get_all_submitted_BA_form($is_submit = FALSE) {
        $this->db->select('b.*,f.frm_name,c.first_name,c.last_name');
        $this->db->from('ba_main b');
        $this->db->join('forms_tb f','b.form_type = f.frm_id');
        $this->db->join('clients_tb c','b.ba_client_ref = c.client_id');
        if($this->session->userdata('role')=='5'){
             $this->db->where(array('b.ba_client_ref'=>$this->session->userdata('user_level_id')));
        }
        if($is_submit){
            $this->db->where('sub_status',$is_submit);
        }
        $this->db->where('f.frm_id<>','7');
        return $this->db->get()->result();
    }

    public function get_client_baform($uid) {
        $this->db->select("clients_tb.client_id id, CONCAT(clients_tb.first_name,' ',clients_tb.last_name) AS name, user_tb.user_email email");
        $this->db->from("ba_main");
        $this->db->join("clients_tb", "clients_tb.client_id = ba_main.ba_client_ref", 'left');
        $this->db->join("user_tb", "user_tb.user_id = clients_tb.u_id", 'left');
        $this->db->where(array('ba_main.ba_id'=> $uid));
        $query = $this->db->get()->row();

        return $query;
    }

    public function get_coach_baform($uid) {
        $this->db->select("coach_tb.coach_id id, CONCAT(coach_tb.first_name,' ',coach_tb.last_name) AS name, user_tb.user_email email");
        $this->db->from("ba_main");
        $this->db->join("client_coach_rel", "client_coach_rel.client_ref = ba_main.ba_client_ref");
        $this->db->join("coach_tb", "coach_tb.coach_id = client_coach_rel.coach_ref");
        $this->db->join("user_tb", "user_tb.user_id = coach_tb.u_id");
        $this->db->where(array('ba_main.ba_id'=> $uid, 'user_tb.status' => "1"));
        $query = $this->db->get()->row();

        return $query;
    }

    public function get_all_admins_notifications() {
        $this->db->select("admin_tb.admin_id id, CONCAT(admin_tb.first_name,' ',admin_tb.last_name) AS name, user_tb.user_email email");
        $this->db->from("admin_tb");
        $this->db->join("user_tb", "user_tb.user_id = admin_tb.u_id");
        $this->db->where(array('user_tb.status' => "1", 'user_tb.role_id' => "2"));
        $query = $this->db->get()->result();

        return $query;
    }

    public function get_all_accounts_notifications() {
        $this->db->select("accounts_tb.accounts_id id, CONCAT(accounts_tb.first_name,' ',accounts_tb.last_name) AS name, user_tb.user_email email");
        $this->db->from("accounts_tb");
        $this->db->join("user_tb", "user_tb.user_id = accounts_tb.u_id");
        $this->db->where(array('user_tb.status' => "1", 'user_tb.role_id' => "3"));
        $query = $this->db->get()->result();

        return $query;
    }

    public function get_all_super_admins_notifications() {
        $this->db->select("user_tb.user_email email");
        $this->db->from("user_tb");
        $this->db->where(array('user_tb.status' => "1", 'user_tb.role_id' => "1"));
        $query = $this->db->get()->result();

        return $query;
    }
}

/* End of file bAForm_model.php */
/* Location: ./application/models/bAForm_model.php */