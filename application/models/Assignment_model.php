<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Assignment_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function insert($data) {
        if ($this->db->insert_batch('assigned_resource', $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function insert_to_coach_forms($data) {
        if ($this->db->insert_batch('coach_forms_tb', $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function assigned_forms($utype) {
        $this->db->select('a.assigned_date,r.res_name,c.first_name,c.last_name,r.res_linq');
        $this->db->from('assigned_resource a');
        $this->db->join('mwdi_resource r', 'a.ares_res_id = r.res_id');
        $this->db->join('user_tb u', 'a.ares_user = u.user_id');
        if ($utype == '4') {
            $this->db->join('coach_tb c', 'u.user_id = c.u_id');
        } else {
            $this->db->join('clients_tb c', 'u.user_id = c.u_id');
        }
        $this->db->order_by('c.first_name', 'DESC');
        return $this->db->get()->result();
    }

    public function load_forms($uid) {
        $this->db->select('a.ares_res_id');
        $this->db->from('assigned_resource a');
        $this->db->where('a.ares_user', $uid);
        $where_clause = $this->db->get_compiled_select();

        $this->db->select('r.res_id,r.res_name');
        $this->db->from('mwdi_resource r');
        $this->db->where('r.res_id NOT IN (' . $where_clause . ')', NULL, FALSE);
        return $this->db->get()->result();
    }

    public function load_application_forms($form_types) {
        $this->db->select('*');
        $this->db->from('forms_tb');
        $this->db->where('sec', $form_types);
        $this->db->where('frm_id<>','7');
        return $this->db->get()->result();
    }

    public function load_unselected_forms($uid) {
        $this->db->select('a.ch_frm_id');
        $this->db->from('coach_forms_tb a');
        $this->db->where('a.ch_coach_id', $uid);
        $where_clause = $this->db->get_compiled_select();

        $this->db->select('r.frm_id,r.frm_name');
        $this->db->from('forms_tb r');
        $this->db->where('r.frm_id NOT IN (' . $where_clause . ')', NULL, FALSE);
        $this->db->where('r.sec', 'F');
        $this->db->where('r.frm_id<>','7');
        return $this->db->get()->result();
    }

    public function assigned_application_forms($utype,$uid=NULL) {
        $this->db->select('x.frm_id,x.frm_name,c.first_name,c.last_name,f.assign_date');
        if ($utype == '4') {
            $this->db->from('coach_forms_tb f');
            $this->db->join('coach_tb c', 'f.ch_coach_id = c.coach_id');
            $this->db->join('forms_tb x', 'f.ch_frm_id = x.frm_id');
            if($uid!=NULL){
                $this->db->where('f.ch_coach_id', $uid);
            }
            
        }  else {
            $this->db->from('client_forms_tb f');
            $this->db->join('clients_tb c', 'f.cf_client_ref = c.client_id');
            $this->db->join('forms_tb x', 'f.cf_frm_id = x.frm_id');
            if($uid!=NULL){
                $this->db->where('f.cf_client_ref', $uid);
            }
        }
        $this->db->where('x.frm_id<>','7');
        $this->db->order_by('x.frm_id','ASC');
        return $this->db->get()->result();
    }
    
    public function insert_to_coach_client_rel($data) {
        if ($this->db->insert_batch('client_coach_rel', $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function get_all_client_coach_rel() {
        $this->db->select('c1.first_name AS coach_fname,c1.last_name AS coach_lname,c.first_name AS client_fname,c.last_name AS client_lname,r.assign_date, c1.coach_id');
        $this->db->from('client_coach_rel r');
        $this->db->join('clients_tb c', 'r.client_ref = c.client_id');
        $this->db->join('coach_tb c1', 'r.coach_ref = c1.coach_id');
        $this->db->join('user_tb t2', 't2.user_id = c1.u_id');
        $this->db->where(array('t2.status' => '1'));
        $return = $this->db->get()->result();

        return $return;
    }
    
    public function get_resources_by_user_id() {
        $this->db->select('m.res_id,m.res_name,r.assigned_date,m.res_linq');
        $this->db->from('mwdi_resource m');
        $this->db->join('assigned_resource r', 'r.ares_res_id = m.res_id');
        $this->db->where(array('r.ares_user_role'=>$this->session->userdata('role'),'r.ares_user'=>$this->session->userdata('uid')));
        return $this->db->get()->result();
    }
    
    public function get_tab_id_from_coach_rel($coach_id,$client_id) {
        $this->db->select('tab_id');
        $this->db->from('client_coach_rel');
        $this->db->where(array('coach_ref'=>$coach_id,'client_ref'=>$client_id));
        return $this->db->get()->row('tab_id');
    }
    
    public function get_coach_form_id($coach_id,$client_id) {
        $this->db->select('chf_id');
        $this->db->from('coach_form_main');
        $this->db->where(array('chf_ch_ref'=>$coach_id,'chf_cl_ref'=>$client_id));
        return $this->db->get()->row('chf_id');
    }
    
    public function update_client_coach_rel($data) {
        $this->db->trans_start();
        $this->db->update_batch('client_coach_rel', $data, 'tab_id');
        $this->db->trans_complete();
        if ($this->db->trans_status()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function update_main_coach_form_coach_id($data2) {
        $this->db->trans_start();
        $this->db->update_batch('coach_form_main', $data2, 'chf_id');
        $this->db->trans_complete();
        if ($this->db->trans_status()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}
