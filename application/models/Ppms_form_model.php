<?php

defined('BASEPATH') OR exit('No direct script access allowed'); 

class Ppms_form_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_download_count() {
        $this->db->select('ppms_id');
        $this->db->from('ppms_form_tb');
        $this->db->where('ppms_client_ref', $this->session->userdata('user_level_id'));
        return $this->db->get()->row('ppms_id');
    }
    
    public function insert_ppms($data) {
        if ($this->db->insert('ppms_form_tb', $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function update_ppms($frm_id,$data) {
        $this->db->where('ppms_id', $frm_id);
        $this->db->update('ppms_form_tb', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function get_submitted_ppms() {
        $this->db->select('count(ppms_id) AS cnt');
        $this->db->from('ppms_form_tb');
        $this->db->where(array('ppms_client_ref'=>$this->session->userdata('user_level_id'),'sub_stat'=>'Y'));
        return $this->db->get()->row('cnt');
    }
    
    public function get_all_submitted_ppms_forms($is_submit = FALSE) {
        $this->db->select('p.*,f.frm_name,c.first_name,c.last_name');
        $this->db->from('ppms_form_tb p');
        $this->db->join('forms_tb f','p.form_type = f.frm_id');
        $this->db->join('clients_tb c','p.ppms_client_ref = c.client_id');
        if($this->session->userdata('role')=='5'){
             $this->db->where(array('p.ppms_client_ref'=>$this->session->userdata('user_level_id')));
        }
        if($is_submit){
            $this->db->where('sub_stat',$is_submit);
        }
        $this->db->where('f.frm_id<>','7');
        return $this->db->get()->result();
    }
}




