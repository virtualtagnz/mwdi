<?php

if (!function_exists('debug')) {

    function debug($var) {
        echo '<pre>';
        var_dump($var);
        echo '</pre>';
    }

}

/*
 * sending email common model
 * @return boolean
 */

if (!function_exists('email_configuration')) {

    function email_configuration() {
        $ci = get_instance();
        $config['protocol'] = $ci->config->item('protocol');
        $config['smtp_host'] = $ci->config->item('smtp_host');
        $config['smtp_port'] = $ci->config->item('smtp_port');
        $config['smtp_user'] = $ci->config->item('smtp_user');
        $config['smtp_pass'] = $ci->config->item('smtp_pass');
        $config['smtp_timeout'] = "4";
        $config['mailtype'] = "html";
        $config['charset'] = "UTF-8";
        $config['newline'] = "\r\n";
        $config['crlf'] = "\r\n";
        return $config;
    }

}

//------------------------------------------------------------------------------

if (!function_exists('send_emails')) {

    function send_emails($email_data) {
        $ci = get_instance();
        $ci->load->library('email', email_configuration());
        $ci->email->set_newline("\r\n");
        $ci->email->from($ci->config->item('smtp_email_from'), $ci->config->item('smtp_name_from'));
        $ci->email->to($email_data['to']);
        $ci->email->subject($email_data['subject']);
        $ci->email->message($email_data['msg']);
        if ($email_data['files_attachments']) {
            foreach ($email_data['files_attachments'] as $files) {
                $ci->email->attach($files);
            }
        }
        if ($ci->email->send()) {
            $ci->email->clear(TRUE);
            return TRUE;
        } else {
            show_error($ci->email->print_debugger());
            return $ci->email->print_debugger();
        }
    }

}

//------------------------------------------------------------------------------

if (!function_exists('upload_files')) {

    function upload_files($dir, $field_name, $type, $width = NULL) {
        $ci = get_instance();
        if (!empty($dir)) {
            $path = $dir;
            if (!is_dir($path)) { //create the folder if it's not already exists
                mkdir($path, 0755, TRUE);
            }
        }
        $config['upload_path'] = $path;
        $config['max_size'] = 10000;
        $config['overwrite'] = TRUE;
        $config['remove_spaces'] = TRUE;
        $config['encrypt_name'] = TRUE;
        if ($type == 'image') {
            $config['allowed_types'] = 'jpg|jpeg|png';
        } elseif ($type == 'files') {
            $config['allowed_types'] = 'doc|docx|xls|xlsx|txt|pdf';
        } elseif ($type == 'common') {
            $config['allowed_types'] = 'doc|docx|xls|xlsx|txt|pdf|jpg|jpeg|png';
        }
        $ci->upload->initialize($config);
        if (!$ci->upload->do_upload($field_name)) {
            $data['stat'] = FALSE;
            $data['fileError'] = $ci->upload->display_errors();
            return $data;
        } else {
            $data['stat'] = TRUE;
            $data['fileArr'] = $ci->upload->data();
            if ($type == 'image') {
                if ($width) {
                    image_process($data['fileArr']['full_path'], $width, $data['fileArr']['file_path'], $data['fileArr']['file_name']);
                }
            }
            return $data;
        }
    }

}

//------------------------------------------------------------------------------

if (!function_exists('image_process')) {

    function image_process($source_path, $height, $file_path, $file) {
        $ci = get_instance();
        $config['image_library'] = 'gd2';
        $config['source_image'] = $source_path;
        $config['new_image'] = $file_path . $file;
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = TRUE;
        $config['height'] = $height;
        $ci->image_lib->initialize($config);
        $ci->image_lib->resize();
    }

}

//------------------------------------------------------------------------------

if (!function_exists('temp_success_msg')) {

    function temp_success_msg() {
        $ci = get_instance();
        return TRUE;
    }

}

//------------------------------------------------------------------------------

if (!function_exists('response_messages')) {

    function response_messages() {
        $CI = & get_instance();
        $html = array();
        if ($CI->session->flashdata('success')) {
            $html[] = '<div class="alert alert-success alert-dismissible" role="alert">';
            $html[] = '    <i class="fa fa-check-circle"></i> ' . $CI->session->flashdata('success');
            $html[] = '    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
            $html[] = '</div>';
        }

        if ($CI->session->flashdata('info')) {
            $html[] = '<div class="alert alert-info alert-dismissible" role="alert">';
            $html[] = '    <i class="fa fa-info-circle"></i> ' . $CI->session->flashdata('info');
            $html[] = '    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
            $html[] = '</div>';
        }

        if ($CI->session->flashdata('notice')) {
            $html[] = '<div class="alert alert-notice alert-dismissible" role="alert">';
            $html[] = '    <i class="fa fa-info-circle"></i> ' . $CI->session->flashdata('notice');
            $html[] = '    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
            $html[] = '</div>';
        }

        if ($CI->session->flashdata('warning')) {
            $html[] = '<div class="alert alert-warning alert-dismissible" role="alert">';
            $html[] = '    <i class="fa fa-info-circle"></i> ' . $CI->session->flashdata('warning');
            $html[] = '    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
            $html[] = '</div>';
        }

        if ($CI->session->flashdata('danger')) {
            $html[] = '<div class="alert alert-danger alert-dismissible" role="alert">';
            $html[] = '    <i class="fa fa-info-circle"></i> ' . $CI->session->flashdata('danger');
            $html[] = '    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
            $html[] = '</div>';
        }

        return implode("\n", $html);
    }

}

//------------------------------------------------------------------------------

if (!function_exists('create_notification')) {

    function create_notification($role, $uid, $txt) {
        $CI = & get_instance();
        $data = array();
        $data['nf_role_id'] = $role;
        $data['nf_user_id'] = $uid;
        $data['notification_txt'] = $txt;
        $data['notify_date'] = date('Y-m-d H:i:s');
        $data['super_mark'] = 'N';
        $data['account_mark'] = 'N';
        $data['admin_mark'] = 'N';
        $data['coach_mark'] = 'N';
        $data['client_mark'] = 'N';
        $CI->Notification_model->insert_notifications($data);
    }

}

//------------------------------------------------------------------------------

if (!function_exists('view_notification')) {

    function view_notification() {
        $CI = & get_instance();
        $rtn_arr = $CI->Notification_model->get_all_notifications();
        return json_encode($rtn_arr);
    }

}

//------------------------------------------------------------------------------

if (!function_exists('get_notification_count')){
    function get_notification_count() {
        $CI = & get_instance();
        $cnt = $CI->Notification_model->get_notification_cnt();
        return json_encode($cnt);
    }
}

//------------------------------------------------------------------------------

if (!function_exists('set_checklist_to_client')) {

    function set_checklist_to_client($client_id) {
        $CI = & get_instance();
        $check_list_arr = $CI->Check_list_model->get_checklist();
        $data = array();
        $i = 0;
        foreach ($check_list_arr as $check_list) {
            $data[$i]['checklist_ref'] = $check_list->checklist_id;
            $data[$i]['client_ref'] = $client_id;
            $data[$i]['is_checked'] = 0;
            $data[$i]['date_mod'] = date('Y-m-d H:i:s');
            $data[$i]['role_mod'] = $CI->session->userdata('user_name');
            $data[$i]['code_mod'] = $CI->session->userdata('uid');
            $i++;
        }
        $CI->Check_list_model->insert_checklist($data);
    }

}

//--------------------------------------------------------------------------

/*
 * Dashboard Statics
 * BA form statics
 * @param client_id
 * @return mixed
 */

if (!function_exists('Calculate_form_progress')) {

    function Calculate_form_progress($arr) {
        $CI = & get_instance();
        $tot_count = count($arr);
        if ($tot_count > 0) {
            $i = 0;
            foreach ($arr as $BA_sec1) {
                if ($BA_sec1 != NULL) {
                    $i++;
                }
            }
            $precentage = (floatval($i) / floatval($tot_count)) * 100;
        } else {
            $precentage = 0;
        }
        return round($precentage,1);
    }

}

/*
 * Add activities
 * @param user_role
 * @param user_id
 * @return mix_array
 */

if (!function_exists('add_activities')) {
    
    function add_activities($user_role,$user_id,$activity_txt) {
        $CI = & get_instance();
        $data = array();
        $data['act_role'] = $user_role;
        $data['act_user'] = $user_id;
        $data['act_txt'] = $activity_txt;
        $data['act_date'] = date('Y-m-d H:i:s');
        $CI->Activity_model->insert_activities($data);
    }
}

//--------------------------------------------------------------------------

/*
 * Send Email from BA Form
 * BA form
 * @param super_admin_list, admins_list, accountats_list, couch_list, client_information
 * @return NULL
 */
if (!function_exists('send_email_confirmation_baform')) {
    function send_email_confirmation_baform($super_admins, $admins, $accounts, $coach, $client) {
        $CI = & get_instance();
        $email_list = array();
        if($super_admins) {
            foreach($super_admins as $string) {
                $email_list[$string->email] = (isset($string->name)?$string->name:NULL);
            }
        }

        if($admins) {
            foreach($admins as $string) {
                $email_list[$string->email] = (isset($string->name)?$string->name:NULL);
            }
        }

        if($accounts) {
            foreach($accounts as $string) {
                $email_list[$string->email] = (isset($string->name)?$string->name:NULL);
            }
        }

        if($coach) {
            $email_list[$coach->email] = (isset($coach->name)?$coach->name:NULL);
        }

        if(count($email_list)>0 && $client) {
            foreach($email_list as $email_user => $name_user) {
                $email_data = $template_data = array();
                $email_data['to'] = $email_user;
                $email_data['subject'] = 'MWDI | BA Form submitted';
                $template_data['name'] = $client->name;
                $email_data['msg'] = $CI->load->view('email_templates/ba_form_admin_account', $template_data, TRUE);
                send_emails($email_data);  
            }        
        }

        if($client) {
            $email_data = $template_data = array();
            $email_data['to'] = $client->email;
            $email_data['subject'] = 'MWDI | BA Form submitted';
            $template_data['name'] = $client->name;
            $email_data['msg'] = $CI->load->view('email_templates/ba_form_client', $template_data, TRUE);
            send_emails($email_data);
        }
    }
}

//--------------------------------------------------------------------------

/*
 * Convert to readable user
 * Global
 * @param category
 * @return category readable
 */
if (!function_exists('readable_client_category')) {
    function readable_client_category($category = false) {
        $readable = '-';
        if($category) { 
            switch ($category) {
                case 'loan-client':
                    $readable = 'Loan client';
                    break;
                case 'cocheee':
                    $readable = 'Coachee ';
                    break;
                case 'both':
                   $readable = 'Both';
                    break;
            }
        }

        return $readable;
    }
}