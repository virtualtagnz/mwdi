<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['error_required'] = 'The field {field} is mandatory, please check again!';