/*
 * helper function for formating date
 */
Number.prototype.padLeft = function (base, chr) {
    var len = (String(base || 10).length - String(this).length) + 1;
    return len > 0 ? new Array(len).join(chr || '0') + this : this;
};
/*
 * Formating date and set label
 */
function timer() {
    var d = new Date();
    var dformat;
    dformat = [d.getFullYear(), (d.getMonth() + 1).padLeft(), d.getDate().padLeft()].join('-') + ' ' + [d.getHours().padLeft(), d.getMinutes().padLeft(), d.getSeconds().padLeft()].join(':');
//    $("label[for='timer']").html('<small>' + dformat + '</small>');
//    $("#" + hidd).val(dformat);
//    $('#' + frm + ' :input').prop("disabled", false);
    return dformat;
}

var my_current_notifications = 0;

function change_current_notification(url) {
    $.ajax({
        type: "POST",
        url: url,
        success: function (results) {
            console.log(JSON.parse(results));
            var recent_cnt = JSON.parse(results);
            my_current_notifications = recent_cnt;
            $('#nft_cnt').text(recent_cnt);
        }
    });
}

